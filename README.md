RESOLVE project
=================

Accès aux pages publiques
-------------------------

https://lecoinal.gricad-pages.univ-grenoble-alpes.fr/resolve

---

[Wiki for RESOLVE project](https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/resolve/wikis/Wiki-RESOLVE-project)

---

```
$ git clone git@gricad-gitlab.univ-grenoble-alpes.fr:lecoinal/resolve.git
```
---

[Documentation for computing source locations from RESOLVE Argentière ZO\_2018 dataset](./docs/index.md)
