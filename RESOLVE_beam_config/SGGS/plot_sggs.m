%$ awk '{print $1}' zreseau_220_224_utm.txt > staname
%$ awk '{print $2 " " $3}' zreseau_220_224_utm.txt > xy
%$ awk -F'.' '{print $1 "." $2}' staname > zzz ; mv zzz staname

clear

load xy;
sn=textread('staname','%s');

mx=mean(xy(:,1));
my=mean(xy(:,2));

% startpoint 5x5=25 points
n=5;
sp=zeros(n*n,2);
dx=150;dy=150;
sp=[-2 -2; -2 -1; -2 0; -2 1; -2 2;...
    -1 -2; -1 -1; -1 0; -1 1; -1 2;...
     0 -2;  0 -1;  0 0;  0 1;  0 2;...
     1 -2;  1 -1;  1 0;  1 1;  1 2;...
     2 -2;  2 -1;  2 0;  2 1;  2 2];
sp(:,1)=sp(:,1)*dx+mx;
sp(:,2)=sp(:,2)*dy+my;

figure(1);clf;hold on;grid on;
plot(xy(:,1),xy(:,2),'^k');
for jj=1:35;
  text(xy(jj,1)+10,xy(jj,2)+10,sn(jj),'fontsize',7);
end
plot(sp(:,1),sp(:,2),'*r');
print('-dsvg','sggs.svg');

formatSpec = '%9.3f %9.3f 2.000\n';
fileID = fopen('sp.txt','w');
fprintf(fileID,formatSpec,sp');
fclose(fileID);


