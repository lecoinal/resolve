# Procédure pour convertir les coordonnées des 35 stations SGGS en UTM, et pour définir une série de points de départ pour l'optimisation

On a 2 x 2h de données, à 100Hz, sur les journées 220 et 224 (année 2019), pour les réseaux 1B (composante GPZ) et SG (composante GNZ).

On a un fichier de coordonnées latitude longitude élévation avec 35 stations (35 lignes) : 13 stations SG et 22 stations 1B.

 * sur les 2h de la journée 220 : 19 stations 1B actives + 11 stations SG actives = total 30 stations sur 35
 * sur les 2h de la journée 224 : 20 stations 1B actives + 12 stations SG actives = total 32 stations sur 35

On va projeter les coordonnées latitude longitude sur une grille UTM.

On prend un centre fixe (quelque soit la journée de données, quelquesoit le subset de stations actives) qui correspond à la moyenne des 35 coordonnées x_UTM et y_UTM. Ce centre définit le starting point central. On place 25 starting points (5x5) autour de ce starting point central, avec des espacements dx=dy=150m.


---

## Projection UTM

```
> awk -F, '{print $2 " " $3 " " $4}' station.csv > latlonelev.txt
```

remove first line with `lat lon elev`

matlab deg2utm.m : https://www.mathworks.com/matlabcentral/fileexchange/10915-deg2utm

```
>> load latlonelev.txt
>> whos
  Name             Size            Bytes  Class     Attributes

  latlonelev      35x3               840  double              

>> [x,y,utmzone] = deg2utm(lat,lon);
```

utmzone = '11U' pour les 35 stations

```
>> formatSpec = '%9.3f\n';       
>> fileID = fopen('x.txt','w');  
>> fprintf(fileID,formatSpec,x);      
>> fclose(fileID);
>> fileID = fopen('y.txt','w'); 
>> fprintf(fileID,formatSpec,y);
>> fclose(fileID);  
```

On en profite pour définir le centre commun à tous les jours, ainsi que diverses coordonnées de points de départ, que l'on renseignera dans la namelist.



---

## Préparation du fichier de métrique, qui sera un input du code

Create `staname.txt` and `elev.txt` files:

```
> awk -F, '{print $1}' station.csv > staname.txt
> awk -F, '{print $1}' station.csv > elev.txt
```

and remove first line with only `"sta"` or `"elev"`.

Paste all those informations together : `staname x_utm y_utm elev`

```
> paste staname.txt x.txt y.txt elev.txt > zreseau_220_224_utm.txt
> rm staname.txt x.txt y.txt elev.txt latlonelev.txt
``` 

And then edit `zreseau_220_224_utm.txt` to complete staname with full station and component names:

```
# Edit with vi and string substitution  
R101 ... R2108 should become SG.R101..GNZ.D.2019.DOY ... SG.R2108..GNZ.D.2019.DOY 
1110 ... 2130 should become 1B.1110..GPZ.D.2019.DOY ... 1B.12130..GPZ.D.2019.DOY

# Use these commands (for example)
:1,13s/^R/SG.R/
:14,$s/^/1B./
:1,13s/\t/..GNZ.D.2019.DOY\t/
:14,$s/\t/..GPZ.D.2019.DOY\t/

```


Elevation in meters and not in km :

```
> awk '{printf("%s %s %s %.2f\n", $1, $2, $3, ($4)*1000 ) }' zreseau_220_224_utm.txt > zzzzz
> mv zzzzz zreseau_220_224_utm.txt
> awk '{printf("%s %s %s %.2f\n", $1, $2, $3, ($4)*1000 ) }' zreseau_220_224_utm_relatif.txt > zzzzz
> mv zzzzz zreseau_220_224_utm_relatif.txt
```



---

## Définition des 25 starting points

On utilise [plot_sggs.m](./plot_sggs.m) (Octave/MatLab file) pour définir 25 points de départ, qui composent une grille 5x5, centrée sur le barycentre des 35 stations, avec des pas d'espace 150m en x et y.

![plot SGGS array and start points](./sggs.svg)

On obtient [ces coordonnées UTM pour les différents points de départ](./sp.txt)

A noter que, pour simplifier, on a positionné les 25 zstart à 2000.0m. En effet le réseau est sur une zone relativement plate, le fichier stations.csv indique que les élévations des 35 stations se situent entre 1939.20m et 2027.78m.

---

## Trouver la station la plus proche de chacun des 25 points de départ

Cette étape est nécessaire pour le calcul du beam cohérent. Il faudra en outre s'assurer que chacune des 25 stations de référence relatives à ces 25 points de départ restent bien actives pour les deux subset de données julday 220 et 224...

Pour Argentière, cette étape était faite "à la main" et l'on fournissait en dur en paramètre d'entrée du code l'index de la station de référence choisie pour un starting point donné. 

Maintenant, pour SGGS, le code fait ce calcul à partir des positions des stations actives du jour et des coordonnées du starting point. Cela ne fonctionne que si on travaille en utm (nn_latlon=0). Attention, comme les stations actives peuvent varier au cours du temps (chaque jour on a un nouveau subset de stations actives), la station de référence pour le calcul du beam cohérent peut elle aussi varier chaque jour... A voir si cela constitue un problème pour l'interprétation des résultats ?





