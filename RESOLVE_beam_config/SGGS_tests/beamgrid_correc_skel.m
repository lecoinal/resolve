clear
addpath ~/SCRIPTS/UTILS/  % import dft.m

%%%%%%%%%%% CHOIX JD %%%%%%%%%%%%%

%%% JD=224
%%%%summerdir='/nfs_scratch/lecoinal/oar.17116870/'
%summerdir='/nfs_scratch/lecoinal/oar.58000900/'
%jd='224';

%%% JD=220
%%%%summerdir='/nfs_scratch/lecoinal/oar.57597815/'
%summerdir='/nfs_scratch/lecoinal/oar.58000896/'
%jd='220';

jd=<<<DOY>>>

if jd == 220;
  summerdir='/nfs_scratch/lecoinal/oar.58000896/'
elseif jd == 224;
  summerdir='/nfs_scratch/lecoinal/oar.58000900/'
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%timestep = 641; % 1-based index timestep (<=7200) % start = 64000;  % pas de temps 641
%xyzc_obj = [487826.78125 ; 5777542.50000 ; 1.97220 ; 1569.65771]; % parametres objectifs 
%%xyzc_obj = [487826.78125 ; 5777542.50000 ; 2 ; 1800]; % parametres objectifs 
%b_obj = 0.49981; % 0.551999;  % valeur de la fonction objectif
%
%%timestep = 6447;
%%start = 644600;
%%xyzc_obj = [487862.96875;5777381.00000;1.97952;1522.53186];
%%b_obj = 0.20688;

ln_plot=1;
dirplots='./';

inputfile=[summerdir,'/inZ.h5']
metricfile=[summerdir,'/zreseau_sorted.txt'];

% Calcul du temps de parcours
XYZ = importdata(metricfile);
staname = XYZ.textdata;
XYZ = XYZ.data;

mx = mean(XYZ(:,1));
my = mean(XYZ(:,2));

ngxy=101; % number of searchgrid_points along gridx and gridy

grillex=mx+linspace(-400,400,ngxy);
grilley=my+linspace(-400,400,ngxy);

Nf = 41;
freqstep = 0.1; % Hz
hfw = 2.0; % Hz % half_freq_width

fe = 100;
count = 100; % 100;  % 1sec à 500Hz

fc = 13.0 % Hz

info = h5info(inputfile)
N = length(info.Datasets)

% Compute frequency vector
for ifreq = 1:Nf
  frequence(ifreq) = (fc-hfw)+(ifreq-1)*freqstep;
end

% prepare BP filter
[BB AA]=butter(4,(fc+[-hfw +hfw])/fe*2);


% ici une boucle sur timestep

%for timestep=1:7200;
for timestep=641:641;
%for timestep=614:614;
%timestep=613;
%start = 61200;  % pas de temps 613
%xyzc_obj = [487776.65625;5777432.00000;1.94829;1504.55408];
%b_obj = 0.21534; 

%%%%%%%

char_timestep=sprintf('%04d',timestep);

%Read in out.h5 the b_obj and xyzc_obj found with beam_optim:
b_obj = double(h5read([summerdir,'/out.h5'],['/Baval'],timestep,1));

if (b_obj >= 0.0); % 0.18) ; % 0.15

xyzc_obj = double(h5read([summerdir,'/out.h5'],['/xyzc'],[timestep,1],[1,4]));

mz = xyzc_obj(3);
mc = xyzc_obj(4);

%grillez=mz; 
%grillec=mc; 

%grillez=[1500, 1700, 1900, 1975, 2025];
%grillec=[1400, 1700, 2100];
grillez=[1500];
grillec=[2100];
%grillez=[1500, 1600, 1700, 1800, 1900, 1950, 1975, 2000, 2025];
%grillec=[1400, 1550, 1700, 1850, 2100];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

start=(timestep-1)*fe;

%correction_rawdata=[1 -1 -1 1 -1 1 -1 -1 1 1 1 -1 -1 1 1 -1 -1 1 -1 1 -1 1 1 -1 1 1 -1 -1 -1 1 1 -1];  % pour le step sur JD=224 ?

% Read 1sec of input data

for inode = 1:N
  node = info.Datasets(inode).Name;
  inode,node,
  rawdata(:,inode) = double(h5read(inputfile,['/',node],start+1,count)); % [Nt, Nsta]
  norm_rawdata(:,inode) = rawdata(:,inode) / max(abs(rawdata(:,inode)));
end
%if ln_plot == 1
%  figure;hold on;imagesc(rawdata');shading flat;colorbar;ylabel('station');xlabel('time');title('rawdata');
%  for inode=1:N
%    plot(1:count,inode+rawdata(:,inode));
%  end
%  ylim([0 N+1]);xlim([0 count+1]);
%  print('-dpng',[dirplots,'/rawdata_',char_timestep,'.png']);
%  figure;hold on;imagesc(norm_rawdata');ylabel('station');xlabel('time');title('normalized rawdata');shading flat;colorbar;
%  for inode=1:N
%    plot(1:count,inode+norm_rawdata(:,inode));
%  end
%  ylim([0 N+1]);xlim([0 count+1]);
%  print('-dpng',[dirplots,'/norm_rawdata_',char_timestep,'.png']);
%end

% norm and BPfilt data and plot filtered data
for inode=1:N
  tmp = filtfilt(BB,AA,rawdata(:,inode));
%  tmp = bandpass(rawdata(:,inode),fc+[-hfw +hfw],fe);
  norm_filtered_rawdata(:,inode)=tmp(:)/max(abs(tmp));
end
%if ln_plot==1;
%  figure;hold on;imagesc(norm_filtered_rawdata');ylabel('station');xlabel('time');title('normalized butterworth bandpass-filtered rawdata');shading flat;colorbar;
%  for inode=1:N
%    plot(1:count,inode+norm_filtered_rawdata(:,inode));
%  end
%  ylim([0 N+1]);xlim([0 count+1]);
%  print('-dpng',[dirplots,'/norm_BPfilt_rawdata_',char_timestep,'.png']);
%end

% polarity correction

% on cherche la correction qui maximise la variance sur norm_filtered_rawdata
maxv = 0;
% N combinaisons possibles pour correction_rawdata
i01=1;  % on fixe le premier à 1, ainsi on ne teste que (N-1) combinaisons
savecc(:) = norm_filtered_rawdata(:,1)*i01;
correction_rawdata(:)=i01*ones(1,N);
for inode=2:N
  maxv=var(savecc(:) + norm_filtered_rawdata(:,inode));
  if (var(savecc(:) - norm_filtered_rawdata(:,inode)) > maxv);
    correction_rawdata(inode)=-1;
  end
  savecc(:) = savecc(:) + norm_filtered_rawdata(:,inode)*correction_rawdata(inode);
end

% polarity correction on rawdata and on norm_filtered_rawdata
for inode=1:N
  corr_rawdata(:,inode) = rawdata(:,inode)*correction_rawdata(inode);
  corr_norm_rawdata(:,inode) = norm_rawdata(:,inode)*correction_rawdata(inode);
  corr_norm_filtered_rawdata(:,inode) = norm_filtered_rawdata(:,inode)*correction_rawdata(inode);
end
       
%if ln_plot==1;
%  figure;hold on;imagesc(corr_rawdata');ylabel('station');xlabel('time');title('polarity corrected rawdata');shading flat;colorbar;
%  for inode=1:N
%    plot(1:count,inode+corr_rawdata(:,inode));
%  end
%  ylim([0 N+1]);xlim([0 count+1]);
%  print('-dpng',[dirplots,'/polcorr_rawdata_',char_timestep,'.png']);
%  figure;hold on;imagesc(corr_norm_rawdata');ylabel('station');xlabel('time');title('polarity corrected normalized rawdata');shading flat;colorbar;
%  for inode=1:N
%    plot(1:count,inode+corr_norm_rawdata(:,inode));
%  end
%  ylim([0 N+1]);xlim([0 count+1]);
%  print('-dpng',[dirplots,'/polcorr_norm_rawdata_',char_timestep,'.png']);
%  figure;hold on;imagesc(corr_norm_filtered_rawdata');ylabel('station');xlabel('time');title('polarity corrected normalized butterworth bandpass-filtered rawdata');shading flat;colorbar;
%  for inode=1:N
%    plot(1:count,inode+corr_norm_filtered_rawdata(:,inode));
%  end
%  ylim([0 N+1]);xlim([0 count+1]);
%  print('-dpng',[dirplots,'/polcorr_norm_BPfilt_rawdata_',char_timestep,'.png']);
%end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Bartlett without any polarity correction
% caspol 1 : without polcorr
% caspol 2 : with polarity correction

for caspol = 1:2;

% case 0 : work with rawdata
% case 1 : work with norm_rawdata
% case 2 : work with norm_filtered_rawdata

for cas = 0:2;

  if (cas == 0);
    if (caspol == 1);
      dvar = rawdata;
      varname = 'rawdata'
      corrfactor = ones(1,N);
    else;
      dvar = corr_rawdata;
      varname = 'polcorrrawdata'
      corrfactor = correction_rawdata;
    end
  elseif (cas == 1);
    if (caspol == 1);
      dvar = norm_rawdata;
      varname = 'Nrawdata'
      corrfactor = ones(1,N);
    else;
      dvar = corr_norm_rawdata;
      varname = 'polcorrNrawdata'
      corrfactor = correction_rawdata;
    end
  else
    if (caspol == 1);
      dvar = norm_filtered_rawdata;
      varname = 'NBBFrawdata'
      corrfactor = ones(1,N);
    else
      dvar = corr_norm_filtered_rawdata;
      varname = 'polcorrNBBFrawdata'
      corrfactor = correction_rawdata;
    end
  end

  phase_data = angle(transpose(dft(dvar,[0:size(dvar,1)-1]/fe,frequence))) ;  % [ Nsta, Nf ]   % [N, Nf]

  if ln_plot==1;
    figure;imagesc(frequence,1:N,phase_data);shading flat;colorbar;xlabel('freq');ylabel('station');
    set(gca,'YDir','normal');
    title(['phase ',varname]);
    print('-dpng',[dirplots,'/phase_',varname,'_',char_timestep,'.png']);
  end

  bartlett_nocsdm =zeros(length(grillex),length(grilley),length(grillez),length(grillec));
  for cc=1:length(grillec)
    ctmp = 1 / grillec(cc);
    for kk=1:length(grillez)
      ktmp = (XYZ(:,3)-grillez(kk)).^2;
      for ii=1:length(grillex)
        for jj=1:length(grilley)
          %dt = sqrt( (XYZ(:,1)-grillex(ii)).^2 + (XYZ(:,2)-grilley(jj)).^2 + (XYZ(:,3)-grillez(kk)).^2 ) / grillec(cc);
          dt = sqrt( (XYZ(:,1)-grillex(ii)).^2 + (XYZ(:,2)-grilley(jj)).^2 + ktmp ) * ctmp ;
          tmp = -2 * pi * dt;
          for ff = 1: Nf
              phase_model = tmp * frequence(ff); % [N]
              zz = sum(exp(i*(phase_data(:,ff)-phase_model(:))));
              bartlett_nocsdm(ii,jj,kk,cc)= bartlett_nocsdm(ii,jj,kk,cc) + zz*zz';
          end
          clear dt
        end
      end
    end
  end
  bartlett_nocsdm = bartlett_nocsdm / (N*N*Nf);

  Imax=0;
  for kk=1:length(grillez)
    for cc=1:length(grillec)
      tmp = squeeze(bartlett_nocsdm(:,:,kk,cc));
      [I J]=max(tmp(:,:));
      [II JJ]=max(I);
      if (II>Imax);
        Imax=II;
        kmax=kk;
        cmax=cc;
        ymax=JJ; % floor(J/ngxy);
        xmax=J(JJ); % J-ngxy*ymax+1; % mod(J,ngxy)+1;
%bartlett_nocsdm(mod(J,ngxy),floor(J/ngxy)+1,kk,cc)
      end
    end
  end
  bartlett_nocsdm(xmax,ymax,kmax,cmax)
  dt = sqrt( (XYZ(:,1)-grillex(xmax)).^2 + (XYZ(:,2)-grilley(ymax)).^2 + (XYZ(:,3)-grillez(kmax)).^2 ) / grillec(cmax);
  dt

  tmp = squeeze(bartlett_nocsdm(:,:,kmax,cmax));

  if ln_plot==1;


    figure;hold on;imagesc(dvar');ylabel('station');xlabel('time');title(varname);shading flat;colorbar;
    for inode=1:N
      plot(1:count,inode+dvar(:,inode));
      plot(fe*dt(inode),inode,'or');
    end
    ylim([0 N+1]);xlim([0 count+1]);
    print('-dpng',[dirplots,'/',varname,'_',char_timestep,'.png']);



    figure(3);clf
    imagesc(grillex,grilley,tmp');
    set(gca,'YDir','normal');
    hold on
    plot(xyzc_obj(1),xyzc_obj(2),'or')
    text(xyzc_obj(1),xyzc_obj(2),num2str(b_obj),'color','r','VerticalAlignment','bottom', 'HorizontalAlignment','left');
    plot(grillex(xmax),grilley(ymax),'*r')
    if ((cas == 1) && (caspol == 1));
      legend('optimum found with beam\_optim','... with beam\_grid')
    end
    mask=corrfactor;
    mask(mask==-1)=NaN;
    plot(mask(:).*XYZ(:,1),mask(:).*XYZ(:,2),'^k')
    mask=-corrfactor;
    mask(mask==-1)=NaN;
    plot(mask(:).*XYZ(:,1),mask(:).*XYZ(:,2),'vw')
    colorbar
    axis tight
    %title({['Bartlett incoherent - ' num2str(max(bartlett_nocsdm(:)))],['z=' num2str(xyzc_obj(3)) ' - c=' num2str(xyzc_obj(4))]});
    %title({['Bartlett incoherent - ' num2str(max(bartlett_nocsdm(:)))],['z=' num2str(grillez(kmax)) ' - c=' num2str(xyzc_obj(4))]});
    title({['incohBa - ' varname ' - ' num2str(max(bartlett_nocsdm(:)))],['z=' num2str(grillez(kmax)) ' - c=' num2str(grillec(cmax))]});
    print('-dpng',[dirplots,'/Ba_incoh_grid_',varname,'_',char_timestep,'.png'])


    for kk=1:length(grillez)
      for cc=1:length(grillec)
        tmp = squeeze(bartlett_nocsdm(:,:,kk,cc));
        [I J]=max(tmp(:,:));
        [II JJ]=max(I);
        Imax=II;
        kmax=kk;
        cmax=cc;
        ymax=JJ; % floor(J/ngxy);
        xmax=J(JJ); % J-ngxy*ymax+1; % mod(J,ngxy)+1;
        figure(4);clf
        imagesc(grillex,grilley,tmp')
        set(gca,'YDir','normal');
        hold on
        plot(xyzc_obj(1),xyzc_obj(2),'or')
        text(xyzc_obj(1),xyzc_obj(2),num2str(b_obj),'color','r','VerticalAlignment','bottom', 'HorizontalAlignment','left');
        plot(grillex(xmax),grilley(ymax),'*r')
        if ((cas == 1) && (caspol == 1));
          legend('optimum found with beam\_optim','... with beam\_grid')
        end
        mask=corrfactor;
        mask(mask==-1)=NaN;
        plot(mask(:).*XYZ(:,1),mask(:).*XYZ(:,2),'^k')
        mask=-corrfactor;
        mask(mask==-1)=NaN;
        plot(mask(:).*XYZ(:,1),mask(:).*XYZ(:,2),'vw')
        colorbar
        axis tight
        %title({['Bartlett incoherent - ' num2str(max(bartlett_nocsdm(:)))],['z=' num2str(xyzc_obj(3)) ' - c=' num2str(xyzc_obj(4))]});
        %title({['Bartlett incoherent - ' num2str(max(bartlett_nocsdm(:)))],['z=' num2str(grillez(kmax)) ' - c=' num2str(xyzc_obj(4))]});
        title({['incohBa - ' varname ' - ' num2str(max(tmp(:)))],['z=' num2str(grillez(kmax)) ' - c=' num2str(grillec(cmax))]});
        print('-dpng',[dirplots,'/Ba_incoh_grid_',varname,'_z',num2str(grillez(kmax)),'_c',num2str(grillec(cmax)),'_',char_timestep,'.png'])
      end
    end

  end

end % end cas

end % end caspol

end % end if b_obj >= 0.15

end % end timestep







%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%
%
%
%
%
%
%
%% 1. Bartlett incohérent
%
%% Compute CSDM
%
%%data = transpose(dft(rawdata,[0:size(rawdata,1)-1]/fe,frequence)) ;  % [ Nsta, Nf ]
%data = transpose(dft(corr_rawdata,[0:size(rawdata,1)-1]/fe,frequence)) ;  % [ Nsta, Nf ]
%phase_data = angle(data); % [N, Nf]
%
%%for ifreq=1:Nf
%%  for inode=1:N
%%    phase_data_rot(inode,ifreq) = phase_data(inode,ifreq) + 0.5*pi*(-correction_rawdata(inode)+1)*0.5;
%%  end
%%end
%
%%for inode=1:N
%%  filtered_norm_rawdata_rot(:,inode) = filtered_norm_rawdata(:,inode)*correction_rawdata(inode);
%%end
%%datarot=transpose(dft(filtered_norm_rawdata_rot,[0:size(rawdata,1)-1]/fe,frequence)) ;
%%phase_datarot = angle(datarot);
%
%
%if ln_plot==1;
%  figure;imagesc(frequence,1:N,phase_data);shading flat;colorbar;xlabel('freq');ylabel('station');title('phase data');print('-dpng','polcorr_phasedata.png');
%end
%%figure;imagesc(frequence,1:N,phase_data_rot);shading flat;colorbar;xlabel('freq');ylabel('station');title('phase data');print('-dpng','phasedata_rot.png');
%%figure;imagesc(frequence,1:N,phase_datarot);shading flat;colorbar;xlabel('freq');ylabel('station');title('phase data');print('-dpng','phasedatarot.png');
%%figure;imagesc(frequence,1:N,mod(phase_datarot-phase_data_rot,2*pi));shading flat;colorbar;xlabel('freq');ylabel('station');title('phase data');print('-dpng','diffphasedatarot.png');
%
%%tmp = transpose(dft(filtered_norm_rawdata,[0:size(filtered_norm_rawdata,1)-1]/fe,frequence)) ;
%%figure;imagesc(frequence,1:N,angle(tmp));shading flat;colorbar;xlabel('freq');ylabel('station');title('phase butterworth bandpass-filtered normalized rawdata');print('-dpng','phase_BPfilt_norm_rawdata.png');
%%phase_data = angle(tmp);
%
%%data = exp(i*angle(data)) ;
%%K = zeros(N,N,Nf) ;
%%for ff = 1 : Nf
%%  K(:,:,ff) = K(:,:,ff) + data(:,ff)*data(:,ff)' ;
%%end
%% on pourrait normaliser K par 98...
%
%%dt = sqrt( (XYZ(:,1)-xyzc_obj(1)).^2 + (XYZ(:,2)-xyzc_obj(2)).^2 + (XYZ(:,3)-xyzc_obj(3)).^2 ) / xyzc_obj(4);
%%
%%%bartlett = 0;
%%%for ff = 1 : Nf
%%%  replica = exp(-i*2*pi*frequence(ff)*dt);
%%%  
%%%  % on pourrait normaliser replica par sqrt(98) % %norm(replica)^2
%%%  beam(ff) = abs(replica'*K(:,:,ff)*replica) / (N*N);    % le abs est superflu ?
%%%  %beam(ff) = (replica'*K(:,:,ff)*replica) / (N*N);    % le abs est superflu ?
%%%  bartlett = bartlett + beam(ff) / Nf;
%%%end
%%  
%%%bartlett
%%
%%% meme calcul mais sans monter en mémoire la csdm
%%bartlett_nocsdm = 0.0;
%%tmp = -2 * pi * dt;
%%for ff = 1 : Nf
%%  phase_model = tmp * frequence(ff); % [N]
%%  zz = sum(exp(i*(phase_data(:,ff)-phase_model(:))));
%%  bartlett_nocsdm = bartlett_nocsdm + zz*zz';
%%end
%%bartlett_nocsdm = bartlett_nocsdm / (N*N*Nf);
%%bartlett_nocsdm
%
%
%
%bartlett_nocsdm =zeros(length(grillex),length(grilley),length(grillez),length(grillec));
%for cc=1:length(grillec)
%  ctmp = 1 / grillec(cc);
%  for kk=1:length(grillez)
%    ktmp = (XYZ(:,3)-grillez(kk)).^2;
%    [cc,kk]
%    for ii=1:length(grillex)
%      for jj=1:length(grilley)
%        %dt = sqrt( (XYZ(:,1)-grillex(ii)).^2 + (XYZ(:,2)-grilley(jj)).^2 + (XYZ(:,3)-xyzc_obj(3)).^2 ) / xyzc_obj(4);
%        %dt = sqrt( (XYZ(:,1)-grillex(ii)).^2 + (XYZ(:,2)-grilley(jj)).^2 + (XYZ(:,3)-grillez(kk)).^2 ) / grillec(cc);
%        dt = sqrt( (XYZ(:,1)-grillex(ii)).^2 + (XYZ(:,2)-grilley(jj)).^2 + ktmp ) * ctmp ;
%        tmp = -2 * pi * dt;
%        for ff = 1: Nf
%            phase_model = tmp * frequence(ff); % [N]
%            zz = sum(exp(i*(phase_data(:,ff)-phase_model(:))));
%            bartlett_nocsdm(ii,jj,kk,cc)= bartlett_nocsdm(ii,jj,kk,cc) + zz*zz';
%        end
%        clear dt
%    end
%    end
%    end
%end
%bartlett_nocsdm = bartlett_nocsdm / (N*N*Nf);
%
%
%%[I J]=max(bartlett_nocsdm(:));
%%bartlett_nocsdm(mod(J,ngxy),floor(J/ngxy)+1)
%
%Imax=0;
%for kk=1:length(grillez)
%    for cc=1:length(grillec)
%tmp = squeeze(bartlett_nocsdm(:,:,kk,cc));
%[I J]=max(tmp(:));
%if (I>Imax);
%  Imax=I;
%  kmax=kk;
%  cmax=cc;
%  xmax=mod(J,ngxy);
%  ymax=floor(J/ngxy)+1;
%%bartlett_nocsdm(mod(J,ngxy),floor(J/ngxy)+1,kk,cc)
%end
%end
%end
%bartlett_nocsdm(xmax,ymax,kmax,cmax)
%
%tmp = squeeze(bartlett_nocsdm(:,:,kmax,cmax));
%
%if ln_plot==1;
%  figure(3);clf
%  %set(gca,'YDir','normal');
%  %imagesc(grillex,grilley,bartlett_nocsdm')
%  imagesc(grillex,grilley,tmp')
%  hold on
%%  plot(xyzc_obj(1),xyzc_obj(2),'or')
%%  text(xyzc_obj(1),xyzc_obj(2),num2str(b_obj),'color','r','VerticalAlignment','bottom', 'HorizontalAlignment','left');
%  plot(grillex(xmax),grilley(ymax),'*r')
%%  legend('optimum found with beam\_optim','... with beam\_grid')
%  mask=correction_rawdata;
%  mask(mask==-1)=NaN;
%  plot(mask(:).*XYZ(:,1),mask(:).*XYZ(:,2),'^k')
%  mask=-correction_rawdata;
%  mask(mask==-1)=NaN;
%  plot(mask(:).*XYZ(:,1),mask(:).*XYZ(:,2),'vw')
%  colorbar
%  axis tight
%  %title({['Bartlett incoherent - ' num2str(max(bartlett_nocsdm(:)))],['z=' num2str(xyzc_obj(3)) ' - c=' num2str(xyzc_obj(4))]});
%  %title({['Bartlett incoherent - ' num2str(max(bartlett_nocsdm(:)))],['z=' num2str(grillez(kmax)) ' - c=' num2str(xyzc_obj(4))]});
%  title({['Bartlett incoherent - ' num2str(max(bartlett_nocsdm(:)))],['z=' num2str(grillez(kmax)) ' - c=' num2str(grillec(cmax))]});
%  print('-dpng','polcorr_Ba_incoh_grid.png')
%end  
%
%
%
%
%
%
%
%
%%if (bartlett_nocsdm(mod(J,ngxy),floor(J/ngxy)+1) > maxv)
%%  maxv = bartlett_nocsdm(mod(J,ngxy),floor(J/ngxy)+1)
%%%  maxi = mod(J,ngxy);
%%%  maxj = floor(J/ngxy)+1;
%%  maxcorrection = correction_rawdata;
%%end
%
%%if ln_plot==1;
%%  figure(3);clf
%%  %set(gca,'YDir','normal');
%%  imagesc(grillex,grilley,bartlett_nocsdm')
%%  hold on
%%  plot(xyzc_obj(1),xyzc_obj(2),'or')
%%  text(xyzc_obj(1),xyzc_obj(2),num2str(b_obj),'color','r','VerticalAlignment','bottom', 'HorizontalAlignment','left');
%%  plot(grillex(mod(J,ngxy)),grilley(floor(J/ngxy)+1),'*r')
%%  legend('optimum found with beam\_optim','... with beam\_grid')
%%  mask=correction_rawdata;
%%  mask(mask==-1)=NaN;
%%  plot(mask(:).*XYZ(:,1),mask(:).*XYZ(:,2),'^k')
%%  mask=-correction_rawdata;
%%  mask(mask==-1)=NaN;
%%  plot(mask(:).*XYZ(:,1),mask(:).*XYZ(:,2),'vw')
%%  colorbar
%%  axis tight
%%  title({['Bartlett incoherent - ' num2str(max(bartlett_nocsdm(:)))],['z=' num2str(xyzc_obj(3)) ' - c=' num2str(xyzc_obj(4))]});
%%  print('-dpng','polcorr_Ba_incoh_grid.png')
%%end  
%%  
%%  
%%  
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%%% 2. Bartlett cohérent
%%%
%%%data = transpose(dft(rawdata,[0:size(rawdata,1)-1]/fe,frequence)) ; % [N,Nf]
%%%data = data(:); % on aligne les 98stations * 41 frequences
%%%data = exp(i*angle(data)) ;
%%phase_data = angle(data);
%%K = zeros(N*Nf,N*Nf) ;
%%K(:,:) = K(:,:) + data(:)*data(:)' ;  % [N*Nf, N*Nf]
%%%norm(K) 4018 = 98*41
%%for ff = 1 : Nf
%%  replica(:,ff) = exp(-i*2*pi*frequence(ff)*dt); % [N, Nf]
%%end
%%replica=replica(:); % on aligne les 98 distances a la source * 41 frequences
%%%norm(replica) % sqrt(4018)
%%bartlettcoh = abs(replica'*K(:,:)*replica) / (N*N*Nf*Nf);  % le abs est superflu ?
%%%bartlettcoh = (replica'*K(:,:)*replica) / (N*N*Nf*Nf);  % le abs est superflu ?
%%
%%bartlettcoh
%%
%%
%%% meme calcul mais sans monter en mémoire la csdm
%%for ff = 1 : Nf
%%  phase_model(:,ff) = -2 * pi * frequence(ff)*dt; % [N,Nf]
%%end
%%phase_model = phase_model(:);
%%zz = sum(exp(i*(phase_data(:)-phase_model(:))));
%%bartlettcoh_nocsdm = zz*zz';
%%bartlettcoh_nocsdm = bartlettcoh_nocsdm / (N*N*Nf*Nf);
%%bartlettcoh_nocsdm
