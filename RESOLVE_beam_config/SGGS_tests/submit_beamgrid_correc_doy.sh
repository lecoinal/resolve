#!/bin/bash
#OAR --project iste-equ-ondes
#OAR -n SGGSbeamgrid
#OAR -l /nodes=1/core=1,walltime=01:00:00

set -e

DOY=$1  # 220 ou 224

source /soft/env.bash

module load MATLAB/R2020a

TMPDIR="/$SHARED_SCRATCH_DIR/$USER/tmpsggs_ZC_$DOY"

mkdir -p $TMPDIR

sed -e "s/<<<DOY>>>/$DOY/" beamgrid_correc_skel.m > $TMPDIR/beamgrid_correc_doy.m

cd $TMPDIR

matlab -nodisplay -nosplash < beamgrid_correc_doy.m > $TMPDIR/result_$OAR_JOB_ID.out

