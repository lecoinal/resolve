#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
#    This file is part of MFP   : a Python code for recombining MFP outputs   #
#    from one CiGri job that executes twice beamforming code on 12-h input    #
#    (in case nn_io can't divide the total number of time-steps)              #
#                                                                             #
#    Copyright (C) 2018 Albanne Lecointre                                     #
#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
###############################################################################


###############################################################
# This is the python code recombine_beamresolve.py:
# - iterate over all dataset
# - look for the good dimension index
# - check that overlapping period is correct
# - merge the two dataset
# - write output dataset into out.h5 file
###############################################################

# $ h5ls oar.00000.ZO_2018_116.0.8570684.00000/out_ZO_2018_116_0.h5 
# Baval                    Dataset {86400}
# infooptim                Dataset {3, 86400}
# patchesInd               Dataset {86400, 98}
# xyc                      Dataset {3, 86400}
# $ h5ls oar.00000.ZO_2018_116.21599750.8570701.00000/out_ZO_2018_116_21599750.h5 
# Baval                    Dataset {86400}
# infooptim                Dataset {3, 86400}
# patchesInd               Dataset {86400, 98}
# xyc                      Dataset {3, 86400}
# $ python2.7 recombine_beamresolve.py oar.00000.ZO_2018_116.0.8570684.00000/out_ZO_2018_116_0.h5 oar.00000.ZO_2018_116.21599750.8570701.00000/out_ZO_2018_116_21599750.h5 1 86400 Baval,infooptim,patchesInd,xyc 
# -> cat all datasets with merging 1 overlapping sample(s)

# $ for jd in $(seq 116 122) ; do ln -sf /bettik/lecointre/00000/oar.00000.ZO_2018_$jd.*.86999*.00000 . ; listfile=$(ls oar*/out*); echo $listfile; python2.7 recombine_beamresolve.py $listfile 1 43200 Baval,infooptim,patchesInd,xyzc; mv out.h5 beam_ZO_2018_${jd}.h5; rm oar*; done

import h5py
import sys
import numpy as np

def check_overlap(var1,var2):
  if ( not np.allclose(var1,var2) ):
    print("Overlapping not equal, Error...")
    print(var1)
    print(var2)
    sys.exit(1)

idir='.'
ifile1=sys.argv[1] 
ifile2=sys.argv[2] 
odir='.'
ofile='out.h5'

h5f1 = h5py.File(idir+'/'+ifile1,'r')
h5f2 = h5py.File(idir+'/'+ifile2,'r')
h5fo = h5py.File(odir+'/'+ofile,'w')

noverlap=int(sys.argv[3])

nit=int(sys.argv[4])

dsetnames=sys.argv[5]

print(dsetnames)
print("")

for node in dsetnames.split(","):
  dataset1=h5f1[node]
  dataset2=h5f2[node]
  typ = np.dtype(dataset1)
  shap = np.array(np.shape(dataset1))
  indice = np.where( shap == nit )[0]
  print(node,typ,shap,indice)
  if len(shap) == 1:   # 1D dataset
    new_dset=np.empty((2*nit-noverlap,),dtype=typ)
    recover1 = dataset1[nit-noverlap:nit]
    recover2 = dataset2[0:noverlap]
    check_overlap(recover1,recover2)
    new_dset[0:nit] = dataset1[0:nit]
    new_dset[nit:2*nit-noverlap] = dataset2[noverlap:nit]
  else:
    if indice == 0:     # 2D dataset with timestep as first dimension
      new_dset=np.empty((2*nit-noverlap,shap[1]),dtype=typ)
      recover1 = dataset1[nit-noverlap:nit,:]
      recover2 = dataset2[0:noverlap,:]
      check_overlap(recover1,recover2)
      new_dset[0:nit,:] = dataset1[0:nit,:]
      new_dset[nit:2*nit-noverlap,:] = dataset2[noverlap:nit,:]
    else:               # 2D dataset with timestep as second dimension
      new_dset=np.empty((shap[0],2*nit-noverlap),dtype=typ)
      recover1 = dataset1[:,nit-noverlap:nit]
      recover2 = dataset2[:,0:noverlap]
      check_overlap(recover1,recover2)
      new_dset[:,0:nit] = dataset1[:,0:nit]
      new_dset[:,nit:2*nit-noverlap] = dataset2[:,noverlap:nit]
  print(np.shape(new_dset))
  print("")

  h5fo.create_dataset(node,shape=np.shape(new_dset), dtype=typ,data=new_dset,compression='gzip', compression_opts=6, fletcher32='True')

h5f1.close()
h5f2.close()
h5fo.close()
