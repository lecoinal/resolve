# recombine beam outputs from CiGri campaign

First you may have to do some modifications into Python script :
 - dir input :  
```
dirinput = "/data/projects/resolve/Argentiere/ALBANNE/BEAM_V0/EXP000/XYZC_ASA047_BOUNDZ5/"
```
 - list of starting points :
```
startpoint = np.array([[0,  100, -180, 180,  20, -240,  50,   50, -80, -90, -90],
                       [0, -120,  180, -40, -20,   70, 110, -200, 230,  90, -90]])

```

***

**Usage:**

```
$ python3.5 recombine_cigri_beam_resolve.py -h
/soft/python/lib/python3.5/site-packages/h5py/__init__.py:34: FutureWarning: Conversion of the second argument of issubdtype from `float` to `np.floating` is deprecated. In future, it will be treated as `np.float64 == np.dtype(float).type`.
  from ._conv import register_converters as _register_converters
usage: recombine_cigri_beam_resolve.py [-h] [--o O] [--n N] [--j1 J1]
                                       [--j2 J2] [--f1 F1] [--f2 F2]

recombine outputs of resolve beamforming CiGri campaign

optional arguments:
  -h, --help  show this help message and exit
  --o O       name of output file (default ./out.h5)
  --n N       number of timesteps (default 86399)
  --j1 J1     first jday (default 114)
  --j2 J2     last jday (default 153)
  --f1 F1     first frequency (default 5.0)
  --f2 F2     last frequency (default 31.0)
```

***

**Exemple on IST-clusters, on interactive mode**

recombine only days 117:118 and central_frequencies 6:0:5:6.6, with considering 86399 timestep/day
```
$ python3.5 recombine_cigri_beam_resolve.py --j1 117 --j2 118 --f1 6 --f2 6.5
```

recombine all existing days and frequencies, with considering 86399 timestep/day (default options)
```
$ python3.5 recombine_cigri_beam_resolve.py
```

recombine all existing days and frequencies, with considering 172799 timestep/day
```
$ python3.5 recombine_cigri_beam_resolve.py -n 172799
```

recombine only days 117:118 and central_frequencies 6:0:5:6.6, with considering 172799 timestep/day
```
$ python3.5 recombine_cigri_beam_resolve.py --j1 117 --j2 118 --f1 6 --f2 6.5 -n 172799
```

***

**Exemple on IST-clusters, on batch mode**

Edit the submission script submit.sh to check the arguments for python execution, check your walltime, and submit.
```
$ oarsub -S "./submit.sh"
```

***

Que mettre dans x,y,z, et positions ? au regard de cette table : http://ws.resif.fr/fdsnws/station/1/query?network=ZO&startafter=2018-01-01T00:00:00.000000&endbefore=2018-12-31T23:59:59.999999&level=station&format=text

En particulier comment faire la conversion lat/lon en degres vers les x/y en m ?

```
lecoinal@ist-oar:~/resolve/RESOLVE_beam_src/recombine_cigri_output$ h5dump -d /positions /data/projects/resolve/Argentiere/PHILIPPE/processed/RESOLVE-ARGENTIERE.mat
HDF5 "/data/projects/resolve/Argentiere/PHILIPPE/processed/RESOLVE-ARGENTIERE.mat" {
DATASET "/positions" {
   DATATYPE  H5T_IEEE_F64LE
   DATASPACE  SIMPLE { ( 3, 98 ) / ( 3, 98 ) }
   DATA {
   (0,0): -331.7, -289.986, -211.501, -174.772, -131.302, -95.3923, -52.3263,
   (0,7): -10.7703, 25.3977, 68.4007, -335.071, -292.13, -254.206, -214.278,
   (0,14): -175.933, -134.62, -96.5743, -53.0823, -13.5193, 26.5727, 69.7827,
   (0,21): 104.156, 144.624, -301.27, -256.67, -221.605, -180.019, -137.432,
   (0,28): -99.3403, -60.4893, -17.1333, 18.1537, 61.0327, 93.6887, 140.617,
   (0,35): 179.986, -259.527, -221.194, -183.69, -143.388, -102.045,
   (0,41): -63.3803, -24.2153, 16.0347, 55.2417, 97.8627, 139.06, 178.295,
   (0,48): 220.556, -224.954, -186.889, -146.41, -107.411, -67.2193,
   (0,54): -27.7853, 13.8377, 51.8657, 94.2917, 132.013, 173.099, 214.243,
   (0,61): 255.473, -192.815, -151.087, -112.192, -67.7093, -29.9753,
   (0,67): 9.36874, 48.6727, 91.3597, 133.591, 170.668, 210.273, 248.219,
   (0,74): 291.477, -114.549, -81.8243, -33.1093, 8.81074, 48.8407, 84.9737,
   (0,81): 129.289, 170.525, 211.526, 248.657, 290.744, 328.181, -77.0053,
   (0,88): -35.5153, 6.69174, 43.9087, 84.8267, 123.413, 169.397, 210.756,
   (0,95): 246.835, 287.269, 322.467,
   (1,0): 28.9293, 1.27835, -61.3437, -89.9447, -124.645, -153.163, -183.122,
   (1,7): -213.606, -240.576, -269.068, 94.3183, 67.3923, 34.8953, 3.02535,
   (1,14): -27.6477, -59.0927, -88.3617, -119.33, -147.903, -178.611,
   (1,20): -210.871, -238.017, -265.353, 129.526, 97.0853, 70.5353, 38.2943,
   (1,27): 9.02035, -16.9597, -52.9427, -85.1487, -110.44, -141.863,
   (1,33): -174.842, -203.736, -229.643, 167.055, 136.762, 106.718, 76.0653,
   (1,40): 44.3943, 14.0143, -15.9647, -44.3907, -75.0357, -107.749,
   (1,46): -136.819, -165.844, -195.894, 199.248, 172.139, 141.304, 113.786,
   (1,53): 77.4713, 51.7553, 24.0233, -9.72665, -42.4467, -70.5907, -101.852,
   (1,60): -134.195, -160.933, 240.149, 209.211, 180.11, 147.504, 119.187,
   (1,67): 88.2843, 59.1253, 31.5333, -8.56765, -35.0607, -67.2777, -94.0197,
   (1,74): -124.757, 243.078, 219.908, 186.429, 154.567, 122.533, 94.9773,
   (1,81): 55.7773, 27.4123, -0.0536531, -31.8577, -64.5087, -87.8847,
   (1,87): 281.064, 248.62, 215.804, 183.915, 155.373, 125.461, 94.5943,
   (1,94): 65.0013, 32.8203, 2.39635, -22.2307,
   (2,0): -33.6564, -28.2914, -13.6514, -7.41936, -1.59836, 1.28264, 4.46464,
   (2,7): 5.28864, 7.83064, 11.9036, -31.0194, -27.7104, -22.8954, -16.5704,
   (2,14): -11.3324, -4.83836, -0.865357, 2.28264, 4.67164, 6.70064, 10.1826,
   (2,21): 14.2036, 20.1626, -29.8844, -25.9654, -21.1234, -15.9554,
   (2,27): -11.0784, -6.00136, -0.130357, 3.64764, 7.48664, 11.2896, 14.0746,
   (2,34): 19.0146, 22.5836, -30.1464, -27.5634, -23.4034, -17.3064,
   (2,40): -10.3284, -3.62536, 2.14064, 6.00864, 9.91864, 13.9066, 18.9116,
   (2,47): 22.6496, 25.8076, -31.5984, -27.7484, -20.9044, -15.2334,
   (2,53): -8.19636, -2.38836, 3.51264, 8.26364, 12.4266, 16.1526, 19.7016,
   (2,60): 23.2346, 25.5646, -28.5764, -23.0614, -16.5274, -10.1284,
   (2,66): -4.70836, 1.55364, 6.98064, 11.2216, 15.3936, 18.3536, 20.8716,
   (2,73): 23.7176, 26.6146, -26.6414, -20.2064, -12.3764, -4.90736, 2.07264,
   (2,80): 7.18664, 12.3676, 16.4136, 18.8746, 20.8336, 24.0126, 26.2886,
   (2,87): -25.4384, -18.2414, -10.9084, -3.01236, 4.33164, 9.11364, 13.9556,
   (2,94): 16.5486, 18.4266, 20.1056, 22.6526
   }
   ATTRIBUTE "MATLAB_class" {
      DATATYPE  H5T_STRING {
         STRSIZE 6;
         STRPAD H5T_STR_NULLTERM;
         CSET H5T_CSET_ASCII;
         CTYPE H5T_C_S1;
      }
      DATASPACE  SCALAR
      DATA {
      (0): "double"
      }
   }
}
}
lecoinal@ist-oar:~/resolve/RESOLVE_beam_src/recombine_cigri_output$ h5dump -d /x /data/projects/resolve/Argentiere/PHILIPPE/processed/RESOLVE-ARGENTIERE.mat
HDF5 "/data/projects/resolve/Argentiere/PHILIPPE/processed/RESOLVE-ARGENTIERE.mat" {
DATASET "/x" {
   DATATYPE  H5T_IEEE_F64LE
   DATASPACE  SIMPLE { ( 1, 98 ) / ( 1, 98 ) }
   DATA {
   (0,0): 342727, 342768, 342847, 342884, 342927, 342963, 343006, 343048,
   (0,8): 343084, 343127, 342723, 342766, 342804, 342844, 342883, 342924,
   (0,16): 342962, 343005, 343045, 343085, 343128, 343163, 343203, 342757,
   (0,24): 342802, 342837, 342878, 342921, 342959, 342998, 343041, 343077,
   (0,32): 343119, 343152, 343199, 343238, 342799, 342837, 342875, 342915,
   (0,40): 342956, 342995, 343034, 343074, 343114, 343156, 343198, 343237,
   (0,48): 343279, 342834, 342872, 342912, 342951, 342991, 343031, 343072,
   (0,56): 343110, 343153, 343190, 343232, 343273, 343314, 342866, 342907,
   (0,64): 342946, 342991, 343028, 343068, 343107, 343150, 343192, 343229,
   (0,72): 343269, 343307, 343350, 342944, 342977, 343025, 343067, 343107,
   (0,80): 343143, 343188, 343229, 343270, 343307, 343349, 343387, 342981,
   (0,88): 343023, 343065, 343102, 343143, 343182, 343228, 343269, 343305,
   (0,96): 343346, 343381
   }
   ATTRIBUTE "MATLAB_class" {
      DATATYPE  H5T_STRING {
         STRSIZE 6;
         STRPAD H5T_STR_NULLTERM;
         CSET H5T_CSET_ASCII;
         CTYPE H5T_C_S1;
      }
      DATASPACE  SCALAR
      DATA {
      (0): "double"
      }
   }
}
}
lecoinal@ist-oar:~/resolve/RESOLVE_beam_src/recombine_cigri_output$ h5dump -d /y /data/projects/resolve/Argentiere/PHILIPPE/processed/RESOLVE-ARGENTIERE.mat
HDF5 "/data/projects/resolve/Argentiere/PHILIPPE/processed/RESOLVE-ARGENTIERE.mat" {
DATASET "/y" {
   DATATYPE  H5T_IEEE_F64LE
   DATASPACE  SIMPLE { ( 1, 98 ) / ( 1, 98 ) }
   DATA {
   (0,0): 5.09195e+06, 5.09192e+06, 5.09186e+06, 5.09183e+06, 5.0918e+06,
   (0,5): 5.09177e+06, 5.09174e+06, 5.09171e+06, 5.09168e+06, 5.09165e+06,
   (0,10): 5.09202e+06, 5.09199e+06, 5.09196e+06, 5.09193e+06, 5.09189e+06,
   (0,15): 5.09186e+06, 5.09183e+06, 5.0918e+06, 5.09177e+06, 5.09174e+06,
   (0,20): 5.09171e+06, 5.09168e+06, 5.09166e+06, 5.09205e+06, 5.09202e+06,
   (0,25): 5.09199e+06, 5.09196e+06, 5.09193e+06, 5.09191e+06, 5.09187e+06,
   (0,30): 5.09184e+06, 5.09181e+06, 5.09178e+06, 5.09175e+06, 5.09172e+06,
   (0,35): 5.09169e+06, 5.09209e+06, 5.09206e+06, 5.09203e+06, 5.092e+06,
   (0,40): 5.09197e+06, 5.09194e+06, 5.09191e+06, 5.09188e+06, 5.09185e+06,
   (0,45): 5.09181e+06, 5.09179e+06, 5.09176e+06, 5.09173e+06, 5.09212e+06,
   (0,50): 5.09209e+06, 5.09206e+06, 5.09204e+06, 5.092e+06, 5.09197e+06,
   (0,55): 5.09195e+06, 5.09191e+06, 5.09188e+06, 5.09185e+06, 5.09182e+06,
   (0,60): 5.09179e+06, 5.09176e+06, 5.09216e+06, 5.09213e+06, 5.0921e+06,
   (0,65): 5.09207e+06, 5.09204e+06, 5.09201e+06, 5.09198e+06, 5.09195e+06,
   (0,70): 5.09191e+06, 5.09189e+06, 5.09185e+06, 5.09183e+06, 5.0918e+06,
   (0,75): 5.09217e+06, 5.09214e+06, 5.09211e+06, 5.09208e+06, 5.09204e+06,
   (0,80): 5.09202e+06, 5.09198e+06, 5.09195e+06, 5.09192e+06, 5.09189e+06,
   (0,85): 5.09186e+06, 5.09183e+06, 5.0922e+06, 5.09217e+06, 5.09214e+06,
   (0,90): 5.09211e+06, 5.09208e+06, 5.09205e+06, 5.09202e+06, 5.09199e+06,
   (0,95): 5.09195e+06, 5.09192e+06, 5.0919e+06
   }
   ATTRIBUTE "MATLAB_class" {
      DATATYPE  H5T_STRING {
         STRSIZE 6;
         STRPAD H5T_STR_NULLTERM;
         CSET H5T_CSET_ASCII;
         CTYPE H5T_C_S1;
      }
      DATASPACE  SCALAR
      DATA {
      (0): "double"
      }
   }
}
}
lecoinal@ist-oar:~/resolve/RESOLVE_beam_src/recombine_cigri_output$ h5dump -d /z /data/projects/resolve/Argentiere/PHILIPPE/processed/RESOLVE-ARGENTIERE.mat
HDF5 "/data/projects/resolve/Argentiere/PHILIPPE/processed/RESOLVE-ARGENTIERE.mat" {
DATASET "/z" {
   DATATYPE  H5T_IEEE_F64LE
   DATASPACE  SIMPLE { ( 1, 98 ) / ( 1, 98 ) }
   DATA {
   (0,0): 2330.08, 2335.44, 2350.08, 2356.31, 2362.13, 2365.01, 2368.2,
   (0,7): 2369.02, 2371.56, 2375.64, 2332.71, 2336.02, 2340.84, 2347.16,
   (0,14): 2352.4, 2358.89, 2362.87, 2366.01, 2368.4, 2370.43, 2373.91,
   (0,21): 2377.94, 2383.89, 2333.85, 2337.77, 2342.61, 2347.78, 2352.65,
   (0,28): 2357.73, 2363.6, 2367.38, 2371.22, 2375.02, 2377.81, 2382.75,
   (0,35): 2386.32, 2333.59, 2336.17, 2340.33, 2346.43, 2353.4, 2360.11,
   (0,42): 2365.87, 2369.74, 2373.65, 2377.64, 2382.64, 2386.38, 2389.54,
   (0,49): 2332.13, 2335.98, 2342.83, 2348.5, 2355.54, 2361.34, 2367.24,
   (0,56): 2372, 2376.16, 2379.89, 2383.43, 2386.97, 2389.3, 2335.16,
   (0,63): 2340.67, 2347.2, 2353.6, 2359.02, 2365.29, 2370.71, 2374.95,
   (0,70): 2379.13, 2382.09, 2384.6, 2387.45, 2390.35, 2337.09, 2343.53,
   (0,77): 2351.36, 2358.82, 2365.8, 2370.92, 2376.1, 2380.15, 2382.61,
   (0,84): 2384.57, 2387.74, 2390.02, 2338.29, 2345.49, 2352.82, 2360.72,
   (0,91): 2368.06, 2372.85, 2377.69, 2380.28, 2382.16, 2383.84, 2386.39
   }
   ATTRIBUTE "MATLAB_class" {
      DATATYPE  H5T_STRING {
         STRSIZE 6;
         STRPAD H5T_STR_NULLTERM;
         CSET H5T_CSET_ASCII;
         CTYPE H5T_C_S1;
      }
      DATASPACE  SCALAR
      DATA {
      (0): "double"
      }
   }
}
}
```

***

A faire en amont pour reorganizer les outputs de campagnes cigri : 

```
CID="14602"
cd /bettik/$USER/$CID
HERE=$(pwd)
for f in $(ls -1d oar* | awk -F. '{print $5 "." $6 "." $7 "." $8}' | sort -u) ; do 
  echo $f
  mkdir -p ../${CID}_RECOMBINE/$f
  for startdir in $(ls -1d oar* | awk -F. '{print $9 "_" $10}' | sort -u) ; do 
    echo $startdir
    sd=${startdir/_/.}
    mkdir -p ../${CID}_RECOMBINE/$f/$startdir
    cd ../${CID}_RECOMBINE/$f/$startdir
    ln -sf $HERE/oar*${f}.${sd}.*/beam*h5 .
    cd $HERE
  done
done
```





