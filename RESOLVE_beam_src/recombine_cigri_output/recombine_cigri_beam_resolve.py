#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
#    This file is part of MFP   : a Python code for recombining MFP outputs   #
#    from a CiGri jobs grid campaign (embarrassingly parallel jobs)           #
#                                                                             #
#    Copyright (C) 2018 Albanne Lecointre                                     #
#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
###############################################################################


import h5py
import sys
import numpy as np
import argparse

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="recombine outputs of resolve beamforming CiGri campaign")
    parser.add_argument("--o", default="./out.h5", type=str,
                        help="name of output file (default ./out.h5)")
    parser.add_argument("--n", default=86399, type=int,
                        help="number of timesteps (default 86399)")
    parser.add_argument("--j1", default=114, type=int, help="first jday (default 114)")
    parser.add_argument("--j2", default=153, type=int, help="last jday (default 153)")
    parser.add_argument("--f1", default=5.0, type=float, help="first frequency (default 5.0)")
    parser.add_argument("--f2", default=31.0, type=float, help="last frequency (default 31.0)")
    parser.add_argument("--c", default="null", type=str, help="cigri campaign id")

    args = parser.parse_args()
    beam_outfile = args.o
    nstep = args.n
    j1 = args.j1
    j2 = args.j2
    fmin = args.f1
    fmax = args.f2
    cid = args.c

    fstep = 2.0 # 1.0 # 0.5
    bw2 = 2.0 # half-bandwidth (5Hz central frequency is between 3Hz and 7Hz)

    # dirinput = "/data/projects/resolve/Argentiere/ALBANNE/BEAM_V0/EXP000/XYZC_ASA047_BOUNDZ5/"
    # dirinput = "/data/projects/resolve/Argentiere/ALBANNE/BEAM/EXP000/XYZC_ASA047_BOUNDZ5/"
    # dirinput = "/bettik/lecointre/14453_RECOMBINE/"
    #dirinput = "/bettik/lecointre/14602_RECOMBINE/"
    #dirinput = "/bettik/lecointre/"+cid+"_RECOMBINE/"
    dirinput = "/bettik/lecoinal/"+cid+"_RECOMBINE/"

    # startpoint = np.array([[0,  100, -180, 180,  20, -240,  50,   50, -80, -90, -90], 
    #                        [0, -120,  180, -40, -20,   70, 110, -200, 230,  90, -90]])
#    startpoint = np.array([[ 220,      100,      -180,       180,       20,   -240,       50,       50,    -80,    -90, -90], 
#                           [-170,     -120,       180,       -40,      -20,     70,      110,     -200,    230,     90, -90],
#                           [2363.732, 2363.732, 2338.8, 2363.732, 2363.732, 2340.3, 2363.732, 2363.732, 2344.4, 2354.3, 2363.732]])

#    startpoint = np.array([[ 220, 100, -180, 180, 20, -240, 50, 50, -80, -90, -90, -30, 90, 200, 330, -380, -270, -150, -10, -100, -420, 100, 390, -340, -260, -170, 190, 270, 350],
#                           [ -170, -120, 180, -40, -20, 70, 110, -200, 230, 90, -90, 320, 230, 150, 50, -10, -90, -180, -290, 350, 60, -330, -40, 150, 240, 320, -290, -220, -140],
#                           [ 2113.73 , 2113.73 , 2088.80 , 2113.73 , 2113.73 , 2090.3 , 2113.73 , 2113.73 , 2094.40 , 2104.30 , 2113.73 , 2090.1 , 2113.73 , 2113.73 , 2113.73 , 2078.70 , 2094.30 , 2108.1 , 2113.73 , 2080.00 , 2069.90 , 2113.73 , 2113.73 , 2073.3 , 2074.70 , 2076.20 , 2113.73 , 2113.73 , 2113.73 ]])

    startpoint = np.array([[ -200.00, -100.00, 0.00, 100.00, 200.00, -200.00, -100.00, 0.00, 100.00, 200.00, -200.00, -100.00, 0.00, 100.00, 200.00, -200.00, -100.00, 0.00, 100.00, 200.00, -200.00, -100.00, 0.00, 100.00, 200.00, 280.00, 0.00, 0.00],
                           [ -100.00, -100.00, -100.00, -100.00, -100.00, -50.00, -50.00, -50.00, -50.00, -50.00, 0.00, 0.00, 0.00, 0.00, 0.00, 50.00, 50.00, 50.00, 50.00, 50.00, 100.00, 100.00, 100.00, 100.00, 100.00, 0.00, -150.00, 150.00],
                           [ 3178.590, 3204.822, 3210.321, 3229.502, 3295.452, 3178.590, 3189.510, 3210.321, 3229.696, 3256.841, 3172.409, 3187.250, 3208.575, 3232.487, 3267.127, 3162.625, 3179.211, 3209.389, 3240.303, 3282.343, 3161.270, 3177.473, 3211.854, 3240.303, 3282.343, 3267.127, 3206.314, 3201.777]])

    # update = 1  # out.h5 should already exists and only startpoint will be updated
    update = 0  # create a new out.h5 file (fail if exists)

    # only if update = 1 : startpoint indices (0-based) that should be updated:
    upd_sp = np.array([2, 5, 8, 9])

################ do not change under this line ###############

    nstart = np.size(startpoint,axis=1)

    nupdstart = len(upd_sp)

    freq_centrale = np.arange(fmin, fmax+fstep, fstep) 

    jd = np.arange(j1,j2+1)
    njd = len(jd)

    if update == 1:
        fout = h5py.File(beam_outfile,'r+')
    elif update == 0:
        fout = h5py.File(beam_outfile,'w-')
    else:
        sys.exit("Error: Invalid value encoutered in update: "+str(update))

    if update == 0: 
        dset = fout.create_dataset("JD", (njd,), dtype='i2', chunks=True, compression="gzip", data=jd[:])
        dset = fout.create_dataset("freq_centrale", (len(freq_centrale),), dtype='f4', chunks=True, compression="gzip", data=freq_centrale[:])
        dset = fout.create_dataset("startpoint", (3,nstart), dtype='f4', chunks=True, compression="gzip", data=startpoint[:,:])
        # write a dataset for positions ?
        # write a dataset for x ?
        # write a dataset for y ?
        # write a dataset for z ?
    else:
        # check that freq_centrale and JD and indices are OK
        if not np.array_equal(freq_centrale,fout['/freq_centrale'][:]):
            sys.exit("Error: freq_centrale differs"+freq_centrale+fout['/freq_centrale'][:])
        if not np.array_equal(jd,fout['/JD'][:]):
            sys.exit("Error: jd differs"+jd+fout['/JD'][:])
        if not np.array_equal(startpoint,fout['/startpoint'][:,:]):
            sys.exit("Error: startpoint differs"+startpoint+fout['/startpoint'][:,:])
        print("startpoint that will be updated are:")
        for isp in np.arange(nupdstart):
            print(startpoint[:,upd_sp[isp]])

    if update == 0:
        dset = fout.create_dataset("data_final_all", (len(freq_centrale), njd, 5, nstart, nstep), dtype='f4', chunks=True, compression="gzip")
        dsetinfo = fout.create_dataset("neval", (len(freq_centrale), njd, nstart, nstep), dtype='i2', chunks=True, compression="gzip")  # 2-byte integer : OK -> 32,767
        for ifreq in np.arange(len(freq_centrale)):
            #freqdir = '{:.1f}'.format(freq_centrale[ifreq]-bw2)+'.{:.1f}'.format(freq_centrale[ifreq]+bw2)
            freqdir = '{:.1f}'.format(freq_centrale[ifreq]-bw2)+'_{:.1f}'.format(freq_centrale[ifreq]+bw2)
            for istart in np.arange(nstart):
                #startdir = str(int(startpoint[0,istart]))+"_"+str(int(startpoint[1,istart]))
                startdir = '{:.2f}'.format(startpoint[0,istart])+"_"+'{:.2f}'.format(startpoint[1,istart])+"_"+'{:.3f}'.format(startpoint[2,istart])
                for ijd in np.arange(njd):
                    print(freqdir,startdir,jd[ijd])
                    #filename = dirinput+"/"+freqdir+"/"+startdir+"/beam_ZO_2018_"+str(jd[ijd])+".h5"
                    filename = dirinput+"/"+freqdir+"/"+startdir+"/beam_"+str(jd[ijd])+".h5"
                    f = h5py.File(filename,'r')
                    dset[ifreq,ijd,0,istart,:] = f['/xyzc'][0,:] # x
                    dset[ifreq,ijd,1,istart,:] = f['/xyzc'][1,:] # y
                    dset[ifreq,ijd,2,istart,:] = f['/xyzc'][2,:] # z
                    dset[ifreq,ijd,3,istart,:] = f['/xyzc'][3,:] # c
                    dset[ifreq,ijd,4,istart,:] = f['/Baval'][:]
                    dsetinfo[ifreq,ijd,istart,:] = f['/infooptim'][0,:] # neval
                    if ( ifreq == 0 ) and ( istart == 0 ):
                        nbsta = np.shape(f['/patchesInd'][:,:])[1]
                        print("nbsta:",nbsta)
                        dsetgap = fout.create_dataset("/"+str(ijd)+"/availdata", (nstep, nbsta), dtype='i1', chunks=True, compression="gzip")  # 1-byte integer as we only have 0/1
                        dsetgap[:,:] = f['/patchesInd'][:,:] # if 0-data was used
                    f.close()

    else:
        for ifreq in np.arange(len(freq_centrale)):
            freqdir = '{:.1f}'.format(freq_centrale[ifreq]-bw2)+'.{:.1f}'.format(freq_centrale[ifreq]+bw2)
            for istart in np.arange(nupdstart):
                startdir = str(int(startpoint[0,upd_sp[istart]]))+"_"+str(int(startpoint[1,upd_sp[istart]]))
                for ijd in np.arange(njd):
                    print(freqdir,startdir,jd[ijd])
                    filename = dirinput+"/"+freqdir+"/"+startdir+"/beam_ZO_2018_"+str(jd[ijd])+".h5"
                    f = h5py.File(filename,'r')
                    fout['/data_final_all'][ifreq,ijd,0,upd_sp[istart],:] = f['/xyzc'][0,:] # x
                    fout['/data_final_all'][ifreq,ijd,1,upd_sp[istart],:] = f['/xyzc'][1,:] # y
                    fout['/data_final_all'][ifreq,ijd,2,upd_sp[istart],:] = f['/xyzc'][2,:] # z
                    fout['/data_final_all'][ifreq,ijd,3,upd_sp[istart],:] = f['/xyzc'][3,:] # c
                    fout['/data_final_all'][ifreq,ijd,4,upd_sp[istart],:] = f['/Baval'][:]
                    fout['/neval'][ifreq,ijd,upd_sp[istart],:] = f['/infooptim'][0,:] # neval
                    f.close()

    fout.close()


#lecoinal@ist-oar:~$ h5ls -rv /data/projects/resolve/Argentiere/PHILIPPE/processed/RESOLVE-ARGENTIERE.mat
#/positions               Dataset {3/3, 98/98}
#    Attribute: MATLAB_class scalar
#        Type:      6-byte null-terminated ASCII string
#        Data:  "double"
#    Location:  1:16321880624
#    Links:     1
#    Storage:   2352 logical bytes, 2352 allocated bytes, 100.00% utilization
#    Type:      native double
#/x                       Dataset {1/1, 98/98}
#    Attribute: MATLAB_class scalar
#        Type:      6-byte null-terminated ASCII string
#        Data:  "double"
#    Location:  1:16321883872
#    Links:     1
#    Storage:   784 logical bytes, 784 allocated bytes, 100.00% utilization
#    Type:      native double
#/y                       Dataset {1/1, 98/98}
#    Attribute: MATLAB_class scalar
#        Type:      6-byte null-terminated ASCII string
#        Data:  "double"
#    Location:  1:16321884928
#    Links:     1
#    Storage:   784 logical bytes, 784 allocated bytes, 100.00% utilization
#    Type:      native double
#/z                       Dataset {1/1, 98/98}
#    Attribute: MATLAB_class scalar
#        Type:      6-byte null-terminated ASCII string
#        Data:  "double"
#    Location:  1:16321885984
#    Links:     1
#    Storage:   784 logical bytes, 784 allocated bytes, 100.00% utilization
#    Type:      native double
#
