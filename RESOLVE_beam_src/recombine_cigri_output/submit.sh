#!/bin/bash
#OAR --project resolve
##OAR --project iste-equ-ondes
#OAR -n recombine_cigri_beam_resolve
#OAR -l /nodes=1/core=1,walltime=10:30:00
##OAR -t besteffort
##OAR -t devel
##OAR -p network_address='luke4'
##OAR -O OAR.%jobname%.%jobid%.stdouterr
##OAR -E OAR.%jobname%.%jobid%.stdouterr

# oarsub -S "./submit.sh"

set -e

date

START=$(date +%s.%N)

cat $OAR_NODE_FILE

# change here to switch between ISTerre cluster or one of the CIMENT-grid clusters
#source /soft/env.bash 
#source /applis/ciment/v2/env.bash
source /applis/site/guix-start.sh
refresh_guix resolve_gnuhdf5

# suppose that we have added these 2 packages :
# guix install -p $GUIX_USER_PROFILE_DIR/resolve_gnuhdf5 python python-h5py

#case "$CLUSTER_NAME" in
#  ISTERRE)
#    module load python/python3.5 
#    ;;
#  *)  # gofree|froggy|luke|dahu|bigfoot|ceciccluster)
#    module load python/2.7.12_gcc-4.6.2
#    ;;
#esac

TMPDIR=$SHARED_SCRATCH_DIR/$USER/oar.$OAR_JOB_ID
#TMPDIR=/scratch_md1200/$USER/oar.$OAR_JOB_ID
echo $TMPDIR
mkdir -p $TMPDIR
PEXE="recombine_cigri_beam_resolve.py"
cp $PEXE $TMPDIR/.
#cp /bettik/lecointre/14453_RECOMBINE/out_13874.h5 $TMPDIR/out.h5  # only if update=1
#rsync -arv --progress /scratch_md1200/lecointre/out_13874.h5 $TMPDIR/out.h5  # only if update=1
#mv /scratch_md1200/lecointre/oar.17472835/out.h5 $TMPDIR/out.h5  # only if update=1

cd $TMPDIR

# recombine only days 117:118 and central_frequencies 6:0:5:6.6, with considering 86399 timestep/day
# python3.5 -u $PEXE --j1 117 --j2 118 --f1 6 --f2 6.5

# recombine all existing days and frequencies, with considering 86399 timestep/day (default options)
# python3.5 -u $PEXE

# recombine all existing days and frequencies, with considering 172799 timestep/day
# python2.7 -u $PEXE --n 172799

# recombine only days 117:118 and central_frequencies 6:0:5:6.6, with considering 172799 timestep/day
#python2.7 -u $PEXE --j1 114 --j2 147 --f1 5 --f2 20 --n 172799 --c 15423
python3 -u $PEXE --j1 123 --j2 142 --f1 5 --f2 69 --n 86400 --c 22269

date
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"

