
# Dépendances

 * hdf5
 * gfortran
 * zlib
 * asa047 (LGPL)  : pas dispo sous guix, actuellement compilé à la main dans le HOME uilisateur.
 * nlopt   # uniquement si nn_nlpot = 1 dans la namelist namoptim (`nn_nlopt = 0 ! nlopt [1] or asa047 [0] library for optimisation`)
 * lapack + blas   # uniquement si ln_csdm = 1 dans la namelist namparam (calcul CSDM explicite), n'est plus utilisé en pratique.

```
!    ln_csdm         ::   1 : build CSDM and compute as conj(replica(:))*CSDM(:,:)*replica(:)
!                         0 : z*conj(z) with z = sum (exp (j*(phi_data-phi_model(:))))`)
```

# Installation sur dahu|luke (GUIX & Gnu)

On utilise ce fichier manifest [manifest_mfp.scm](./manifest_mfp.scm) pour construire un nouveau profil resolve_gnuhdf5 :

```
> source /applis/site/guix-start.sh

> refresh_guix mfp

> guix package -m manifest_mfp.scm -p $GUIX_USER_PROFILE_DIR/mfp

# Activation et vérification de l'environnement
> refresh_guix mfp
Activate profile  /var/guix/profiles/per-user/lecoinal/mfp
The following packages are currently installed in /var/guix/profiles/per-user/lecoinal/mfp:
zlib              	1.2.13	out    	/gnu/store/slzq3zqwj75lbrg4ly51hfhbv2vhryv5-zlib-1.2.13
gfortran-toolchain	11.3.0	out    	/gnu/store/l9l5860iqcys3nshp8cpb4kxl7ngydlj-gfortran-toolchain-11.3.0
gcc-toolchain     	12.3.0	out    	/gnu/store/spxcaq8gnmckhzz9a1wm3qc9dmz5bvsd-gcc-toolchain-12.3.0
openblas          	0.3.20	out    	/gnu/store/z9aavic2jyvp6jyfxnl9ka3i8vfk5phc-openblas-0.3.20
lapack            	3.9.0 	out    	/gnu/store/pgkqf7k640jcfsbr8x1dw3k60pfc8c93-lapack-3.9.0
nlopt             	2.7.1 	out    	/gnu/store/g9f7paj21iwxqihrz6cwdysa601lsjqv-nlopt-2.7.1
hdf5              	1.14.0	fortran	/gnu/store/hipq9ni55fwazl4y1m0vmfwsh2f4imrm-hdf5-1.14.0-fortran
hdf5              	1.14.0	out    	/gnu/store/zxf7wvby40px4b19cscm552g469h9a84-hdf5-1.14.0

```

Et la lib ASAO47 est compilée ici :
```
/bettik/lecoinal/lib/gnu-devel-4.6.2/libbrentasa047.a
```

# Compilation du code

```
> which h5fc
/var/guix/profiles/per-user/lecoinal/mfp/bin/h5fc
> make -f Makefile_guix clean 
rm -f glovarmodule.o modcomputebeam_optim.o modcomputebeam.o beamforming_simulated_annealing beamforming beamforming_optim *.mod *.o
> make -f Makefile_guix beamforming_optim
h5fc -I/var/guix/profiles/per-user/lecoinal/mfp/include -shlib  -O2 -cpp   -c -o glovarmodule.o glovarmodule.f90 
h5fc -I/var/guix/profiles/per-user/lecoinal/mfp/include -shlib  -O2 -cpp   -c -o modcomputebeam.o glovarmodule.o modcomputebeam.f90 
gfortran: warning: glovarmodule.o: linker input file unused because linking not done
h5fc -I/var/guix/profiles/per-user/lecoinal/mfp/include -shlib  -O2 -cpp   -c -o modcomputebeam_optim.o glovarmodule.o modcomputebeam_optim.f90 
gfortran: warning: glovarmodule.o: linker input file unused because linking not done
#h5fc -I/var/guix/profiles/per-user/lecoinal/mfp/include -shlib  -O2 -cpp   -o beamforming_optim glovarmodule.o modcomputebeam.o modcomputebeam_optim.o beamforming_optim.f90 -llapack -lblas   -L/home/lecoinal/lib/gnu-devel-4.6.2 -lbrentasa047 -lnlopt
h5fc -I/var/guix/profiles/per-user/lecoinal/mfp/include -shlib  -O2 -cpp   -o beamforming_optim glovarmodule.o modcomputebeam.o modcomputebeam_optim.o beamforming_optim.f90 -llapack -lblas   -L/bettik/lecoinal/lib/gnu-devel-4.6.2 -lbrentasa047 -lnlopt
>
```

Cela crée le fichier binaire `beamforming_optim`

```
> ./beamforming_optim
 The namelist does not exist
>
```

Il faut maintenant préparer la configuration de l'expérience (namelist, fichier métrique, préparation des inputs...)
