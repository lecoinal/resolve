!###############################################################################
!#    This file is part of MFP : a Fortran code for computing MFP              #
!#    (Matched Field Processing), also named bartlett or beamforming,          #
!#    over a regular grid on (x,y,z,c) or (theta,c).                           #
!#                                                                             #
!#    Copyright (C) 2017 Albanne Lecointre                                     #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################

!  =======================================================================================
!                       ***  Fortran program beamforming.f90  ***
!   This is the main F90 program to compute beamforming over a research grid
!    
!  =======================================================================================
!   History :  1.0  : 03/2017  : A. Lecointre : Initial version
!	        
!	grid search (c,theta) OR (x,y,z,c)	    
!		 
!  ---------------------------------------------------------------------------------------
!
!  ---------------------------------------------------------------------------------------
!   routines      : description
!                 : 
!                 : 
!                 : 
!                 :
!  ---------------------------------------------------------------------------------------
!  $Id: beamforming.f90 145 2018-07-19 12:49:21Z lecointre $
!  ---------------------------------------------------------------------------------------

PROGRAM BEAMFORMING
  USE HDF5
  USE H5LT      ! Lite Interface HDF5
  USE glovarmodule , only : CSDM, CSDMP,phi_data,phi_model, nn_removezero,nkzero,ln_coh, ref_inode_coh,Nr,xr,yr,zr,Nf,freq
  USE modcomputebeam
  IMPLICIT NONE

  INTEGER, PARAMETER :: n_profile_it = 500
  INTEGER :: it,nit,Nrit
  CHARACTER(len=256) :: cit

  INTEGER :: kk,kki ! Loop integer for stations
  INTEGER :: ios ! dummy loop char index
  INTEGER :: iwcl ! ,Nr ! The number of line in a file
  CHARACTER(len=256), DIMENSION(:), ALLOCATABLE :: staname ! the stations name
  !REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: xr,yr,zr,lat,lon ! the stations coordinates
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: lat,lon ! the stations coordinates
  INTEGER :: nivBruit,normalisation,azimuth,Fs
  INTEGER :: method,ln_csdm
  REAL(KIND=4), DIMENSION(:,:), ALLOCATABLE :: rdata,rdata_large
  COMPLEX(KIND=8), DIMENSION(:,:), ALLOCATABLE :: KKff
  LOGICAL :: l_exist_namelist
  REAL(KIND=8) :: tmin,tmax,tstep,xmin,xmax,xstep,ymin,ymax,ystep,zmin,zmax,zstep,cmin,cmax,cstep,fmin,fmax,fstep
  INTEGER(HID_T)     :: infile_id,outfile_id
  INTEGER(HSIZE_T), DIMENSION(1) :: startin1
  INTEGER(HSIZE_T), DIMENSION(1) :: countin1
  INTEGER(HSIZE_T), DIMENSION(1) :: dims1
  INTEGER(HSIZE_T), DIMENSION(2) :: dims2
  INTEGER(HSIZE_T), DIMENSION(3) :: dims3
  INTEGER(HSIZE_T), DIMENSION(4) :: dims4
  INTEGER(HSIZE_T), DIMENSION(5) :: dims5
  INTEGER                        :: error
  INTEGER :: ff,i ! ,nn_npt
  INTEGER :: jbeg,jlen,joverlap,njlen,offset
  CHARACTER(len=256) :: cn_ifile
  INTEGER :: Nt,Nc,Nx,Ny,Nz
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: theta,cel,xgrid,ygrid,zgrid

  REAL(KIND=8), DIMENSION(:,:,:), ALLOCATABLE :: beamf ! (Nt,Nc,Nf)
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: beam,beamstd ! (Nt,Nc)
  REAL(KIND=8), DIMENSION(:,:,:,:,:), ALLOCATABLE :: xybeamf ! (Nx,Ny,Nz,Nc,Nf)
  REAL(KIND=8), DIMENSION(:,:,:,:), ALLOCATABLE :: xybeam,xybeamstd ! (Nx,Ny,Nz,Nc)

  INTEGER :: nn_latlon,nn_io

  INTEGER(KIND=4), DIMENSION(:,:), ALLOCATABLE :: patchesInd ! logical for stations = zero or not

  ! Instrumentation du code pour analyse de performance
  INTEGER,DIMENSION(8) :: values,values0
  REAL(KIND=4) :: elps,sval0,svali           ! in sec
  CHARACTER(len=10), DIMENSION(3) :: b

 ! Declare NAMELIST
  NAMELIST /namparam/ method,ln_csdm,nivBruit,normalisation,azimuth,Fs,cn_ifile,jbeg,jlen,joverlap,nit
  NAMELIST /namcoh/ ln_coh
  NAMELIST /namio/ nn_io
  NAMELIST /namtheta/ tmin,tmax,tstep
  NAMELIST /namxy/ xmin,xmax,xstep,ymin,ymax,ystep
  NAMELIST /namHsource/ zmin,zmax,zstep
  NAMELIST /namspeed/ cmin,cmax,cstep
  NAMELIST /namindice_freq/ fmin,fmax,fstep
  NAMELIST /namcoord/ nn_latlon
  NAMELIST /naminput/ nn_removezero

  CALL date_and_time(VALUES=values)
  svali=values(5)*3600+values(6)*60+values(7)+0.001*values(8)

  ! Read namelist
  INQUIRE(file='namelist', exist=l_exist_namelist)
  IF (.NOT. l_exist_namelist) THEN
     PRINT *,'The namelist does not exist'
     STOP
  ENDIF
  OPEN(11,file='namelist')
    REWIND 11
    READ(11,namparam)
    REWIND 11
    READ(11,namcoh)
    REWIND 11
    READ(11,namio)
    REWIND 11
    READ(11,namtheta)
    REWIND 11
    READ(11,namxy)
    REWIND 11
    READ(11,namHsource)
    REWIND 11
    READ(11,namspeed)
    REWIND 11
    READ(11,namindice_freq)
    READ(11,namcoord)
    REWIND 11
    READ(11,naminput)
  CLOSE(11)

  ! Check argument namelist
  IF ( ln_csdm == 1 ) then
    print *,'ln_csdm == 1 (compute CSDM explicitely) not available'
    print *, 'use ln_csdm = 0 instead'
    CALL exit(1)
  ELSE
    IF ( nivBruit /= 0 ) THEN
      print *, 'nivBruit /= 0 (Add noise on CSDM diagonal) not available in case of ln_csdm = 0'
      print *, 'use nivBruit = 0 or ln_csdm = 1'
      CALL exit(1)
    ENDIF
    IF ( normalisation /= 1 ) THEN
      print *, 'normalisation /= 1 (whitening) not available in case of ln_csdm = 0'
      print *, 'All the CSDM terms must have modulus = 1 (bartlett is computed as sum of Nsta complex exponential)'
      print *, 'use normalisation = 1 or ln_csdm = 1'
      CALL exit(1)
    ENDIF
  ENDIF

! Initialise frequency vector
  IF ( fstep < 0 ) THEN
    Nf=INT(-fstep)
    ALLOCATE(freq(Nf))
    OPEN(10, file="freq.txt")
    DO ff=1,Nf
      READ(UNIT=10,FMT=*) freq(ff)
    ENDDO
    CLOSE(10)
  ELSE
    Nf=(fmax-fmin)/fstep+1
    ALLOCATE(freq(Nf))
    freq = (/(fmin + ((i-1)*fstep),i=1,Nf)/) ! assign to y in increments of fstep starting at fmin
  ENDIF
  print *,'freq',freq

print *,'Read the station list and their geographical location from zreseau.txt'
! Read the station list and their geographical location from zreseau.txt input file
  iwcl=0
  OPEN(10, file="zreseau.txt", form="formatted", iostat=ios,status="old")
  DO WHILE (ios==0)
    READ(10,*,iostat=ios)
    IF (ios==0) iwcl=iwcl+1
  ENDDO
  Nr=iwcl
  CLOSE(10)
  IF ( nn_latlon == 1 ) THEN
    ALLOCATE(staname(Nr),lat(Nr),lon(Nr),xr(Nr),yr(Nr),zr(Nr))
    OPEN(10, file="zreseau.txt")
    DO kk=1,Nr
      READ(UNIT=10,FMT=*) staname(kk),lon(kk),lat(kk),zr(kk)
      CALL distlatlong(lat(1), lon(1), lat(kk), lon(kk), xr(kk), yr(kk))
    ENDDO
    CLOSE(10)
    xr(:)=xr(:)-SUM(xr(:))/Nr
    yr(:)=yr(:)-SUM(yr(:))/Nr
  ELSE
    ALLOCATE(staname(Nr),xr(Nr),yr(Nr),zr(Nr))
    OPEN(10, file="zreseau.txt")
    DO kk=1,Nr
      READ(UNIT=10,FMT=*) staname(kk),xr(kk),yr(kk),zr(kk)
    ENDDO
    CLOSE(10)
  ENDIF

! Read input data using read_one_trace subroutine
  CALL h5open_f(error)
  if (error/=0) call exit(error)
  ! Open input data file
  CALL h5fopen_f(TRIM(cn_ifile), H5F_ACC_RDONLY_F, infile_id, error)
  if (error/=0) call exit(error)

  IF ( MOD(nit,nn_io) /= 0 ) THEN
    print *,'Warning, nit:',nit,' nn_io:',nn_io
    nit = nit - MOD(nit,nn_io)
    print *,'Set nit to a new value:',nit 
  ENDIF

  IF ( nn_io == nit ) THEN
  ! Read input data using read_one_trace subroutine
    njlen=joverlap*(nit-1)+jlen
    ALLOCATE(rdata_large(njlen,Nr))  ! data is only used to compute the Cross Spectral Density Matrix ( K[Nr,Nr,Nf] )
    countin1=(/njlen/)
    startin1=(/jbeg/)
    print *,'Read input data: offset,count: ',startin1,countin1
    DO kk=1,Nr
      IF (MOD(kk,500)==0)  print *,'lecture',kk,TRIM(staname(kk)),'/',Nr
      CALL read_one_trace(infile_id,"/"//TRIM(staname(kk)),1,startin1,countin1,njlen,rdata_large(:,kk))
    ENDDO
  ELSE
    njlen=joverlap*(nn_io-1)+jlen
    ALLOCATE(rdata_large(njlen,Nr))
    countin1=(/njlen/)
    startin1=(/jbeg/)
  ENDIF

!  ALLOCATE(rdata(jlen,Nr))  ! data is only used to compute the Cross Spectral Density Matrix ( K[Nr,Nr,Nf] )
!  countin1=(/jlen/)
!  startin1=(/jbeg/)
!  DO kk=1,Nr
!! NB : ici on lit en double alors que les donnees sont en SP dans le input h5 file
!    print *,'lecture',kk,TRIM(staname(kk)),'/',Nr
!    CALL read_one_trace(infile_id,"/"//TRIM(staname(kk)),1,startin1,countin1,jlen,rdata(:,kk))
!  ENDDO

  ALLOCATE(nkzero(Nr),patchesInd(Nr,nit))

  IF ( nn_removezero == 0 ) ALLOCATE(rdata(jlen,Nr))  ! data is only used to compute the Cross Spectral Density Matrix ( K[Nr,Nr,Nf] )

  ! compute ref_inode_coh : look for the closest station from starting point
  ! ref_inode_coh is the 1-based index for reference sensor
  IF ( ln_coh == 1 ) THEN
    IF ( nn_removezero == 1 ) THEN
      print *, 'nn_removezero and ln_coh are not compatible'
      CALL exit(1)
    ENDIF
    print *,'compute ref_inode_coh as the node closest to the center of the array'
    !CALL compute_ref_inode_coh(xstep,ystep,zstep,ref_inode_coh)
    !CALL compute_ref_inode_coh(xstep,ystep,ref_inode_coh)
    CALL compute_ref_inode_coh(0.d0,0.d0,ref_inode_coh)   ! ici on suppose que le centre de zero est en 0,0 ... a adapter selon latlon...
    print *,'1-based index ref_inode_coh',ref_inode_coh,staname(ref_inode_coh)
  ENDIF

  CALL date_and_time(VALUES=values)
  sval0=values(5)*3600+values(6)*60+values(7)+0.001*values(8)
  elps=sval0-svali
  print *,'read inputs and init:',elps

  ! Create output file
  CALL h5fcreate_f("./out.h5", H5F_ACC_TRUNC_F, outfile_id, error)
  if (error/=0) call exit(error)

  Nc=(cmax-cmin)/cstep+1
  ALLOCATE(cel(Nc))
  cel = (/(cmin + ((i-1)*cstep),i=1,Nc)/)

  dims1=(/Nr/)
  CALL h5ltmake_dataset_float_f(outfile_id,"Xr",1,dims1,REAL(xr(:)),error)
  if (error/=0) call exit(error)
  CALL h5ltmake_dataset_float_f(outfile_id,"Yr",1,dims1,REAL(yr(:)),error)
  if (error/=0) call exit(error)
  IF ( nn_latlon == 1 ) THEN
    CALL h5ltmake_dataset_float_f(outfile_id,"lon",1,dims1,REAL(lon(:)),error)
    if (error/=0) call exit(error)
    CALL h5ltmake_dataset_float_f(outfile_id,"lat",1,dims1,REAL(lat(:)),error)
    if (error/=0) call exit(error)
  ENDIF
  dims1=(/Nc/)
  CALL h5ltmake_dataset_float_f(outfile_id,"speed",1,dims1,REAL(cel(:)),error)
  if (error/=0) call exit(error)
  dims1=(/Nf/)
  CALL h5ltmake_dataset_float_f(outfile_id,"freq",1,dims1,REAL(freq(:)),error)
  if (error/=0) call exit(error)


  IF ( azimuth == 1 ) THEN   ! c,theta
    Nt=(tmax-tmin)/tstep+1
    ALLOCATE(theta(Nt))
    theta = (/(tmin + ((i-1)*tstep),i=1,Nt)/) * 4.d0 * DATAN(1.D0) / 180.d0
    dims1=(/Nt/)
    CALL h5ltmake_dataset_float_f(outfile_id,"theta",1,dims1,REAL(theta(:)),error)
    if (error/=0) call exit(error)
  ELSE                       ! x,y
    Nx=(xmax-xmin)/xstep+1
    ALLOCATE(xgrid(Nx))
    xgrid = (/(xmin + ((i-1)*xstep),i=1,Nx)/)
    dims1=(/Nx/)
    CALL h5ltmake_dataset_float_f(outfile_id,"xgrid",1,dims1,REAL(xgrid(:)),error)
    if (error/=0) call exit(error)
    Ny=(ymax-ymin)/ystep+1
    ALLOCATE(ygrid(Ny))
    ygrid = (/(ymin + ((i-1)*ystep),i=1,Ny)/)
    dims1=(/Ny/)
    CALL h5ltmake_dataset_float_f(outfile_id,"ygrid",1,dims1,REAL(ygrid(:)),error)
    if (error/=0) call exit(error)
    Nz=(zmax-zmin)/zstep+1
    ALLOCATE(zgrid(Nz))
    zgrid = (/(zmin + ((i-1)*zstep),i=1,Nz)/)
    dims1=(/Nz/)
    CALL h5ltmake_dataset_float_f(outfile_id,"zgrid",1,dims1,REAL(zgrid(:)),error)
    if (error/=0) call exit(error)
    dims1=(/Nr/)
    CALL h5ltmake_dataset_float_f(outfile_id,"Zr",1,dims1,REAL(zr(:)),error)
    if (error/=0) call exit(error)

    ALLOCATE(xybeam(Nx,Ny,Nz,Nc)) 

  ENDIF


  DO it=1,nit

    write(cit,'(I6.6)') it  ! limit 999999 sur nit max ...

 
!    print *,'====> it: ',it
 
    IF ( nn_io == nit ) THEN ! Full input dataset already read before iterate

      ! compute offset to access rdata_large in memory
      offset = joverlap*(it-1)+1

    ELSE ! must read input every nn_io iterations

      IF ( MOD(it-1,nn_io) == 0 ) THEN ! read nn_io input steps
        print *,'Read input data: offset,count: ',startin1,countin1
        CALL date_and_time(b(1), b(2), b(3), values0)
        DO kk=1,Nr
        IF (MOD(kk,500)==0)  print *,'lecture',kk,TRIM(staname(kk)),'/',Nr
          CALL read_one_trace(infile_id,"/"//TRIM(staname(kk)),1,startin1,countin1,njlen,rdata_large(:,kk))
        ENDDO
        CALL date_and_time(VALUES=values)
        elps = values(5)*3600+values(6)*60+values(7)+0.001*values(8) - ( values0(5)*3600+values0(6)*60+values0(7)+0.001*values0(8) )
        print *,'Read_input ',b(1), b(2), elps
        ! increment startin1
        startin1 = startin1 + ( nn_io * joverlap )
      ENDIF

      ! compute offset to access rdata_large in memory
      offset = joverlap*MOD(it-1,nn_io)+1 ! joverlap*(it-1)+1

    ENDIF

    ! access rdata_large in memory, compute patchsInd,nkzero,Nrit
    nkzero(:)=1
!    print *,'dataset jbeg jend :',offset,offset+jlen-1
    DO kk=1,Nr
      IF ( MAXVAL(ABS(rdata_large(offset:offset+jlen-1,kk))) == 0 ) nkzero(kk) = 0 ! mask for zero data
    ENDDO
    patchesInd(:,it)=nkzero(:)
    Nrit=SUM(nkzero)
    IF ( nn_removezero == 1 ) THEN
!      print *,'Remove zero data and use only ',Nrit,' dataset / ',Nr
      ALLOCATE(rdata(jlen,Nrit))
      kki=0
      DO kk=1,Nr
        IF ( nkzero(kk) == 1 ) THEN
          kki=kki+1
          rdata(:,kki) = rdata_large(offset:offset+jlen-1,kk)
        ENDIF
      ENDDO 
    ELSE
!      print *,'Nrit/Nr: ',Nrit,Nr,' but keep zero dataset'
      DO kk=1,Nr
        rdata(1:jlen,kk)=rdata_large(offset:offset+jlen-1,kk)
!print *,kk,rdata(1:2,kk),rdata(jlen-1:jlen,kk)
      ENDDO
      Nrit=Nr
    ENDIF

    ! Compute the Cross-Spectral Density Matrix K[Nr,Nr,Nf]
    IF ( ln_csdm == 1 ) THEN
      ALLOCATE(CSDMP(Nrit*(Nrit+1)/2,Nf))
    ELSE
      ALLOCATE(phi_data(Nrit,Nf),phi_model(Nrit))
    ENDIF

    IF ( ln_csdm == 1 ) THEN
      CALL computecsdmp_SA(jlen,Nrit,rdata,Nf,normalisation,freq,Fs,nivBruit) 
    ELSE
      CALL compute_phidata(jlen,Nrit,rdata,Nf,freq,Fs)
    ENDIF

!  CALL computecsdm(jlen,Nr,rdata,Nf,normalisation,freq,Fs,nivBruit)
!  DEALLOCATE(rdata)

!! Inverse CSDM if method adaptive
!  IF ( method == 1 ) THEN !    if method == 'mvdr':
!    ALLOCATE(KKff(Nr,Nr))
!    DO ff=1,Nf
!      KKff(:,:)=CSDM(:,:,ff)
!      CALL invCsdm(Nr,Nr,KKff)  ! Caution : K is updated...
!      CSDM(:,:,ff)=KKff(:,:)
!    ENDDO
!    DEALLOCATE(KKff)
!  ENDIF


  IF ( azimuth == 1 ) THEN   ! c,theta
!    ALLOCATE(beamf(Nt,Nc,Nf),beam(Nt,Nc),beamstd(Nt,Nc)) 
!    CALL computebeamct(theta,xr,yr,freq,cel,method,Nr,Nt,Nf,Nc,beam,beamstd,beamf)  ! rename as beamforming_code_ct ! only one CSDM arg !
!print *, 'shape(beam): ',shape(beam)
!print *, 'shape(beamf): ',shape(beamf)
!print *, 'sum(beam): ',sum(beam)
!print *, 'max(beam): ',maxval(beam)
!print *, 'min(beam): ',minval(beam)
!    dims3=(/Nt,Nc,Nf/)
!    CALL h5ltmake_dataset_float_f(outfile_id,"beamf",3,dims3,REAL(beamf(:,:,:)),error)
!    if (error/=0) call exit(error)
!    dims2=(/Nt,Nc/)
!    CALL h5ltmake_dataset_float_f(outfile_id,"beam",2,dims2,REAL(beam(:,:)),error)
!    if (error/=0) call exit(error)
!    CALL h5ltmake_dataset_float_f(outfile_id,"beamstd",2,dims2,REAL(beamstd(:,:)),error)
!    if (error/=0) call exit(error)
    print *,'azimuth == 1 not available'
    print *, 'use azimuth = 0 instead'
    CALL exit(1)
  ELSE                       ! x,y

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ce bloc !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    IF ( ln_coh == 1 ) then
      CALL computebeamgrid_exp(Nx,Ny,Nz,Nc,xgrid,ygrid,zgrid,cel,xybeam)
    else
      print *,'to do : computebeamgrid (version avec csdm_packed construite et montée en mémoire)'
      !ALLOCATE(xybeamf(Nx,Ny,Nz,Nc,Nf),xybeam(Nx,Ny,Nz,Nc),xybeamstd(Nx,Ny,Nz,Nc)) 
      !CALL computebeam_nocsdm(xgrid,ygrid,zgrid,xr,yr,zr,freq,cel,method,Nr,Nx,Ny,Nz,Nf,Nc,xybeam,xybeamstd,xybeamf)  ! rename as beamforming_code_xy ! only one CSDM arg !
    endif

    !ALLOCATE(xybeamf(Nx,Ny,Nz,Nc,Nf),xybeam(Nx,Ny,Nz,Nc),xybeamstd(Nx,Ny,Nz,Nc)) 
    !CALL computebeam(xgrid,ygrid,zgrid,xr,yr,zr,freq,cel,method,Nr,Nx,Ny,Nz,Nf,Nc,xybeam,xybeamstd,xybeamf)  ! rename as beamforming_code_xy ! only one CSDM arg !
!print *, 'shape(beam): ',shape(xybeam)
!print *, 'shape(beamf): ',shape(xybeamf)
!print *, 'sum(beam): ',sum(xybeam)
!print *, 'max(beam): ',maxval(xybeam)
!print *, 'min(beam): ',minval(xybeam)
!    dims5=(/Nx,Ny,Nz,Nc,Nf/)
!    CALL h5ltmake_dataset_float_f(outfile_id,"beamf",5,dims5,REAL(xybeamf(:,:,:,:,:)),error)
!    if (error/=0) call exit(error)

! il faut refaire l'ecriture a chaque nit iterations ?

    dims4=(/Nx,Ny,Nz,Nc/)
    CALL h5ltmake_dataset_float_f(outfile_id,"beam_"//trim(cit),4,dims4,REAL(xybeam(:,:,:,:)),error)
    if (error/=0) call exit(error)
!    CALL h5ltmake_dataset_float_f(outfile_id,"beamstd",4,dims4,REAL(xybeamstd(:,:,:,:)),error)
!    if (error/=0) call exit(error)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  ENDIF

    IF ( ln_csdm == 1 ) THEN
      DEALLOCATE(CSDMP)
    ELSE
      DEALLOCATE(phi_data,phi_model)
    ENDIF
 
    IF ( nn_removezero == 1 ) DEALLOCATE(rdata)

    IF ( (MOD(it,n_profile_it)==0) .OR. (it==nit) ) THEN
      CALL date_and_time(VALUES=values)
      svali=values(5)*3600+values(6)*60+values(7)+0.001*values(8)
      elps=svali-sval0
      sval0=svali
      !print *,'it | totsec | sec / active trace:',it,elps,elps/Nrit
      print *,'it | totsec :',it,elps
    ENDIF

  ENDDO ! end time loop on nit

  CALL h5fclose_f(infile_id, error)
  if (error/=0) call exit(error)

  CALL h5fclose_f(outfile_id, error)
  if (error/=0) call exit(error)

  CALL h5close_f(error)
  if (error/=0) call exit(error)

END PROGRAM BEAMFORMING
