!###############################################################################
!#    This file is part of MFP : a Fortran code for computing MFP              #
!#    (Matched Field Processing), also named bartlett or beamforming,          #
!#    and find a local optimum along (x,y,z,c) parameters.                     #
!#                                                                             #
!#    Copyright (C) 2017 Albanne Lecointre                                     #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################


!  =======================================================================================
!                       ***  Fortran program beamforming_optim.f90  ***
!   This is the main F90 program to find maximum of beamforming function
!    
!  =======================================================================================
!   History :  1.0  : 03/2017  : A. Lecointre : Initial version
!                                               grid search : (x,y,c)    
!              2.0  : 07/2018  : A. Lecointre : grid search : (x,y,z)    
!  ---------------------------------------------------------------------------------------
!
!  ---------------------------------------------------------------------------------------
!   routines      : description
!                 : 
!                 : 
!                 : 
!                 :
!  ---------------------------------------------------------------------------------------
!  $Id: beamforming_optim.f90 160 2019-01-18 15:54:44Z lecointre $
!  ---------------------------------------------------------------------------------------

PROGRAM BEAMFORMING_OPTIM
  USE HDF5
  USE H5LT      ! Lite Interface HDF5
!  USE glovarmodule , only : N,xr,yr,zr,freq,CSDMP,phi_data,phi_model,Nr,Nf,cel,nn_removezero,nkzero,nevals,Ngc,gridlocs,vmfile_id,gridlocsmask,ttgr
  USE glovarmodule , only : N,xr,yr,zr,freq,CSDMP,phi_data,phi_model,Nr,Nf,cel,zzz,nn_removezero,nkzero,nevals, &
Ngc,gridlocs,vmfile_id,ttgr,distxgr,minN,dphi,Baf,xyzcBB, ln_coh, ref_inode_coh, ln_randomdata, &
dn_c0, dn_cx, dn_cxx, dn_cy, dn_cyy, dn_cxy, dn_zwidth, &
dn_cxxx, dn_cyyy, dn_cxxy, dn_cxyy, &
dn_cxxxx, dn_cyyyy, dn_cxxxy, dn_cxxyy, dn_cxyyy

  USE modcomputebeam
  USE modcomputebeam_optim
  IMPLICIT NONE
include 'nlopt.f'

  INTEGER, PARAMETER :: n_profile_it = 500
  INTEGER :: it,nit,Nrit
  INTEGER :: Nx,Ny,Nc,ix,iy,ic
  INTEGER :: kk,kki ! Loop integer for stations
  INTEGER :: ios ! dummy loop char index
  INTEGER :: iwcl  ! The number of line in a file
  CHARACTER(len=256), DIMENSION(:), ALLOCATABLE :: staname ! the stations name

  CHARACTER(len=10), DIMENSION(3) :: b

  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: lat,lon ! the stations coordinates
  INTEGER :: nivBruit,normalisation,azimuth,Fs
  INTEGER :: method,ln_csdm,ln_vm
  REAL(KIND=4), DIMENSION(:,:), ALLOCATABLE :: rdata,rdata_large
  LOGICAL :: l_exist_namelist
  REAL(KIND=8) :: xmin,xmax,xstep,ymin,ymax,ystep,zmin,zmax,zstep,cmin,cmax,cstep,fmin,fmax,fstep
  INTEGER(HID_T)     :: infile_id,outfile_id,locfile_id
  INTEGER(HSIZE_T), DIMENSION(1) :: startin1
  INTEGER(HSIZE_T), DIMENSION(1) :: countin1
  INTEGER(HSIZE_T), DIMENSION(1) :: dims1
  INTEGER(HSIZE_T), DIMENSION(2) :: dims2
  INTEGER(HSIZE_T), DIMENSION(3) :: dims3
  INTEGER                        :: error
  INTEGER :: ff,i
  INTEGER :: jbeg,jlen,joverlap,njlen,offset
  CHARACTER(len=256) :: cn_ifile,cn_vmfile

  ! Minimisation
  REAL(KIND=8) :: celmoy,xgridmoy,ygridmoy ! variables of the function to minimize
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: start3,step3,grad0
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: xxxmin ! the coord of the pt which is estimated to minimize the function
  REAL(KIND=8) :: ynewlo   ! the minimum value of the function
  REAL(KIND=8) :: dn_reqmin != 1.0D-08  ! namelist
  REAL(KIND=8) :: dn_istep_x != 1.0D+00  ! namelist
  REAL(KIND=8) :: dn_istep_y != 1.0D+00  ! namelist
  REAL(KIND=8) :: dn_istep_z != 1.0D+00  ! namelist
  REAL(KIND=8) :: dn_istep_c != 1.0D+00  ! namelist
  INTEGER :: nn_zpen,nn_optim,nn_nlopt
  INTEGER :: nn_konvge,nn_kcount  ! namelist
  INTEGER :: icount ! the number of function evaluations used
  INTEGER :: numres ! the number of restarts
  INTEGER :: ifault ! error indicator
                    ! 0, no errors detected
                    ! 1, REQMIN, N, or KONVGE has an illegal value
                    ! 2, iteration terminated because KCOUNT was exceeded without convergence

  INTEGER :: nn_latlon,nn_io,nn_diags

  ! Output
  INTEGER, DIMENSION(:,:), ALLOCATABLE :: infooptim ! output info about optimisation
  REAL(KIND=4), DIMENSION(:,:), ALLOCATABLE :: xyzmin ! xyz that minimize -Bartlett
  REAL(KIND=4), DIMENSION(:), ALLOCATABLE :: Baval,Baval_zpen ! Bartlett corresponding to xyzmin
  INTEGER(KIND=4), DIMENSION(:,:), ALLOCATABLE :: patchesInd ! logical for stations = zero or not

  ! Instrumentation du code pour analyse de performance
  INTEGER,DIMENSION(8) :: values,values0
  REAL(KIND=4) :: elps,sval0,svali           ! in sec

integer(kind=8) :: opt
integer :: algorithm
integer(kind=4) :: ires
!external :: myfunc
real(kind=4) :: tol
!real(kind=4), dimension(:), allocatable :: tolN
REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: lb, ub

  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: buf

  NAMELIST /namparam/ method,ln_csdm,nivBruit,normalisation,azimuth,Fs,cn_ifile,jbeg,jlen,joverlap,nit
  NAMELIST /namcoh/ ln_coh ! , ref_inode_coh
  NAMELIST /namio/ nn_io
  NAMELIST /velmodel/ ln_vm, cn_vmfile, Ngc
  NAMELIST /namxy/ xmin,xmax,xstep,ymin,ymax,ystep
  NAMELIST /namHsource/ zmin,zmax,zstep
  NAMELIST /namspeed/ cmin,cmax,cstep
  NAMELIST /namindice_freq/ fmin,fmax,fstep
  NAMELIST /namoptim/ nn_optim,nn_nlopt,dn_reqmin,dn_istep_x,dn_istep_y,dn_istep_z,dn_istep_c,nn_konvge,nn_kcount
  NAMELIST /nambound/ nn_zpen, dn_c0, dn_cx, dn_cy, dn_cxx, dn_cxy, dn_cyy, &
                      dn_cxxx, dn_cxxy, dn_cxyy, dn_cyyy, &
                      dn_cxxxx, dn_cxxxy, dn_cxxyy, dn_cxyyy, dn_cyyyy, dn_zwidth
  NAMELIST /namcoord/ nn_latlon
  NAMELIST /naminput/ nn_removezero
  NAMELIST /namrandom/ ln_randomdata
  NAMELIST /namdiags/ nn_diags

  CALL date_and_time(VALUES=values)
  svali=values(5)*3600+values(6)*60+values(7)+0.001*values(8)

  ! Declare NAMELIST
  ! Read namelist
  INQUIRE(file='namelist', exist=l_exist_namelist)
  IF (.NOT. l_exist_namelist) THEN
     PRINT *,'The namelist does not exist'
     STOP
  ENDIF
  OPEN(11,file='namelist')
    REWIND 11
    READ(11,namparam)
    REWIND 11
    READ(11,namcoh)
    REWIND 11
    READ(11,namio)
    REWIND 11
    READ(11,velmodel)
    REWIND 11
    READ(11,namxy)
    REWIND 11
    READ(11,namHsource)
    REWIND 11
    READ(11,namspeed)
    REWIND 11
    READ(11,namindice_freq)
    REWIND 11
    READ(11,namoptim)
    REWIND 11
    READ(11,nambound)
    REWIND 11
    READ(11,namcoord)
    REWIND 11
    READ(11,naminput)
    REWIND 11
    READ(11,namdiags)
    REWIND 11
    READ(11,namrandom)
  CLOSE(11)

  ! Check argument namelist
  IF ( nn_optim == 1 ) THEN
     IF ( nn_nlopt == 1 ) then
       print *, 'nn_nlopt == 1 (NLopt) not available in case of nn_optim=1 (x,y,c) San Jacinto'
       print *, 'use nn_nlopt = 0 (nelmin) instead'
       CALL exit(1)
     ENDIF
     IF ( ln_vm == 1 ) THEN
       print *, 'ln_vm == 1 (velocity model) not available in case of nn_optim=1 (x,y,c) San Jacinto'
       print *, 'use ln_vm = 0 instead'
       CALL exit(1)
     ENDIF
  ELSEIF ( nn_optim == 2 ) THEN
     IF ( ( ln_csdm == 1 ) .AND. ( ln_vm == 1 ) ) THEN
       print *, 'ln_vm == 1 (velocity model) not available in case of ln_csdm and nn_optim=2 (x,y,z) Glenhaven'
       print *, 'use ln_vm = 0 instead, or use ln_csdm = 0'
       CALL exit(1)
     ENDIF
  ELSEIF ( nn_optim == 3 ) THEN
!    IF ( nn_nlopt == 0 ) THEN
!      print *,'nn_nlopt == 0 (nelmin) not available in case of nn_optim=3 (x,y,c) Argentiere'
!      print *, 'use nn_nlopt = 1 (NLopt) instead'
!      CALL exit(1)
!    ENDIF
    IF ( ln_csdm == 1 ) then
      print *,'ln_csdm == 1 (compute CSDM explicitely) not available in case of nn_optim=3 (x,y,c) Argentiere'
      print *, 'use ln_csdm = 0 instead'
      CALL exit(1)
    ENDIF
  ELSEIF ( nn_optim == 4 ) THEN
!    IF ( nn_nlopt == 0 ) THEN
!      print *,'nn_nlopt == 0 (nelmin) not available in case of nn_optim=4 (x,y,z,c) Argentiere'
!      print *, 'use nn_nlopt = 1 (NLopt) instead'
!      CALL exit(1)
!    ENDIF
    IF ( ln_csdm == 1 ) then
      print *,'ln_csdm == 1 (compute CSDM explicitely) not available in case of nn_optim=4 (x,y,z,c) Argentiere'
      print *, 'use ln_csdm = 0 instead'
      CALL exit(1)
    ENDIF
  ENDIF
  IF ( method == 1 ) THEN !    if method == 'mvdr':
    print *,'method = 1 (mvdr) not available in case of optim'
    print *, 'use bartlett instead'
    CALL exit(1)
  ENDIF
  IF ( azimuth == 1 ) THEN   ! c,theta
    print *, 'azimuth = 1 not available in case of optim'
    CALL exit(1)
  ENDIF
  IF ( ln_csdm == 0 ) THEN
    IF ( nivBruit /= 0 ) THEN
      print *, 'nivBruit /= 0 (Add noise on CSDM diagonal) not available in case of optim and ln_csdm = 0'
      print *, 'use nivBruit = 0 or ln_csdm = 1'
      CALL exit(1)
    ENDIF
    IF ( normalisation /= 1 ) THEN
      print *, 'normalisation /= 1 (whitening) not available in case of optim and ln_csdm = 0'
      print *, 'All the CSDM terms must have modulus = 1 (bartlett is computed as sum of Nsta complex exponential)'
      print *, 'use normalisation = 1 or ln_csdm = 1'
      CALL exit(1)
    ENDIF
  ELSE
    IF ( ln_vm == 1 ) THEN
      print *, 'ln_vm = 1 (modele de vitesse 3D) not available in case of optim and ln_csdm = 1'
      print *, 'Please use ln_csdm = 0 (bartlett is computed as sum of Nsta complex exponential)'
      print *, 'if you want to apply a velocity model'
      CALL exit(1)
    ENDIF
  ENDIF

  ! Fix the number of parameters to optimize
  if ( nn_optim == 4 ) then
    N=4 ! Argentiere xyzc
  else
    N=3 ! the number of variables of the function for nelmin minimisation
  endif
  ALLOCATE(start3(N), step3(N), grad0(N), xxxmin(N) )

  if ( nn_zpen > 0 ) then
    if ( nn_optim == 4 ) then
      ALLOCATE(Baval_zpen(nit))
    else
      print *, 'nn_zpen = 1/2 (Apply penalty if function converge above parametric surface)'
      print *,'not available in case of optim different from 4 (xyzc argentiere)'
      print *, 'Please use nn_zpen = 0'
      CALL exit(1)
    endif
  endif

  if ( nn_nlopt == 1 ) then
    opt = 0
    algorithm = NLOPT_LN_NELDERMEAD ! NLOPT_GN_CRS2_LM ! NLOPT_GN_DIRECT ! NLOPT_LN_SBPLX ! NLOPT_LN_COBYLA ! NLOPT_LN_NELDERMEAD
    call nlo_create(opt, algorithm, N)
    print *,'NLopt opt',opt
    allocate(lb(N),ub(N))
    lb(1)=xmin
    ub(1)=xmax
    lb(2)=ymin
    ub(2)=ymax
    if ( nn_optim == 2 ) then ! xyz
      lb(3)=zmin
      ub(3)=zmax
    elseif ( nn_optim == 3 ) then ! xyc
      lb(3)=cmin
      ub(3)=cmax
    elseif ( nn_optim == 4 ) then ! xyzc
      lb(3)=zmin
      ub(3)=zmax
      lb(4)=cmin
      ub(4)=cmax
    endif
    IF ( ln_csdm == 1 ) then
      call nlo_set_min_objective(ires, opt, myfunc, 0)
    else
      if ( nn_optim == 2 ) then
        IF ( ln_vm == 1 ) THEN
          call nlo_set_min_objective(ires, opt, myfunc_expvm, 0)
        else
          call nlo_set_min_objective(ires, opt, myfunc_exp, 0)
        endif
      elseif ( nn_optim == 3 ) then
        call nlo_set_min_objective(ires, opt, fBa_xyc_exp, 0)
      elseif ( nn_optim == 4 ) then
        if ( nn_zpen == 1 ) then
          call nlo_set_min_objective(ires, opt, fBa_xyzc_exp_zpen, 0)
        elseif ( nn_zpen == 2 ) then
          print *, 'nn_zpen = 2 (Apply penalty if function converge above AND UNDER parametric surface)'
          print *, 'not available with NLopt, please create the function fBa_xyzc_exp_zpen2'
          print *, 'or Please use nn_zpen = 0/1'
          CALL exit(1)
          !call nlo_set_min_objective(ires, opt, fBa_xyzc_exp_zpen2, 0)
        else
          call nlo_set_min_objective(ires, opt, fBa_xyzc_exp, 0)
        endif
      endif
    endif
    print *,'ires min',ires
    call nlo_set_lower_bounds(ires, opt, lb)
    print *,'ires lower bnd',ires
    call nlo_set_upper_bounds(ires, opt, ub)
    print *,'ires upper bnd',ires
    tol=0.0 ! dn_reqmin
    call nlo_set_ftol_abs(ires, opt, tol)
    !call nlo_set_ftol_rel(ires, opt, tol)
    print *,'ires ftol',ires
!tol=0.0
!call nlo_set_xtol_abs1(ires, opt, tol)
!print *,'ires xtol',ires
!allocate(tolN(N))
!tolN(:)=0.0 ! dn_reqmin
!call nlo_set_xtol_rel(ires, opt, tolN)
!print *,'ires xtol',ires
    call nlo_set_maxeval(ires, opt, nn_kcount)
    print *,'ires maxeval',ires
    if ( nn_optim == 2 ) then
      step3 = (/ dn_istep_x, dn_istep_y, dn_istep_z /)
    elseif ( nn_optim == 3 ) then
      step3 = (/ dn_istep_x, dn_istep_y, dn_istep_c /)
    elseif ( nn_optim == 4 ) then
      step3 = (/ dn_istep_x, dn_istep_y, dn_istep_z, dn_istep_c /)
    endif
    call nlo_set_initial_step(ires, opt, step3);
  else
    if ( nn_optim == 1 ) then
      step3 = (/ dn_istep_x, dn_istep_y, dn_istep_c /)
    elseif ( nn_optim == 2 ) then
      step3 = (/ dn_istep_x, dn_istep_y, dn_istep_z /)
    elseif ( nn_optim == 3 ) then
      step3 = (/ dn_istep_x, dn_istep_y, dn_istep_c /)
    elseif ( nn_optim == 4 ) then
      step3 = (/ dn_istep_x, dn_istep_y, dn_istep_z, dn_istep_c /)
    endif
  endif

  ! 
  IF ( ln_randomdata == 1 ) then
    CALL init_random_seed() ! on init la graine une seule fois au depart
                            ! a partir de param d'horloge
  endif

! Initialise frequency vector
  IF ( fstep < 0 ) THEN
    Nf=INT(-fstep)
    ALLOCATE(freq(Nf))
    OPEN(10, file="freq.txt")
    DO ff=1,Nf
      READ(UNIT=10,FMT=*) freq(ff)
    ENDDO
    CLOSE(10)
  ELSE
    Nf=(fmax-fmin)/fstep+1
    ALLOCATE(freq(Nf))
    freq = (/(fmin + ((i-1)*fstep),i=1,Nf)/) ! assign to y in increments of fstep starting at fmin
  ENDIF
  print *,'freq',freq

  ! Initialize HDF5 context
  CALL h5open_f(error)
  if (error/=0) call exit(error)

print *,'Read the station list and their geographical location from zreseau.txt'
! Read the station list and their geographical location from zreseau.txt input file
  iwcl=0
  OPEN(10, file="zreseau.txt", form="formatted", iostat=ios,status="old")
  DO WHILE (ios==0)
    READ(10,*,iostat=ios)
    IF (ios==0) iwcl=iwcl+1
  ENDDO
  Nr=iwcl
  CLOSE(10)
  IF ( nn_latlon == 1 ) THEN
    ALLOCATE(staname(Nr),lat(Nr),lon(Nr),xr(Nr),yr(Nr),zr(Nr))
    OPEN(10, file="zreseau.txt")
    DO kk=1,Nr
      READ(UNIT=10,FMT=*) staname(kk),lon(kk),lat(kk),zr(kk)
      CALL distlatlong(lat(1), lon(1), lat(kk), lon(kk), xr(kk), yr(kk))
    ENDDO
    CLOSE(10)
    xr(:)=xr(:)-SUM(xr(:))/Nr
    yr(:)=yr(:)-SUM(yr(:))/Nr
  ELSE
      ALLOCATE(staname(Nr),xr(Nr),yr(Nr),zr(Nr))
    IF ( nn_optim == 2 ) THEN ! GlenHaven
      OPEN(10, file="zreseau.txt")
      DO kk=1,Nr
        READ(UNIT=10,FMT=*) staname(kk) ! ,xr(kk),yr(kk),zr(kk)
      ENDDO
      CLOSE(10)
      ! Read station coordinates from StationLocs.h5 file
      CALL h5fopen_f("StationLocs.h5", H5F_ACC_RDONLY_F, locfile_id, error)
      if (error/=0) call exit(error)
      ALLOCATE(buf(Nr,3))
      dims2=(/Nr,3/)
      CALL h5ltread_dataset_f(locfile_id, "/stationLocs", H5T_NATIVE_DOUBLE, buf, dims2, error)
      if (error/=0) call exit(error)
      CALL h5fclose_f(locfile_id, error)
      if (error/=0) call exit(error)
      xr=buf(:,1)
      yr=buf(:,2)
      zr=buf(:,3)
    ELSE ! Argentiere
      OPEN(10, file="zreseau.txt")
      DO kk=1,Nr
        READ(UNIT=10,FMT=*) staname(kk),xr(kk),yr(kk),zr(kk)
      ENDDO
      CLOSE(10)
    ENDIF
  ENDIF

  ! Before iterate, initialize starting for optimisation
  IF ( nn_optim == 2 ) THEN ! Glenhaven (x,y,z) grid, velocity is fixed
    cel = cstep
    start3 = (/ xstep , ystep , zstep /)  ! may be overwritten by nelmin
print *,'Init: ',start3
  ELSEIF ( nn_optim == 3 ) THEN
    zzz = zstep
  ENDIF

  ! Open input data file
  CALL h5fopen_f(TRIM(cn_ifile), H5F_ACC_RDONLY_F, infile_id, error)
  if (error/=0) call exit(error)


  ! Modele de vitesse 3D
  IF ( ln_vm == 1 ) THEN
    CALL h5fopen_f(TRIM(cn_vmfile), H5F_ACC_RDONLY_F, vmfile_id, error)
    if (error/=0) call exit(error)
    dims2=(/Ngc,3/)
    ALLOCATE(gridlocs(Ngc,3))
!    ALLOCATE(gridlocsmask(Ngc))
    CALL h5ltread_dataset_f(vmfile_id, "/gridlocs", H5T_NATIVE_INTEGER, gridlocs, dims2, error)
    if (error/=0) call exit(error)
! temporary mask
!    gridlocsmask(1:2000000)=.false.
!    gridlocsmask(2000001:3000000)=.true.
!    gridlocsmask(3000001:4000000)=.false.
  ENDIF

  IF ( MOD(nit,nn_io) /= 0 ) THEN
    print *,'Warning, nit:',nit,' nn_io:',nn_io
    nit = nit - MOD(nit,nn_io)
    print *,'Set nit to a new value:',nit 
  ENDIF

  IF ( nn_io == nit ) THEN
  ! Read input data using read_one_trace subroutine
    njlen=joverlap*(nit-1)+jlen
    ALLOCATE(rdata_large(njlen,Nr))  ! data is only used to compute the Cross Spectral Density Matrix ( K[Nr,Nr,Nf] )
    countin1=(/njlen/)
    startin1=(/jbeg/)
    print *,'Read input data: offset,count: ',startin1,countin1
    DO kk=1,Nr
      IF (MOD(kk,500)==0)  print *,'lecture',kk,TRIM(staname(kk)),'/',Nr
      CALL read_one_trace(infile_id,"/"//TRIM(staname(kk)),1,startin1,countin1,njlen,rdata_large(:,kk))
    ENDDO
  ELSE
    njlen=joverlap*(nn_io-1)+jlen
    ALLOCATE(rdata_large(njlen,Nr))
    countin1=(/njlen/)
    startin1=(/jbeg/)
  ENDIF

  ALLOCATE(infooptim(nit,3),xyzmin(nit,N),Baval(nit))
  ALLOCATE(nkzero(Nr),patchesInd(Nr,nit))

  IF ( nn_removezero == 0 ) ALLOCATE(rdata(jlen,Nr))  ! data is only used to compute the Cross Spectral Density Matrix ( K[Nr,Nr,Nf] )

  if ( nn_diags == 1 ) then
    ALLOCATE(dphi(Nr,Nf,nn_kcount+nn_konvge),Baf(Nf,nn_kcount+nn_konvge))
    ALLOCATE(xyzcBB(nn_kcount+nn_konvge,6))
    dphi(:,:,:)=-9999.0
    Baf(:,:)=-9999.0
    xyzcBB(:,:)=-9999.0
  endif

  ! compute ref_inode_coh : look for the closest station from starting point
  ! ref_inode_coh is the 1-based index for reference sensor
  IF ( ln_coh == 1 ) THEN
    IF ( nn_removezero == 1 ) THEN
      print *, 'nn_removezero and ln_coh are not compatible'
      CALL exit(1)
    ENDIF
    !CALL compute_ref_inode_coh(xstep,ystep,zstep,ref_inode_coh)
    CALL compute_ref_inode_coh(xstep,ystep,ref_inode_coh)
    print *,'1-based index ref_inode_coh',ref_inode_coh,staname(ref_inode_coh)
  ENDIF

  CALL date_and_time(VALUES=values)
  sval0=values(5)*3600+values(6)*60+values(7)+0.001*values(8)
  elps=sval0-svali
  print *,'read inputs and init:',elps
 
  ALLOCATE(ttgr(Nr),distxgr(Nr))
  minN = -1 ! dummy initialization (to be sure that 1st iteration would effectively read ttgr

!OPEN(10, file="foo.dat", form="unformatted")

  DO it=1,nit
 
!    print *,'====> it: ',it
 
    IF ( nn_io == nit ) THEN ! Full input dataset already read before iterate

      ! compute offset to access rdata_large in memory
      offset = joverlap*(it-1)+1

    ELSE ! must read input every nn_io iterations

      IF ( MOD(it-1,nn_io) == 0 ) THEN ! read nn_io input steps
        print *,'Read input data: offset,count: ',startin1,countin1
        CALL date_and_time(b(1), b(2), b(3), values0)
        DO kk=1,Nr
        IF (MOD(kk,500)==0)  print *,'lecture',kk,TRIM(staname(kk)),'/',Nr
          CALL read_one_trace(infile_id,"/"//TRIM(staname(kk)),1,startin1,countin1,njlen,rdata_large(:,kk))
        ENDDO
        CALL date_and_time(VALUES=values)
        elps = values(5)*3600+values(6)*60+values(7)+0.001*values(8) - ( values0(5)*3600+values0(6)*60+values0(7)+0.001*values0(8) )
        print *,'Read_input ',b(1), b(2), elps
        ! increment startin1
        startin1 = startin1 + ( nn_io * joverlap )
      ENDIF

      ! compute offset to access rdata_large in memory
      offset = joverlap*MOD(it-1,nn_io)+1 ! joverlap*(it-1)+1

    ENDIF

    ! access rdata_large in memory, compute patchsInd,nkzero,Nrit
    nkzero(:)=1
!    print *,'dataset jbeg jend :',offset,offset+jlen-1
    DO kk=1,Nr
      IF ( MAXVAL(ABS(rdata_large(offset:offset+jlen-1,kk))) == 0 ) nkzero(kk) = 0 ! mask for zero data
    ENDDO
    patchesInd(:,it)=nkzero(:)
    Nrit=SUM(nkzero)
    IF ( nn_removezero == 1 ) THEN
!      print *,'Remove zero data and use only ',Nrit,' dataset / ',Nr
      ALLOCATE(rdata(jlen,Nrit))
      kki=0
      DO kk=1,Nr
        IF ( nkzero(kk) == 1 ) THEN
          kki=kki+1
          rdata(:,kki) = rdata_large(offset:offset+jlen-1,kk)
        ENDIF
      ENDDO 
    ELSE
!      print *,'Nrit/Nr: ',Nrit,Nr,' but keep zero dataset'
      DO kk=1,Nr
        rdata(1:jlen,kk)=rdata_large(offset:offset+jlen-1,kk)
!print *,kk,rdata(1:2,kk),rdata(jlen-1:jlen,kk)
      ENDDO
      Nrit=Nr
    ENDIF

    ! Compute the Cross-Spectral Density Matrix K[Nr,Nr,Nf]
    IF ( ln_csdm == 1 ) THEN
      ALLOCATE(CSDMP(Nrit*(Nrit+1)/2,Nf))
    ELSE
      ALLOCATE(phi_data(Nrit,Nf),phi_model(Nrit))
    ENDIF

    IF ( ln_csdm == 1 ) THEN
      CALL computecsdmp_SA(jlen,Nrit,rdata,Nf,normalisation,freq,Fs,nivBruit) 
    ELSE
      CALL compute_phidata(jlen,Nrit,rdata,Nf,freq,Fs)
    ENDIF

!print *,'phidata'
!DO ff=1,Nf
!print *,ff,phi_data(1:2,ff),phi_data(Nrit-1:Nrit,ff)
!ENDDO


    ! Minimisation(-beam)
    ! https://people.sc.fsu.edu/~jburkardt/f77_src/asa047/asa047.html
    IF ( nn_optim == 1 ) THEN ! (x,y,c) grid, z is fixed but not used, no altitude for stations replica = f(x,y only), no z

!      WRITE(*,"(A20, A20, A16, A7, A7, A7)") , 'start3' , 'xxxmin' , 'ynewlo' , 'icount','numres','ifault'
      Nx=(xmax-xmin)/xstep+1
      Ny=(ymax-ymin)/ystep+1
      Nc=(cmax-cmin)/cstep+1
      DO ix = 1,Nx 
        xgridmoy = xmin + (ix-1)*xstep
        DO iy = 1,Ny 
          ygridmoy = ymin + (iy-1)*ystep
          DO ic = 1,Nc 
            celmoy = cmin + (ic-1)*cstep
            start3 = (/ xgridmoy , ygridmoy , celmoy /)  ! may be overwritten by nelmin
!            WRITE(*,"(3F7.1)",advance="no") , start3
            IF ( ln_csdm == 1 ) then
              CALL nelmin(BA_xy_SJFZ , N , start3 , xxxmin , ynewlo , &
                          dn_reqmin, step3,  nn_konvge, nn_kcount, icount, numres, ifault )
            ELSE
              CALL nelmin(BA_xy_SJFZ_exp , N , start3 , xxxmin , ynewlo , &
                          dn_reqmin, step3,  nn_konvge, nn_kcount, icount, numres, ifault )
            ENDIF
!            WRITE(*,"(3F7.1,1X,F14.9,1X,I4,1X,I4,7X,I1)") , xxxmin,ynewlo,icount,numres,ifault
            IF ( ifault == 1 ) CALL EXIT(ifault)
          ENDDO
        ENDDO
      ENDDO

    ELSEIF ( nn_optim == 3 ) THEN ! Argentiere : x,y,c
        
      ! Always initialize start3 to the same x,y,c value          
      start3 = (/ xstep , ystep , cstep /) 

      if ( nn_nlopt == 1 ) then

!        call fBa_xyc_exp(ynewlo, N, start3, grad0, 0)
!        print *,'first estim start3:',start3,'ynewlo:',ynewlo
        nevals=0 ! init global counter to get the total number of objective function evaluations
        call nlo_optimize(ires, opt, start3, ynewlo) ! MAJ de start3 automatiquement
        if ( ires < 0 ) CALL EXIT(ires)
!        print *,'ires optimize',ires
!        print *,'optim start3:',start3,'ynewlo:',ynewlo
        !call nlo_get_numevals(icount, opt)
        infooptim(it,1) = nevals ! icount
        xyzmin(it,:) = REAL(start3(:))
        Baval(it) = REAL(-ynewlo)
        infooptim(it,3) = ires
 
      else

!        ynewlo=BA_xyc_exp(start3)
!        print *,'first estim optim:',start3,'ynewlo:',ynewlo
        CALL nelmin(BA_xyc_exp , N , start3 , xxxmin , ynewlo , &
                    dn_reqmin, step3,  nn_konvge, nn_kcount, icount, numres, ifault )
        IF ( ifault == 1 ) CALL EXIT(ifault)
!        print *,'optim xxxmin:',xxxmin,'ynewlo:',ynewlo
        infooptim(it,1) = icount
        infooptim(it,2) = numres
        infooptim(it,3) = ifault
        xyzmin(it,:) = REAL(xxxmin(:))
        Baval(it) = REAL(-ynewlo)

      endif

    ELSEIF ( nn_optim == 4 ) THEN ! Argentiere : x,y,z,c
 
      ! Always initialize start3 to the same x,y,z,c value          
      start3 = (/ xstep , ystep , zstep, cstep /) 

      if ( nn_nlopt == 1 ) then

!        call fBa_xyzc_exp(ynewlo, N, start3, grad0, 0)
!        print *,'first estim start3:',start3,'ynewlo:',ynewlo
        nevals=0 ! init global counter to get the total number of objective function evaluations
        call nlo_optimize(ires, opt, start3, ynewlo) ! MAJ de start3 automatiquement
        if ( ires < 0 ) CALL EXIT(ires)
        if ( nn_zpen > 0 ) then ! MAJ ynewlo sans appliquer la penalite
          Baval_zpen(it) = REAL(-ynewlo)
          call fBa_xyzc_exp(ynewlo, N, start3, grad0, 0)
        endif
!        print *,'ires optimize',ires
!        print *,'optim start3:',start3,'ynewlo:',ynewlo
        !call nlo_get_numevals(icount, opt)
        infooptim(it,1) = nevals ! icount
        xyzmin(it,:) = REAL(start3(:))
        Baval(it) = REAL(-ynewlo)
        infooptim(it,3) = ires
 
      else ! ASA047

!        ynewlo=BA_xyzc_exp(start3)
!        print *,'first estim optim:',start3,'ynewlo:',ynewlo
        if ( nn_zpen == 1 ) then
          if ( nn_diags == 1 ) then
            nevals=0 ! init global counter to get the total number of objective function evaluations
            if ( ln_coh == 1 ) then
              CALL nelmin(BAcoh_xyzc_exp_zpen_diags , N , start3 , xxxmin , ynewlo , &
                          dn_reqmin, step3,  nn_konvge, nn_kcount, icount, numres, ifault )
            else
              CALL nelmin(BA_xyzc_exp_zpen_diags , N , start3 , xxxmin , ynewlo , &
                          dn_reqmin, step3,  nn_konvge, nn_kcount, icount, numres, ifault )
            endif
          else
            if ( ln_coh == 1 ) then
              CALL nelmin(BAcoh_xyzc_exp_zpen , N , start3 , xxxmin , ynewlo , &
                          dn_reqmin, step3,  nn_konvge, nn_kcount, icount, numres, ifault )
            else
              CALL nelmin(BA_xyzc_exp_zpen , N , start3 , xxxmin , ynewlo , &
                          dn_reqmin, step3,  nn_konvge, nn_kcount, icount, numres, ifault )
            endif
          endif
          Baval_zpen(it) = REAL(-ynewlo)
          if ( ln_coh == 1 ) then
            ynewlo = BAcoh_xyzc_exp(xxxmin) ! MAJ ynewlo sans appliquer la penalite
          else
            ynewlo = BA_xyzc_exp(xxxmin) ! MAJ ynewlo sans appliquer la penalite
          endif
        elseif ( nn_zpen == 2) then

          if ( nn_diags == 1 ) then
            print *, 'The configuration with nn_zpen == 2 and nn_diags == 1 is not available for the moment'
            CALL exit(1)
          else
            if ( ln_coh == 1 ) then
              CALL nelmin(BAcoh_xyzc_exp_zpen2 , N , start3 , xxxmin , ynewlo , &
                          dn_reqmin, step3,  nn_konvge, nn_kcount, icount, numres, ifault )
            else
              CALL nelmin(BA_xyzc_exp_zpen2 , N , start3 , xxxmin , ynewlo , &
                          dn_reqmin, step3,  nn_konvge, nn_kcount, icount, numres, ifault )
            endif
          endif
          Baval_zpen(it) = REAL(-ynewlo)
          if ( ln_coh == 1 ) then
            ynewlo = BAcoh_xyzc_exp(xxxmin) ! MAJ ynewlo sans appliquer la penalite
          else
            ynewlo = BA_xyzc_exp(xxxmin) ! MAJ ynewlo sans appliquer la penalite
          endif

        else ! no z penalty
          if ( nn_diags == 1 ) then
            print *, 'The configuration with nn_zpen == 0 and nn_diags == 1 is not available for the moment'
            print *, 'use nn_zpen = 1 with very large z value instead'
            CALL exit(1)
          else
            if ( ln_coh == 1 ) then
              CALL nelmin(BAcoh_xyzc_exp , N , start3 , xxxmin , ynewlo , &
                          dn_reqmin, step3,  nn_konvge, nn_kcount, icount, numres, ifault )
            else      
              CALL nelmin(BA_xyzc_exp , N , start3 , xxxmin , ynewlo , &
                          dn_reqmin, step3,  nn_konvge, nn_kcount, icount, numres, ifault )
            endif
          endif
        endif
        IF ( ifault == 1 ) CALL EXIT(ifault)
!        print *,'optim xxxmin:',xxxmin,'ynewlo:',ynewlo
        infooptim(it,1) = icount
        infooptim(it,2) = numres
        infooptim(it,3) = ifault
        xyzmin(it,:) = REAL(xxxmin(:))
        Baval(it) = REAL(-ynewlo)

      endif

    ELSEIF ( nn_optim == 2 ) THEN ! (x,y,z) grid, velocity is fixed

      if ( nn_nlopt == 1 ) then

        IF ( ln_csdm == 1 ) then
          call myfunc(ynewlo, N, start3, grad0, 0)
        ELSE
          IF ( ln_vm == 1 ) THEN
            call myfunc_expvm(ynewlo, N, start3, grad0, 0)
          ELSE
            call myfunc_exp(ynewlo, N, start3, grad0, 0)
          ENDIF
        ENDIF
!        print *,'first estim start3:',start3,'ynewlo:',ynewlo
        nevals=0 ! init global counter to get the total number of objective function evaluations
        call nlo_optimize(ires, opt, start3, ynewlo) ! MAJ de start3 automatiquement
        if ( ires < 0 ) CALL EXIT(ires)
!        print *,'ires optimize',ires
!        print *,'optim start3:',start3,'ynewlo:',ynewlo
        !call nlo_get_numevals(icount, opt)
        infooptim(it,1) = nevals ! icount
        xyzmin(it,:) = REAL(start3(:))
        Baval(it) = REAL(-ynewlo)
        infooptim(it,3) = ires

      else

!       WRITE(*,"(3F10.1)",advance="no") , start3
        IF ( ln_csdm == 1 ) then
          ynewlo=BA_xyz(start3)
!          print *,'first estim optim:',start3,'ynewlo:',ynewlo
          CALL nelmin(BA_xyz , N , start3 , xxxmin , ynewlo , &
                      dn_reqmin, step3,  nn_konvge, nn_kcount, icount, numres, ifault )
        ELSE
          IF ( ln_vm == 1 ) THEN
            ynewlo=BA_xyz_expvm(start3)
!            print *,'first estim optim:',start3,'ynewlo:',ynewlo
            CALL nelmin(BA_xyz_expvm , N , start3 , xxxmin , ynewlo , &
                        dn_reqmin, step3,  nn_konvge, nn_kcount, icount, numres, ifault )
          ELSE
            ynewlo=BA_xyz_exp(start3)
!            print *,'first estim optim:',start3,'ynewlo:',ynewlo
            CALL nelmin(BA_xyz_exp , N , start3 , xxxmin , ynewlo , &
                        dn_reqmin, step3,  nn_konvge, nn_kcount, icount, numres, ifault )
          ENDIF
        ENDIF
        infooptim(it,1) = icount
        infooptim(it,2) = numres
        infooptim(it,3) = ifault
        xyzmin(it,:) = REAL(xxxmin(:))
        Baval(it) = REAL(-ynewlo)
!      WRITE(*,"(A7, A7, A7)",advance="no") , 'icount','numres','ifault'
      !WRITE(*,"(3F10.1,1X,F14.9,1X,I4,1X,I4,7X,I1)") , xxxmin,ynewlo,icount,numres,ifault
!      WRITE(*,"(I6,1X,I4,7X,I1)") , icount,numres,ifault
!        print *,'optim xxxmin:',xxxmin,'ynewlo:',ynewlo
        IF ( ifault == 1 ) CALL EXIT(ifault)
        IF ( ( ifault == 2 ) .OR. &  ! not converge
             ( xxxmin(1) < xmin ) .OR. ( xxxmin(1) > xmax ) .OR. & ! x out of bounds
             ( xxxmin(2) < ymin ) .OR. ( xxxmin(2) > ymax ) .OR. & ! y out of bounds
             ( xxxmin(3) < zmin ) .OR. ( xxxmin(3) > zmax ) ) THEN ! z out of bounds
          start3 = (/ xstep , ystep , zstep /)
!print *,'Update init: ',start3
        ELSE
          start3 = xxxmin ! update start for the next iteration
        ENDIF

      endif ! end if nn_nlopt

    ENDIF ! end if nn_optim

    IF ( ln_csdm == 1 ) THEN
      DEALLOCATE(CSDMP)
    ELSE
      DEALLOCATE(phi_data,phi_model)
    ENDIF
 
    IF ( nn_removezero == 1 ) DEALLOCATE(rdata)

    IF ( (MOD(it,n_profile_it)==0) .OR. (it==nit) ) THEN
      CALL date_and_time(VALUES=values)
      svali=values(5)*3600+values(6)*60+values(7)+0.001*values(8)
      elps=svali-sval0
      sval0=svali
      !print *,'it | totsec | sec / active trace:',it,elps,elps/Nrit
      print *,'it | totsec :',it,elps
    ENDIF

  ENDDO ! End loop on it

!CLOSE(10)

  DEALLOCATE(rdata_large)

  ! close input file
  CALL h5fclose_f(infile_id, error)
  if (error/=0) call exit(error)

  ! Write outputs
  IF ( nn_optim == 2 .OR. nn_optim == 3 .OR. nn_optim == 4 ) THEN ! (x,y,z) grid, velocity is fixed OR (x,y,c) grid, z is fixed OR (x,y,z,c) grid
    CALL date_and_time(b(1), b(2), b(3), values0)

    ! Create output file
    CALL h5fcreate_f("./out.h5", H5F_ACC_TRUNC_F, outfile_id, error)
    if (error/=0) call exit(error)

    dims2=(/nit,3/)
    CALL h5ltmake_dataset_int_f(outfile_id,"infooptim",2,dims2,infooptim(:,:),error)
    if (error/=0) call exit(error)

    dims2=(/nit,N/)
    IF ( nn_optim == 2 ) THEN 
      CALL h5ltmake_dataset_float_f(outfile_id,"xyz",2,dims2,xyzmin(:,:),error)    
    ELSEIF ( nn_optim == 3 ) THEN
      CALL h5ltmake_dataset_float_f(outfile_id,"xyc",2,dims2,xyzmin(:,:),error)    
    ELSEIF ( nn_optim == 4 ) THEN
      CALL h5ltmake_dataset_float_f(outfile_id,"xyzc",2,dims2,xyzmin(:,:),error)    
    ENDIF
    if (error/=0) call exit(error)

    dims2=(/Nr,nit/)
    CALL h5ltmake_dataset_int_f(outfile_id,"patchesInd",2,dims2,patchesInd(:,:),error)
    if (error/=0) call exit(error)

    dims1=(/nit/)
    CALL h5ltmake_dataset_float_f(outfile_id,"Baval",1,dims1,Baval(:),error)    
    if (error/=0) call exit(error)

    IF ( nn_diags == 1 ) THEN
      dims3=(/Nr,Nf,nevals/) ! nn_kcount+nn_konvge/)
      CALL h5ltmake_dataset_float_f(outfile_id,"dphi",3,dims3,dphi(:,:,1:nevals),error)    
      dims2=(/Nf,nevals/)
      CALL h5ltmake_dataset_float_f(outfile_id,"Baf",2,dims2,Baf(:,1:nevals),error)    
      dims2=(/nevals,6/)
      CALL h5ltmake_dataset_float_f(outfile_id,"xyzcBB",2,dims2,xyzcBB(1:nevals,:),error)    
      dims1=(/Nf/)
      CALL h5ltmake_dataset_float_f(outfile_id,"freq",1,dims1,REAL(freq(:)),error)    
    ENDIF

    CALL h5fclose_f(outfile_id, error)
    if (error/=0) call exit(error)

  ENDIF

  if ( nn_nlopt == 1 ) call nlo_destroy(opt)

  ! Close file for Modele de vitesse 3D
  IF ( ln_vm == 1 ) THEN
    CALL h5fclose_f(vmfile_id, error)
    if (error/=0) call exit(error)
  ENDIF

  CALL h5close_f(error)
  if (error/=0) call exit(error)

  CALL date_and_time(VALUES=values)
  elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)-sval0
  print *,'write outputs:',elps

  CALL date_and_time(VALUES=values)
  elps = values(5)*3600+values(6)*60+values(7)+0.001*values(8) - ( values0(5)*3600+values0(6)*60+values0(7)+0.001*values0(8) )
  print *,'Write_output ',b(1), b(2), elps

END PROGRAM BEAMFORMING_OPTIM
