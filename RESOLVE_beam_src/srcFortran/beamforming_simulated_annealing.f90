!###############################################################################
!#    This file is part of MFP : a Fortran code for computing MFP              #
!#    (Matched Field Processing), also named bartlett or beamforming,          #
!#    and find a local optimum along (x,y,c) parameters, thanks to MCMC method #
!#    (simulated annealing).                                                   #
!#                                                                             #
!#    Copyright (C) 2017 Albanne Lecointre                                     #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################

!  =======================================================================================
!                       ***  Fortran program beamforming_simulated_annealing.f90  ***
!   This is the main F90 program to compute beamforming over a research grid
!    
!  =======================================================================================
!   History :  1.0  : 04/2018  : A. Lecointre : Initial version
!	        
!	grid search (x,y,c) only : NO (c,theta) and NO (z)	    
!		 
!  ---------------------------------------------------------------------------------------
!
!  ---------------------------------------------------------------------------------------
!   routines      : description
!                 : 
!                 : 
!                 : 
!                 :
!  ---------------------------------------------------------------------------------------
!  $Id: beamforming_simulated_annealing.f90 156 2018-10-12 13:14:40Z lecointre $
!  ---------------------------------------------------------------------------------------

PROGRAM BEAMFORMING_SIMULATED_ANNEALING
  USE HDF5
  USE H5LT      ! Lite Interface HDF5
  USE modcomputebeam
  USE glovarmodule , only : phi_data,phi_model,CSDMP,nn_removezero,nkzero,N
  IMPLICIT NONE

  INTEGER :: kk,kki ! Loop integer for stations
  INTEGER :: ios ! dummy loop char index
  INTEGER :: iwcl,Nr ! The number of line in a file
  CHARACTER(len=256), DIMENSION(:), ALLOCATABLE :: staname ! the stations name
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: xr,yr,zr,lat,lon ! the stations coordinates
  REAL(KIND=4), DIMENSION(:), ALLOCATABLE :: rxr,ryr,rzr,rlat,rlon ! the stations coordinates
  INTEGER :: nivBruit,normalisation,azimuth,Fs
  INTEGER :: method,ln_csdm
  REAL(KIND=4), DIMENSION(:,:), ALLOCATABLE :: rdata,rdata_large
  LOGICAL :: l_exist_namelist
  REAL(KIND=8) :: tmin,tmax,tstep,xmin,xmax,xstep,ymin,ymax,ystep,zmin,zmax,zstep,cmin,cmax,cstep,fmin,fmax,fstep
  INTEGER(HID_T)     :: infile_id,outfile_id
  INTEGER(HSIZE_T), DIMENSION(1) :: startin1
  INTEGER(HSIZE_T), DIMENSION(1) :: countin1
  INTEGER(HSIZE_T), DIMENSION(1) :: dims1
  INTEGER(HSIZE_T), DIMENSION(2) :: dims2
  INTEGER(HSIZE_T), DIMENSION(3) :: dims3
  INTEGER                        :: error
  INTEGER :: ff,i ! ,nn_npt
  INTEGER :: jbeg,njlen,jlen,nit,it,Nrit,joverlap,offset
  CHARACTER(len=256) :: cn_ifile
  INTEGER :: Nf,Nc,Nx,Ny,Nz
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: freq,cel,xgrid,ygrid,zgrid
  REAL(KIND=4), DIMENSION(:), ALLOCATABLE :: rfreq,rcel,rxgrid,rygrid,rzgrid

  INTEGER :: NSA,i_restart,Step_param,N_pdf
  REAL(KIND=8) :: T0,T0min,T_param_beg,T_param_end
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: xBa ! (N_pdf+1,N)
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: fBa   ! (N_pdf+1)
  REAL(KIND=4), DIMENSION(:,:,:), ALLOCATABLE :: rxBa ! (N_pdf+1,N)
  REAL(KIND=4), DIMENSION(:,:), ALLOCATABLE :: rfBa   ! (N_pdf+1)
  INTEGER(KIND=4), DIMENSION(:,:), ALLOCATABLE :: patchesInd ! logical for stations = zero or not

!REAL(KIND=8), DIMENSION(15000) :: rmsSA,likelihoodSA,TSA
!INTEGER, DIMENSION(15000) :: bSA
  INTEGER :: nn_latlon,nn_io

  ! Instrumentation du code pour analyse de performance
  INTEGER,DIMENSION(8) :: values
  REAL(KIND=4) :: elps,sval0,svali           ! in sec

  REAL(KIND=8) :: weight

 ! Declare NAMELIST
  NAMELIST /namparam/ method,ln_csdm,nivBruit,normalisation,azimuth,Fs,cn_ifile,jbeg,jlen,joverlap,nit
  NAMELIST /namio/ nn_io
  NAMELIST /namtheta/ tmin,tmax,tstep
  NAMELIST /namxy/ xmin,xmax,xstep,ymin,ymax,ystep
  NAMELIST /namHsource/ zmin,zmax,zstep
  NAMELIST /namspeed/ cmin,cmax,cstep
  NAMELIST /namindice_freq/ fmin,fmax,fstep
  NAMELIST /namsa/ NSA,i_restart,Step_param,N_pdf,T0,T0min,T_param_beg,T_param_end,N
  NAMELIST /namcoord/ nn_latlon
  NAMELIST /naminput/ nn_removezero
  NAMELIST /namborehole/ weight

  CALL date_and_time(VALUES=values)
  svali=values(5)*3600+values(6)*60+values(7)+0.001*values(8)

  ! Read namelist
  INQUIRE(file='namelist', exist=l_exist_namelist)
  IF (.NOT. l_exist_namelist) THEN
     PRINT *,'The namelist does not exist'
     STOP
  ENDIF
  OPEN(11,file='namelist')
    REWIND 11
    READ(11,namparam)
    REWIND 11
    READ(11,namio)
    REWIND 11
    READ(11,namtheta)
    REWIND 11
    READ(11,namxy)
    REWIND 11
    READ(11,namHsource)
    REWIND 11
    READ(11,namspeed)
    REWIND 11
    READ(11,namindice_freq)
    REWIND 11
    READ(11,namsa)
    REWIND 11
    READ(11,namcoord)
    REWIND 11
    READ(11,naminput)
    REWIND 11
    READ(11,namborehole)
  CLOSE(11)

  IF ( method == 1 ) THEN !    if method == 'mvdr':
    print *, 'method = 1 (mvdr) not available in case of simulated annealing'
    print *, 'use bartlett instead'
    CALL exit(1)
  ENDIF
  IF ( azimuth == 1 ) THEN   ! c,theta
    print *, 'azimuth = 1 not available in case of simulated annealing'
    CALL exit(1)
  ENDIF
  IF ( ln_csdm == 0 ) THEN
    IF ( nivBruit /= 0 ) THEN
      print *, 'nivBruit /= 0 (Add noise on CSDM diagonal) not available in case of simulated annealing and ln_csdm = 0'
      print *, 'use nivBruit = 0 or use ln_csdm = 1'
      CALL exit(1)
    ENDIF
    IF ( normalisation /= 1 ) THEN
      print *, 'normalisation /= 1 (whitening) not available in case of simulated annealing and ln_csdm = 0'
      print *, 'All the CSDM terms must have modulus = 1 (bartlett is computed as sum of exp(j*(phimodel-phidata)))'
      print *, 'use normalisation = 1 or use ln_csdm = 1'
      CALL exit(1)
    ENDIF
  ENDIF

! Read the station list and their geographical location from zreseau.txt input file
  iwcl=0
  OPEN(10, file="zreseau.txt", form="formatted", iostat=ios,status="old")
  DO WHILE (ios==0)
    READ(10,*,iostat=ios)
    IF (ios==0) iwcl=iwcl+1
  ENDDO
  Nr=iwcl
  CLOSE(10)
  IF ( nn_latlon == 1 ) THEN
    ALLOCATE(staname(Nr),lat(Nr),lon(Nr),xr(Nr),yr(Nr),zr(Nr))
    OPEN(10, file="zreseau.txt")
    DO kk=1,Nr
      READ(UNIT=10,FMT=*) staname(kk),lon(kk),lat(kk),zr(kk)
      CALL distlatlong(lat(1), lon(1), lat(kk), lon(kk), xr(kk), yr(kk))
    ENDDO
    CLOSE(10)
    xr(:)=xr(:)-SUM(xr(:))/Nr
    yr(:)=yr(:)-SUM(yr(:))/Nr
! on ne centre pas la grille en zr ?
  ELSE
    ALLOCATE(staname(Nr),xr(Nr),yr(Nr),zr(Nr))
    OPEN(10, file="zreseau.txt")
    DO kk=1,Nr
      READ(UNIT=10,FMT=*) staname(kk),xr(kk),yr(kk),zr(kk)
    ENDDO
    CLOSE(10)
  ENDIF

  ! Initialize HDF5 context and open input file
  CALL h5open_f(error)
  if (error/=0) call exit(error)
  CALL h5fopen_f(TRIM(cn_ifile), H5F_ACC_RDONLY_F, infile_id, error)
  if (error/=0) call exit(error)

  IF ( MOD(nit,nn_io) /= 0 ) THEN
    print *,'Warning, nit:',nit,' nn_io:',nn_io
    nit = nit - MOD(nit,nn_io)
    print *,'Set nit to a new value:',nit 
  ENDIF

  IF ( nn_io == nit ) THEN
! Read input data using read_one_trace subroutine
    njlen=joverlap*(nit-1)+jlen ! nit*jlen
    ALLOCATE(rdata_large(njlen,Nr))  ! data is only used to compute the Cross Spectral Density Matrix ( K[Nr,Nr,Nf] )
    countin1=(/njlen/)
    startin1=(/jbeg/)
    print *,'Read input data: offset,count: ',startin1,countin1
    DO kk=1,Nr
      IF (MOD(kk,500)==0)  print *,'lecture',kk,TRIM(staname(kk)),'/',Nr
      CALL read_one_trace(infile_id,"/"//TRIM(staname(kk)),1,startin1,countin1,njlen,rdata_large(:,kk))
    ENDDO
  ELSE
    njlen=joverlap*(nn_io-1)+jlen
    ALLOCATE(rdata_large(njlen,Nr))
    countin1=(/njlen/)
    startin1=(/jbeg/)
  ENDIF

  ! Metrics
  Nc=(cmax-cmin)/cstep+1
  ALLOCATE(cel(Nc))
  cel = (/(cmin + ((i-1)*cstep),i=1,Nc)/)
  IF ( fstep < 0 ) THEN
    Nf=INT(-fstep)
    ALLOCATE(freq(Nf))
    OPEN(10, file="freq.txt")
    DO ff=1,Nf
      READ(UNIT=10,FMT=*) freq(ff)
    ENDDO
    CLOSE(10)
  ELSE
    Nf=(fmax-fmin)/fstep+1
    ALLOCATE(freq(Nf))
    freq = (/(fmin + ((i-1)*fstep),i=1,Nf)/) ! assign to y in increments of fstep starting at fmin
  ENDIF
  Nx=(xmax-xmin)/xstep+1
  ALLOCATE(xgrid(Nx))
  xgrid = (/(xmin + ((i-1)*xstep),i=1,Nx)/)
  Ny=(ymax-ymin)/ystep+1
  ALLOCATE(ygrid(Ny))
  ygrid = (/(ymin + ((i-1)*ystep),i=1,Ny)/)
  Nz=(zmax-zmin)/zstep+1
  ALLOCATE(zgrid(Nz))
  zgrid = (/(zmin + ((i-1)*zstep),i=1,Nz)/)

  ! Temporary array re-used for each it
  IF ( nn_removezero == 0) THEN
    ALLOCATE(rdata(jlen,Nr))
    ALLOCATE(CSDMP(Nr*(Nr+1)/2,Nf))
    ALLOCATE(phi_data(Nr,Nf),phi_model(Nr))
  ENDIF
!  ALLOCATE(CSDM(Nr,Nr,Nf))
  ALLOCATE(xBa(N_pdf+1,N),fBa(N_pdf+1),rxBa(N_pdf+1,N,nit),rfBa(N_pdf+1,nit))

  ALLOCATE(nkzero(Nr),patchesInd(Nr,nit))

  CALL init_random_seed() ! on init la graine une seule fois au depart
                          ! a partir de param d'horloge

  CALL date_and_time(VALUES=values)
  sval0=values(5)*3600+values(6)*60+values(7)+0.001*values(8)
  elps=sval0-svali
  print *,'read inputs and init:',elps

  DO it=1,nit

!    CALL init_random_seed0() ! on re-init toujours la meme graine pour s'assurer des resultats previsibles

    IF ( nn_io == nit ) THEN ! Full input dataset already read before iterate

      ! compute offset to access rdata_large in memory
      offset = joverlap*(it-1)+1

    ELSE ! must read input every nn_io iterations

      IF ( MOD(it-1,nn_io) == 0 ) THEN ! read nn_io input steps
        print *,'Read input data: offset,count: ',startin1,countin1
        DO kk=1,Nr
        IF (MOD(kk,500)==0)  print *,'lecture',kk,TRIM(staname(kk)),'/',Nr
          CALL read_one_trace(infile_id,"/"//TRIM(staname(kk)),1,startin1,countin1,njlen,rdata_large(:,kk))
        ENDDO
        ! increment startin1
        startin1 = startin1 + ( nn_io * joverlap )
      ENDIF

      ! compute offset to access rdata_large in memory
      offset = joverlap*MOD(it-1,nn_io)+1 ! joverlap*(it-1)+1

    ENDIF

    ! access rdata_large in memory, compute patchsInd,nkzero,Nrit
    nkzero(:)=1
    print *,'dataset jbeg jend :',offset,offset+jlen-1
    DO kk=1,Nr
      IF ( MAXVAL(ABS(rdata_large(offset:offset+jlen-1,kk))) == 0 ) nkzero(kk) = 0 ! mask for zero data
    ENDDO
    patchesInd(:,it)=nkzero(:)
    Nrit=SUM(nkzero)
    IF ( nn_removezero == 1 ) THEN
      print *,'Remove zero data and use only ',Nrit,' dataset / ',Nr
      ALLOCATE(rdata(jlen,Nrit))
      kki=0
      DO kk=1,Nr
        IF ( nkzero(kk) == 1 ) THEN
          kki=kki+1
          rdata(:,kki) = rdata_large(offset:offset+jlen-1,kk)
        ENDIF
      ENDDO 
    ELSE
      print *,'Nrit/Nr: ',Nrit,Nr,' but keep zero dataset'
      DO kk=1,Nr
        rdata(1:jlen,kk)=rdata_large(offset:offset+jlen-1,kk)
      ENDDO
      Nrit=Nr
    ENDIF

    ! Compute the Cross-Spectral Density Matrix K[Nr,Nr,Nf]
    IF ( nn_removezero == 1 ) THEN
      IF ( ln_csdm == 1 ) THEN
        ALLOCATE(CSDMP(Nrit*(Nrit+1)/2,Nf))
      ELSE
        ALLOCATE(phi_data(Nrit,Nf),phi_model(Nrit))
      ENDIF
    ENDIF

    IF ( ln_csdm == 1 ) THEN
      CALL computecsdmp_SA(jlen,Nrit,rdata,Nf,normalisation,freq,Fs,nivBruit) 
!      CALL computecsdm_SA(jlen,Nrit,rdata,Nf,normalisation,freq,Fs,nivBruit) 
    ELSE
      CALL compute_phidata(jlen,Nrit,rdata,Nf,freq,Fs)
    ENDIF

    IF ( nn_removezero == 1)   DEALLOCATE(rdata)

    ! Apply weight to borehole data: borehole must be first data (first column of packed storage CSDM lower part)
    IF ( ln_csdm == 1 ) THEN
      CSDMP(1,:) = weight * weight * CSDMP(1,:)
      DO kk = 2,Nrit
        CSDMP(kk,:) = weight * CSDMP(kk,:)
      ENDDO
    ENDIF

    IF ( N == 3 ) THEN
      CALL recuit_simule_MFP_X_Y_V(xgrid,ygrid,xr,yr,freq,cel,Nr,Nx,Ny,Nf,Nc,weight, &
                                   NSA,i_restart,Step_param,N_pdf, &
                                   T0,T0min,T_param_beg,T_param_end,ln_csdm,xBa,fBa) !, & 
!                                   rmsSA,likelihoodSA,TSA,bSA)  
    ELSEIF ( N == 4 ) THEN
      CALL recuit_simule_MFP_X_Y_Z_V(xgrid,ygrid,zgrid,xr,yr,zr,freq,cel,Nr,Nx,Ny,Nz,Nf,Nc,weight, &
                                     NSA,i_restart,Step_param,N_pdf, &
                                     T0,T0min,T_param_beg,T_param_end,ln_csdm,xBa,fBa) !, & 
    ENDIF

    IF ( nn_removezero == 1) THEN
      IF ( ln_csdm == 1 ) THEN
        DEALLOCATE(CSDMP)
      ELSE
        DEALLOCATE(phi_data,phi_model)
      ENDIF
    ENDIF

    ! Save result(it) as SP real
    rxBa(:,:,it)=REAL(xBa(:,:))
    rfBa(:,it)=REAL(fBa(:))

    CALL date_and_time(VALUES=values)
    svali=values(5)*3600+values(6)*60+values(7)+0.001*values(8)
    elps=svali-sval0
    sval0=svali
    print *,'nit | totsec | sec/trace:',it,elps,elps/Nr

  ENDDO ! End loop on iterations over 1800 sec

  DEALLOCATE(rdata_large)

  ! close input file
  CALL h5fclose_f(infile_id, error)
  if (error/=0) call exit(error)

  ! Create output file
  CALL h5fcreate_f("./out.h5", H5F_ACC_TRUNC_F, outfile_id, error)
  if (error/=0) call exit(error)

  ! Write outputs
  ALLOCATE(rxr(Nr),ryr(Nr),rzr(Nr),rlat(Nr),rlon(Nr),rcel(Nc),rfreq(Nf),rxgrid(Nx),rygrid(Ny),rzgrid(Nz))
  dims1=(/Nr/)
  rxr(:)=REAL(xr(:))
  CALL h5ltmake_dataset_float_f(outfile_id,"Xr",1,dims1,rxr(:),error)
  if (error/=0) call exit(error)
  ryr(:)=REAL(yr(:))
  CALL h5ltmake_dataset_float_f(outfile_id,"Yr",1,dims1,ryr(:),error)
  if (error/=0) call exit(error)
  rzr(:)=REAL(zr(:))
  CALL h5ltmake_dataset_float_f(outfile_id,"Zr",1,dims1,rzr(:),error)
  if (error/=0) call exit(error)
  rlon(:)=REAL(lon(:))
  CALL h5ltmake_dataset_float_f(outfile_id,"lon",1,dims1,rlon(:),error)
  if (error/=0) call exit(error)
  rlat(:)=REAL(lat(:))
  CALL h5ltmake_dataset_float_f(outfile_id,"lat",1,dims1,rlat(:),error)
  if (error/=0) call exit(error)
  dims1=(/Nc/)
  rcel(:)=REAL(cel(:))
  CALL h5ltmake_dataset_float_f(outfile_id,"speed",1,dims1,rcel(:),error)
  if (error/=0) call exit(error)
  dims1=(/Nf/)
  rfreq(:)=REAL(freq(:))
  CALL h5ltmake_dataset_float_f(outfile_id,"freq",1,dims1,rfreq(:),error)
  if (error/=0) call exit(error)
  dims1=(/Nx/)
  rxgrid=REAL(xgrid(:))
  CALL h5ltmake_dataset_float_f(outfile_id,"xgrid",1,dims1,rxgrid(:),error)
  if (error/=0) call exit(error)
  dims1=(/Ny/)
  rygrid=REAL(ygrid(:))
  CALL h5ltmake_dataset_float_f(outfile_id,"ygrid",1,dims1,rygrid(:),error)
  if (error/=0) call exit(error)
  dims1=(/Nz/)
  rzgrid=REAL(zgrid(:))
  CALL h5ltmake_dataset_float_f(outfile_id,"zgrid",1,dims1,rzgrid(:),error)
  if (error/=0) call exit(error)
  dims3=(/N_pdf+1,N,nit/)
  CALL h5ltmake_dataset_float_f(outfile_id,"xBa",3,dims3,rxBa(:,:,:),error)
  if (error/=0) call exit(error)
  dims2=(/N_pdf+1,nit/)
  CALL h5ltmake_dataset_float_f(outfile_id,"fBa",2,dims2,rfBa(:,:),error)
  if (error/=0) call exit(error)
!dims1=(/NSA/)
!CALL h5ltmake_dataset_float_f(outfile_id,"rmsSA",1,dims1,REAL(rmsSA(:)),error)
!if (error/=0) call exit(error)
!CALL h5ltmake_dataset_float_f(outfile_id,"lSA",1,dims1,REAL(likelihoodSA(:)),error)
!if (error/=0) call exit(error)
!CALL h5ltmake_dataset_float_f(outfile_id,"TSA",1,dims1,REAL(TSA(:)),error)
!if (error/=0) call exit(error)
!CALL h5ltmake_dataset_int_f(outfile_id,"bSA",1,dims1,bSA(:),error)
!if (error/=0) call exit(error)

  CALL h5fclose_f(outfile_id, error)
  if (error/=0) call exit(error)

  CALL h5close_f(error)
  if (error/=0) call exit(error)

  CALL date_and_time(VALUES=values)
  elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)-sval0
  print *,'write outputs:',elps

END PROGRAM BEAMFORMING_SIMULATED_ANNEALING
