!###############################################################################
!#    This file is part of MFP : a Fortran code for computing MFP              #
!#    (Matched Field Processing), also named bartlett or beamforming,          #
!#    over a regular grid on (x,y,z,c) or (theta,c).                           #
!#                                                                             #
!#    Copyright (C) 2017 Albanne Lecointre                                     #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################


!#---------------------------------
!$Id: glovarmodule.f90 160 2019-01-18 15:54:44Z lecointre $
!#---------------------------------

MODULE glovarmodule
  USE HDF5

  IMPLICIT NONE

#ifdef lk_float
  COMPLEX(KIND=4), DIMENSION(:,:,:), ALLOCATABLE :: CSDM
  COMPLEX(KIND=4), DIMENSION(:,:), ALLOCATABLE :: CSDMP ! Packed Storage CSDM (UPLO='L')
#else
  COMPLEX(KIND=8), DIMENSION(:,:,:), ALLOCATABLE :: CSDM
  COMPLEX(KIND=8), DIMENSION(:,:), ALLOCATABLE :: CSDMP
#endif
  INTEGER :: ln_coh, ref_inode_coh
  INTEGER :: ln_randomdata
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: phi_data ! Nr,Nf
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: phi_model ! Nr
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: xr,yr,zr,freq
  INTEGER :: N
  INTEGER :: Nr,Nf
  REAL(KIND=8) :: cel,zzz
  INTEGER, DIMENSION(:), ALLOCATABLE :: nkzero
  INTEGER :: nn_removezero
  INTEGER :: nevals ! global counter for the objective function number of evaluations (NLopt)

  REAL(KIND=4), DIMENSION(:,:,:), ALLOCATABLE :: dphi  ! only for nn_diags
  REAL(KIND=4), DIMENSION(:,:), ALLOCATABLE :: Baf,xyzcBB  ! only for nn_diags

  INTEGER :: Ngc  ! number of grid cells for 3D velocity model
  INTEGER(KIND=4), DIMENSION(:,:), ALLOCATABLE :: gridlocs
!  LOGICAL, DIMENSION(:), ALLOCATABLE :: gridlocsmask
  INTEGER(HID_T)     :: vmfile_id
  integer, dimension(1) :: minN
  integer, dimension(3) :: xgr
#ifdef lk_gridcell_double
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: ttgr
#else
  REAL(KIND=4), DIMENSION(:), ALLOCATABLE :: ttgr
#endif
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: distxgr ! distance[Nr] between current velocity model gridnode and the Nr stations

! fsurf function parameters for z penalty (zpen option), read from namelist nambound
  REAL(KIND=8) :: dn_c0, dn_cx, dn_cxx, dn_cy, dn_cyy, dn_cxy
  REAL(KIND=8) :: dn_cxxx, dn_cyyy, dn_cxxy, dn_cxyy
  REAL(KIND=8) :: dn_cxxxx, dn_cyyyy, dn_cxxxy, dn_cxxyy, dn_cxyyy
  REAL(KIND=8) :: dn_zwidth

END MODULE glovarmodule

