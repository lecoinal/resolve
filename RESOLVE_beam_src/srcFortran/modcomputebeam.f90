!###############################################################################
!#    This file is part of MFP : a Fortran code for computing MFP              #
!#    (Matched Field Processing), also named bartlett or beamforming,          #
!#    over a regular grid on (x,y,z,c) or (theta,c).                           #
!#                                                                             #
!#    Copyright (C) 2017 Albanne Lecointre                                     #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################



!=======================================================================================
!                       ***  Fortran code modcomputebeam.f90  ***
!   This is the Fortran module associated to beamforming Fortran code
!    
!
!=======================================================================================
!   History :  1.0  : 03/2017  : A. Lecointre : Initial version
!               
!       describe here       
!       describe here
!       describe here    
!
!---------------------------------------------------------------------------------------
!
!
!---------------------------------------------------------------------------------------
!   routines      : description
!                 : 
!                 : 
!                 : 
!                 :
!
!---------------------------------------------------------------------------------------
!  $Id: modcomputebeam.f90 155 2018-10-11 13:00:43Z lecointre $
!---------------------------------------------------------------------------------------
!  

#ifdef lk_integer
#  define MY_HDF5_INPUT_TYPE H5T_NATIVE_INTEGER
#else
#  define MY_HDF5_INPUT_TYPE H5T_NATIVE_REAL
#endif

MODULE modcomputebeam

 implicit none

contains

SUBROUTINE get_m2jPiF_over_c(frequencies, celerities, Nf, Nc, m2jPiF_over_c)
INTEGER, INTENT(IN) :: Nf ! Number of frequencies
INTEGER, INTENT(IN) :: Nc ! Number of celerities
REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: frequencies
REAL(KIND=8), DIMENSION(Nc), INTENT(IN) :: celerities
COMPLEX(KIND=8) :: m2jPi 
COMPLEX(KIND=8), DIMENSION(Nf,Nc), INTENT(OUT) :: m2jPiF_over_c
INTEGER :: cc,ff

m2jPi = -2.d0 * (0.d0,1.d0) * 4.d0 * atan(1.d0)
do cc=1,Nc
  do ff=1,Nf
    m2jPiF_over_c(ff,cc) = (frequencies(ff) / celerities(cc)) * m2jPi
  end do
end do
END SUBROUTINE get_m2jPiF_over_c

SUBROUTINE recuit_simule_MFP_X_Y_V(xg,yg,xr,yr,freq,c,Nr,Nx,Ny,Nf,Nc,W,NSA,i_restart,Step_param, &
                                   N_pdf,T0,Tmin,T_param_beg,T_param_end,ln_csdm,X1,Bart_f) ! , & 
!                                   RMS_SA,likelihood_SA,T_SA,b_SA)
USE glovarmodule, only : N
INTEGER, INTENT(IN) :: ln_csdm
INTEGER, INTENT(IN) :: Nr ! Array size: nb of sensors
INTEGER, INTENT(IN) :: Nf ! Number of frequencies
INTEGER, INTENT(IN) :: Nc ! Number of celerities
INTEGER, INTENT(IN) :: Nx,Ny ! Array size: nb of searching grid points in the xy directions
REAL(KIND=8), DIMENSION(Nr), INTENT(IN) :: xr,yr ! Position of sensors
REAL(KIND=8), DIMENSION(Nx), INTENT(IN) :: xg ! search grid
REAL(KIND=8), DIMENSION(Ny), INTENT(IN) :: yg ! search grid
REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: freq ! frequencies
REAL(KIND=8), DIMENSION(Nc), INTENT(IN) :: c ! celerities
INTEGER, INTENT(IN) :: NSA,i_restart,Step_param,N_pdf
REAL(KIND=8), INTENT(IN) :: W,T0,Tmin,T_param_beg,T_param_end
REAL(KIND=8), DIMENSION(N_pdf+1,N), INTENT(OUT) :: X1
REAL(KIND=8), DIMENSION(N_pdf+1), INTENT(OUT) :: Bart_f

REAL(KIND=8), DIMENSION(N) :: MAX_candidate,MIN_candidate,delta_candidate,delta_candidate_T_param
REAL(KIND=8), DIMENSION(NSA,N) :: X
REAL(KIND=8), DIMENSION(N) :: Xisam1
REAL(KIND=8), DIMENSION(NSA) :: inv_2_T_SA_2  ! 1 / ( 2 * T_SA * T_SA )
REAL(KIND=8), DIMENSION(NSA) :: likelihood_SA,RMS_SA,T_SA
!REAL(KIND=8), DIMENSION(NSA),INTENT(out) :: likelihood_SA,RMS_SA,T_SA
!INTEGER, DIMENSION(NSA), INTENT(OUT) :: b_SA
REAL(KIND=8) :: Bartlett
COMPLEX(KIND=8) :: p2jPi
REAL(KIND=8) :: m2Pi
INTEGER :: i 
INTEGER, DIMENSION(1) :: idx
REAL(KIND=8) :: T_SA_step
REAL(KIND=8), DIMENSION(N) :: Xcand
INTEGER :: isa
REAL(KIND=8) :: test_likelihood,likelihood_cand,RMS_cand,rn
REAL(KIND=8), DIMENSION(NSA) :: T_param
REAL(KIND=8) :: T_param_step

REAL(KIND=8) :: inv_Nr_2,inv_Nf

inv_Nr_2 = 1.d0 / ( (Nr-1+W) * (Nr-1+W) )
inv_Nf = 1.d0 / Nf

!b_SA(:)=0

MAX_candidate(1) = MAXVAL(xg)
MAX_candidate(2) = MAXVAL(yg)
MAX_candidate(3) = MAXVAL(c)
MIN_candidate(1) = MINVAL(xg)
MIN_candidate(2) = MINVAL(yg)
MIN_candidate(3) = MINVAL(c)
likelihood_SA(:) = 0.d0
RMS_SA(:) = 9999.d0   ! huge or NaN value ?
delta_candidate(:) = MAX_candidate(:) - MIN_candidate(:)
X(:,:) = 0.d0
X(1,1) = SUM(xg)/Nx
X(1,2) = SUM(yg)/Ny
X(1,3) = SUM(c)/Nc

!T_param(:) = 1.d0 / Step_param
T_param_step = (T_param_end-T_param_beg)/(NSA-1)
T_param(1:NSA) = (/(T_param_beg + ((i-1)*T_param_step),i=1,NSA)/)
T_param(:) = T_param(:) / Step_param
m2Pi = - 2.d0 * 4.d0 * atan(1.d0)
p2jPi = 2.d0 * (0.d0,1.d0) * 4.d0 * atan(1.d0)
Xcand(:) = X(1,:)
IF ( ln_csdm == 1 ) THEN
  CALL compute_Bartlett(Xcand,Nr,xr,yr,p2jPi,inv_Nr_2,inv_Nf,Nf,freq,Bartlett)
ELSE
  CALL compute_Bartlett_exp(Xcand,Nr,xr,yr,m2Pi,inv_Nr_2,inv_Nf,Nf,freq,W,Bartlett)
ENDIF
RMS_SA(1) = log(1/Bartlett)
T_SA_step = (log(Tmin)-log(T0))/(NSA-N_pdf-1)
T_SA(1:NSA-N_pdf) = (/(log(T0) + ((i-1)*T_SA_step),i=1,NSA-N_pdf)/)
T_SA(NSA-N_pdf+1:NSA)=T_SA(NSA-N_pdf)
T_SA = EXP(T_SA(:))
inv_2_T_SA_2(:) = 1.d0 / (2.d0 * T_SA(:) * T_SA(:))

likelihood_SA(1) = EXP(-(RMS_SA(1)**2) * inv_2_T_SA_2(1))


! Extraction des parametres
DO isa = 2,NSA

  IF ( MOD(isa,i_restart)==0 ) THEN
    idx = MINLOC(RMS_SA(1:NSA-1))
    RMS_SA(isa) = RMS_SA(idx(1))
    likelihood_SA(isa) = EXP(-(RMS_SA(isa)**2) * inv_2_T_SA_2(isa))
    X(isa,:) = X(idx(1),:)
  ELSE
    Xisam1(:) = X(isa-1,:)
    delta_candidate_T_param(:) = delta_candidate(:)*T_param(isa)
    CALL generate_params(Xisam1,delta_candidate_T_param,MAX_candidate,MIN_candidate,Xcand)
    IF ( ln_csdm == 1 ) THEN
      CALL compute_Bartlett(Xcand,Nr,xr,yr,p2jPi,inv_Nr_2,inv_Nf,Nf,freq,Bartlett)
    ELSE
      CALL compute_Bartlett_exp(Xcand,Nr,xr,yr,m2Pi,inv_Nr_2,inv_Nf,Nf,freq,W,Bartlett)
    ENDIF

    ! Choix des prochains candidats

    RMS_cand = log(1/Bartlett)
    likelihood_cand = EXP(-(RMS_cand**2) * inv_2_T_SA_2(isa))
    test_likelihood = MIN(1.d0,likelihood_cand/likelihood_SA(isa-1))

    CALL random_number(rn)
    IF ( rn < test_likelihood ) THEN
!b_SA(isa)=1
      likelihood_SA(isa) = likelihood_cand
      X(isa,:) = Xcand
      RMS_SA(isa) = RMS_cand
    ELSE
      X(isa,:) = X(isa-1,:)
      RMS_SA(isa) = RMS_SA(isa-1)
      likelihood_SA(isa) = EXP(-(RMS_SA(isa)**2) * inv_2_T_SA_2(isa))
    ENDIF
  ENDIF

ENDDO ! End loop isa

Bart_f(:) = 1.d0 / EXP( RMS_SA(NSA-N_pdf:NSA) )
X1(:,:) = X(NSA-N_pdf:NSA,:)

END SUBROUTINE recuit_simule_MFP_X_Y_V

SUBROUTINE recuit_simule_MFP_X_Y_Z_V(xg,yg,zg,xr,yr,zr,freq,c,Nr,Nx,Ny,Nz,Nf,Nc,W,NSA,i_restart,Step_param, &
                                     N_pdf,T0,Tmin,T_param_beg,T_param_end,ln_csdm,X1,Bart_f)  
USE glovarmodule, only : N
INTEGER, INTENT(IN) :: ln_csdm
INTEGER, INTENT(IN) :: Nr ! Array size: nb of sensors
INTEGER, INTENT(IN) :: Nf ! Number of frequencies
INTEGER, INTENT(IN) :: Nc ! Number of celerities
INTEGER, INTENT(IN) :: Nx,Ny,Nz ! Array size: nb of searching grid points in the xy directions
REAL(KIND=8), DIMENSION(Nr), INTENT(IN) :: xr,yr,zr ! Position of sensors
REAL(KIND=8), DIMENSION(Nx), INTENT(IN) :: xg ! search grid
REAL(KIND=8), DIMENSION(Ny), INTENT(IN) :: yg ! search grid
REAL(KIND=8), DIMENSION(Nz), INTENT(IN) :: zg ! search grid
REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: freq ! frequencies
REAL(KIND=8), DIMENSION(Nc), INTENT(IN) :: c ! celerities
INTEGER, INTENT(IN) :: NSA,i_restart,Step_param,N_pdf
REAL(KIND=8), INTENT(IN) :: W,T0,Tmin,T_param_beg,T_param_end
REAL(KIND=8), DIMENSION(N_pdf+1,N), INTENT(OUT) :: X1
REAL(KIND=8), DIMENSION(N_pdf+1), INTENT(OUT) :: Bart_f

REAL(KIND=8), DIMENSION(N) :: MAX_candidate,MIN_candidate,delta_candidate,delta_candidate_T_param
REAL(KIND=8), DIMENSION(NSA,N) :: X
REAL(KIND=8), DIMENSION(N) :: Xisam1
REAL(KIND=8), DIMENSION(NSA) :: inv_2_T_SA_2  ! 1 / ( 2 * T_SA * T_SA )
REAL(KIND=8), DIMENSION(NSA) :: likelihood_SA,RMS_SA,T_SA
!REAL(KIND=8), DIMENSION(NSA),INTENT(out) :: likelihood_SA,RMS_SA,T_SA
!INTEGER, DIMENSION(NSA), INTENT(OUT) :: b_SA
REAL(KIND=8) :: Bartlett
COMPLEX(KIND=8) :: p2jPi
REAL(KIND=8) :: m2Pi
INTEGER :: i 
INTEGER, DIMENSION(1) :: idx
REAL(KIND=8) :: T_SA_step
REAL(KIND=8), DIMENSION(N) :: Xcand
INTEGER :: isa
REAL(KIND=8) :: test_likelihood,likelihood_cand,RMS_cand,rn
REAL(KIND=8), DIMENSION(NSA) :: T_param
REAL(KIND=8) :: T_param_step

REAL(KIND=8) :: inv_Nr_2,inv_Nf

inv_Nr_2 = 1.d0 / ( (Nr-1+W) * (Nr-1+W) )
inv_Nf = 1.d0 / Nf

!b_SA(:)=0

MAX_candidate(1) = MAXVAL(xg)
MAX_candidate(2) = MAXVAL(yg)
MAX_candidate(3) = MAXVAL(zg)
MAX_candidate(4) = MAXVAL(c)
MIN_candidate(1) = MINVAL(xg)
MIN_candidate(2) = MINVAL(yg)
MIN_candidate(3) = MINVAL(zg)
MIN_candidate(4) = MINVAL(c)
likelihood_SA(:) = 0.d0
RMS_SA(:) = 9999.d0   ! huge or NaN value ?
delta_candidate(:) = MAX_candidate(:) - MIN_candidate(:)
X(:,:) = 0.d0
X(1,1) = SUM(xg)/Nx
X(1,2) = SUM(yg)/Ny
X(1,3) = SUM(zg)/Nz
X(1,4) = SUM(c)/Nc

!T_param(:) = 1.d0 / Step_param
T_param_step = (T_param_end-T_param_beg)/(NSA-1)
T_param(1:NSA) = (/(T_param_beg + ((i-1)*T_param_step),i=1,NSA)/)
T_param(:) = T_param(:) / Step_param
m2Pi = - 2.d0 * 4.d0 * atan(1.d0)
p2jPi = 2.d0 * (0.d0,1.d0) * 4.d0 * atan(1.d0)
Xcand(:) = X(1,:)
IF ( ln_csdm == 1 ) THEN
  CALL compute_Bartlett_z(Xcand,Nr,xr,yr,zr,p2jPi,inv_Nr_2,inv_Nf,Nf,freq,Bartlett)
ELSE
  CALL compute_Bartlett_z_exp(Xcand,Nr,xr,yr,zr,m2Pi,inv_Nr_2,inv_Nf,Nf,freq,W,Bartlett)
ENDIF
RMS_SA(1) = log(1/Bartlett)
T_SA_step = (log(Tmin)-log(T0))/(NSA-N_pdf-1)
T_SA(1:NSA-N_pdf) = (/(log(T0) + ((i-1)*T_SA_step),i=1,NSA-N_pdf)/)
T_SA(NSA-N_pdf+1:NSA)=T_SA(NSA-N_pdf)
T_SA = EXP(T_SA(:))
inv_2_T_SA_2(:) = 1.d0 / (2.d0 * T_SA(:) * T_SA(:))

likelihood_SA(1) = EXP(-(RMS_SA(1)**2) * inv_2_T_SA_2(1))


! Extraction des parametres
DO isa = 2,NSA

  IF ( MOD(isa,i_restart)==0 ) THEN
    idx = MINLOC(RMS_SA(1:NSA-1))
    RMS_SA(isa) = RMS_SA(idx(1))
    likelihood_SA(isa) = EXP(-(RMS_SA(isa)**2) * inv_2_T_SA_2(isa))
    X(isa,:) = X(idx(1),:)
  ELSE
    Xisam1(:) = X(isa-1,:)
    delta_candidate_T_param(:) = delta_candidate(:)*T_param(isa)
    CALL generate_params(Xisam1,delta_candidate_T_param,MAX_candidate,MIN_candidate,Xcand)
    IF ( ln_csdm == 1 ) THEN
      CALL compute_Bartlett_z(Xcand,Nr,xr,yr,zr,p2jPi,inv_Nr_2,inv_Nf,Nf,freq,Bartlett)
    ELSE
      CALL compute_Bartlett_z_exp(Xcand,Nr,xr,yr,zr,m2Pi,inv_Nr_2,inv_Nf,Nf,freq,W,Bartlett)
    ENDIF

    ! Choix des prochains candidats

    RMS_cand = log(1/Bartlett)
    likelihood_cand = EXP(-(RMS_cand**2) * inv_2_T_SA_2(isa))
    test_likelihood = MIN(1.d0,likelihood_cand/likelihood_SA(isa-1))

    CALL random_number(rn)
    IF ( rn < test_likelihood ) THEN
!b_SA(isa)=1
      likelihood_SA(isa) = likelihood_cand
      X(isa,:) = Xcand
      RMS_SA(isa) = RMS_cand
    ELSE
      X(isa,:) = X(isa-1,:)
      RMS_SA(isa) = RMS_SA(isa-1)
      likelihood_SA(isa) = EXP(-(RMS_SA(isa)**2) * inv_2_T_SA_2(isa))
    ENDIF
  ENDIF

ENDDO ! End loop isa

Bart_f(:) = 1.d0 / EXP( RMS_SA(NSA-N_pdf:NSA) )
X1(:,:) = X(NSA-N_pdf:NSA,:)

END SUBROUTINE recuit_simule_MFP_X_Y_Z_V

#ifdef lk_float

SUBROUTINE compute_Bartlett(xyv,Nr,xr,yr,p2jPi,inv_Nr_2,inv_Nf,Nf,freq,bb)
USE glovarmodule , only : N,CSDMP,nn_removezero,nkzero ! share csdm matrix
INTEGER, INTENT(IN) :: Nr ! Array size: nb of sensors
INTEGER, INTENT(IN) :: Nf ! Number of frequencies
REAL(KIND=8), DIMENSION(Nr), INTENT(IN) :: xr,yr ! Position of sensors
REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: freq ! frequencies
REAL(KIND=8), INTENT(OUT) :: bb
COMPLEX(KIND=8) :: p2jPi
COMPLEX(KIND=4), DIMENSION(Nr) :: replica
REAL(KIND=4), DIMENSION(Nr) :: tt
INTEGER :: ff,kk,kki,Nr0
COMPLEX(KIND=4) :: CDOTC
REAL(KIND=8), DIMENSION(N), INTENT(IN) :: xyv
REAL(KIND=8), INTENT(IN) :: inv_Nr_2,inv_Nf
COMPLEX(KIND=4), DIMENSION(Nr) :: CC
COMPLEX(KIND=4) :: alpha,beta
alpha=(1.0,0.0)
beta=(0.0,0.0)
  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = REAL( ( ((xr(kk)-xyv(1))**2+(yr(kk)-xyv(2))**2)**0.5 ) / xyv(3) )  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = REAL( ( ((xr(:)-xyv(1))**2+(yr(:)-xyv(2))**2)**0.5 ) / xyv(3) )  ! [Nr]
  ENDIF
bb = 0.d0
DO ff=1,Nf
  replica(:) = EXP(p2jPi * tt(:) * freq(ff)) ! [Nr] 
  CALL CHPMV('L',Nr0,alpha,CSDMP(:,ff),replica(1:Nr0),1,beta,CC(1:Nr0),1)
  bb = bb + ABS(CDOTC(Nr,replica,1,CC,1)) * inv_Nr_2   
ENDDO
bb = bb * inv_Nf
END SUBROUTINE compute_Bartlett

SUBROUTINE compute_Bartlett_z(xyzv,Nr,xr,yr,zr,p2jPi,inv_Nr_2,inv_Nf,Nf,freq,bb)
USE glovarmodule , only : N,CSDMP,nn_removezero,nkzero ! share csdm matrix
INTEGER, INTENT(IN) :: Nr ! Array size: nb of sensors
INTEGER, INTENT(IN) :: Nf ! Number of frequencies
REAL(KIND=8), DIMENSION(Nr), INTENT(IN) :: xr,yr,zr ! Position of sensors
REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: freq ! frequencies
REAL(KIND=8), INTENT(OUT) :: bb
COMPLEX(KIND=8) :: p2jPi
COMPLEX(KIND=4), DIMENSION(Nr) :: replica
REAL(KIND=4), DIMENSION(Nr) :: tt
INTEGER :: ff,kk,kki,Nr0
COMPLEX(KIND=4) :: CDOTC
REAL(KIND=8), DIMENSION(N), INTENT(IN) :: xyzv
REAL(KIND=8), INTENT(IN) :: inv_Nr_2,inv_Nf
COMPLEX(KIND=4), DIMENSION(Nr) :: CC
COMPLEX(KIND=4) :: alpha,beta
alpha=(1.0,0.0)
beta=(0.0,0.0)
  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = REAL( ( ((xr(kk)-xyzv(1))**2+(yr(kk)-xyzv(2))**2+(zr(kk)-xyzv(3))**2)**0.5 ) / xyzv(4) )  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = REAL( ( ((xr(:)-xyzv(1))**2+(yr(:)-xyzv(2))**2+(zr(:)-xyzv(3))**2)**0.5 ) / xyzv(4) )  ! [Nr]
  ENDIF
bb = 0.d0
DO ff=1,Nf
  replica(:) = EXP(p2jPi * tt(:) * freq(ff)) ! [Nr] 
!  CALL CHEMV('L',Nr,alpha,CSDM(:,:,ff),Nr,replica,1,beta,CC,1)
  CALL CHPMV('L',Nr0,alpha,CSDMP(:,ff),replica(1:Nr0),1,beta,CC(1:Nr0),1)
  bb = bb + ABS(CDOTC(Nr,replica,1,CC,1)) * inv_Nr_2   
ENDDO
bb = bb * inv_Nf
END SUBROUTINE compute_Bartlett_z

#else

SUBROUTINE compute_Bartlett(xyv,Nr,xr,yr,p2jPi,inv_Nr_2,inv_Nf,Nf,freq,bb)
USE glovarmodule , only : N,CSDMP,nn_removezero,nkzero ! share csdm matrix
INTEGER, INTENT(IN) :: Nr ! Array size: nb of sensors
INTEGER, INTENT(IN) :: Nf ! Number of frequencies
REAL(KIND=8), DIMENSION(Nr), INTENT(IN) :: xr,yr ! Position of sensors
REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: freq ! frequencies
REAL(KIND=8), INTENT(OUT) :: bb
COMPLEX(KIND=8) :: p2jPi
COMPLEX(KIND=8), DIMENSION(Nr) :: replica
REAL(KIND=8), DIMENSION(Nr) :: tt
INTEGER :: ff,kk,kki,Nr0
COMPLEX(KIND=8) :: ZDOTC
REAL(KIND=8), DIMENSION(N), INTENT(IN) :: xyv
REAL(KIND=8), INTENT(IN) :: inv_Nr_2,inv_Nf
COMPLEX(KIND=8), DIMENSION(Nr) :: CC
COMPLEX(KIND=8) :: alpha,beta
alpha=(1.d0,0.d0)
beta=(0.d0,0.d0)
  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-xyv(1))**2+(yr(kk)-xyv(2))**2)**0.5 ) / xyv(3)  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-xyv(1))**2+(yr(:)-xyv(2))**2)**0.5 ) / xyv(3)  ! [Nr]
  ENDIF
bb = 0.d0
DO ff=1,Nf
  replica(:) = EXP(p2jPi * tt(:) * freq(ff)) ! [Nr] 
  CALL ZHPMV('L',Nr0,alpha,CSDMP(:,ff),replica(1:Nr0),1,beta,CC(1:Nr0),1)
  bb = bb + ABS(ZDOTC(Nr,replica,1,CC,1)) * inv_Nr_2   
ENDDO
bb = bb * inv_Nf
END SUBROUTINE compute_Bartlett

SUBROUTINE compute_Bartlett_z(xyzv,Nr,xr,yr,zr,p2jPi,inv_Nr_2,inv_Nf,Nf,freq,bb)
USE glovarmodule , only : N,CSDMP,nn_removezero,nkzero ! share csdm matrix
INTEGER, INTENT(IN) :: Nr ! Array size: nb of sensors
INTEGER, INTENT(IN) :: Nf ! Number of frequencies
REAL(KIND=8), DIMENSION(Nr), INTENT(IN) :: xr,yr,zr ! Position of sensors
REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: freq ! frequencies
REAL(KIND=8), INTENT(OUT) :: bb
COMPLEX(KIND=8) :: p2jPi
COMPLEX(KIND=8), DIMENSION(Nr) :: replica
REAL(KIND=8), DIMENSION(Nr) :: tt
INTEGER :: ff,kk,kki,Nr0
COMPLEX(KIND=8) :: ZDOTC
REAL(KIND=8), DIMENSION(N), INTENT(IN) :: xyzv
REAL(KIND=8), INTENT(IN) :: inv_Nr_2,inv_Nf
COMPLEX(KIND=8), DIMENSION(Nr) :: CC
COMPLEX(KIND=8) :: alpha,beta
alpha=(1.d0,0.d0)
beta=(0.d0,0.d0)
  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-xyzv(1))**2+(yr(kk)-xyzv(2))**2+(zr(kk)-xyzv(3))**2)**0.5 ) / xyzv(4)  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-xyzv(1))**2+(yr(:)-xyzv(2))**2+(zr(:)-xyzv(3))**2)**0.5 ) / xyzv(4)  ! [Nr]
  ENDIF
bb = 0.d0
DO ff=1,Nf
  replica(:) = EXP(p2jPi * tt(:) * freq(ff)) ! [Nr] 
  CALL ZHPMV('L',Nr0,alpha,CSDMP(:,ff),replica(1:Nr0),1,beta,CC(1:Nr0),1)
  bb = bb + ABS(ZDOTC(Nr,replica,1,CC,1)) * inv_Nr_2   
ENDDO
bb = bb * inv_Nf
END SUBROUTINE compute_Bartlett_z

#endif

SUBROUTINE compute_Bartlett_exp(xyv,Nr,xr,yr,m2Pi,inv_Nr_2,inv_Nf,Nf,freq,ww,bb)
USE glovarmodule , only : N,phi_model,phi_data,nn_removezero,nkzero ! share csdm matrix
INTEGER, INTENT(IN) :: Nr ! Array size: nb of sensors
INTEGER, INTENT(IN) :: Nf ! Number of frequencies
REAL(KIND=8), DIMENSION(Nr), INTENT(IN) :: xr,yr ! Position of sensors
REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: freq ! frequencies
REAL(KIND=8), INTENT(OUT) :: bb
REAL(KIND=8), INTENT(IN) :: m2Pi,ww
REAL(KIND=8), DIMENSION(Nr) :: tt
INTEGER :: ff,kk,kki,Nr0,ii
REAL(KIND=8), DIMENSION(N), INTENT(IN) :: xyv
REAL(KIND=8), INTENT(IN) :: inv_Nr_2,inv_Nf
COMPLEX(KIND=8) :: zz
COMPLEX(KIND=8), PARAMETER :: cj = (0.d0,1.d0)
  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-xyv(1))**2+(yr(kk)-xyv(2))**2)**0.5 ) / xyv(3)  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-xyv(1))**2+(yr(:)-xyv(2))**2)**0.5 ) / xyv(3)  ! [Nr]
  ENDIF
  tt(:) = tt(:) * m2Pi

  bb = 0.d0
  DO ff = 1, Nf
    phi_model(:) = tt(:)*freq(ff)
    zz = ww * EXP ( cj * (phi_data(1,ff)-phi_model(1)) )
    DO ii = 2, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
    ENDDO
    bb = bb + zz*CONJG(zz)
  ENDDO
  bb = bb * inv_Nr_2 * inv_Nf
END SUBROUTINE compute_Bartlett_exp

SUBROUTINE compute_Bartlett_z_exp(xyzv,Nr,xr,yr,zr,m2Pi,inv_Nr_2,inv_Nf,Nf,freq,ww,bb)
USE glovarmodule , only : N,phi_model,phi_data,nn_removezero,nkzero ! share csdm matrix
INTEGER, INTENT(IN) :: Nr ! Array size: nb of sensors
INTEGER, INTENT(IN) :: Nf ! Number of frequencies
REAL(KIND=8), DIMENSION(Nr), INTENT(IN) :: xr,yr,zr ! Position of sensors
REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: freq ! frequencies
REAL(KIND=8), INTENT(OUT) :: bb
REAL(KIND=8), INTENT(IN) :: m2Pi,ww
REAL(KIND=8), DIMENSION(Nr) :: tt
INTEGER :: ff,kk,kki,Nr0,ii
REAL(KIND=8), DIMENSION(N), INTENT(IN) :: xyzv
REAL(KIND=8), INTENT(IN) :: inv_Nr_2,inv_Nf
COMPLEX(KIND=8) :: zz
COMPLEX(KIND=8), PARAMETER :: cj = (0.d0,1.d0)
  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-xyzv(1))**2+(yr(kk)-xyzv(2))**2+(zr(kk)-xyzv(3))**2)**0.5 ) / xyzv(4)  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-xyzv(1))**2+(yr(:)-xyzv(2))**2+(zr(:)-xyzv(3))**2)**0.5 ) / xyzv(4)  ! [Nr]
  ENDIF
  tt(:) = tt(:) * m2Pi

  bb = 0.d0
  DO ff = 1, Nf
    phi_model(:) = tt(:)*freq(ff)
    zz = ww * EXP ( cj * (phi_data(1,ff)-phi_model(1)) )
    DO ii = 2, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
    ENDDO
    bb = bb + zz*CONJG(zz)
  ENDDO
  bb = bb * inv_Nr_2 * inv_Nf
END SUBROUTINE compute_Bartlett_z_exp

SUBROUTINE generate_params(X,delta_candidate,MAX_candidate,MIN_candidate,candidate)
USE glovarmodule , only : N
  REAL(KIND=8), DIMENSION(N), INTENT(IN) :: MAX_candidate,MIN_candidate,delta_candidate
  REAL(KIND=8), DIMENSION(N), INTENT(IN) :: X
  REAL(KIND=8), DIMENSION(N), INTENT(OUT) :: candidate
  REAL(KIND=8) :: maxi,mini,op1,op2,rn
  INTEGER :: kk
  DO kk=1,N
    op1 = X(kk) + delta_candidate(kk)
    op2 = MAX_candidate(kk)
    maxi = MIN(op1,op2)
    op1 = X(kk) - delta_candidate(kk)
    op2 = MIN_candidate(kk)
    mini = MAX(op1,op2)
    CALL random_number(rn)
    candidate(kk) = mini + (maxi-mini)*rn
  ENDDO
END SUBROUTINE generate_params

SUBROUTINE computebeam(xg,yg,zg,xr,yr,zr,freq,c,method,Nr,Nx,Ny,Nz,Nf,Nc,beam,beamstd,beamf)
USE glovarmodule , only : CSDM ! can be K or Kinv depending on mvdr or not
INTEGER, INTENT(IN) :: Nr ! Array size: nb of sensors
INTEGER, INTENT(IN) :: Nf ! Number of frequencies
INTEGER, INTENT(IN) :: Nc ! Number of celerities
INTEGER, INTENT(IN) :: Nx,Ny,Nz ! Array size: nb of searching grid points in the xyz directions
REAL(KIND=8), DIMENSION(Nr), INTENT(IN) :: xr,yr,zr ! Position of sensors
REAL(KIND=8), DIMENSION(Nx), INTENT(IN) :: xg ! search grid
REAL(KIND=8), DIMENSION(Ny), INTENT(IN) :: yg ! search grid
REAL(KIND=8), DIMENSION(Nz), INTENT(IN) :: zg ! search grid
REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: freq ! frequencies
REAL(KIND=8), DIMENSION(Nc), INTENT(IN) :: c ! celerities
INTEGER, INTENT(IN) :: method ! bartlett/mvdr
LOGICAL :: method_linear

INTEGER :: kk,ii,jj,cc,ff
REAL(KIND=8), DIMENSION(Nr) :: a ! , inv_pi_a_4 ! Distance source recepteur [Nr]
!REAL(KIND=8) :: pi,pi4
COMPLEX(KIND=8), DIMENSION(Nr) :: omega, conjOmega
COMPLEX(KIND=8), DIMENSION(Nr) :: replica

REAL(KIND=8), DIMENSION(Nx,Ny,Nz,Nc,Nf), INTENT(OUT) :: beamf
REAL(KIND=8), DIMENSION(Nx,Ny,Nz,Nc), INTENT(OUT) :: beam,beamstd
COMPLEX(KIND=8), DIMENSION(Nf,Nc) :: m2jPiF_over_c

REAL(KIND=8) :: inv_sqrt_Nr

inv_sqrt_Nr = 1.d0 / SQRT(DBLE(Nr))

!pi=4.d0*atan(1.d0)
!pi4 = 4.d0 * pi


if (method == 0) then
  method_linear = .true.
else if (method == 1) then
  method_linear = .false.
else
  print *,'undefined method : ',method
  return
end if

beam(:,:,:,:)=0.d0
beamstd(:,:,:,:)=0.d0
CALL get_m2jPiF_over_c(freq, c, Nf, Nc, m2jPiF_over_c(:,:))
do kk=1,Nz
  do ii=1,Nx
    do jj=1,Ny
      a(:) = ((xr(:)-xg(ii))**2+(yr(:)-yg(jj))**2+(zr(:)-zg(kk))**2)**0.5 ! [Nr]
!      inv_pi_a_4(:) = 1.d0 / ( pi4 * a(:) )
      do cc=1,Nc
        do ff=1,Nf
          omega(:) = EXP(a(:) * m2jPiF_over_c(ff,cc)) * inv_sqrt_Nr !* inv_pi_a_4(:)
!          omega(:) = omega(:) / ABS(omega(:))  ! multi par invpia4 puis normaliser est inutile...
!          omega(:) = omega(:) / sqrt(SUM(ABS(omega(:))**2))   ! pareil que / sqrt(Nr) ?
          conjOmega(:) = CONJG(omega(:))
          if (method_linear) then ! bartlett
  ! calcul de la norme de omega mais en fait omega est de norme 1 donc inutile de calculer sa norme...
            ! ancien: replica(:) = conjOmega(:) / sqrt(SUM(ABS(conjOmega(:))**2)) ~ conjOmega(:) / 1
            beamf(ii,jj,kk,cc,ff) = ABS(SUM(conjOmega(:)*MATMUL(CSDM(:,:,ff),omega(:))))
          else ! mvdr
            replica(:) = omega(:) / SUM(conjOmega(:) * MATMUL(CSDM(:,:,ff),omega(:)) ) / Nr
            beamf(ii,jj,kk,cc,ff) = ABS(SUM(CONJG(replica(:)) * MATMUL(CSDM(:,:,ff),replica(:))))
          end if
          beam(ii,jj,kk,cc) = beam(ii,jj,kk,cc)+beamf(ii,jj,kk,cc,ff)
        enddo
      enddo
    enddo
  enddo
enddo

beam(:,:,:,:) = beam(:,:,:,:)/Nf
do kk=1,Nz
  do ii=1,Nx
    do jj=1,Ny
      do cc=1,Nc
        beamstd(ii,jj,kk,cc) = SQRT( SUM((beamf(ii,jj,kk,cc,:) - beam(ii,jj,kk,cc))**2) / Nf )
      enddo
    enddo
  enddo
enddo

END SUBROUTINE computebeam

SUBROUTINE computebeamgrid_exp(Nx,Ny,Nz,Nc,xg,yg,zg,c,beam)
USE glovarmodule , only : Nr,xr,yr,zr,Nf,freq,phi_data,phi_model,nn_removezero,nkzero
INTEGER, INTENT(IN) :: Nx,Ny,Nz,Nc ! Array size: nb of searching grid points along the xyz directions and along celerities dimension
INTEGER :: kk,ii,jj,cc
REAL(KIND=8), DIMENSION(Nx), INTENT(IN) :: xg ! search grid
REAL(KIND=8), DIMENSION(Ny), INTENT(IN) :: yg ! search grid
REAL(KIND=8), DIMENSION(Nz), INTENT(IN) :: zg ! search grid
REAL(KIND=8), DIMENSION(Nc), INTENT(IN) :: c ! celerities: search grid
INTEGER :: kki,kkr,Nr0
INTEGER :: ff
REAL(KIND=8), DIMENSION(Nr) :: tt
REAL(KIND=8) :: m2Pi
REAL(KIND=8) :: buf
COMPLEX(KIND=8) :: zz
COMPLEX(KIND=8), PARAMETER :: cj = ( 0.d0, 1.d0 )
REAL(KIND=8), DIMENSION(Nx,Ny,Nz,Nc), INTENT(OUT) :: beam
m2Pi = - 8.d0 * atan(1.d0)
do kk=1,Nz
  do ii=1,Nx
    do jj=1,Ny
      do cc=1,Nc
        IF ( nn_removezero == 1 ) THEN
          kki=0
          DO kkr=1,Nr
            IF ( nkzero(kkr) == 1 ) THEN
              kki=kki+1
              tt(kki) = ( ((xr(kkr)-xg(ii))**2+(yr(kkr)-yg(jj))**2+(zr(kkr)-zg(kk))**2)**0.5 ) / c(cc)  ! [Nr]
            ENDIF
          ENDDO
          Nr0=kki
        ELSE
          Nr0=Nr
          tt(:) = ( ((xr(:)-xg(ii))**2+(yr(:)-yg(jj))**2+(zr(:)-zg(kk))**2)**0.5 ) / c(cc)  ! [Nr]
        ENDIF
        tt(1:Nr0) = m2Pi * tt(1:Nr0)
        buf = 0.d0
        DO ff = 1, Nf
          phi_model(1:Nr0) = tt(1:Nr0)*freq(ff)
          zz = (0.d0,0.d0)
          DO kki = 1, Nr0
            zz = zz + EXP ( cj * (phi_data(kki,ff)-phi_model(kki)) )
          ENDDO
          buf = buf + zz*CONJG(zz)
        ENDDO
        beam(ii,jj,kk,cc) = buf / (Nr0*Nr0*Nf)

      enddo
    enddo
  enddo
enddo
END SUBROUTINE computebeamgrid_exp

SUBROUTINE computebeamct(theta,xr,yr,freq,c,method,Nr,Nt,Nf,Nc,beam,beamstd,beamf)
USE glovarmodule , only : CSDM ! can be K or Kinv depending on mvdr or not
INTEGER, INTENT(IN) :: Nr ! Array size: nb of sensors
INTEGER, INTENT(IN) :: Nf ! Number of frequencies
INTEGER, INTENT(IN) :: Nc ! Number of celerities
INTEGER, INTENT(IN) :: Nt ! Array size: nb of theta for the grid (azimuth) search
REAL(KIND=8), DIMENSION(Nr), INTENT(IN) :: xr,yr ! Position of sensors
REAL(KIND=8), DIMENSION(Nt), INTENT(IN) :: theta ! azimuth search grid
REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: freq ! frequencies
REAL(KIND=8), DIMENSION(Nc), INTENT(IN) :: c ! celerities
INTEGER, INTENT(IN) :: method ! bartlett/mvdr
LOGICAL :: method_linear

INTEGER :: tt,cc,ff
REAL(KIND=8), DIMENSION(Nr) :: a !            [Nr]
COMPLEX(KIND=8), DIMENSION(Nr)  :: omega
COMPLEX(KIND=8), DIMENSION(Nr) :: replica

REAL(KIND=8), DIMENSION(Nt,Nc,Nf), INTENT(OUT) :: beamf
REAL(KIND=8), DIMENSION(Nt,Nc), INTENT(OUT) :: beam,beamstd
COMPLEX(KIND=8), DIMENSION(Nf,Nc) :: m2jPiF_over_c

if (method == 0) then
  method_linear = .true.
else if (method == 1) then
  method_linear = .false.
else
  print *,'undefined method : ',method
  return
end if

beam(:,:)=0.d0
beamstd(:,:)=0.d0
CALL get_m2jPiF_over_c(freq, c, Nf, Nc, m2jPiF_over_c(:,:))
do tt=1,Nt
  a(:) = xr(:) * SIN(theta(tt)) + yr(:) * COS(theta(tt))
  do cc=1,Nc
    do ff=1,Nf
      omega(:) = EXP(m2jPiF_over_c(ff,cc) * a(:))           ! [Nr]
      if (method_linear) then ! bartlett
        replica(:) = omega(:) / sqrt(SUM(ABS(omega(:))**2))
        beamf(tt,cc,ff) = ABS(SUM(CONJG(replica(:))*MATMUL(CSDM(:,:,ff),replica(:))))
      else ! mvdr
        replica(:) = omega(:) / SUM(CONJG(omega(:)) * MATMUL(CSDM(:,:,ff),omega(:)) ) / Nr
        beamf(tt,cc,ff) = ABS(SUM(CONJG(replica(:)) * MATMUL(CSDM(:,:,ff),replica(:))))
      end if
      beam(tt,cc) = beam(tt,cc)+beamf(tt,cc,ff)
    enddo
  enddo
enddo

beam(:,:) = beam(:,:)/Nf
do tt=1,Nt
  do cc=1,Nc
    beamstd(tt,cc) = SQRT( SUM((beamf(tt,cc,:) - beam(tt,cc))**2) / Nf )
  enddo
enddo

END SUBROUTINE computebeamct

SUBROUTINE invCsdm(M,N,A)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: M,N ! Dimension of A
  INTEGER :: LDA
  COMPLEX(KIND=8), INTENT(INOUT), DIMENSION(M,N) :: A
  INTEGER :: INFO
  INTEGER, DIMENSION(N) :: IPIV
!  INTEGER, PARAMETER :: LWORK=100*N  ! IBM: For optimal performance, lwork ≥100*n...
!                                     ! TODO : Check LWORK=-1 option to find the optimal LWORK...
  INTEGER :: LWORK
  !COMPLEX(KIND=8), DIMENSION(LWORK) :: WORK
  COMPLEX(KIND=8), DIMENSION(:), ALLOCATABLE :: WORK
  LDA=M
  CALL ZGETRF(M,N,A,LDA,IPIV,INFO)
  IF (INFO /= 0) THEN
    print *,'ZGETRF INFO',INFO
    CALL EXIT(1)
  ENDIF
  LWORK=100*N
  ALLOCATE(WORK(LWORK))
  CALL ZGETRI(N, A, LDA, IPIV, WORK, LWORK, INFO)
  DEALLOCATE(WORK)
  IF (INFO /= 0) THEN
    print *,'ZGETRI INFO',INFO
    CALL EXIT(1)
  ENDIF
END SUBROUTINE invCsdm

SUBROUTINE computecsdm(npt,Nr,data,Nf,norm,freq,Fs,nivBruit) 
  USE glovarmodule , only : CSDM  ! share csdm (or conj csdm) matrix
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: npt,Nr,Nf,norm
  REAL(KIND=4), DIMENSION(npt,Nr), INTENT(INOUT) :: data
  REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: freq
  INTEGER, INTENT(IN) :: nivBruit
  INTEGER, INTENT(IN) :: Fs
  INTEGER :: kk,i,j,kk2
  REAL(KIND=8), DIMENSION(npt) :: t
  COMPLEX(KIND=8), DIMENSION(:), ALLOCATABLE :: A
!  COMPLEX(KIND=8), DIMENSION(npt) :: A
  REAL(KIND=8) :: pi,normK,energie
  COMPLEX(KIND=8), DIMENSION(Nf,Nr) :: y
  COMPLEX(KIND=8) :: cj  ! complex i
  COMPLEX(KIND=8) :: m2jpi  ! -2 * i * pi
! 1. Preprocessing (DFT, normalisation, ...)
! Normalisation trace par trace: Normalisation par l'energie
  IF (norm == 2) THEN  ! normTps ! normalisation par l'energie
    DO kk=1,Nr
      energie = SQRT(SUM(data(:,kk)**2.d0))
      IF (energie /= 0) data(:,kk) = data(:,kk) / energie
    ENDDO
  ENDIF
  ! Compute DFT
  DO i=1,npt
    t(i) = DBLE(i-1)/Fs
  ENDDO
  pi = 4.d0 * DATAN(1.d0)
  cj = DCMPLX(0.d0,1.d0)
  m2jpi = -2.d0 * cj * pi
  ALLOCATE(A(npt))
  DO i=1,Nf
    DO j=1,npt
      A(j) = EXP( m2jpi * (freq(i)*t(j)) )
    ENDDO
    DO kk=1,Nr
      y(i,kk) = SUM( A(:) * data(:,kk) )
    ENDDO
  ENDDO
  DEALLOCATE(A)
  IF (norm == 1) THEN  ! normSp ! prewhitening
    DO i=1,Nf
      DO kk=1,Nr
        y(i,kk) = EXP( cj * (DATAN2(DIMAG(y(i,kk)),DBLE(y(i,kk)))) )  !        data = np.exp(1j*np.angle(data))
      ENDDO
    ENDDO
  ENDIF
! 2. Compute K (CSDM)
  DO i=1,Nf
    DO kk=1,Nr
      DO kk2=1,Nr
        CSDM(kk2,kk,i) = y(i,kk2) * CONJG(y(i,kk))
      ENDDO
    ENDDO
  ENDDO
! # Add noise on CSDM diagonal
  IF (nivBruit /= 0 ) THEN
    DO i=1,Nf
#ifdef lk_float
      normK = NORM2((/NORM2(real(CSDM(:,:,i))),NORM2(imag(CSDM(:,:,i)))/)) ! scalar
#else
      normK = NORM2((/NORM2(dble(CSDM(:,:,i))),NORM2(dimag(CSDM(:,:,i)))/)) ! scalar
#endif
      DO kk=1,Nr
        CSDM(kk,kk,i) = CSDM(kk,kk,i) + (10**(-nivBruit/10.d0))*normK
      ENDDO
    ENDDO
  ENDIF
! # Normalisation de K
  DO i=1,Nf
#ifdef lk_float
    CSDM(:,:,i) = CSDM(:,:,i) / NORM2((/NORM2(real(CSDM(:,:,i))),NORM2(imag(CSDM(:,:,i)))/))
#else
    CSDM(:,:,i) = CSDM(:,:,i) / NORM2((/NORM2(dble(CSDM(:,:,i))),NORM2(dimag(CSDM(:,:,i)))/))
#endif
  ENDDO
END SUBROUTINE computecsdm

SUBROUTINE computecsdm_SA(npt,Nr,data,Nf,norm,freq,Fs,nivBruit)
  USE glovarmodule , only : CSDM  ! share csdm (or conj csdm) matrix
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: npt,Nr,Nf,norm
  REAL(KIND=4), DIMENSION(npt,Nr), INTENT(INOUT) :: data
  REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: freq
  INTEGER, INTENT(IN) :: nivBruit
  INTEGER, INTENT(IN) :: Fs
  INTEGER :: kk,i,j,kk2
  REAL(KIND=8), DIMENSION(npt) :: t
  COMPLEX(KIND=8), DIMENSION(:), ALLOCATABLE :: A
  REAL(KIND=8) :: pi,normK,energie
  COMPLEX(KIND=8), DIMENSION(Nf,Nr) :: y
  COMPLEX(KIND=8) :: cj  ! complex i
#ifdef lk_mkl
  COMPLEX(KIND=8) :: cy  ! temporary complex
#endif
  COMPLEX(KIND=8) :: m2jpi  ! -2 * i * pi
!  COMPLEX(KIND=8), DIMENSION(Nr,Nr,Nf), INTENT(OUT) :: K
! 1. Preprocessing (DFT, normalisation, ...)
! Normalisation trace par trace: Normalisation par l'energie
  IF (norm == 2) THEN  ! normTps ! normalisation par l'energie
    DO kk=1,Nr
      energie = SQRT(SUM(data(:,kk)**2.d0))
      IF (energie /= 0) data(:,kk) = data(:,kk) / energie
    ENDDO
  ENDIF
  ! Compute DFT
  DO i=1,npt
    t(i) = DBLE(i-1)/Fs
  ENDDO
  pi = 4.d0 * DATAN(1.d0)
  cj = DCMPLX(0.d0,1.d0)
  m2jpi = -2.d0 * cj * pi
  ALLOCATE(A(npt))
  DO i=1,Nf
    DO j=1,npt
      A(j) = EXP( m2jpi * (freq(i)*t(j)) )
    ENDDO
    DO kk=1,Nr
      y(i,kk) = SUM( A(:) * data(:,kk) )
    ENDDO
  ENDDO
  DEALLOCATE(A)
  IF (norm == 1) THEN  ! normSp ! prewhitening
    DO i=1,Nf
      DO kk=1,Nr
        y(i,kk) = EXP( -cj * (DATAN2(DIMAG(y(i,kk)),DBLE(y(i,kk)))) )  !        data = np.exp(1j*np.angle(data))
      ENDDO
    ENDDO
  ENDIF
! 2. Compute K (CSDM)
#ifdef lk_mkl
! mkl : ?HEMV : only upper part of CSDM is necessary
  DO i=1,Nf
    DO kk=1,Nr
      cy = CONJG(y(i,kk))
      DO kk2=kk,Nr
        CSDM(kk2,kk,i) = y(i,kk2) * cy
      ENDDO
    ENDDO
  ENDDO
#else
  DO i=1,Nf
    DO kk=1,Nr
      DO kk2=1,Nr
#ifdef lk_nl
        CSDM(kk2,kk,i) = CONJG(y(i,kk2)) * y(i,kk)
#else
        CSDM(kk2,kk,i) = y(i,kk2) * CONJG(y(i,kk))
#endif
      ENDDO
    ENDDO
  ENDDO
#endif
! # Add noise on CSDM diagonal
  IF (nivBruit /= 0 ) THEN
    DO i=1,Nf
#ifdef lk_float
      normK = NORM2((/NORM2(real(CSDM(:,:,i))),NORM2(imag(CSDM(:,:,i)))/)) ! scalar
#else
      normK = NORM2((/NORM2(dble(CSDM(:,:,i))),NORM2(dimag(CSDM(:,:,i)))/)) ! scalar
#endif
      DO kk=1,Nr
        CSDM(kk,kk,i) = CSDM(kk,kk,i) + (10**(-nivBruit/10.d0))*normK
      ENDDO
    ENDDO
  ENDIF
END SUBROUTINE computecsdm_SA

SUBROUTINE computecsdmp_SA(npt,Nr,data,Nf,norm,freq,Fs,nivBruit)
  USE glovarmodule , only : CSDMP  ! share csdm (or conj csdm) matrix
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: npt,Nr,Nf,norm
  REAL(KIND=4), DIMENSION(npt,Nr), INTENT(INOUT) :: data
  REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: freq
  INTEGER, INTENT(IN) :: nivBruit
  INTEGER, INTENT(IN) :: Fs
  INTEGER :: kk,i,j,kk2
  REAL(KIND=8), DIMENSION(npt) :: t
  COMPLEX(KIND=8), DIMENSION(:), ALLOCATABLE :: A
  REAL(KIND=8) :: pi,energie
  COMPLEX(KIND=8), DIMENSION(Nf,Nr) :: y
  COMPLEX(KIND=8) :: cj  ! complex i
  COMPLEX(KIND=8) :: cy  ! temporary complex
  COMPLEX(KIND=8) :: m2jpi  ! -2 * i * pi
! 1. Preprocessing (DFT, normalisation, ...)
! Normalisation trace par trace: Normalisation par l'energie
  IF (norm == 2) THEN  ! normTps ! normalisation par l'energie
    DO kk=1,Nr
      energie = SQRT(SUM(data(:,kk)**2.d0))
      IF (energie /= 0) data(:,kk) = data(:,kk) / energie
    ENDDO
  ENDIF
  ! Compute DFT
  DO i=1,npt
    t(i) = DBLE(i-1)/Fs
  ENDDO
  pi = 4.d0 * DATAN(1.d0)
  cj = DCMPLX(0.d0,1.d0)
  m2jpi = -2.d0 * cj * pi
  ALLOCATE(A(npt))
  DO i=1,Nf
    DO j=1,npt
      A(j) = EXP( m2jpi * (freq(i)*t(j)) )
    ENDDO
    DO kk=1,Nr
      y(i,kk) = SUM( A(:) * data(:,kk) )
    ENDDO
  ENDDO
  DEALLOCATE(A)
  IF (norm == 1) THEN  ! normSp ! prewhitening
    DO i=1,Nf
      DO kk=1,Nr
        y(i,kk) = EXP( -cj * (DATAN2(DIMAG(y(i,kk)),DBLE(y(i,kk)))) )  !        data = np.exp(1j*np.angle(data))
      ENDDO
    ENDDO
  ENDIF
! 2. Compute K (CSDM)
! mkl : ?HEMV : only upper part of CSDM is necessary
  DO i=1,Nf
    j = 0
    DO kk=1,Nr
      cy = CONJG(y(i,kk))
      DO kk2=kk,Nr
        j=j+1
        CSDMP(j,i) = y(i,kk2) * cy
      ENDDO
    ENDDO
  ENDDO
! # Add noise on CSDM diagonal
  IF (nivBruit /= 0 ) THEN
    print *,'nivBruit = ',nivBruit,' not available with packed storage CSDM'
  ENDIF
END SUBROUTINE computecsdmp_SA

SUBROUTINE compute_phidata(npt,Nr,data,Nf,freq,Fs)
  USE glovarmodule , only : phi_data, ln_coh, ref_inode_coh, ln_randomdata
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: npt,Nr,Nf
  REAL(KIND=4), DIMENSION(npt,Nr), INTENT(INOUT) :: data
  REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: freq
  INTEGER, INTENT(IN) :: Fs
  INTEGER :: kk,i,j
  REAL(KIND=8), DIMENSION(npt) :: t
  COMPLEX(KIND=8), DIMENSION(:), ALLOCATABLE :: A
  REAL(KIND=8) :: pi,yy,rn
  COMPLEX(KIND=8), DIMENSION(Nf,Nr) :: y
  COMPLEX(KIND=8), PARAMETER :: cj = (0.d0,1.d0)  ! complex i
  COMPLEX(KIND=8) :: m2jpi  ! -2 * i * pi
! 1. Preprocessing (DFT, normalisation, ...)
  ! Compute DFT
  DO i=1,npt
    t(i) = DBLE(i-1)/Fs
  ENDDO
  pi = 4.d0 * DATAN(1.d0)
!  cj = DCMPLX(0.d0,1.d0)
  m2jpi = -2.d0 * cj * pi
  ALLOCATE(A(npt))
  DO i=1,Nf
    DO j=1,npt
      A(j) = EXP( m2jpi * (freq(i)*t(j)) )
    ENDDO
    DO kk=1,Nr
      y(i,kk) = SUM( A(:) * data(:,kk) )
    ENDDO
  ENDDO
  DEALLOCATE(A)
  DO i=1,Nf
    DO kk=1,Nr
      if (ln_randomdata == 1) then
        CALL random_number(rn)  ! [0;1[
        phi_data(kk,i) = 2.d0 * pi * rn
      else
        phi_data(kk,i) = DATAN2(DIMAG(y(i,kk)),DBLE(y(i,kk)))
      endif
    ENDDO
    if (ln_coh == 1) then
      yy = phi_data(ref_inode_coh,i)
      DO kk=1,Nr
        phi_data(kk,i) = phi_data(kk,i) - yy
      ENDDO
    endif
  ENDDO
END SUBROUTINE compute_phidata

SUBROUTINE read_one_trace(file_id,dsetname,rank,offset,count,nb,rbuf)
  USE HDF5
  IMPLICIT NONE
  INTEGER(HID_T), INTENT(in)     :: file_id
  CHARACTER(LEN=*), INTENT(in)   :: dsetname
  INTEGER, INTENT(in)            :: rank,nb
  INTEGER(HSIZE_T), DIMENSION(rank), INTENT(in) :: offset
  INTEGER(HSIZE_T), DIMENSION(rank), INTENT(in) :: count
  REAL(KIND=4), DIMENSION(nb), INTENT(out) :: rbuf
#ifdef lk_integer
  INTEGER, DIMENSION(nb) :: buf
#endif
  INTEGER(HID_T) :: dset_id,space_id,memspace_id
  INTEGER                        :: error
  INTEGER(HSIZE_T), DIMENSION(1) :: memcount
  INTEGER(HSIZE_T), DIMENSION(1), PARAMETER :: memoffset = (/0/) 
  INTEGER(HSIZE_T), DIMENSION(1) :: indims

  memcount=(/nb/)
  indims = (/nb/)

  CALL h5dopen_f(file_id, dsetname, dset_id, error)
if (error/=0) call exit(error)
  CALL h5dget_space_f(dset_id, space_id, error)
if (error/=0) call exit(error)
  CALL h5sselect_hyperslab_f (space_id, H5S_SELECT_SET_F, offset, count, error)
if (error/=0) call exit(error)
  CALL h5screate_simple_f(1, memcount, memspace_id, error)
if (error/=0) call exit(error)
  CALL h5sselect_hyperslab_f(memspace_id, H5S_SELECT_SET_F, memoffset, memcount, error)
if (error/=0) call exit(error)
#ifdef lk_integer
  buf(:)=0
  CALL H5dread_f(dset_id, MY_HDF5_INPUT_TYPE, buf, indims, error, memspace_id, space_id)
  if (error/=0) call exit(error)
  rbuf = REAL(buf)
#else
  rbuf(:)=0.0
  CALL H5dread_f(dset_id, MY_HDF5_INPUT_TYPE, rbuf, indims, error, memspace_id, space_id)
  if (error/=0) call exit(error)
#endif
  CALL h5sclose_f(space_id, error)
if (error/=0) call exit(error)
  CALL h5sclose_f(memspace_id, error)
if (error/=0) call exit(error)
  CALL h5dclose_f(dset_id, error)
if (error/=0) call exit(error)

END SUBROUTINE read_one_trace

SUBROUTINE distlatlong(latA, lonA, latB, lonB, XX, YY)
! Calcule les coordonnées de la station B en prenant A comme référence.
!     Géodésique avec approximation Terre sphérique
!     latA,lonA,latB,lonB sont donnés en degrés
!     XX [ m ] : On projette B en Bo tel que lonBo=lonB et latBo=latA
!     YY [ m ] : On projette B en Bz tel que lonBz=lonA et latBz=latB
  IMPLICIT NONE
  REAL(KIND=8), INTENT(in) :: latA, lonA, latB, lonB
  REAL(KIND=8), INTENT(out) :: XX, YY
  REAL(KIND=8), PARAMETER :: rT = 6378000.d0 !  Rayon Terrestre en m
  REAL(KIND=8) :: latradA, lonradA, latradB, lonradB
  REAL(KIND=8), PARAMETER :: pi = 4.d0 * ATAN(1.D0)

  ! from degrees to radians
  latradA = (pi*latA)/180.d0
  lonradA = (pi*lonA)/180.d0
  latradB = (pi*latB)/180.d0
  lonradB = (pi*lonB)/180.d0
  ! calcul coordonnees
  XX = rT * DACOS((DSIN(latradA) * DSIN(latradA)) + (DCOS(latradA) * DCOS(latradA) * DCOS(lonradB - lonradA)))
  YY = rT * DACOS((DSIN(latradA) * DSIN(latradB)) + (DCOS(latradA) * DCOS(latradB) ))
  IF ( latB < latA ) YY=-YY
  IF ( lonB < lonA ) XX=-XX
END SUBROUTINE distlatlong

SUBROUTINE init_random_seed()
  INTEGER :: i, n, clock
  INTEGER, DIMENSION(:), ALLOCATABLE :: seed
  CALL RANDOM_SEED(size = n)
  ALLOCATE(seed(n))
  CALL SYSTEM_CLOCK(COUNT=clock)
  seed = clock + 37 * (/ (i - 1, i = 1, n) /)
  CALL RANDOM_SEED(PUT = seed)
  DEALLOCATE(seed)
END SUBROUTINE init_random_seed

!only for debug: force seed init as luke4/intel13
!SUBROUTINE init_random_seed0()
!  INTEGER, DIMENSION(2) :: seed
!  seed(1)=2147483562
!  seed(2)=2147483398
!  CALL RANDOM_SEED(PUT = seed)
!END SUBROUTINE init_random_seed0

!subroutine compute_ref_inode_coh(xsp,ysp,zsp,idx)
subroutine compute_ref_inode_coh(xsp,ysp,idx)
  USE glovarmodule, only : Nr,xr,yr ! ,zr
  implicit none
  INTEGER :: kk
  INTEGER, INTENT(out) :: idx
  REAL(KIND=8), INTENT(IN) :: xsp,ysp ! ,zsp ! starting point coordinates (utm)
  REAL(KIND=8) :: dist, mindist
  idx=1 ! dummy init
  mindist=(xr(idx)-xsp)**2 + (yr(idx)-ysp)**2 !+ (zr(idx)-zsp)**2
  DO kk=1, Nr
    dist = (xr(kk)-xsp)**2 + (yr(kk)-ysp)**2 !+ (zr(kk)-zsp)**2
    if (dist < mindist) then
      mindist=dist
      idx=kk
    endif
  enddo
  print *,'1-based index of reference sensor is ',idx
  print *,'and its horizontal distance from starting point is ',SQRT(mindist),' m'
END SUBROUTINE compute_ref_inode_coh

end MODULE modcomputebeam
