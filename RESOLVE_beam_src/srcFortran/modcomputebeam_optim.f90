!###############################################################################
!#    This file is part of MFP : a Fortran code for computing MFP              #
!#    (Matched Field Processing), also named bartlett or beamforming,          #
!#    over a regular grid on (x,y,z,c) or (theta,c).                           #
!#                                                                             #
!#    Copyright (C) 2017 Albanne Lecointre                                     #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################

!=======================================================================================
!                       ***  Fortran code modcomputebeam_optim.f90  ***
!   This is the Fortran module associated to beamforming_optim Fortran code
!    
!
!=======================================================================================
!   History :  1.0  : 03/2017  : A. Lecointre : Initial version
!               
!       describe here       
!       describe here
!       describe here    
!
!---------------------------------------------------------------------------------------
!
!
!---------------------------------------------------------------------------------------
!   routines      : description
!                 : 
!                 : 
!                 : 
!                 :
!
!---------------------------------------------------------------------------------------
!  $Id: modcomputebeam_optim.f90 160 2019-01-18 15:54:44Z lecointre $
!---------------------------------------------------------------------------------------
!  

MODULE modcomputebeam_optim
use HDF5
use H5LT
 implicit none

contains

REAL(KIND=8) FUNCTION BA_xy_SJFZ(xxx)
  use glovarmodule , only : N,xr,yr,freq,CSDM,Nr,Nf 
  implicit none
  REAL(KIND=8), DIMENSION(N) :: xxx
  REAL(KIND=8), DIMENSION(Nr) :: tt
  COMPLEX(KIND=8), DIMENSION(Nr) :: replica
  REAL(KIND=8) :: pi
  COMPLEX(KIND=8) :: Deuxjpi,cctmp,ctmprr1
  INTEGER :: rr,ff,rr1,rr2

  pi=4.d0*atan(1.d0)
  Deuxjpi = 2.d0 * (0.d0,1.d0) * pi

  BA_xy_SJFZ = 0.d0

  tt(:) = ( ((xr(:)-xxx(1))**2+(yr(:)-xxx(2))**2)**0.5 ) / xxx(3)  ! [Nr]

  DO ff=1,Nf
    DO rr=1,Nr
      replica(rr) = EXP( Deuxjpi * tt(rr) * freq(ff) )
    ENDDO
    cctmp = ( 0.d0 , 0.d0 )
    DO rr1=1,Nr
      ctmprr1 = ( 0.d0 , 0.d0 )
      DO rr2=1,Nr
        ctmprr1 = ctmprr1 + CONJG(replica(rr2))*CSDM(rr1,rr2,ff)
      ENDDO
      cctmp = cctmp + ctmprr1*replica(rr1)
    ENDDO
    BA_xy_SJFZ = BA_xy_SJFZ + ABS(cctmp) / (Nr*Nr)
  ENDDO
  BA_xy_SJFZ = -BA_xy_SJFZ / Nf
  return
END FUNCTION BA_xy_SJFZ

REAL(KIND=8) FUNCTION BA_xy_SJFZ_exp(xxx)
  use glovarmodule , only : N,xr,yr,freq,phi_data,phi_model,Nr,Nf 
  implicit none
  REAL(KIND=8), DIMENSION(N) :: xxx
  REAL(KIND=8), DIMENSION(Nr) :: tt
  REAL(KIND=8) :: m2Pi
  COMPLEX(KIND=8) :: zz
  COMPLEX(KIND=8), PARAMETER :: cj = (0.d0,1.d0)
  INTEGER :: ff,ii

  m2Pi = - 8.d0 * atan(1.d0)

  tt(:) = m2Pi * ( ((xr(:)-xxx(1))**2+(yr(:)-xxx(2))**2)**0.5 ) / xxx(3)  ! [Nr]

  BA_xy_SJFZ_exp = 0.d0
  DO ff = 1, Nf
    phi_model(:) = tt(:)*freq(ff)
    zz = (0.d0,0.d0)
    DO ii = 1, Nr
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
    ENDDO
    BA_xy_SJFZ_exp = BA_xy_SJFZ_exp + zz*CONJG(zz)
  ENDDO
  BA_xy_SJFZ_exp = BA_xy_SJFZ_exp / (Nr*Nr*Nf)
  return
END FUNCTION BA_xy_SJFZ_exp

REAL(KIND=8) FUNCTION BA_xyz(xxx)
  use glovarmodule , only : N,xr,yr,zr,freq,CSDMP,Nr,Nf,cel,nn_removezero,nkzero 
  implicit none
  REAL(KIND=8), DIMENSION(N) :: xxx
  REAL(KIND=8) :: pi
  COMPLEX(KIND=8) :: p2jPi
  INTEGER :: ff
  INTEGER :: kk,kki,Nr0
#ifdef lk_float
  COMPLEX(KIND=4) :: CDOTC,alpha,beta
  COMPLEX(KIND=4), DIMENSION(Nr) :: CC,replica
  REAL(KIND=4), DIMENSION(Nr) :: tt
  alpha=(1.0,0.0)
  beta=(0.0,0.0)
#else
  COMPLEX(KIND=8) :: ZDOTC,alpha,beta
  COMPLEX(KIND=8), DIMENSION(Nr) :: CC,replica
  REAL(KIND=8), DIMENSION(Nr) :: tt
  alpha=(1.d0,0.d0)
  beta=(0.d0,0.d0)
#endif

  pi=4.d0*atan(1.d0)
  p2jPi = 2.d0 * (0.d0,1.d0) * pi

  BA_xyz = 0.d0

  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
#ifdef lk_float
        tt(kki) = REAL( ( ((xr(kk)-xxx(1))**2+(yr(kk)-xxx(2))**2+(zr(kk)-xxx(3))**2)**0.5 ) / cel )  ! [Nr]
#else
        tt(kki) = ( ((xr(kk)-xxx(1))**2+(yr(kk)-xxx(2))**2+(zr(kk)-xxx(3))**2)**0.5 ) / cel  ! [Nr]
#endif
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
#ifdef lk_float
    tt(:) = REAL( ( ((xr(:)-xxx(1))**2+(yr(:)-xxx(2))**2+(zr(:)-xxx(3))**2)**0.5 ) / cel )  ! [Nr]
#else
    tt(:) = ( ((xr(:)-xxx(1))**2+(yr(:)-xxx(2))**2+(zr(:)-xxx(3))**2)**0.5 ) / cel  ! [Nr]
#endif
  ENDIF

  DO ff=1,Nf
    replica(1:Nr0) = EXP(p2jPi * tt(1:Nr0) * freq(ff)) ! [Nr] 
#ifdef lk_float
    CALL CHPMV('L',Nr0,alpha,CSDMP(:,ff),replica(1:Nr0),1,beta,CC(1:Nr0),1)
    BA_xyz = BA_xyz + ABS(CDOTC(Nr0,replica(1:Nr0),1,CC(1:Nr0),1)) / (Nr0*Nr0)
#else
    CALL ZHPMV('L',Nr0,alpha,CSDMP(:,ff),replica(1:Nr0),1,beta,CC(1:Nr0),1)
    BA_xyz = BA_xyz + ABS(ZDOTC(Nr0,replica(1:Nr0),1,CC(1:Nr0),1)) / (Nr0*Nr0)
#endif
  ENDDO
  BA_xyz = -BA_xyz / Nf

  return
END FUNCTION BA_xyz

REAL(KIND=8) FUNCTION BA_xyz_exp(xxx)
  use glovarmodule , only : N,xr,yr,zr,freq,phi_data,phi_model,Nr,Nf,cel,nn_removezero,nkzero 
  implicit none
  REAL(KIND=8), DIMENSION(N) :: xxx
  REAL(KIND=8), DIMENSION(Nr) :: tt
  REAL(KIND=8) :: m2pi
  INTEGER :: ff,ii
  INTEGER :: kk,kki,Nr0
  COMPLEX(KIND=8) :: zz
  COMPLEX(KIND=8), PARAMETER :: cj = ( 0.d0, 1.d0 )

  m2Pi = - 8.d0 * atan(1.d0)

  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-xxx(1))**2+(yr(kk)-xxx(2))**2+(zr(kk)-xxx(3))**2)**0.5 ) / cel  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-xxx(1))**2+(yr(:)-xxx(2))**2+(zr(:)-xxx(3))**2)**0.5 ) / cel  ! [Nr]
  ENDIF

  tt(1:Nr0) = m2Pi * tt(1:Nr0)

  BA_xyz_exp = 0.d0
  DO ff = 1, Nf
    phi_model(1:Nr0) = tt(1:Nr0)*freq(ff)
    zz = (0.d0,0.d0)
    DO ii = 1, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
    ENDDO
    BA_xyz_exp = BA_xyz_exp + zz*CONJG(zz)
  ENDDO
  BA_xyz_exp = - BA_xyz_exp / (Nr0*Nr0*Nf)
  return
END FUNCTION BA_xyz_exp

REAL(KIND=8) FUNCTION BA_xyc_exp(xxx)
  use glovarmodule , only : N,xr,yr,zr,freq,phi_data,phi_model,Nr,Nf,zzz,nn_removezero,nkzero 
  implicit none
  REAL(KIND=8), DIMENSION(N) :: xxx
  REAL(KIND=8), DIMENSION(Nr) :: tt
  REAL(KIND=8) :: m2pi
  INTEGER :: ff,ii
  INTEGER :: kk,kki,Nr0
  COMPLEX(KIND=8) :: zz
  COMPLEX(KIND=8), PARAMETER :: cj = ( 0.d0, 1.d0 )

  m2Pi = - 8.d0 * atan(1.d0)

  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-xxx(1))**2+(yr(kk)-xxx(2))**2+(zr(kk)-zzz)**2)**0.5 ) / xxx(3)  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-xxx(1))**2+(yr(:)-xxx(2))**2+(zr(:)-zzz)**2)**0.5 ) / xxx(3)  ! [Nr]
  ENDIF

  tt(1:Nr0) = m2Pi * tt(1:Nr0)

  BA_xyc_exp = 0.d0
  DO ff = 1, Nf
    phi_model(1:Nr0) = tt(1:Nr0)*freq(ff)
    zz = (0.d0,0.d0)
    DO ii = 1, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
    ENDDO
    BA_xyc_exp = BA_xyc_exp + zz*CONJG(zz)
  ENDDO
  BA_xyc_exp = - BA_xyc_exp / (Nr0*Nr0*Nf)
!print *,'BA_xyc_exp',BA_xyc_exp
!print *,'xxx',xxx
  return
END FUNCTION BA_xyc_exp

REAL(KIND=8) FUNCTION BA_xyzc_exp(xxx)
  use glovarmodule , only : N,xr,yr,zr,freq,phi_data,phi_model,Nr,Nf,nn_removezero,nkzero 
  implicit none
  REAL(KIND=8), DIMENSION(N) :: xxx
  REAL(KIND=8), DIMENSION(Nr) :: tt
  REAL(KIND=8) :: m2pi
  INTEGER :: ff,ii
  INTEGER :: kk,kki,Nr0
  COMPLEX(KIND=8) :: zz
  COMPLEX(KIND=8), PARAMETER :: cj = ( 0.d0, 1.d0 )

  m2Pi = - 8.d0 * atan(1.d0)

  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-xxx(1))**2+(yr(kk)-xxx(2))**2+(zr(kk)-xxx(3))**2)**0.5 ) / xxx(4)  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-xxx(1))**2+(yr(:)-xxx(2))**2+(zr(:)-xxx(3))**2)**0.5 ) / xxx(4)  ! [Nr]
  ENDIF

  tt(1:Nr0) = m2Pi * tt(1:Nr0)

  BA_xyzc_exp = 0.d0
  DO ff = 1, Nf
    phi_model(1:Nr0) = tt(1:Nr0)*freq(ff)
    zz = (0.d0,0.d0)
    DO ii = 1, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
    ENDDO
    BA_xyzc_exp = BA_xyzc_exp + zz*CONJG(zz)
  ENDDO
  BA_xyzc_exp = - BA_xyzc_exp / (Nr0*Nr0*Nf)
!WRITE(*,"(5F12.6)") , -BA_xyzc_exp,xxx
!WRITE(10) -BA_xyzc_exp
!WRITE(10) xxx
  return
END FUNCTION BA_xyzc_exp

REAL(KIND=8) FUNCTION BA_xyzc_exp_zpen(xxx)
  use glovarmodule , only : N,xr,yr,zr,freq,phi_data,phi_model,Nr,Nf,nn_removezero,nkzero 
  implicit none
  REAL(KIND=8), DIMENSION(N) :: xxx
  REAL(KIND=8), DIMENSION(Nr) :: tt
  REAL(KIND=8) :: m2pi,zsurf
  INTEGER :: ff,ii
  INTEGER :: kk,kki,Nr0
  COMPLEX(KIND=8) :: zz
  COMPLEX(KIND=8), PARAMETER :: cj = ( 0.d0, 1.d0 )

  m2Pi = - 8.d0 * atan(1.d0)

  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-xxx(1))**2+(yr(kk)-xxx(2))**2+(zr(kk)-xxx(3))**2)**0.5 ) / xxx(4)  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-xxx(1))**2+(yr(:)-xxx(2))**2+(zr(:)-xxx(3))**2)**0.5 ) / xxx(4)  ! [Nr]
  ENDIF

  tt(1:Nr0) = m2Pi * tt(1:Nr0)

  BA_xyzc_exp_zpen = 0.d0
  DO ff = 1, Nf
    phi_model(1:Nr0) = tt(1:Nr0)*freq(ff)
    zz = (0.d0,0.d0)
    DO ii = 1, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
    ENDDO
    BA_xyzc_exp_zpen = BA_xyzc_exp_zpen + DBLE(zz*CONJG(zz))
  ENDDO
  BA_xyzc_exp_zpen = - BA_xyzc_exp_zpen / (Nr0*Nr0*Nf)
  call fsurf(xxx(1),xxx(2),zsurf)
  if ( xxx(3) > zsurf ) then
    if ( xxx(3) > (zsurf+5.0) ) then
      BA_xyzc_exp_zpen = 0.d0
    else
      BA_xyzc_exp_zpen = BA_xyzc_exp_zpen * (zsurf+5.0-xxx(3))/5.0
    endif
  endif
  return
END FUNCTION BA_xyzc_exp_zpen

REAL(KIND=8) FUNCTION BA_xyzc_exp_zpen2(xxx)
  use glovarmodule , only : N,xr,yr,zr,freq,phi_data,phi_model,Nr,Nf,nn_removezero,nkzero,dn_zwidth 
  implicit none
  REAL(KIND=8), DIMENSION(N) :: xxx
  REAL(KIND=8), DIMENSION(Nr) :: tt
  REAL(KIND=8) :: m2pi,zsurf,zbot
  INTEGER :: ff,ii
  INTEGER :: kk,kki,Nr0
  COMPLEX(KIND=8) :: zz
  COMPLEX(KIND=8), PARAMETER :: cj = ( 0.d0, 1.d0 )

  m2Pi = - 8.d0 * atan(1.d0)

  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-xxx(1))**2+(yr(kk)-xxx(2))**2+(zr(kk)-xxx(3))**2)**0.5 ) / xxx(4)  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-xxx(1))**2+(yr(:)-xxx(2))**2+(zr(:)-xxx(3))**2)**0.5 ) / xxx(4)  ! [Nr]
  ENDIF

  tt(1:Nr0) = m2Pi * tt(1:Nr0)

  BA_xyzc_exp_zpen2 = 0.d0
  DO ff = 1, Nf
    phi_model(1:Nr0) = tt(1:Nr0)*freq(ff)
    zz = (0.d0,0.d0)
    DO ii = 1, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
    ENDDO
    BA_xyzc_exp_zpen2 = BA_xyzc_exp_zpen2 + DBLE(zz*CONJG(zz))
  ENDDO
  BA_xyzc_exp_zpen2 = - BA_xyzc_exp_zpen2 / (Nr0*Nr0*Nf)
  call fsurf(xxx(1),xxx(2),zsurf)
  zbot = zsurf-dn_zwidth
  if ( xxx(3) > zsurf ) then
    if ( xxx(3) > (zsurf+5.0) ) then
      BA_xyzc_exp_zpen2 = 0.d0
    else
      BA_xyzc_exp_zpen2 = BA_xyzc_exp_zpen2 * (zsurf+5.0-xxx(3))/5.0
    endif
  elseif ( xxx(3) < zbot ) then
    if ( xxx(3) < (zbot-5.0) ) then
      BA_xyzc_exp_zpen2 = 0.d0
    else
      BA_xyzc_exp_zpen2 = BA_xyzc_exp_zpen2 * (zbot-5.0-xxx(3))/(-5.0)
    endif
  endif
  return
END FUNCTION BA_xyzc_exp_zpen2

REAL(KIND=8) FUNCTION BAcoh_xyzc_exp(xxx)
  use glovarmodule , only : N,xr,yr,zr,freq,phi_data,phi_model,Nr,Nf,nn_removezero,nkzero,ref_inode_coh 
  implicit none
  REAL(KIND=8), DIMENSION(N) :: xxx
  REAL(KIND=8), DIMENSION(Nr) :: tt
  REAL(KIND=8) :: m2pi,yy
  INTEGER :: ff,ii
  INTEGER :: kk,kki,Nr0
  COMPLEX(KIND=8) :: zz
  COMPLEX(KIND=8), PARAMETER :: cj = ( 0.d0, 1.d0 )

  m2Pi = - 8.d0 * atan(1.d0)

  IF ( nn_removezero == 1 ) THEN
    print *, 'nn_removezero and ln_coh are not commpatible'
    CALL exit(1)
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-xxx(1))**2+(yr(kk)-xxx(2))**2+(zr(kk)-xxx(3))**2)**0.5 ) / xxx(4)  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-xxx(1))**2+(yr(:)-xxx(2))**2+(zr(:)-xxx(3))**2)**0.5 ) / xxx(4)  ! [Nr]
  ENDIF

  tt(1:Nr0) = m2Pi * tt(1:Nr0)

  yy = tt(ref_inode_coh)
  DO ii=1, Nr0
    tt(ii) = tt(ii) - yy
  ENDDO

  zz = (0.d0,0.d0)
  DO ff = 1, Nf
    phi_model(1:Nr0) = tt(1:Nr0)*freq(ff)
    DO ii = 1, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
    ENDDO
  ENDDO
  BAcoh_xyzc_exp = - DBLE(zz*CONJG(zz)) / (Nr0*Nr0*Nf*Nf)
  return
END FUNCTION BAcoh_xyzc_exp

REAL(KIND=8) FUNCTION BAcoh_xyzc_exp_zpen(xxx)
  use glovarmodule , only : N,xr,yr,zr,freq,phi_data,phi_model,Nr,Nf,nn_removezero,nkzero,ref_inode_coh 
  implicit none
  REAL(KIND=8), DIMENSION(N) :: xxx
  REAL(KIND=8), DIMENSION(Nr) :: tt
  REAL(KIND=8) :: m2pi,zsurf,yy
  INTEGER :: ff,ii
  INTEGER :: kk,kki,Nr0
  COMPLEX(KIND=8) :: zz
  COMPLEX(KIND=8), PARAMETER :: cj = ( 0.d0, 1.d0 )

  m2Pi = - 8.d0 * atan(1.d0)

  IF ( nn_removezero == 1 ) THEN
    print *, 'nn_removezero and ln_coh are not commpatible'
    CALL exit(1)
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-xxx(1))**2+(yr(kk)-xxx(2))**2+(zr(kk)-xxx(3))**2)**0.5 ) / xxx(4)  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-xxx(1))**2+(yr(:)-xxx(2))**2+(zr(:)-xxx(3))**2)**0.5 ) / xxx(4)  ! [Nr]
  ENDIF

  tt(1:Nr0) = m2Pi * tt(1:Nr0)

  yy = tt(ref_inode_coh)
  DO ii=1, Nr0
    tt(ii) = tt(ii) - yy
  ENDDO

  zz = (0.d0,0.d0)
  DO ff = 1, Nf
    phi_model(1:Nr0) = tt(1:Nr0)*freq(ff)
    DO ii = 1, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
    ENDDO
  ENDDO
  BAcoh_xyzc_exp_zpen = - DBLE(zz*CONJG(zz)) / (Nr0*Nr0*Nf*Nf)
  call fsurf(xxx(1),xxx(2),zsurf)
  if ( xxx(3) > zsurf ) then
    if ( xxx(3) > (zsurf+5.0) ) then
      BAcoh_xyzc_exp_zpen = 0.d0
    else
      BAcoh_xyzc_exp_zpen = BAcoh_xyzc_exp_zpen * (zsurf+5.0-xxx(3))/5.0
    endif
  endif
  return
END FUNCTION BAcoh_xyzc_exp_zpen

REAL(KIND=8) FUNCTION BAcoh_xyzc_exp_zpen2(xxx)
  use glovarmodule , only : N,xr,yr,zr,freq,phi_data,phi_model,Nr,Nf,nn_removezero,nkzero,ref_inode_coh,dn_zwidth 
  implicit none
  REAL(KIND=8), DIMENSION(N) :: xxx
  REAL(KIND=8), DIMENSION(Nr) :: tt
  REAL(KIND=8) :: m2pi,zsurf,zbot,yy
  INTEGER :: ff,ii
  INTEGER :: kk,kki,Nr0
  COMPLEX(KIND=8) :: zz
  COMPLEX(KIND=8), PARAMETER :: cj = ( 0.d0, 1.d0 )

  m2Pi = - 8.d0 * atan(1.d0)

  IF ( nn_removezero == 1 ) THEN
    print *, 'nn_removezero and ln_coh are not commpatible'
    CALL exit(1)
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-xxx(1))**2+(yr(kk)-xxx(2))**2+(zr(kk)-xxx(3))**2)**0.5 ) / xxx(4)  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-xxx(1))**2+(yr(:)-xxx(2))**2+(zr(:)-xxx(3))**2)**0.5 ) / xxx(4)  ! [Nr]
  ENDIF

  tt(1:Nr0) = m2Pi * tt(1:Nr0)

  yy = tt(ref_inode_coh)
  DO ii=1, Nr0
    tt(ii) = tt(ii) - yy
  ENDDO

  zz = (0.d0,0.d0)
  DO ff = 1, Nf
    phi_model(1:Nr0) = tt(1:Nr0)*freq(ff)
    DO ii = 1, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
    ENDDO
  ENDDO
  BAcoh_xyzc_exp_zpen2 = - DBLE(zz*CONJG(zz)) / (Nr0*Nr0*Nf*Nf)
  call fsurf(xxx(1),xxx(2),zsurf)
  zbot = zsurf-dn_zwidth
  if ( xxx(3) > zsurf ) then
    if ( xxx(3) > (zsurf+5.0) ) then
      BAcoh_xyzc_exp_zpen2 = 0.d0
    else
      BAcoh_xyzc_exp_zpen2 = BAcoh_xyzc_exp_zpen2 * (zsurf+5.0-xxx(3))/5.0
    endif
  elseif ( xxx(3) < zbot ) then
    if ( xxx(3) < (zbot-5.0) ) then
      BAcoh_xyzc_exp_zpen2 = 0.d0
    else
      BAcoh_xyzc_exp_zpen2 = BAcoh_xyzc_exp_zpen2 * (zbot-5.0-xxx(3))/(-5.0)
    endif
  endif
  return
END FUNCTION BAcoh_xyzc_exp_zpen2

REAL(KIND=8) FUNCTION BA_xyzc_exp_zpen_diags(xxx)
  use glovarmodule , only : N,xr,yr,zr,freq,phi_data,phi_model,Nr,Nf,nn_removezero,nkzero,nevals,dphi,Baf,xyzcBB 
  implicit none
  REAL(KIND=8), DIMENSION(N) :: xxx
  REAL(KIND=8), DIMENSION(Nr) :: tt
  REAL(KIND=8) :: m2pi,zsurf
  INTEGER :: ff,ii
  INTEGER :: kk,kki,Nr0
  COMPLEX(KIND=8) :: zz
  COMPLEX(KIND=8), PARAMETER :: cj = ( 0.d0, 1.d0 )

  nevals=nevals+1  ! increment global counter in the objective function

  m2Pi = - 8.d0 * atan(1.d0)

  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-xxx(1))**2+(yr(kk)-xxx(2))**2+(zr(kk)-xxx(3))**2)**0.5 ) / xxx(4)  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-xxx(1))**2+(yr(:)-xxx(2))**2+(zr(:)-xxx(3))**2)**0.5 ) / xxx(4)  ! [Nr]
  ENDIF

  tt(1:Nr0) = m2Pi * tt(1:Nr0)

  BA_xyzc_exp_zpen_diags = 0.d0
  DO ff = 1, Nf
    phi_model(1:Nr0) = tt(1:Nr0)*freq(ff)
    zz = (0.d0,0.d0)
    DO ii = 1, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
      dphi(ii,ff,nevals) = REAL(phi_data(ii,ff)-phi_model(ii))
    ENDDO
    BA_xyzc_exp_zpen_diags = BA_xyzc_exp_zpen_diags + DBLE(zz*CONJG(zz))
    Baf(ff,nevals) = REAL(zz*CONJG(zz))/(Nr0*Nr0)
  ENDDO
  BA_xyzc_exp_zpen_diags = - BA_xyzc_exp_zpen_diags / (Nr0*Nr0*Nf)
WRITE(*,"(F12.6)",advance="no") -BA_xyzc_exp_zpen_diags
  DO ii = 1, N
    xyzcBB(nevals,ii) = xxx(ii)
  ENDDO
  xyzcBB(nevals,N+1) = -BA_xyzc_exp_zpen_diags
  call fsurf(xxx(1),xxx(2),zsurf)
  if ( xxx(3) > zsurf ) then
    if ( xxx(3) > (zsurf+5.0) ) then
      BA_xyzc_exp_zpen_diags = 0.d0
    else
      BA_xyzc_exp_zpen_diags = BA_xyzc_exp_zpen_diags * (zsurf+5.0-xxx(3))/5.0
    endif
  endif
WRITE(*,"(5F12.6)") -BA_xyzc_exp_zpen_diags,xxx
  xyzcBB(nevals,N+2) = -BA_xyzc_exp_zpen_diags
  return
END FUNCTION BA_xyzc_exp_zpen_diags

REAL(KIND=8) FUNCTION BAcoh_xyzc_exp_zpen_diags(xxx)
  use glovarmodule , only : N,xr,yr,zr,freq,phi_data,phi_model,Nr,Nf,nn_removezero,nkzero,nevals,dphi,Baf,xyzcBB,ref_inode_coh 
  implicit none
  REAL(KIND=8), DIMENSION(N) :: xxx
  REAL(KIND=8), DIMENSION(Nr) :: tt
  REAL(KIND=8) :: m2pi,zsurf,yy
  INTEGER :: ff,ii
  INTEGER :: kk,kki,Nr0
  COMPLEX(KIND=8) :: zz
  COMPLEX(KIND=8), PARAMETER :: cj = ( 0.d0, 1.d0 )

  nevals=nevals+1  ! increment global counter in the objective function

  m2Pi = - 8.d0 * atan(1.d0)

  IF ( nn_removezero == 1 ) THEN
    print *, 'nn_removezero and ln_coh are not commpatible'
    CALL exit(1)
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-xxx(1))**2+(yr(kk)-xxx(2))**2+(zr(kk)-xxx(3))**2)**0.5 ) / xxx(4)  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-xxx(1))**2+(yr(:)-xxx(2))**2+(zr(:)-xxx(3))**2)**0.5 ) / xxx(4)  ! [Nr]
  ENDIF

  tt(1:Nr0) = m2Pi * tt(1:Nr0)

  yy = tt(ref_inode_coh)
  DO ii=1, Nr0
    tt(ii) = tt(ii) - yy
  ENDDO

  zz = (0.d0,0.d0)
  DO ff = 1, Nf
    phi_model(1:Nr0) = tt(1:Nr0)*freq(ff)
    DO ii = 1, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
      dphi(ii,ff,nevals) = REAL(phi_data(ii,ff)-phi_model(ii))
    ENDDO
    Baf(ff,nevals) = REAL(zz*CONJG(zz))/(Nr0*Nr0*ff*ff)
  ENDDO
  BAcoh_xyzc_exp_zpen_diags = - DBLE(zz*CONJG(zz)) / (Nr0*Nr0*Nf*Nf)
WRITE(*,"(F12.6)",advance="no") -BAcoh_xyzc_exp_zpen_diags
  DO ii = 1, N
    xyzcBB(nevals,ii) = xxx(ii)
  ENDDO
  xyzcBB(nevals,N+1) = -BAcoh_xyzc_exp_zpen_diags
  call fsurf(xxx(1),xxx(2),zsurf)
  if ( xxx(3) > zsurf ) then
    if ( xxx(3) > (zsurf+5.0) ) then
      BAcoh_xyzc_exp_zpen_diags = 0.d0
    else
      BAcoh_xyzc_exp_zpen_diags = BAcoh_xyzc_exp_zpen_diags * (zsurf+5.0-xxx(3))/5.0
    endif
  endif
WRITE(*,"(5F12.6)") -BAcoh_xyzc_exp_zpen_diags,xxx
  xyzcBB(nevals,N+2) = -BAcoh_xyzc_exp_zpen_diags
  return
END FUNCTION BAcoh_xyzc_exp_zpen_diags

REAL(KIND=8) FUNCTION BA_xyz_expvm(xxx)
  use glovarmodule , only : minN,xgr,ttgr,N,xr,yr,zr,freq,phi_data,phi_model,Nr,Nf,nn_removezero,nkzero,vmfile_id,distxgr 
  implicit none
  REAL(KIND=8), DIMENSION(N) :: xxx
  REAL(KIND=8), DIMENSION(Nr) :: tt
  REAL(KIND=8) :: m2pi
  INTEGER :: ff,ii
  INTEGER :: kk,kki,Nr0
  COMPLEX(KIND=8) :: zz
  COMPLEX(KIND=8), PARAMETER :: cj = ( 0.d0, 1.d0 )
  integer, dimension(1) :: newmin
  REAL(KIND=8), DIMENSION(Nr) :: distxopt_over_distxgr
  integer :: error
  character(len=7) :: c_minN
  INTEGER(HSIZE_T), DIMENSION(1) :: dims1ttgr

  m2Pi = - 8.d0 * atan(1.d0)

! Find the nearest velocity model grid cell (id and position) corresponding to the x candidate
  CALL FindNearestNod(xxx,newmin,xgr) ! [minN] = FindNearestNod(x,gridlocs,nnods)
if ( newmin(1) /= minN(1) ) then
  minN(1) = newmin(1)
! Read ttgr corresponding to minN
  write(c_minN,'(I0.7)') minN
  dims1ttgr=(/Nr/)
#ifdef lk_gridcell_double
  CALL h5ltread_dataset_f(vmfile_id, "/GridCell/"//TRIM(c_minN), H5T_NATIVE_DOUBLE, ttgr, dims1ttgr, error)
#else
  CALL h5ltread_dataset_f(vmfile_id, "/GridCell/"//TRIM(c_minN), H5T_NATIVE_REAL, ttgr, dims1ttgr, error)
#endif
  if (error/=0) call exit(error)
#ifdef lk_inv_vapp
endif
#else
  ! MAJ de var globale distxgr
  DO kk=1,Nr
    distxgr(kk) = (xgr(1)-xr(kk))**2+(xgr(2)-yr(kk))**2+(xgr(3)-zr(kk))**2 
  ENDDO
endif
#endif

#ifdef lk_inv_vapp

  DO kk=1,Nr
    distxopt_over_distxgr(kk) = sqrt( (xxx(1)-xr(kk))**2+(xxx(2)-yr(kk))**2+(xxx(3)-zr(kk))**2 )
  ENDDO

#else

! ratio distance between x candidate and all station over
! distance between xgr and all stations (int could be sufficient ?)
  DO kk=1,Nr
    distxopt_over_distxgr(kk) = sqrt( ((xxx(1)-xr(kk))**2+(xxx(2)-yr(kk))**2+(xxx(3)-zr(kk))**2) / distxgr(kk) )
  ENDDO

#endif

  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = dble(ttgr(kk)) * distxopt_over_distxgr(kk)
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = dble(ttgr(:)) * distxopt_over_distxgr(:)
  ENDIF
  tt(1:Nr0) = m2Pi * tt(1:Nr0)

  BA_xyz_expvm = 0.d0
  DO ff = 1, Nf
    phi_model(1:Nr0) = tt(1:Nr0)*freq(ff)
    zz = (0.d0,0.d0)
    DO ii = 1, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
    ENDDO
    BA_xyz_expvm = BA_xyz_expvm + zz*CONJG(zz)
  ENDDO
  BA_xyz_expvm = - BA_xyz_expvm / (Nr0*Nr0*Nf)
  return
END FUNCTION BA_xyz_expvm


subroutine myfunc(val, n, x, grad, need_gradient) ! , f_data)
  use glovarmodule , only : xr,yr,zr,freq,CSDMP,Nr,Nf,cel,nn_removezero,nkzero,nevals 
  implicit none
  integer :: n
  REAL(KIND=8), DIMENSION(n) :: x,grad
  INTEGER :: need_gradient
  REAL(KIND=8) :: val
  REAL(KIND=8) :: pi
  COMPLEX(KIND=8) :: p2jPi
  INTEGER :: ff,kk,kki,Nr0
#ifdef lk_float
  REAL(KIND=4), DIMENSION(Nr) :: tt
  COMPLEX(KIND=4), DIMENSION(Nr) :: CC,replica
  COMPLEX(KIND=4) :: CDOTC,alpha,beta
  alpha=(1.0,0.0)
  beta=(0.0,0.0)
#else
  REAL(KIND=8), DIMENSION(Nr) :: tt
  COMPLEX(KIND=8), DIMENSION(Nr) :: CC,replica
  COMPLEX(KIND=8) :: ZDOTC,alpha,beta
  alpha=(1.d0,0.d0)
  beta=(0.d0,0.d0)
#endif

  nevals=nevals+1  ! increment global counter in the objective function

  pi=4.d0*atan(1.d0)
  p2jPi = 2.d0 * (0.d0,1.d0) * pi

  val = 0.d0

  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
#ifdef lk_float
        tt(kki) = REAL( ( ((xr(kk)-x(1))**2+(yr(kk)-x(2))**2+(zr(kk)-x(3))**2)**0.5 ) / cel )  ! [Nr]
#else
        tt(kki) = ( ((xr(kk)-x(1))**2+(yr(kk)-x(2))**2+(zr(kk)-x(3))**2)**0.5 ) / cel  ! [Nr]
#endif
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
#ifdef lk_float
    tt(:) = REAL( ( ((xr(:)-x(1))**2+(yr(:)-x(2))**2+(zr(:)-x(3))**2)**0.5 ) / cel )  ! [Nr]
#else
    tt(:) = ( ((xr(:)-x(1))**2+(yr(:)-x(2))**2+(zr(:)-x(3))**2)**0.5 ) / cel  ! [Nr]
#endif
  ENDIF

  DO ff=1,Nf
    replica(1:Nr0) = EXP(p2jPi * tt(1:Nr0) * freq(ff)) ! [Nr] 
#ifdef lk_float
    CALL CHPMV('L',Nr0,alpha,CSDMP(:,ff),replica(1:Nr0),1,beta,CC(1:Nr0),1)
    val = val + ABS(CDOTC(Nr0,replica(1:Nr0),1,CC(1:Nr0),1)) / (Nr0*Nr0)
#else
    CALL ZHPMV('L',Nr0,alpha,CSDMP(:,ff),replica(1:Nr0),1,beta,CC(1:Nr0),1)
    val = val + ABS(ZDOTC(Nr0,replica(1:Nr0),1,CC(1:Nr0),1)) / (Nr0*Nr0)
#endif
  ENDDO
  val = -val / Nf
END subroutine myfunc

subroutine FindNearestNod(xyzpos,nodeindex,xgr)
  use glovarmodule , only : gridlocs ! ,gridlocsmask
  implicit none
  REAL(KIND=8), DIMENSION(3), INTENT(IN) :: xyzpos
  INTEGER, DIMENSION(1), INTENT(OUT) :: nodeindex
  INTEGER, DIMENSION(3), INTENT(OUT) :: xgr
! calcul en Int as gridlocs is Integer ?
  nodeindex = MINLOC( (NINT(xyzpos(1))-gridlocs(:,1))**2 + (NINT(xyzpos(2))-gridlocs(:,2))**2 + (NINT(xyzpos(3))-gridlocs(:,3))**2) ! , gridlocsmask)
  xgr = gridlocs(nodeindex(1),:)
!print *,'node: ',xgr
end subroutine FindNearestNod

subroutine myfunc_expvm(val, n, x, grad, need_gradient) ! , f_data)
  use glovarmodule , only : minN,xgr,ttgr,xr,yr,zr,freq,phi_data,phi_model,Nr,Nf,nn_removezero,nkzero,nevals,vmfile_id,distxgr
  implicit none
  integer :: n
  REAL(KIND=8), DIMENSION(n) :: x,grad
  INTEGER :: need_gradient
  REAL(KIND=8) :: val
  REAL(KIND=8), DIMENSION(Nr) :: tt
  REAL(KIND=8) :: m2Pi
  INTEGER :: ff,ii
  COMPLEX(KIND=8) :: zz
  COMPLEX(KIND=8), PARAMETER :: cj = ( 0.d0, 1.d0)
  INTEGER :: kk,kki,Nr0
  integer, dimension(1) :: newmin
  REAL(KIND=8), DIMENSION(Nr) :: distxopt_over_distxgr
  integer :: error
  character(len=7) :: c_minN
  INTEGER(HSIZE_T), DIMENSION(1) :: dims1ttgr

  nevals=nevals+1  ! increment global counter in the objective function

  m2Pi=-8.d0*atan(1.d0)

! Find the nearest velocity model grid cell (id and position) corresponding to the x candidate
  CALL FindNearestNod(x,newmin,xgr) ! [minN] = FindNearestNod(x,gridlocs,nnods)

if ( newmin(1) /= minN(1) ) then
  minN(1) = newmin(1)
! Read ttgr corresponding to minN
  write(c_minN,'(I0.7)') minN
  dims1ttgr=(/Nr/)
#ifdef lk_gridcell_double
  CALL h5ltread_dataset_f(vmfile_id, "/GridCell/"//TRIM(c_minN), H5T_NATIVE_DOUBLE, ttgr, dims1ttgr, error)
#else
  CALL h5ltread_dataset_f(vmfile_id, "/GridCell/"//TRIM(c_minN), H5T_NATIVE_REAL, ttgr, dims1ttgr, error)
#endif
  if (error/=0) call exit(error)
#ifdef lk_inv_vapp
endif
#else
  ! MAJ de var globale distxgr
  DO kk=1,Nr
    distxgr(kk) = (xgr(1)-xr(kk))**2+(xgr(2)-yr(kk))**2+(xgr(3)-zr(kk))**2
  ENDDO
endif
#endif

#ifdef lk_inv_vapp

  DO kk=1,Nr
    distxopt_over_distxgr(kk) = sqrt( (x(1)-xr(kk))**2+(x(2)-yr(kk))**2+(x(3)-zr(kk))**2 )
  ENDDO

#else

! ratio distance between x candidate and all station over
! distance between xgr and all stations (int could be sufficient ?)
  DO kk=1,Nr
    distxopt_over_distxgr(kk) = sqrt( ((x(1)-xr(kk))**2+(x(2)-yr(kk))**2+(x(3)-zr(kk))**2) / distxgr(kk) )
  ENDDO

#endif

  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = dble(ttgr(kk)) * distxopt_over_distxgr(kk)
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = dble(ttgr(:)) * distxopt_over_distxgr(:)
  ENDIF
  tt(1:Nr0) = m2Pi * tt(1:Nr0)

  val = 0.d0
  DO ff = 1, Nf
    phi_model(1:Nr0) = tt(1:Nr0)*freq(ff)
    zz = (0.d0,0.d0)
    DO ii = 1, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
    ENDDO
    val = val + zz*CONJG(zz)
  ENDDO
  val = - val / (Nr0*Nr0*Nf)
END subroutine myfunc_expvm

subroutine myfunc_exp(val, n, x, grad, need_gradient) ! , f_data)
  use glovarmodule , only : xr,yr,zr,freq,phi_data,phi_model,Nr,Nf,cel,nn_removezero,nkzero,nevals
  implicit none
  integer :: n
  REAL(KIND=8), DIMENSION(n) :: x,grad
  INTEGER :: need_gradient
  REAL(KIND=8) :: val
  REAL(KIND=8), DIMENSION(Nr) :: tt
  REAL(KIND=8) :: m2Pi
  INTEGER :: ff,ii
  COMPLEX(KIND=8) :: zz
  COMPLEX(KIND=8), PARAMETER :: cj = ( 0.d0, 1.d0)
  INTEGER :: kk,kki,Nr0

  nevals=nevals+1  ! increment global counter in the objective function

  m2Pi=-8.d0*atan(1.d0)

  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-x(1))**2+(yr(kk)-x(2))**2+(zr(kk)-x(3))**2)**0.5 ) / cel  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-x(1))**2+(yr(:)-x(2))**2+(zr(:)-x(3))**2)**0.5 ) / cel  ! [Nr]
  ENDIF
  tt(1:Nr0) = m2Pi * tt(1:Nr0)

  val = 0.d0
  DO ff = 1, Nf
    phi_model(1:Nr0) = tt(1:Nr0)*freq(ff)
    zz = (0.d0,0.d0)
    DO ii = 1, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
    ENDDO
    val = val + zz*CONJG(zz)
  ENDDO
  val = - val / (Nr0*Nr0*Nf)
END subroutine myfunc_exp

subroutine fBa_xyc_exp(val, n, x, grad, need_gradient) ! , f_data)
  use glovarmodule , only : xr,yr,zr,freq,phi_data,phi_model,Nr,Nf,zzz,nn_removezero,nkzero,nevals
  implicit none
  integer :: n
  REAL(KIND=8), DIMENSION(n) :: x,grad
  INTEGER :: need_gradient
  REAL(KIND=8) :: val
  REAL(KIND=8), DIMENSION(Nr) :: tt
  REAL(KIND=8) :: m2Pi
  INTEGER :: ff,ii
  COMPLEX(KIND=8) :: zz
  COMPLEX(KIND=8), PARAMETER :: cj = ( 0.d0, 1.d0)
  INTEGER :: kk,kki,Nr0

  nevals=nevals+1  ! increment global counter in the objective function

  m2Pi=-8.d0*atan(1.d0)

  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-x(1))**2+(yr(kk)-x(2))**2+(zr(kk)-zzz)**2)**0.5 ) / x(3)  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-x(1))**2+(yr(:)-x(2))**2+(zr(:)-zzz)**2)**0.5 ) / x(3)  ! [Nr]
  ENDIF
  tt(1:Nr0) = m2Pi * tt(1:Nr0)

  val = 0.d0
  DO ff = 1, Nf
    phi_model(1:Nr0) = tt(1:Nr0)*freq(ff)
    zz = (0.d0,0.d0)
    DO ii = 1, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
    ENDDO
    val = val + zz*CONJG(zz)
  ENDDO
  val = - val / (Nr0*Nr0*Nf)

!print *,'optf',nevals,val
!print *,'optx',x

END subroutine fBa_xyc_exp

subroutine fBa_xyzc_exp(val, n, x, grad, need_gradient) ! , f_data)
  use glovarmodule , only : xr,yr,zr,freq,phi_data,phi_model,Nr,Nf,nn_removezero,nkzero,nevals
  implicit none
  integer :: n
  REAL(KIND=8), DIMENSION(n) :: x,grad
  INTEGER :: need_gradient
  REAL(KIND=8) :: val
  REAL(KIND=8), DIMENSION(Nr) :: tt
  REAL(KIND=8) :: m2Pi
  INTEGER :: ff,ii
  COMPLEX(KIND=8) :: zz
  COMPLEX(KIND=8), PARAMETER :: cj = ( 0.d0, 1.d0)
  INTEGER :: kk,kki,Nr0

  nevals=nevals+1  ! increment global counter in the objective function

  m2Pi=-8.d0*atan(1.d0)

  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-x(1))**2+(yr(kk)-x(2))**2+(zr(kk)-x(3))**2)**0.5 ) / x(4)  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-x(1))**2+(yr(:)-x(2))**2+(zr(:)-x(3))**2)**0.5 ) / x(4)  ! [Nr]
  ENDIF
  tt(1:Nr0) = m2Pi * tt(1:Nr0)

  val = 0.d0
  DO ff = 1, Nf
    phi_model(1:Nr0) = tt(1:Nr0)*freq(ff)
    zz = (0.d0,0.d0)
    DO ii = 1, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
    ENDDO
    val = val + zz*CONJG(zz)
  ENDDO
  val = - val / (Nr0*Nr0*Nf)
END subroutine fBa_xyzc_exp

subroutine fBa_xyzc_exp_zpen(val, n, x, grad, need_gradient) ! , f_data)
  use glovarmodule , only : xr,yr,zr,freq,phi_data,phi_model,Nr,Nf,nn_removezero,nkzero,nevals
  implicit none
  integer :: n
  REAL(KIND=8), DIMENSION(n) :: x,grad
  INTEGER :: need_gradient
  REAL(KIND=8) :: val
  REAL(KIND=8), DIMENSION(Nr) :: tt
  REAL(KIND=8) :: m2Pi,zsurf
  INTEGER :: ff,ii
  COMPLEX(KIND=8) :: zz
  COMPLEX(KIND=8), PARAMETER :: cj = ( 0.d0, 1.d0)
  INTEGER :: kk,kki,Nr0

  nevals=nevals+1  ! increment global counter in the objective function

  m2Pi=-8.d0*atan(1.d0)

  IF ( nn_removezero == 1 ) THEN
    kki=0
    DO kk=1,Nr
      IF ( nkzero(kk) == 1 ) THEN
        kki=kki+1
        tt(kki) = ( ((xr(kk)-x(1))**2+(yr(kk)-x(2))**2+(zr(kk)-x(3))**2)**0.5 ) / x(4)  ! [Nr]
      ENDIF
    ENDDO
    Nr0=kki
  ELSE
    Nr0=Nr
    tt(:) = ( ((xr(:)-x(1))**2+(yr(:)-x(2))**2+(zr(:)-x(3))**2)**0.5 ) / x(4)  ! [Nr]
  ENDIF
  tt(1:Nr0) = m2Pi * tt(1:Nr0)

  val = 0.d0
  DO ff = 1, Nf
    phi_model(1:Nr0) = tt(1:Nr0)*freq(ff)
    zz = (0.d0,0.d0)
    DO ii = 1, Nr0
      zz = zz + EXP ( cj * (phi_data(ii,ff)-phi_model(ii)) )
    ENDDO
    val = val + zz*CONJG(zz)
  ENDDO
  val = - val / (Nr0*Nr0*Nf)
  call fsurf(x(1),x(2),zsurf)
  if ( x(3) > zsurf ) then
    if ( x(3) > (zsurf+5.0) ) then
      val = 0.d0
    else
      val = val * (zsurf+5.0-x(3))/5.0
    endif
  endif
END subroutine fBa_xyzc_exp_zpen

subroutine fsurf(xx,yy,zz)
  use glovarmodule , only : dn_c0, dn_cx, dn_cy, dn_cxx, dn_cyy, dn_cxy, &
                            dn_cxxx, dn_cyyy, dn_cxxy, dn_cxyy, &
                            dn_cxxxx, dn_cyyyy, dn_cxxxy, dn_cxxyy, dn_cxyyy

  implicit none
  real(kind=8), intent(in) :: xx,yy
  real(kind=8), intent(out) :: zz
!  real(kind=8), parameter :: p00 = 2367 , p10 = 0.08378 , p01 = -0.0425 , p20 = -4.837E-05 , p11 = 1.698E-05 , p02 = -0.0001038
!     Linear model Poly22:
!     f(x,y) = p00 + p10*x + p01*y + p20*x^2 + p11*x*y + p02*y^2
!     Coefficients (with 95% confidence bounds):
!       p00 =        2367  (2366, 2368)
!       p10 =     0.08378  (0.08126, 0.0863)
!       p01 =     -0.0425  (-0.04561, -0.03939)
!       p20 =  -4.837e-05  (-6.523e-05, -3.152e-05)
!       p11 =   1.698e-05  (-1.678e-05, 5.074e-05)
!       p02 =  -0.0001038  (-0.00013, -7.761e-05)
!  zz = p00 + p10*xx + p01*yy + p20*xx*xx + p11*xx*yy + p02*yy*yy
  zz = dn_c0 + dn_cx*xx + dn_cy*yy + dn_cxx*xx*xx + dn_cxy*xx*yy + dn_cyy*yy*yy + &
       dn_cxxx*xx*xx*xx + dn_cyyy*yy*yy*yy + dn_cxxy*xx*xx*yy + dn_cxyy*xx*yy*yy + &
       dn_cxxxx*xx*xx*xx*xx + dn_cyyyy*yy*yy*yy*yy + dn_cxxxy*xx*xx*xx*yy + dn_cxxyy*xx*xx*yy*yy + dn_cxyyy*xx*yy*yy*yy
end subroutine fsurf

end MODULE modcomputebeam_optim
