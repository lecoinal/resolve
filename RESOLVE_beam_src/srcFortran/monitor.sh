#!/bin/bash

set -e

j="$1" # "8040935"
f="OAR.beam.${j}.stdout"

node=$(head -1 $f | sed -e "s/luke//")
grep "it | totsec | sec / active trace:" $f | awk -F: '{print $2}' > zzzzz_${j}
cat zzzzz_${j} | awk -v var="$node" '{print $0 " " var}' > perf_${j}.txt
rm zzzzz_${j}
