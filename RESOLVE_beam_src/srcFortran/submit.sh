#!/bin/bash
#OAR --project imagin
##OAR --project iste-equ-ondes
#OAR -l /core=1,walltime=0:12:00
#OAR -n beamSA
##OAR -p network_address='luke4'
#OAR -p network_address='luke56'
##OAR -p network_address='ist-calcul6.ujf-grenoble.fr'


#  =======================================================================================
#   ***  Shell wrapper to submit beamforming_simulated_annealing on luke (batch or Cigri) cluster or 
#        on ISTERRE clusters ***
#    
#  =======================================================================================
#   History :  1.0  : 11/2016  : A. Lecointre : Initial version
#               
#  ---------------------------------------------------------------------------------------
#  $Id: submit.sh 155 2018-10-11 13:00:43Z lecointre $
#  ---------------------------------------------------------------------------------------

# HELP for Module and Compilation and Linking:
# $ module load intel-devel/18.0.1 hdf5/1.8.20_serial_intel-18.0.1 zlib/1.2.11_intel-18.0.1 szip/2.1.1_intel-18.0.1
# $ make clean
# rm -f modcomputebeam.o beamforming_simulated_annealing *.mod 
# $ make
# h5fc     -cpp -Dlk_mkl -O2  -c -o modcomputebeam.o modcomputebeam.f90 
# h5fc     -cpp -Dlk_mkl -O2  -o beamforming_simulated_annealing modcomputebeam.o beamforming_simulated_annealing.f90 -mkl=sequential   
# 

#########################################################

CAMPAIGN_JOB_ID=$1  # doit etre unique, utilise pour tagguer TMPDIR, facilite tri doublons ou jobs manquants
JDAY=$2
JBEG=$3
fmask=$4

FREQ_INPUT_DATA="PREPROCESS_BP_1_12_DEC100Hz_WITH_BOREHOLE"
#FREQ_INPUT_DATA="PREPROCESS_BP_1_12_DEC100Hz"

IFILE="SG_2014_${JDAY}.h5"

#########################################################

# Function to get parameter from namelist
getParam() {
   pair=$(grep "^ *$1 *= *" namelist.skel | sed 's/!.*$//')
   if [[ -z "${pair}" ]] ; then
      echo 0
   else
      echo $(echo ${pair} | awk -F = '{ print $2 }' | awk '{ print $1 }')
   fi
}

#########################################################

set -e

# change here if you are on ISTerre or CIMENT-grid cluster
#source /soft/env.bash
source /applis/ciment/v2/env.bash

##########################################################

START=$(date +%s.%N)

cat $OAR_NODE_FILE

echo "$JDAY $JBEG"

# If not cigri, output dir is arbitrary Process_ID
PID=$$
#CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=$PID}  # substitute if not initialized
CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=00000}  # substitute if not initialized

case "$CLUSTER_NAME" in
  luke)
    IDIR="/$SHARED_SCRATCH_DIR/lecointre/";
    module load intel-devel/13 hdf5/1.8.14_intel-13.0.1 szip/2.1_gcc-4.6.2 zlib/1.2.7_gcc-4.6.2
    #module load gnu-devel/4.6.2 hdf5/1.8.14-openmpi-1.6.5_gcc-4.6.2 szip/2.1_gcc-4.6.2 zlib/1.2.7_gcc-4.6.2
    #module load gnu-devel/4.6.2 hdf5/1.8.14-openmpi-1.6.5_gcc-4.6.2 szip/2.1_gcc-4.6.2 zlib/1.2.7_gcc-4.6.2 lapack/3.4.0_gcc-4.6.2 blas/19.04.2011_gcc-4.6.2
    #module load gnu-devel/4.6.2 hdf5/1.8.14-openmpi-1.6.5_gcc-4.6.2 szip/2.1_gcc-4.6.2 zlib/1.2.7_gcc-4.6.2 atlas/3.10.2_gcc-4.6.2
    ;;
  ISTERRE)
    IDIR="/media/luke4_scratch_md1200/lecointre/";
    #module load zlib/1.2.8_intel-15.0.1 szip/2.1_intel-15.0.1 hdf5/1.8.14_intel-15.0.1 intel-devel/15.0.1
    module load intel-devel/18.0.1 hdf5/1.8.20_serial_intel-18.0.1 zlib/1.2.11_intel-18.0.1 szip/2.1.1_intel-18.0.1
    #module load gnu-devel/6.3.0 hdf5/1.8.20_serial_gcc_6.3.0 zlib/1.2.11_gcc-6.3.0 szip/2.1.1_gcc_6.3.0
    ;;
  *)
    echo "$CLUSTER_NAME not supported";
    exit 1;
    ;;
esac

TMPDIR="/$SHARED_SCRATCH_DIR/$USER/oar.$CIGRI_CAMPAIGN_ID.$JDAY.$OAR_JOB_ID.$CAMPAIGN_JOB_ID"
mkdir -p /$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID

EXE="beamforming_simulated_annealing"

echo "TMPDIR : $TMPDIR"
mkdir -p $TMPDIR

#Get code and namelist
cp $EXE $TMPDIR/.
sed -e "s/<<<JBEG>>>/$JBEG/" -e "s/<<<IFILE>>>/$IFILE/" namelist_chloe.skel > $TMPDIR/namelist

# Add borehole
echo "B946.EHZ.2014.${JDAY} -116.5925  33.5373 1286.3536" > $TMPDIR/zreseau.txt

# Get metrics and mask
grep -v -f /$SHARED_SCRATCH_DIR/$USER/INPUT_FILES/$JDAY/$fmask /$SHARED_SCRATCH_DIR/$USER/INPUT_FILES/zreseau_$JDAY.txt >> $TMPDIR/zreseau.txt
#cat /$SHARED_SCRATCH_DIR/$USER/INPUT_FILES/zreseau_$JDAY.txt > $TMPDIR/zreseau.txt

cd $TMPDIR

# get inputs
ln -sf $IDIR/SAN_JACINTO/${FREQ_INPUT_DATA}/HDF5/${IFILE} .

wc -l zreseau.txt  # nb_sta after trim energy limit

#/usr/bin/time -v ./$EXE
#valgrind --leak-check=yes ./$EXE
./$EXE

mv out.h5 out_${IFILE%\.h5}_${JBEG}.h5

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"

mv $TMPDIR /$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID/.
