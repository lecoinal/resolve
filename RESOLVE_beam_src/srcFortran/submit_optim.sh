#!/bin/bash
#OAR --project imagin
#OAR -l /core=1,walltime=200:00:00
#OAR -n beam
#OAR -p network_address='luke56'

#  =======================================================================================
#   ***  Shell wrapper to submit beamforming_optim on luke (batch or Cigri) cluster or 
#        on ISTERRE clusters ***
#    
#  =======================================================================================
#   History :  1.0  : 07/2018  : A. Lecointre : Initial version
#               
#  ---------------------------------------------------------------------------------------
#  $Id: submit_optim.sh 158 2018-10-29 10:29:49Z lecointre $
#  ---------------------------------------------------------------------------------------

# compilation sur luke:
# source /applis/ciment/v2/env.bash'
# module load intel-devel/13 hdf5/1.8.14_intel-13.0.1 szip/2.1_gcc-4.6.2 zlib/1.2.7_gcc-4.6.2
# make
# h5pfc     -cpp -Dlk_float   -c -o glovarmodule.o glovarmodule.f90 
# h5pfc     -cpp -Dlk_float   -c -o modcomputebeam_optim.o glovarmodule.o modcomputebeam_optim.f90 
# h5pfc     -cpp -Dlk_float   -c -o modcomputebeam.o glovarmodule.o modcomputebeam.f90 
# h5pfc     -cpp -Dlk_float   -o beamforming_simulated_annealing glovarmodule.o modcomputebeam.o beamforming_simulated_annealing.f90 -mkl=sequential   
# h5pfc     -cpp -Dlk_float   -o beamforming glovarmodule.o modcomputebeam.o beamforming.f90 -mkl=sequential   
# h5pfc     -cpp -Dlk_float   -o beamforming_optim glovarmodule.o modcomputebeam.o modcomputebeam_optim.o beamforming_optim.f90 -mkl=sequential    -L/home/lecointre/lib/intel-devel-13 -lbrentasa047

#########################################################

CIGRI_JOB_ID=$1
IFILE=$2 # 2015_06_26-17_00_00.h5 
JBEG=$3 # $((2206*250)) # 551500 # 0 or 449625
STARTX=$4
STARTY=$5
STARTZ=$6

#NBSTA=1397
NBSTA=10050

VelModel3D=1   # only if NBSTA=10050

#########################################################

set -e

# change here if you are on ISTerre or CIMENT-grid cluster
#source /soft/env.bash
source /applis/ciment/v2/env.bash

#INPUT_DIR="/$SHARED_SCRATCH_DIR/chmielm/REORGANIZED"
INPUT_DIR="/$SHARED_SCRATCH_DIR/$USER/GOSIA/INPUT_FILES/"  # zreseau.txt reorganised as 3D velocity model

VelModel3D_FILE="/$SHARED_SCRATCH_DIR/lecointre/GOSIA/3DVelModel/Final_4/out/ttgrsple/travelTimeTable3Dv2_0_32.h5"  # ttgr (not inv_vapp) and F32

##########################################################

START=$(date +%s.%N)

cat $OAR_NODE_FILE

echo "$IFILE $JBEG $STARTX $STARTY $STARTZ"

# If not cigri, output dir is arbitrary Process_ID
PID=$$
#CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=$PID}  # substitute if not initialized
CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=00000}  # substitute if not initialized

case "$CLUSTER_NAME" in
  luke)
    module load intel-devel/13 hdf5/1.8.14_intel-13.0.1 szip/2.1_gcc-4.6.2 zlib/1.2.7_gcc-4.6.2
    #module load gnu-devel/4.6.2 hdf5/1.8.14-openmpi-1.6.5_gcc-4.6.2 szip/2.1_gcc-4.6.2 zlib/1.2.7_gcc-4.6.2
    #module load gnu-devel/4.6.2 hdf5/1.8.14-openmpi-1.6.5_gcc-4.6.2 szip/2.1_gcc-4.6.2 zlib/1.2.7_gcc-4.6.2 lapack/3.4.0_gcc-4.6.2 blas/19.04.2011_gcc-4.6.2
    #module load gnu-devel/4.6.2 hdf5/1.8.14-openmpi-1.6.5_gcc-4.6.2 szip/2.1_gcc-4.6.2 zlib/1.2.7_gcc-4.6.2 atlas/3.10.2_gcc-4.6.2
    ;;
  ISTERRE)
    #module load zlib/1.2.8_intel-15.0.1 szip/2.1_intel-15.0.1 hdf5/1.8.14_intel-15.0.1 intel-devel/15.0.1
    module load intel-devel/18.0.1 hdf5/1.8.20_serial_intel-18.0.1 zlib/1.2.11_intel-18.0.1 szip/2.1.1_intel-18.0.1
    #module load gnu-devel/6.3.0 hdf5/1.8.20_serial_gcc_6.3.0 zlib/1.2.11_gcc-6.3.0 szip/2.1.1_gcc_6.3.0
    ;;
  *)
    echo "$CLUSTER_NAME not supported";
    exit 1;
    ;;
esac

TMPDIR="/$SHARED_SCRATCH_DIR/$USER/oar.$CIGRI_CAMPAIGN_ID.${IFILE/\.h5/}.$JBEG.$OAR_JOB_ID.$CIGRI_JOB_ID"
mkdir -p /$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID

EXE="beamforming_optim"

echo "TMPDIR : $TMPDIR"
mkdir -p $TMPDIR

#Get code and namelist
cp $EXE $TMPDIR/.
cp freq.txt $TMPDIR/.
sed -e "s/<<<JBEG>>>/$JBEG/" -e "s/<<<IFILE>>>/$IFILE/" \
    -e "s/<<<STARTX>>>/$STARTX/" -e "s/<<<STARTY>>>/$STARTY/" -e "s/<<<STARTZ>>>/$STARTZ/" namelist.skel > $TMPDIR/namelist

# Get metrics and mask
cat ${INPUT_DIR}/zreseau_${IFILE/\.h5/}.txt > $TMPDIR/zreseau.txt
if [ $NBSTA == 1397 ] ; then
  awk '{print "A" $4 "_" $5}' GlenhavenStationsSelection.txt > $TMPDIR/GlenhavenStationsSelection.txt
fi

cd $TMPDIR

if [ $NBSTA == 1397 ] ; then
  grep -f GlenhavenStationsSelection.txt zreseau.txt > zzzzz
  mv zzzzz zreseau.txt
fi

# get inputs
ln -sf ${INPUT_DIR}/${IFILE} .

if [ $VelModel3D == 1 ] ; then
  ln -sf $VelModel3D_FILE GridTrav3.h5
fi

ln -sf /bettik/lecointre/GOSIA/INPUT_FILES/StationLocs.h5 .

wc -l zreseau.txt  # nb_sta after trim energy limit

#/usr/bin/time -v ./$EXE
#valgrind --leak-check=yes ./$EXE
./$EXE

mv out.h5 out_${IFILE/\.h5/}_${JBEG}.h5

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"

mv $TMPDIR /$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID/.
