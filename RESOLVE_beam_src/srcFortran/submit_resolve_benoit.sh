#!/bin/bash
#OAR --project resolve
#OAR -l /core=1,walltime=00:10:00
#OAR -n beam
##OAR -p network_address='luke56'
##OAR -t besteffort

#  =======================================================================================
#   ***  Shell wrapper to submit beamforming_optim on luke (batch or Cigri) cluster or 
#        on ISTERRE clusters ***
#    
#  =======================================================================================
#   History :  1.0  : 07/2018  : A. Lecointre : Initial version
#               
#  ---------------------------------------------------------------------------------------
#  $Id$
#  ---------------------------------------------------------------------------------------

# compilation sur luke:
# source /applis/ciment/v2/env.bash'
# module load intel-devel/13 hdf5/1.8.14_intel-13.0.1 szip/2.1_gcc-4.6.2 zlib/1.2.7_gcc-4.6.2
# make beamforming_optim
# h5pfc     -O2 -cpp   -c -o glovarmodule.o glovarmodule.f90 
# h5pfc     -O2 -cpp   -c -o modcomputebeam.o glovarmodule.o modcomputebeam.f90 
# h5pfc     -O2 -cpp   -c -o modcomputebeam_optim.o glovarmodule.o modcomputebeam_optim.f90 
# h5pfc     -O2 -cpp   -o beamforming_optim glovarmodule.o modcomputebeam.o modcomputebeam_optim.o beamforming_optim.f90 -mkl=sequential    -L/home/lecointre/lib/intel-devel-13 -lbrentasa047 -I/home/lecointre/softs/nlopt-2.4.2/include -L/home/lecointre/softs/nlopt-2.4.2/lib -lnlopt -lm

#########################################################

CIGRI_JOB_ID=00000 # $1 # 00000
IFILE=ZO_2018_121.h5

JBEG1=14917750 # 08:17:15.5
FMIN=7.5
FMAX=11.5

OPTIM=4 # (xyzc)
XSTART=$1 # -90 # $8
YSTART=$2 # -90 # $9
ZSTART=$3 # 2363.732  # ${10}
REF=$4


#########################################################

set -e

# change here if you are on ISTerre or CIMENT-grid cluster
#source /soft/env.bash
source /applis/ciment/v2/env.bash


case "$CLUSTER_NAME" in
  luke|dahu|bigfoot|froggy|ceciccluster)  # dahu
    INPUT_DIR="$SHARED_SCRATCH_DIR/$USER/RESOLVE/PREPROCESS/EXP000/"
    METRIC_DIR="$SHARED_SCRATCH_DIR/$USER/RESOLVE/METRIC/"
    ;;
  *)  # gofree
    INPUT_DIR="/cigri/home/lecoin56/RESOLVE/PREPROCESS/EXP000/"
    METRIC_DIR="/cigri/home/lecoin56/RESOLVE/METRIC/"
    ;;
esac

##########################################################

START=$(date +%s.%N)

cat $OAR_NODE_FILE

echo "$IFILE $JBEG1 $FMIN $FMAX $OPTIM $XSTART $YSTART $ZSTART"

# If not cigri, output dir is arbitrary Process_ID
PID=$$
#CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=$PID}  # substitute if not initialized
CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=00000}  # substitute if not initialized

case "$CLUSTER_NAME" in
  ISTERRE)
    module load intel-devel/18.0.1 hdf5/1.8.20_serial_intel-18.0.1 zlib/1.2.11_intel-18.0.1 szip/2.1.1_intel-18.0.1
    echo "$CLUSTER_NAME not supported, please copy input files into SHARED_SCRATCH_DIR";
    exit 1;
    ;;
  *)
    module load intel-devel/13 
    module load hdf5/1.8.14_intel-13.0.1
    module load szip/2.1_gcc-4.6.2
    module load zlib/1.2.7_gcc-4.6.2
    ;;
esac

case "$CLUSTER_NAME" in
  luke|dahu|bigfoot)  # dahu
    TMPDIR="$SHARED_SCRATCH_DIR/$USER/oar.$OAR_JOB_ID/oar.$CIGRI_CAMPAIGN_ID.$CLUSTER_NAME.${IFILE/\.h5/}.$FMIN.$FMAX.$XSTART.$YSTART.$ZSTART.$OAR_JOB_ID.$CIGRI_JOB_ID"
    mkdir -p $SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID
    ;;
  froggy|ceciccluster) 
    TMPDIR="$LOCAL_SCRATCH_DIR/$USER/oar.$OAR_JOB_ID/oar.$CIGRI_CAMPAIGN_ID.$CLUSTER_NAME.${IFILE/\.h5/}.$FMIN.$FMAX.$XSTART.$YSTART.$ZSTART.$OAR_JOB_ID.$CIGRI_JOB_ID"
    mkdir -p $SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID
    ;;
  gofree)
    module load irods
    TMPDIR="$LOCAL_SCRATCH_DIR/$USER/oar.$OAR_JOB_ID/oar.$CIGRI_CAMPAIGN_ID.$CLUSTER_NAME.${IFILE/\.h5/}.$FMIN.$FMAX.$XSTART.$YSTART.$ZSTART.$OAR_JOB_ID.$CIGRI_JOB_ID"
    imkdir -p /cigri/home/lecoin56/$CIGRI_CAMPAIGN_ID
    ;;
esac

EXE="beamforming_optim"

echo "TMPDIR : $TMPDIR"
mkdir -p $TMPDIR

#Get code and namelist
cp $EXE $TMPDIR/.

sed -e "s/<<<JBEG>>>/$JBEG1/" -e "s/<<<IFILE>>>/$IFILE/" -e "s/<<<XSTART>>>/$XSTART/" -e "s/<<<YSTART>>>/$YSTART/" -e "s/<<<ZSTART>>>/$ZSTART/" \
    -e "s/<<<FMIN>>>/$FMIN/" -e "s/<<<FMAX>>>/$FMAX/" -e "s/<<<OPTIM>>>/$OPTIM/" \
    -e "s/<<<REF>>>/$REF/" namelist_arg.skel > $TMPDIR/namelist1

cd $TMPDIR

case "$CLUSTER_NAME" in
  luke|dahu|bigfoot|froggy|ceciccluster) # dahu
    # Get metrics and mask
    cat ${METRIC_DIR}/zreseau_${IFILE/\.h5/}.txt > zreseau.txt
    # get inputs
    ln -sf ${INPUT_DIR}/${IFILE} .
    ;;
  *)  # gofree
    secure_iget -v ${METRIC_DIR}/zreseau_${IFILE/\.h5/}.txt zreseau.txt
    secure_iget -v ${INPUT_DIR}/${IFILE} .
    ;;
esac

wc -l zreseau.txt  # nb_sta after trim energy limit

#/usr/bin/time -v ./$EXE
#valgrind --leak-check=yes ./$EXE

ln -sf namelist1 namelist
./$EXE
h5repack -i out.h5 -o out_${IFILE/\.h5/}_${JBEG1}.h5 -f GZIP=6 -v
rm out.h5

rm $EXE zreseau.txt ${IFILE}

case "$CLUSTER_NAME" in
  luke|dahu|bigfoot|froggy|ceciccluster)
    mv $TMPDIR /$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID/.
    sync 
    ;;
  *) # gofree
    module load irods
    secure_iput -rfv $TMPDIR /cigri/home/lecoin56/$CIGRI_CAMPAIGN_ID/.
    rm -r $TMPDIR
    ;;
esac

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"
