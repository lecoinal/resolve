#!/bin/bash
#OAR --project resolve
#OAR -l /core=1,walltime=0:10:00
#OAR -n beam_sggs
##OAR -p network_address='luke56'
##OAR -t besteffort

#  =======================================================================================
#   ***  Shell wrapper to submit beamforming_optim on luke (batch or Cigri) cluster or 
#        on ISTERRE clusters ***
#    
#  =======================================================================================
#   History :  1.0  : 07/2018  : A. Lecointre : Initial version
#               
#  ---------------------------------------------------------------------------------------
#  $Id$
#  ---------------------------------------------------------------------------------------

# compilation sur luke:
# source /applis/ciment/v2/env.bash'
# module load intel-devel/13 hdf5/1.8.14_intel-13.0.1 szip/2.1_gcc-4.6.2 zlib/1.2.7_gcc-4.6.2
# make beamforming_optim
# h5pfc     -O2 -cpp   -c -o glovarmodule.o glovarmodule.f90 
# h5pfc     -O2 -cpp   -c -o modcomputebeam.o glovarmodule.o modcomputebeam.f90 
# h5pfc     -O2 -cpp   -c -o modcomputebeam_optim.o glovarmodule.o modcomputebeam_optim.f90 
# h5pfc     -O2 -cpp   -o beamforming_optim glovarmodule.o modcomputebeam.o modcomputebeam_optim.o beamforming_optim.f90 -mkl=sequential    -L/home/lecointre/lib/intel-devel-13 -lbrentasa047 -I/home/lecointre/softs/nlopt-2.4.2/include -L/home/lecointre/softs/nlopt-2.4.2/lib -lnlopt -lm

#########################################################


CIGRI_JOB_ID=$1 # 00000 # $1 # 00000
JDAY=$2 # 220  # 224
FMIN=$3 # 11.0 # $3 # 5.0
FMAX=$4 # 15.0 # $4 # 9.0
XSTART=$5
YSTART=$6

CIGRI_JOB_ID=00000 # $1 # 00000
JDAY=220 # 224  # 224
FMIN=11.0 # $3 # 11.0 # $3 # 5.0
FMAX=15.0 # $4 # 15.0 # $4 # 9.0
XSTART=487895.217 # 5777397.564 $5
YSTART=5777397.564 # $6
#XSTART=0.0 # 487895.217 # 5777397.564 $5
#YSTART=0.0 # 5777397.564 # $6

#########################################################

set -e

source /applis/site/guix-start.sh

INPUT_DIR="/mantis/home/lecoinal/SGGS/"
METRIC_DIR="/mantis/home/lecoinal/SGGS/METRIC/"

##########################################################

getParam() {
   # Get a parameter value from namelist
   value=$(grep "^ *$1 *= *" namelist | sed 's/!.*$//' | awk -F= '{print $2}')
   echo $value
}


START=$(date +%s.%N)

cat $OAR_NODE_FILE

echo "$JDAY $FMIN $FMAX $XSTART $YSTART"

# If not cigri, output dir is arbitrary Process_ID
PID=$$
#CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=$PID}  # substitute if not initialized
CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=00000}  # substitute if not initialized

refresh_guix resolve_gnuhdf5

TMPDIR=$SHARED_SCRATCH_DIR/PROJECTS/pr-resolve/$USER/tmp/oar.$OAR_JOB_ID/

OUTDIR=$SHARED_SCRATCH_DIR/PROJECTS/pr-resolve/$USER/$CIGRI_CAMPAIGN_ID/${FMIN}_${FMAX}/${XSTART}_${YSTART}_${ZSTART}/
mkdir -p $OUTDIR

EXE="beamforming_optim"

echo "TMPDIR : $TMPDIR"
mkdir -p $TMPDIR

#Get code and namelist
cp $EXE $TMPDIR/.

sed -e "s/<<<XSTART>>>/$XSTART/" -e "s/<<<YSTART>>>/$YSTART/" \
    -e "s/<<<FMIN>>>/$FMIN/" -e "s/<<<FMAX>>>/$FMAX/" namelist_sggs.skel > $TMPDIR/namelist

cd $TMPDIR

iget ${INPUT_DIR}/1B_2019_${JDAY}.h5 .
iget ${INPUT_DIR}/SG_2019_${JDAY}.h5 .

#iget ${METRIC_DIR}/zreseau_${JDAY}_utm.txt zreseau.txt

iget ${METRIC_DIR}/zreseau_220_224_utm.txt zreseau.txt
#iget ${METRIC_DIR}/zreseau_220_224_utm_relatif.txt zreseau.txt
sed -e "s/DOY/$JDAY/" zreseau.txt > zzzzz


wc -l zreseau.txt

mv 1B_2019_${JDAY}.h5 in.h5
for dset in $(h5ls SG_2019_${JDAY}.h5 | awk '{print $1}') ; do 
  h5copy -i SG_2019_${JDAY}.h5 -o in.h5 -s $dset -d $dset
done
rm SG_2019_${JDAY}.h5

h5ls in.h5 | awk '{print $1}' > list_avail_sta.txt

grep -f list_avail_sta.txt zzzzz > zreseau.txt
wc -l zreseau.txt
sort -k 1 zreseau.txt > zzzzz
mv zzzzz zreseau.txt
rm list_avail_sta.txt
#rm zzzzz list_avail_sta.txt

./$EXE

rm $EXE # in.h5

mv $TMPDIR $OUTDIR

sync

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"
