#!/bin/bash
#OAR --project resolve
#OAR -l /core=1,walltime=0:01:00
#OAR -n beam_tetr
##OAR -p network_address='luke56'
##OAR -t besteffort
#OAR -t devel

#  =======================================================================================
#   ***  Shell wrapper to submit beamforming_optim on dahu|luke (batch or Cigri) cluster  ***
#    
#  =======================================================================================
#   History :  1.0  : 07/2018  : A. Lecointre : Initial version
#               
#  ---------------------------------------------------------------------------------------
#  $Id$
#  ---------------------------------------------------------------------------------------

#########################################################

# 00000 124 11.0 15.0 -200.00	-100.00	3178.590 


#CIGRI_JOB_ID=$1 # 00000 # $1 # 00000
#JDAY=$2 # 220  # 224
#FMIN=$3 # 11.0 # $3 # 5.0
#FMAX=$4 # 15.0 # $4 # 9.0
#XSTART=$5
#YSTART=$6
#ZSTART=$7 # 45.856 # mean lat

CIGRI_JOB_ID=00000 # $1 # 00000
JDAY=124 # $1 # 141 # $1 # 124 # 123 - 142 
FMIN=11.0 # 48.0
FMAX=15.0 # 52.0 # entre 5 et 50 ou 70 par pas de 2Hz
XSTART=0.0 # 6.82  # mean lon among 59 stations
YSTART=0.0 # 45.856 # mean lat
ZSTART=3208.575 # 0.0 # 45.856 # mean lat

#########################################################

set -e

source /applis/site/guix-start.sh
refresh_guix resolve_gnuhdf5

INPUT_DIR="/bettik/lecoinal/pr-teterousse/PREPROCESS/"
METRIC_DIR="/bettik/lecoinal/pr-teterousse/METRIC_invert_D_1.2/"

##########################################################

getParam() {
   # Get a parameter value from namelist
   value=$(grep "^ *$1 *= *" namelist | sed 's/!.*$//' | awk -F= '{print $2}')
   echo $value
}


START=$(date +%s.%N)

cat $OAR_NODE_FILE

echo "$JDAY $FMIN $FMAX $XSTART $YSTART $ZSTART"

# If not cigri, output dir is arbitrary Process_ID
PID=$$
#CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=$PID}  # substitute if not initialized
CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=00000}  # substitute if not initialized


TMPDIR=$SHARED_SCRATCH_DIR/$USER/tmp/oar.$OAR_JOB_ID/

OUTDIR=$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID/${FMIN}_${FMAX}/${XSTART}_${YSTART}_${ZSTART}/
mkdir -p $OUTDIR

EXE="beamforming_optim"

echo "TMPDIR : $TMPDIR"
mkdir -p $TMPDIR

#Get code and namelist
cp $EXE $TMPDIR/.

sed -e "s/<<<XSTART>>>/$XSTART/" -e "s/<<<YSTART>>>/$YSTART/" -e "s/<<<ZSTART>>>/$ZSTART/" \
    -e "s/<<<FMIN>>>/$FMIN/" -e "s/<<<FMAX>>>/$FMAX/" namelist_tetr.skel > $TMPDIR/namelist

cd $TMPDIR

ln -s ${INPUT_DIR}/2022_${JDAY}.h5 in.h5
ln -s ${METRIC_DIR}/zreseau_TETR_2022_${JDAY}.txt zreseau.txt

./$EXE

rm $EXE # in.h5

mv out.h5 beam_${JDAY}.h5

#mv $TMPDIR/* $OUTDIR/.
mv $TMPDIR $OUTDIR/.

sync

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"
