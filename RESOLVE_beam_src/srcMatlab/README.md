Codes matlab pour calcul du bartlett cohérent / incohérent sur une grille régulière.

Ces codes proposent les deux versions: cohérente (somme toutes les subfréquences de façon cohérentes) et incohérente (revient à calculer un Bartlett pour chaque subfréquence indépendamment des autres subfréquences, et à chercher à optimiser la somme)

Ces codes proposent les versions avec et sans monter en mémoire la CSDM. En effet, on a toujours utilisé l'option `normalisation = 1` (normSp  : normalisation fréquentielle : blanchiement: prewhitening). Donc dans la CSDM, tous les module sont à 1, il n'y a que les phases, c'est pour cela qu'une réécriture plus compacte de la fonction Bartlett est possible, sans monter toute la CSDM en mémoire).

NB : Pour les travaux de Chloé, on ajoutait une N+1 ième station forage (borehole), dont l'amplitude dans la CSDM n'était pas 1. On s'arrangeait pour mettre cette station en premier, ainsi dans la CSDM elle occuperait la première ligne et première colonne (paramètre `weight` dans namelist section `namborehole`).
