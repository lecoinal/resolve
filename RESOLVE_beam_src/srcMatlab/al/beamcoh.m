clear

summerdir='/data//projects/';  % execute on IST-nodes
%summerdir='/summer/';           % execute on luke-nodes


%%%%%%%

jd='121';

inputfile=[summerdir,'/resolve/Argentiere/ALBANNE/PREPROCESS/EXP000/ZO_2018_',jd,'.h5']
metricfile=[summerdir,'/resolve/Argentiere/ALBANNE/METRIC/zreseau_ZO_2018_',jd,'.txt'];

Nf = 41;
freqstep = 0.1; % Hz
hfw = 2.0; % Hz % half_freq_width

fe = 500;
count = 500;  % 1sec à 500Hz



% cet echantillon : https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/resolve/wikis/BeamSuiviOptim2
start = 13809750 % # 07:40:19.5  55239
fc = 9.5 % Hz
xyzc_obj = [376.521; 33.6255; 2340.23; 1587.38];   % parametres objectifs 
b_obj = 0.952574;  % valeur de la fonction objectif: d'apr�s wiki resolve section 4.2

%% cet echantillon : https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/resolve/wikis/BeamformingCoh
%start = 4349000 % # 02:24:58.0  17396
%fc = 6.0
%xyzc_obj = [283.924; -235.454; 2379.8; 1710.49];   % parametres objectifs 
%b_obj = 0.803768;  % valeur de la fonction objectif: d'apr�s wiki resolve section 5


% Read 1sec of input data
info = h5info(inputfile)
N = length(info.Datasets)
for inode = 1:N
  node = info.Datasets(inode).Name;
  rawdata(:,inode) = double(h5read(inputfile,['/',node],start+1,count)); % [Nt, Nsta]
end
figure;imagesc(rawdata);shading flat;colorbar;print('-dpng','tmp.png');

% Compute frequency vector
for ifreq = 1:Nf
  frequence(ifreq) = (fc-hfw)+(ifreq-1)*freqstep;
end

% 1. Bartlett incohérent

% Compute CSDM

%addpath ~/SCRIPTS/UTILS/  % import dft.m
data = transpose(dft(rawdata,[0:size(rawdata,1)-1]/fe,frequence)) ;  % [ Nsta, Nf ]
figure;imagesc(angle(data));shading flat;colorbar;print('-dpng','tmp2.png');
phase_data = angle(data); % [N, Nf]
data = exp(i*angle(data)) ;
K = zeros(N,N,Nf) ;
for ff = 1 : Nf
  K(:,:,ff) = K(:,:,ff) + data(:,ff)*data(:,ff)' ;
end
% on pourrait normaliser K par 98...
% Calcul du temps de parcours
XYZ = importdata(metricfile);
XYZ = XYZ.data;
dt = sqrt( (XYZ(:,1)-xyzc_obj(1)).^2 + (XYZ(:,2)-xyzc_obj(2)).^2 + (XYZ(:,3)-xyzc_obj(3)).^2 ) / xyzc_obj(4);

bartlett = 0;
for ff = 1 : Nf
  replica = exp(-i*2*pi*frequence(ff)*dt);
  
  % on pourrait normaliser replica par sqrt(98) % %norm(replica)^2
  beam(ff) = abs(replica'*K(:,:,ff)*replica) / (N*N);    % le abs est superflu ?
  %beam(ff) = (replica'*K(:,:,ff)*replica) / (N*N);    % le abs est superflu ?
  bartlett = bartlett + beam(ff) / Nf;
end
  
bartlett

% meme calcul mais sans monter en mémoire la csdm
bartlett_nocsdm = 0.0;
for ff = 1 : Nf
  phase_model = -2 * pi * frequence(ff)*dt; % [N]
  zz = sum(exp(i*(phase_data(:,ff)-phase_model(:))));
  bartlett_nocsdm = bartlett_nocsdm + zz*zz';
end
bartlett_nocsdm = bartlett_nocsdm / (N*N*Nf);
bartlett_nocsdm

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 2. Bartlett cohérent

data = transpose(dft(rawdata,[0:size(rawdata,1)-1]/fe,frequence)) ; % [N,Nf]
data = data(:); % on aligne les 98stations * 41 frequences
data = exp(i*angle(data)) ;
phase_data = angle(data);
K = zeros(N*Nf,N*Nf) ;
K(:,:) = K(:,:) + data(:)*data(:)' ;  % [N*Nf, N*Nf]
%norm(K) 4018 = 98*41
for ff = 1 : Nf
  replica(:,ff) = exp(-i*2*pi*frequence(ff)*dt); % [N, Nf]
end
replica=replica(:); % on aligne les 98 distances a la source * 41 frequences
%norm(replica) % sqrt(4018)
bartlettcoh = abs(replica'*K(:,:)*replica) / (N*N*Nf*Nf);  % le abs est superflu ?
%bartlettcoh = (replica'*K(:,:)*replica) / (N*N*Nf*Nf);  % le abs est superflu ?

bartlettcoh


% meme calcul mais sans monter en mémoire la csdm
for ff = 1 : Nf
  phase_model(:,ff) = -2 * pi * frequence(ff)*dt; % [N,Nf]
end
phase_model = phase_model(:);
zz = sum(exp(i*(phase_data(:)-phase_model(:))));
bartlettcoh_nocsdm = zz*zz';
bartlettcoh_nocsdm = bartlettcoh_nocsdm / (N*N*Nf*Nf);
bartlettcoh_nocsdm
