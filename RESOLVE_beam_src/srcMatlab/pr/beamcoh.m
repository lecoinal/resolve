clear

summerdir='/data/projects/';  % execute on IST-nodes
%summerdir='/summer/';           % execute on luke-nodes

%%%%%%%
jd='121';

inputfile=[summerdir,'/resolve/Argentiere/ALBANNE/PREPROCESS/EXP000/ZO_2018_',jd,'.h5']
metricfile=[summerdir,'/resolve/Argentiere/ALBANNE/METRIC/zreseau_ZO_2018_',jd,'.txt'];

Nf = 21;%41;
hfw = 2;%2.0; % Hz % half_freq_width

fe = 500;
count = 500;  % 1sec à 500Hz

% cet echantillon : https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/resolve/wikis/BeamSuiviOptim2
start = 13809750 % # 07:40:19.5  55239
fc = 9.5 % Hz
xyzc_obj = [376.521; 33.6255; 2340.23; 1587.38];   % parametres objectifs 
b_obj = 0.952574;  % valeur de la fonction objectif: d'apr�s wiki resolve section 4.2
grillex=xyzc_obj(1)+linspace(-200,200,51);
grilley=xyzc_obj(2)+linspace(-200,200,51);

% Read 1sec of input data
info = h5info(inputfile)
N = length(info.Datasets)
for inode = 1:N
  node = info.Datasets(inode).Name;
  rawdata(:,inode) = double(h5read(inputfile,['/',node],start+1,count)); % [Nt, Nsta]
end
%figure;imagesc(rawdata');shading flat;colorbar;print('-dpng','tmp.png');

% Compute frequency vector
frequence=fc+linspace(-hfw,hfw,Nf);

% 1. Bartlett incohérent
% Compute CSDM

%addpath ~/SCRIPTS/UTILS/  % import dft.m
data = transpose(dft(rawdata,[0:size(rawdata,1)-1]/fe,frequence)) ;  % [ Nsta, Nf ]
%figure;imagesc(angle(data));shading flat;colorbar;print('-dpng','tmp2.png');
phase_data = angle(data); % [N, Nf]
data = exp(1i*phase_data) ;
K = zeros(N,N,Nf) ;
for ff = 1 : Nf
  K(:,:,ff) = data(:,ff)*data(:,ff)' ;
end
% on pourrait normaliser K par 98...
% Calcul du temps de parcours
XYZ = importdata(metricfile);
XYZ = XYZ.data;

bartlett=zeros(length(grillex),length(grilley));
for ii=1:length(grillex)
    ii
    for jj=1:length(grilley)
        
        %dt = sqrt( (XYZ(:,1)-xyzc_obj(1)).^2 + (XYZ(:,2)-xyzc_obj(2)).^2 + (XYZ(:,3)-xyzc_obj(3)).^2 ) / xyzc_obj(4);
        dt = sqrt( (XYZ(:,1)-grillex(ii)).^2 + (XYZ(:,2)-grilley(jj)).^2 + (XYZ(:,3)-xyzc_obj(3)).^2 ) / xyzc_obj(4);
        
        for ff = 1 : Nf
            replica = exp(-1i*2*pi*frequence(ff)*dt);
            % on pourrait normaliser replica par sqrt(98) % %norm(replica)^2
            beam = abs(replica'*K(:,:,ff)*replica) / (N*N);    % le abs est superflu ?
            %beam(ff) = (replica'*K(:,:,ff)*replica) / (N*N);    % le abs est superflu ?
            bartlett(ii,jj) = bartlett(ii,jj) + beam / Nf;
            clear replica beam
        end
        
        clear dt
    end
end 

figure(1);clf
imagesc(grillex,grilley,bartlett')
hold on
plot(XYZ(:,1),XYZ(:,2),'*k')
plot(xyzc_obj(1),xyzc_obj(2),'or')
colorbar
axis tight
title(['Bartlett incoherent - ' num2str(max(bartlett(:)))])

% meme calcul mais sans monter en mémoire la csdm

bartlett_nocsdm =zeros(length(grillex),length(grilley));
for ii=1:length(grillex)
    ii
    for jj=1:length(grilley)
        dt = sqrt( (XYZ(:,1)-grillex(ii)).^2 + (XYZ(:,2)-grilley(jj)).^2 + (XYZ(:,3)-xyzc_obj(3)).^2 ) / xyzc_obj(4);
        for ff = 1: Nf
            phase_model = -2 * pi * frequence(ff)*dt; % [N]
            zz = sum(exp(i*(phase_data(:,ff)-phase_model(:))));
            bartlett_nocsdm(ii,jj)= bartlett_nocsdm(ii,jj) + zz*zz';
        end
        clear dt
    end
end
bartlett_nocsdm = bartlett_nocsdm / (N*N*Nf);

figure(2);clf
imagesc(grillex,grilley,bartlett_nocsdm')
hold on
plot(XYZ(:,1),XYZ(:,2),'*k')
plot(xyzc_obj(1),xyzc_obj(2),'or')
colorbar
axis tight
title(['Bartlett incoherent - ' num2str(max(bartlett_nocsdm(:)))])


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 2. Bartlett cohérent

[val,capteur_reference]=min(sqrt(XYZ(:,1).^2+XYZ(:,2).^2));
data = transpose(dft(rawdata,[0:size(rawdata,1)-1]/fe,frequence)) ; % [N,Nf]
data=data.*(ones(size(data,1),1)*conj(data(capteur_reference,:)));%%%On enlève la phase de référence à un capteur

data = data(:); % on aligne les 98stations * 41 frequences
phase_data = angle(data); % [N, Nf]
data = exp(1i*phase_data) ;
%data=data*conj(data(1));

K = zeros(N*Nf,N*Nf) ;
K=data(:)*data(:)' ;  % [N*Nf, N*Nf]
%norm(K) 4018 = 98*41

% dt = sqrt( (XYZ(:,1)-grillex(ceil(length(grillex)/2))).^2 + (XYZ(:,2)-grilley(ceil(length(grilley)/2))).^2 + (XYZ(:,3)-xyzc_obj(3)).^2 ) / xyzc_obj(4);
% replica= exp(-1i*2*pi*dt*frequence); % [N, Nf]
% replica=replica(:);
% K=replica*replica';

bartlett_coh=zeros(length(grillex),length(grilley));
for ii=1:length(grillex)
    ii
    for jj=1:length(grilley)
        dt = sqrt( (XYZ(:,1)-grillex(ii)).^2 + (XYZ(:,2)-grilley(jj)).^2 + (XYZ(:,3)-xyzc_obj(3)).^2 ) / xyzc_obj(4);
        
        replica= exp(-1i*2*pi*dt*frequence); % [N, Nf]
        replica=replica.*(ones(size(replica,1),1)*conj(replica(capteur_reference,:)));%%%On enlève la phase de référence à un capteur
        replica=replica(:); % on aligne les 98 distances a la source * 41 frequences
        %norm(replica) % sqrt(4018)
        bartlett_coh(ii,jj) = abs(replica'*K*replica) / (N*N*Nf*Nf);  % le abs est superflu ?
        clear replica dt
    end
end

figure(3);clf
imagesc(grillex,grilley,bartlett_coh')
hold on
plot(XYZ(:,1),XYZ(:,2),'*k')
plot(xyzc_obj(1),xyzc_obj(2),'or')
colorbar
axis tight
title(['Bartlett coherent - ' num2str(max(bartlett_coh(:)))])

% %%%Difference entre bartlett coherent et bartlett incoherent
% figure(4);clf
% imagesc(grillex,grilley,(bartlett_coh-bartlett_nocsdm)')
% hold on
% plot(XYZ(:,1),XYZ(:,2),'*k')
% plot(xyzc_obj(1),xyzc_obj(2),'or')
% colorbar
% axis tight
% title(['Bartlett coherent - Bartlett incoherent'])



