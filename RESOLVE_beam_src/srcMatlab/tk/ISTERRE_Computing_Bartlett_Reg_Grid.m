clear; clc; 
% close all
addpath Functions/  

%% Parameters
% Fixed parameters
samp_fq = 500;

% Computation parameters
shot_number = 30004;
t_window_length = 0.6; % Window length in seconds
num_iterations = 1; % Amount of time iterations
fc = 11.0; % Central frequency in Hz
Nf = 41; % Amount of sub-frequencies
freqstep = 0.1; % Frequency step in Hz
vel_vec=[100 200 300 400 500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 2300 2600 3000 4000 5000]; % velocity vector | optional: xyzc_obj(1,4);
% vel_vec = [1200];

% Domain parameters
grid_cell_size = 5; % size of grid cell in meters
max_rec_offset = 100; % max offset to include receivers (m)

% Looping parameters
choice_loop_over_shots = 0; % 0:no | 1:yes
station_list = importdata('C:\Users\tjeer\Documents\PhD\CdV_Field_campaign_Summer2022\Active_shots\CdV_Summer22_shots_ONLY_STATION_NUMBERS_SL3.csv');
choice_loop_over_offsets = 0; % 0:no | 1:yes
offset_vec = 100;

% Flags
% Incoherent or coherent
choice_incoh_or_coh = 0; %0: incoherent, 1: coherent
% Input parameters
choice_input_source = 0; % 0: all local files | 1: from cluster run output
% Plotting parameters
choice_plot_all_vel = 0; % 0:no, plot only best result | 1:yes

%% Files & directories
if choice_input_source == 0 % 0: all local files
    metricfile='C:/Users/tjeer/Documents/PhD/CdV_Field_campaign_Summer2022/Stryde_array/CdV_Summer22_Station_coordinates_centred.txt';
elseif choice_input_source == 1 % 1: from cluster run output
    metricfile='...';
end
dirplots= 'C:/Users/tjeer/Downloads/Plots/';
outputfile = '...';

%% Potential loops
% Barcl_mat = zeros(length(station_list),4);
% Barcl_mat(:,1) = station_list;
% 
% % Loop over shots
% countYYY = 0;
% for YYY = station_list'
%     close all
%     countYYY = countYYY + 1;
%     shot_number = YYY;
% 
% % Loop over offsets
% countXXX = 1;
% for XXX = 1:length(offset_vec)
%     max_rec_offset = offset_vec(XXX);
%     countXXX=countXXX + 1;


%% Shot info
shot_info = readtable('C:\Users\tjeer\Documents\PhD\CdV_Field_campaign_Summer2022\Active_shots\CdV_Summer22_shots_time_location_CENTRED.csv','ReadRowNames',true);
shot_datetime = datetime(shot_info{num2str(shot_number),"ShotTimePickedUTC"},'InputFormat','yyyy-MM-dd HH:mm:ss.SSS','TimeZone','UTC');
shot_jd = shot_info{num2str(shot_number),"JulianDay"};
shot_hour = hour(shot_datetime);
shot_XYZ = [shot_info{num2str(shot_number),"EastingLV95"} shot_info{num2str(shot_number),"NorthingLV95"} shot_info{num2str(shot_number),"Elevation"}];

t0_sample = (minute(shot_datetime)*60 + second(shot_datetime)) * samp_fq;

%% Receiver info
if choice_input_source == 0 % 0: all local files
    XYZ = importdata(metricfile);
    channel_list = XYZ(:,1);
    XYZ = XYZ(:,2:4);
elseif choice_input_source == 1 % 1: from cluster run output
    XYZ_info = importdata(metricfile);
    XYZ = XYZ_info.data;
end

%% Selecting channels
if choice_input_source == 0 % 0: all local files
    s_coords = repmat(shot_XYZ,[size(XYZ,1) 1]);
    sr_offset = sqrt(sum((s_coords - XYZ).^2,2));
    channel_vec = channel_list(sr_offset < max_rec_offset);
    XYZ = XYZ(sr_offset < max_rec_offset,:);
    sr_offset_n = sr_offset(sr_offset < max_rec_offset);
elseif choice_input_source == 1 % 1: from cluster run output
    channel_vec = zeros(length(XYZ_info.rowheaders));
    for ish = 1:length(XYZ_info.rowheaders)
        tmp = regexp(XYZ_info.rowheaders(ish),'\d*','Match');
        tmp = str2double(tmp{1,1}{1,1});
        channel_vec(ish) = tmp;
    end
    clear tmp;
end

%% Setting up domain
grillex=linspace(min(XYZ(:,1)),max(XYZ(:,1)),floor((max(XYZ(:,1))-min(XYZ(:,1)))/grid_cell_size));
grilley=linspace(min(XYZ(:,2)),max(XYZ(:,2)),floor((max(XYZ(:,2))-min(XYZ(:,2)))/grid_cell_size));
grillez=shot_XYZ(3); % Can be a vector too

%% Prepare frequency vector
hfw = (Nf-1) * freqstep / 2; % Half frequency width in Hz

% Compute frequency vector
freq_vec = zeros(1,Nf);
for ifreq = 1:Nf
  freq_vec(ifreq) = (fc-hfw)+(ifreq-1)*freqstep;
end

%% Barclett computation
inputfile=['E:/Cuolm_da_Vi_STRYDE_HDF5/CdV_Stryde_2022_' sprintf('%03d',shot_jd) '_' sprintf('%02d',shot_hour) '.h5'];
info = h5info(inputfile);
num_samples_per_it = samp_fq * t_window_length; % Number of samples in one time iteration


% for-loop over all time steps
for timestep=1:num_iterations    
    data = zeros(num_samples_per_it,length(channel_vec));

    % Read data
    for inode = 1:length(channel_vec)
      node = info.Datasets(channel_vec(inode)-1000).Name;
      data(:,inode) = double(h5read(inputfile,['/',node],t0_sample+((timestep-1)*num_samples_per_it),num_samples_per_it)); % [Nt, Nsta]
    end

    % Compute Bartlett matrix in either a incoherent or coherent way
    if choice_incoh_or_coh == 0 %0: incoherent, 1: coherent
        dvar = 'Incoh';
        %Compute phase of the data
        phase_data_incoh = angle(transpose(pr_MFP_Discrete_Fourier_Transform(data,(0:size(data,1)-1)/samp_fq,freq_vec))) ;  % [ Nsta, Nf ]   % [N, Nf]
        
        %Compute full Bartlett matrix
        [bartlett_nocsdm] = tk_MFP_Computing_Bartlett_matrix_incoherent(grillex,grilley,grillez,vel_vec,XYZ,Nf,freq_vec,phase_data_incoh,channel_vec);
    
    elseif choice_incoh_or_coh == 1
        dvar = 'Coh';
        %Determine reference station & data
        [~,idx] = min(sr_offset_n);

        %Remove reference station and compute phase of the data
        [phase_data] = tk_MFP_Computing_data_phase_coherent(data,samp_fq,freq_vec,idx);
        
        %Compute full Bartlett matrix
        [bartlett_nocsdm] = tk_MFP_Computing_Bartlett_matrix_coherent(phase_data,grillex,grilley,grillez,vel_vec,XYZ,Nf,freq_vec,channel_vec,idx);

    end
    
    %Compute Bartlett maxima for either only optimal velocity or all velocities: 0:only for optimal velocity or 1:all velocities
    if choice_plot_all_vel == 0 
        [tmp,xmax,ymax,kmax,cmax] = tk_MFP_Computing_max_Bartlett_value_opt_vel(grillez,vel_vec,bartlett_nocsdm);
        tk_MFP_Plotting_Bartlett_regular_grid(grillex,grilley,tmp,xmax,ymax,shot_XYZ,XYZ,choice_input_source,outputfile,timestep,shot_number,max_rec_offset,grillez,kmax,fc,vel_vec,cmax,dirplots,dvar);
    elseif choice_plot_all_vel == 1 
        for kk=1:length(grillez)
          for cc=1:length(vel_vec)
            [tmp,kmax,cmax,ymax,xmax] = tk_MFP_Computing_max_Bartlett_value_all_vel(bartlett_nocsdm,kk,cc);
            tk_MFP_Plotting_Bartlett_regular_grid(grillex,grilley,tmp,xmax,ymax,shot_XYZ,XYZ,choice_input_source,outputfile,timestep,shot_number,max_rec_offset,grillez,kmax,fc,vel_vec,cmax,dirplots,dvar);
          end
        end
    end

    err_reg_gr = sqrt((shot_XYZ(1)-grillex(xmax)).^2 + (shot_XYZ(2)-grilley(ymax)).^2 + (shot_XYZ(3)-shot_XYZ(3)).^2);

end % end timestep

% Saving max. Bartlett-values in matrix
%  Barcl_mat(countYYY,countXXX) = max(tmp(:));
% end % end for-loop shots
% end
% end
