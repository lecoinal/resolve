
As discussed, I collected the Matlab code and related functions to compute and plot the Bartlett values on a regular grid. In the beginning of the code, you can select either the Coherent or Incoherent method. 

The main script is called 'ISTERRE_Computing_Bartlett_Reg_Grid.m'. The other files are the required functions. 
