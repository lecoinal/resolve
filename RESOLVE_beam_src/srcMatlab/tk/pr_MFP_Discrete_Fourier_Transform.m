function y=pr_MFP_Discrete_Fourier_Transform(x,t,f,varargin)
% Discrete Fourier Transform
%   Computes the discrete Fourier transform for a vector of
%   desired frequencies.
% Version: 9 Jan 98
% Usage:  y=dft(x,t,f,'pond')
% Input:   x - data series values
%          t - data series times
%          f - frequencies
%          pond - ponderation par le dt
% Output:  y - DFT at frequencies f
%
% NB : dft et fft donnent le meme resultat (amplitude comprise)
%   sous reserve que le vecteur temps commence a 0,
%   sinon il faut appliquer a la fft un dephasage de 2*pi*freq*time(1) :
%       fft(x).*exp(-i*2*pi*f*t(1)) = dft(x,t,f)

if nargin<3
  error('Too few input arguments');
end

% si x est un vecteur on le met dans le bon sens.
if numel(x) == length(x) ; x = x(:) ; end
% si x est une matrice a plus de 2 dim, on se ramene a deux dim
resh = false ;
if numel(x) > size(x,1)*size(x,2)
    S = size(x) ;
    resh = true ;
    x = reshape(x,[size(x,1) numel(x)/size(x,1)]) ;
end

pond = false ;
for vv = 1:length(varargin)
    switch(varrargin{vv})
        case 'pond'
            pond = true ;
        otherwise
            error([varargin{vv} ' n''ets pas une option reconnue.']) ;
    end
end

% i=sqrt(-1);

% Traditional summation form
%n=length(t);
%m=length(f);
%y=zeros(n,1);
%k=[0:n-1]';
%for j=0:n-1
%  y(j+1)=sum(x.*exp(i*2*pi*j*k/n));
%end

% Complex exp. vector form
%m=length(f);
%y=zeros(n,1);
%for j=1:m
%  y(j)=sum(x.*exp(i*2*pi*f(j)*t));
%end
% Complex exp. matrix form
A=exp(-1i*2*pi*t(:)*(f(:).'));

if pond
    dt = gradient(t(:)) ;
    y=A.'*(x.*dt(:,ones(size(x,2),1))) ;
else
    y=A.'*x ;
end

% Trig. matrix form = LSFT
%A=[cos(2*pi*t*f') sin(2*pi*t*f')];
%y=A'*x;

% si on a fait un reshape on remets tout dans le bon ordre
if resh
    y = reshape(y,[length(f) S(2:end)]) ;
end

