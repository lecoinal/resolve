function [bartlett_nocsdm] = tk_MFP_Computing_Bartlett_matrix_coherent(phase_data,grillex,grilley,grillez,vel_vec,XYZ,Nf,freq_vec,channel_vec,idx)

data = exp(1i*phase_data);
K=data(:)*data(:)' ;  % [N*Nf, N*Nf]
bartlett_nocsdm =zeros(length(grillex),length(grilley),length(grillez),length(vel_vec));
for cc=1:length(vel_vec)
    for kk=1:length(grillez)
          for ii=1:length(grillex)
                for jj=1:length(grilley)
                distance_grille=sqrt( (XYZ(:,1)-grillex(ii)).^2 + (XYZ(:,2)-grilley(jj)).^2 + (XYZ(:,3)-grillez(kk)).^2);
                time_grille=1/vel_vec(cc)*distance_grille;
                time_grille=time_grille-mean(time_grille);
                replica= exp(-1i*2*pi*time_grille*freq_vec); % [N, Nf]
                replica=replica.*(ones(size(replica,1),1)*conj(replica(idx,:)));
                replica=replica(:);
                bartlett_nocsdm(ii,jj,kk,cc) = abs(replica'*K*replica) / (length(channel_vec)^2*Nf*Nf);
                clear replica time_grille distance_grille
                end
          end
    end
end

end

