function [bartlett_nocsdm] = tk_MFP_Computing_Bartlett_matrix_incoherent(grillex,grilley,grillez,vel_vec,XYZ,Nf,freq_vec,phase_data_incoh,channel_vec)

bartlett_nocsdm =zeros(length(grillex),length(grilley),length(grillez),length(vel_vec));
for cc=1:length(vel_vec)
ctmp = 1 / vel_vec(cc);
    for kk=1:length(grillez)
      ktmp = (XYZ(:,3)-grillez(kk)).^2;
          for ii=1:length(grillex)
                for jj=1:length(grilley)
                  dt = sqrt( (XYZ(:,1)-grillex(ii)).^2 + (XYZ(:,2)-grilley(jj)).^2 + ktmp ) * ctmp ;
                  tmp = -2 * pi * dt;
                      for ff = 1: Nf
                          phase_model = tmp * freq_vec(ff); % [N]
                          zz = sum(exp(1i*(phase_data_incoh(:,ff)-phase_model(:))));
                          bartlett_nocsdm(ii,jj,kk,cc)= bartlett_nocsdm(ii,jj,kk,cc) + zz*zz';
                      end
                  clear dt
                end
          end
    end
end
bartlett_nocsdm = bartlett_nocsdm / (length(channel_vec)*length(channel_vec)*Nf);

end

