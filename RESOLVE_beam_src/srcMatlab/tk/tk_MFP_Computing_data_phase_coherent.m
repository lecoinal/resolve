function [phase_data] = tk_MFP_Computing_data_phase_coherent(data,samp_fq,freq_vec,idx)

toto=data./(max(abs(data)));
toto(isnan(toto))=0;
data_tmp = transpose(pr_MFP_Discrete_Fourier_Transform(toto,(0:size(toto,1)-1)/samp_fq,freq_vec)) ;
data_tmp=data_tmp.*(ones(size(data_tmp,1),1)*conj(data_tmp(idx,:)));%%%On enlève la phase de référence à un capteur
data_tmp = data_tmp(:); % on aligne les 98stations * 41 frequences
phase_data = angle(data_tmp); % [N, Nf]

end

