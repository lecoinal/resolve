function [tmp,kmax,cmax,ymax,xmax] = tk_MFP_Computing_max_Bartlett_value_all_vel(bartlett_nocsdm,kk,cc)

tmp = squeeze(bartlett_nocsdm(:,:,kk,cc));
[I, J]=max(tmp(:,:));
[II, JJ]=max(I);
Imax=II;
kmax=kk;
cmax=cc;
ymax=JJ; % floor(J/ngxy);
xmax=J(JJ); % J-ngxy*ymax+1; % mod(J,ngxy)+1;

end

