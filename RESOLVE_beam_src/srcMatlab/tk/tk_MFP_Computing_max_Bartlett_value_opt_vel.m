function [tmp,xmax,ymax,kmax,cmax] = tk_MFP_Computing_max_Bartlett_value_opt_vel(grillez,vel_vec,bartlett_nocsdm)

Imax=0;
for kk=1:length(grillez)
    for cc=1:length(vel_vec)
        tmp = squeeze(bartlett_nocsdm(:,:,kk,cc));
        [I, J]=max(tmp(:,:));
        [II, JJ]=max(I);
        if (II>Imax)
            Imax=II;
            kmax=kk;
            cmax=cc;
            ymax=JJ; % floor(J/ngxy);
            xmax=J(JJ); % J-ngxy*ymax+1; % mod(J,ngxy)+1;
        end
    end
end
bartlett_nocsdm(xmax,ymax,kmax,cmax)
tmp = squeeze(bartlett_nocsdm(:,:,kmax,cmax));

end

