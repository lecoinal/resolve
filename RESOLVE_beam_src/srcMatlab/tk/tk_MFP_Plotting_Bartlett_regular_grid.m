function [] = tk_MFP_Plotting_Bartlett_regular_grid(grillex,grilley,tmp,xmax,ymax,shot_XYZ,XYZ,input_source,outputfile,timestep, ...
    shot_number,max_rec_offset,grillez,kmax,fc,vel_vec,cmax,dirplots,dvar)

f = figure; clf
f.Position = [0 0 2000 1000];
imagesc(grillex,grilley,tmp')
set(gca,'YDir','normal');
hold on
plot(grillex(xmax),grilley(ymax),'*k','MarkerSize',16,'MarkerFaceColor','r')
plot(shot_XYZ(1),shot_XYZ(2),"pentagramk",'MarkerSize',14,'MarkerFaceColor','k')
plot(XYZ(:,1),XYZ(:,2),'^k','MarkerFaceColor','k')
ylabel('Northing [m]','fontweight','bold','fontsize',13)
xlabel('Easting [m]','fontweight','bold','fontsize',13)
c = colorbar;
c.Label.String = 'Bartlett value [-]';
c.Label.FontSize = 13;
c.Label.FontWeight = 'bold';
if input_source == 0 %0: all local files
    legend('Highest grid cell','Real shot location','Nodes','FontSize',13,'fontweight','bold')
elseif input_source == 1 %1: from cluster run output
    bav_out = double(h5read(outputfile,'/Baval'));
    xyzc_out = double(h5read(outputfile,'/xyzc'));
    plot(xyzc_out(timestep,1),xyzc_out(timestep,2),'or','MarkerSize',12,'MarkerFaceColor','r')
    text(xyzc_out(timestep,1),xyzc_out(timestep,2),num2str(bav_out(timestep)),'color','r','VerticalAlignment','bottom', 'HorizontalAlignment','left');
    legend('Highest grid cell','Real shot location','Nodes','Solution found by gradient-descent optimalization')
end
axis tight
axis equal
title({['Shot: ' num2str(shot_number) '           Type: ' dvar ...
    '         Max. Offset:' num2str(max_rec_offset) '          Max. Barclett grid cell: ' num2str(max(tmp(:)))],[...
    '        Elevation x-y plane: ' num2str(grillez(kmax)) 'm' '         Centr. freq.: ' num2str(fc) 'Hz'...
    '        Uniform velocity: ' num2str(vel_vec(cmax)) 'm/s']},'FontSize',16);
% print('-dpng',[dirplots 'Shot' num2str(shot_number) '__' dvar '__MaxOff' num2str(max_rec_offset) ...
%     '__Elev' num2str(grillez(kmax)) 'm' '__CFreq' num2str(fc) 'Hz' ...
%     '__Vel' num2str(vel_vec(cmax)) 'mps' '__MaxBarcl' num2str(max(tmp(:))) '.png'])
% exportgraphics(f,[dirplots 'Shot' num2str(shot_number) '__' dvar '__MaxOff' num2str(max_rec_offset) ...
%     '__Elev' num2str(grillez(kmax)) 'm' '__CFreq' num2str(fc) 'Hz' ...
%     '__Vel' num2str(vel_vec(cmax)) 'mps' '__MaxBarcl' num2str(max(tmp(:))) '.png'],'Resolution',300)

end

