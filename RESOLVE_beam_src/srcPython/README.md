Code de calcul du beamforming sur une grille régulière x,y,z,c

Provient de ce dépôt svn :
```
URL : https://forge.osug.fr/svn/isterre-beamforming
Relative URL: ^/
Racine du dépôt : https://forge.osug.fr/svn/isterre-beamforming
UUID du dépôt : 1b74f725-c18c-4769-884c-371b33f4ab11
Révision : 160
Type de nœud : répertoire
Tâche programmée : normale
Auteur de la dernière modification : lecointre
Révision de la dernière modification : 160
Date de la dernière modification: 2019-01-18 16:54:44 +0100 (ven. 18 janv. 2019)

```

Voir l'historique des révisions [./svnlog.txt](svnlog.txt)

Il s'agit du bartlett incohérent.

Il s'agit de l'expression avec la CSDM montée en mémoire (Cross Spectral density Matrix).
