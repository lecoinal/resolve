!=======================================================================================
!                       ***  Fortran code computebeam.f90  ***
!   This is the Fortran code to compute beam
!    
!
!=======================================================================================
!   History :  1.0  : 12/2013  : A. Lecointre : Initial version
!               
!       describe here       
!       describe here
!       describe here    
!
!---------------------------------------------------------------------------------------
!
!
!---------------------------------------------------------------------------------------
!   routines      : description
!                 : 
!                 : 
!                 : 
!                 :
!
!---------------------------------------------------------------------------------------
!  $Id: computebeam.f90 35 2013-12-19 14:27:05Z lecointre $
!  


SUBROUTINE get_m2jPiF_over_c(frequencies, celerities, Nf, Nc, m2jPiF_over_c)
INTEGER, INTENT(IN) :: Nf ! Number of frequencies
INTEGER, INTENT(IN) :: Nc ! Number of celerities
REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: frequencies
REAL(KIND=8), DIMENSION(Nc), INTENT(IN) :: celerities
COMPLEX(KIND=8) :: m2jPi 
COMPLEX(KIND=8), DIMENSION(Nf,Nc), INTENT(OUT) :: m2jPiF_over_c
INTEGER :: cc,ff

m2jPi = -2.d0 * (0.d0,1.d0) * 4.d0 * atan(1.d0)
do cc=1,Nc
  do ff=1,Nf
    m2jPiF_over_c(ff,cc) = (frequencies(ff) / celerities(cc)) * m2jPi
  end do
end do
END SUBROUTINE get_m2jPiF_over_c

SUBROUTINE computebeam(xg,yg,zg,xr,yr,zr,freq,c,K,method,Nr,Nx,Ny,Nz,Nf,Nc,Kinv,beam,beamstd,beamf)

INTEGER, INTENT(IN) :: Nr ! Array size: nb of sensors
INTEGER, INTENT(IN) :: Nf ! Number of frequencies
INTEGER, INTENT(IN) :: Nc ! Number of celerities
INTEGER, INTENT(IN) :: Nx,Ny,Nz ! Array size: nb of searching grid points in the xyz directions
REAL(KIND=8), DIMENSION(Nr), INTENT(IN) :: xr,yr,zr ! Position of sensors
REAL(KIND=8), DIMENSION(Nx), INTENT(IN) :: xg ! search grid
REAL(KIND=8), DIMENSION(Ny), INTENT(IN) :: yg ! search grid
REAL(KIND=8), DIMENSION(Nz), INTENT(IN) :: zg ! search grid
REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: freq ! frequencies
REAL(KIND=8), DIMENSION(Nc), INTENT(IN) :: c ! celerities
COMPLEX(KIND=8), DIMENSION(Nr,Nr,Nf), INTENT(IN) :: K ! csdm matrix
COMPLEX(KIND=8), DIMENSION(Nr,Nr,Nf), INTENT(IN), OPTIONAL :: Kinv ! inverse of csdm matrix
CHARACTER(LEN=*), INTENT(IN) :: method ! bartlett/mvdr
LOGICAL :: method_linear

INTEGER :: kk,ii,jj,cc,ff
REAL(KIND=8), DIMENSION(Nr) :: a, inv_pi_a_4 ! Distance source recepteur [Nr]
REAL(KIND=8) :: pi,pi4
COMPLEX(KIND=8), DIMENSION(Nr) :: omega, conjOmega
COMPLEX(KIND=8), DIMENSION(Nr) :: replica

REAL(KIND=8), DIMENSION(Nx,Ny,Nz,Nc,Nf), INTENT(OUT) :: beamf
REAL(KIND=8), DIMENSION(Nx,Ny,Nz,Nc), INTENT(OUT) :: beam,beamstd
COMPLEX(KIND=8), DIMENSION(Nf,Nc) :: m2jPiF_over_c


pi=4.d0*atan(1.d0)
pi4 = 4.d0 * pi

if (method == 'bartlett') then
  method_linear = .true.
else if (method == 'mvdr') then
  method_linear = .false.
else
  print *,'undefined method : ',method
  return
end if

beam(:,:,:,:)=0.d0
beamstd(:,:,:,:)=0.d0
CALL get_m2jPiF_over_c(freq, c, Nf, Nc, m2jPiF_over_c(:,:))
do kk=1,Nz
  do ii=1,Nx
    do jj=1,Ny
      a(:) = ((xr(:)-xg(ii))**2+(yr(:)-yg(jj))**2+(zr(:)-zg(kk))**2)**0.5 ! [Nr]
      inv_pi_a_4(:) = 1.d0 / ( pi4 * a(:) )
      do cc=1,Nc
        do ff=1,Nf
          omega(:) = EXP(a(:) * m2jPiF_over_c(ff,cc)) * inv_pi_a_4(:)
          omega(:) = omega(:) / ABS(omega(:))
          omega(:) = omega(:) / sqrt(SUM(ABS(omega(:))**2))
          conjOmega(:) = CONJG(omega(:))
          if (method_linear) then ! bartlett
            ! calcul de la norme de omega mais en fait omega est de norme 1 donc inutile de calculer sa norme...
            ! ancien: replica(:) = conjOmega(:) / sqrt(SUM(ABS(conjOmega(:))**2)) ~ conjOmega(:) / 1
            beamf(ii,jj,kk,cc,ff) = ABS(SUM(conjOmega(:)*MATMUL(K(:,:,ff),omega(:))))
          else ! mvdr
            replica(:) = omega(:) / SUM(conjOmega(:) * MATMUL(Kinv(:,:,ff),omega(:)) ) / Nr
            beamf(ii,jj,kk,cc,ff) = ABS(SUM(CONJG(replica(:)) * MATMUL(Kinv(:,:,ff),replica(:))))
          end if
          beam(ii,jj,kk,cc) = beam(ii,jj,kk,cc)+beamf(ii,jj,kk,cc,ff)
        enddo
      enddo
    enddo
  enddo
enddo

beam(:,:,:,:) = beam(:,:,:,:)/Nf
do kk=1,Nz
  do ii=1,Nx
    do jj=1,Ny
      do cc=1,Nc
        beamstd(ii,jj,kk,cc) = SQRT( SUM((beamf(ii,jj,kk,cc,:) - beam(ii,jj,kk,cc))**2) / Nf )
      enddo
    enddo
  enddo
enddo

END SUBROUTINE computebeam


SUBROUTINE computebeamct(theta,xr,yr,freq,c,K,method,Nr,Nt,Nf,Nc,Kinv,beam,beamstd,beamf)

INTEGER, INTENT(IN) :: Nr ! Array size: nb of sensors
INTEGER, INTENT(IN) :: Nf ! Number of frequencies
INTEGER, INTENT(IN) :: Nc ! Number of celerities
INTEGER, INTENT(IN) :: Nt ! Array size: nb of theta for the grid (azimuth) search
REAL(KIND=8), DIMENSION(Nr), INTENT(IN) :: xr,yr ! Position of sensors
REAL(KIND=8), DIMENSION(Nt), INTENT(IN) :: theta ! azimuth search grid
REAL(KIND=8), DIMENSION(Nf), INTENT(IN) :: freq ! frequencies
REAL(KIND=8), DIMENSION(Nc), INTENT(IN) :: c ! celerities
COMPLEX(KIND=8), DIMENSION(Nr,Nr,Nf), INTENT(IN) :: K ! csdm matrix
COMPLEX(KIND=8), DIMENSION(Nr,Nr,Nf), INTENT(IN), OPTIONAL :: Kinv ! inverse of csdm matrix
CHARACTER(LEN=*), INTENT(IN) :: method ! bartlett/mvdr
LOGICAL :: method_linear

INTEGER :: tt,cc,ff
REAL(KIND=8), DIMENSION(Nr) :: a !            [Nr]
COMPLEX(KIND=8), DIMENSION(Nr)  :: omega
COMPLEX(KIND=8), DIMENSION(Nr) :: replica

REAL(KIND=8), DIMENSION(Nt,Nc,Nf), INTENT(OUT) :: beamf
REAL(KIND=8), DIMENSION(Nt,Nc), INTENT(OUT) :: beam,beamstd
COMPLEX(KIND=8), DIMENSION(Nf,Nc) :: m2jPiF_over_c

if (method == 'bartlett') then
  method_linear = .true.
else if (method == 'mvdr') then
  method_linear = .false.
else
  print *,'undefined method : ',method
  return
end if

beam(:,:)=0.d0
beamstd(:,:)=0.d0
CALL get_m2jPiF_over_c(freq, c, Nf, Nc, m2jPiF_over_c(:,:))
do tt=1,Nt
  a(:) = xr(:) * SIN(theta(tt)) + yr(:) * COS(theta(tt))
  do cc=1,Nc
    do ff=1,Nf
      omega(:) = EXP(m2jPiF_over_c(ff,cc) * a(:))           ! [Nr]
      if (method_linear) then ! bartlett
        replica(:) = omega(:) / sqrt(SUM(ABS(omega(:))**2))
        beamf(tt,cc,ff) = ABS(SUM(CONJG(replica(:))*MATMUL(K(:,:,ff),replica(:))))
      else ! mvdr
        replica(:) = omega(:) / SUM(CONJG(omega(:)) * MATMUL(Kinv(:,:,ff),omega(:)) ) / Nr
        beamf(tt,cc,ff) = ABS(SUM(CONJG(replica(:)) * MATMUL(Kinv(:,:,ff),replica(:))))
      end if
      beam(tt,cc) = beam(tt,cc)+beamf(tt,cc,ff)
    enddo
  enddo
enddo

beam(:,:) = beam(:,:)/Nf
do tt=1,Nt
  do cc=1,Nc
    beamstd(tt,cc) = SQRT( SUM((beamf(tt,cc,:) - beam(tt,cc))**2) / Nf )
  enddo
enddo

END SUBROUTINE computebeamct
