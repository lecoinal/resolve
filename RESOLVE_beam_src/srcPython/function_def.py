# -*- coding: utf-8 -*-

#  =======================================================================================
#                       ***  Python script function_def.py  ***
#   This is the functions associated to beamforming code
#    
#  =======================================================================================
#   History :  1.0  : 12/2013  : A. Lecointre : Initial version
#	        
#	describe here	    
#	describe here
#	describe here	 
#  ---------------------------------------------------------------------------------------
#
#  ---------------------------------------------------------------------------------------
#   routines      : description
#                 : 
#                 : 
#                 : 
#                 :
#  ---------------------------------------------------------------------------------------
#  $Id: function_def.py 117 2017-03-23 09:20:23Z lecointre $
#  



import numpy as np
import numpy.linalg
import pylab
import math
#import datetime
import sys

######################################################################################################

def dft(x,t,f):
    """compute discrete fourier transform
    x : [len(t) x Nr]
    t : [len(t)]
    f : [len(f)]
    """
    # si x est une matrice a plus de 2 dim, on se ramene a deux dim
    resh = False
    if np.ndim(x) > 2:
        S1 = np.size(x,1)
        S2 = np.size(x,2)
        resh = True
        x = np.reshape(x,(np.size(x,0),S1*S2),order='F')
    A=np.exp(-1j*2*np.pi*np.outer(f,t)) # [len(f) x len(t)]   np.outer: produit exterieur
    y=np.dot(A,x)                      # [len(f) x Nr ]  (prendre la transposée de A)
    # si on a fait un reshape on remet tout dans le bon ordre
    if resh:
        y = np.reshape(y,(len(f),S1,S2),order='F')
    return y

#######################################################################################################

def invCsdm(K, Nr, Nf):
    """Compute inverse of CSDM (if needed)
    """
    Kinv = np.empty((Nr,Nr,Nf),dtype=np.complex128)
    for ff in range(Nf):
        Kinv[:,:,ff] = numpy.linalg.inv(K[:,:,ff])
    return Kinv

def csdm(data,Xr,Yr,fe,freq,nivBruit=0,normalisation='noNorm',dtBouts=0,svd=0,meig=1,methodFabian=0,ln_readreplica=0):
    # 1. Initialisation
    dnBouts = len(data)
    Nbouts = 0
    Nr = len(Xr)
    Nf = len(freq)
    # 2. Preprocessing (DFT, normalisation, ...)
    # Normalisation trace par trace: Normalisation par l'energie
    if normalisation=='normTps':
        energie=np.sqrt(np.sum(data*data,0))
        for rr in range(Nr):
            if energie[rr] != 0:
                data[:,rr] = data[:,rr] / energie[rr] 
#        data = data / np.sqrt(np.sum(data*data,0)) # broadcast automatique par np.size(data,0)
        # Normalisation par le max: data = data / np.max(data,0)
    # Decoupage en bouts
    if dtBouts:
        dnBouts = round(dtBouts*fe)
    Nbouts = int(np.floor(len(data)/dnBouts))
    if Nbouts > 1:
        data = data[0:Nbouts*dnBouts,:]  # Split
        data = np.reshape(data,(np.ceil(len(data)/Nbouts),Nbouts,Nr),order='F') # Reshape with Fortran indexing
        data = np.transpose(data,(0,2,1))
    data = dft(data,np.arange(float(len(data)))/fe,freq) # array plutot que matrix
    if ln_readreplica:
        import scipy.io as sio
        mat_contents = sio.loadmat('replicas.mat')
        data = mat_contents['replicas']
    if normalisation=='normSp':  # prewhitening
        data = np.exp(1j*np.angle(data))
#    # Normalisation d(w) (a la place de la normalisation de CSDM finale
#    for rr in range(Nr):
#	for bb in range(int(Nbouts)):
#	    data[:,rr,bb] = data[:,rr,bb] / numpy.linalg.norm(data[:,rr,bb])
    # 3. Compute K (CSDM)
    K = np.zeros((Nr,Nr,Nf),dtype=np.complex128) # ,order='C')
    if Nbouts > 1:
	    # method nouveau : data x data.conj() : pour beamxy, faut remettre a data.conj() x data ...
        for ff in range(Nf):
            for bb in range(int(Nbouts)):
                K[:,:,ff] = K[:,:,ff] +  np.outer(data[ff,:,bb],data[ff,:,bb].conj())
            #    K[:,:,ff] = K[:,:,ff] +  np.outer(data[ff,:,bb].conj(),data[ff,:,bb])
    else:
        for ff in range(Nf):
            K[:,:,ff] = K[:,:,ff] +  np.outer(data[ff,:],data[ff,:].conj())
            #K[:,:,ff] = K[:,:,ff] +  np.outer(data[ff,:].conj(),data[ff,:])
    # Singular Value Decomposition
    SSVD = np.zeros((Nr,Nf),dtype=np.float64)
    if svd:
        for ff in range(Nf):
            U,S,Vh = numpy.linalg.svd(K[:,:,ff])
	    SSVD[:,ff] = S
            Um = np.zeros_like(U)
            Vm = np.zeros_like(Vh)
            Sm = np.zeros_like(S)
            if svd==1:
                Um[:,:meig]=U[:,:meig]
                Vm[:,:meig]=Vh[:,:meig]
                Sm[:meig]=S[:meig]
            elif svd==-1:
                Um[:,meig:]=U[:,meig:]
                Vm[:,meig:]=Vh[:,meig:]
                Sm[meig:]=S[meig:]
            elif svd==-2:
                eig=meig-1
                Um[:,eig]=U[:,eig]
                Vm[:,eig]=Vh[:,eig]
                Sm[eig]=S[eig]
	    if methodFabian:
		K[:,:,ff] = np.dot(Um, np.dot(np.diag(Sm), Vm))
            else:
                K[:,:,ff] = np.dot(U, np.dot(np.diag(Sm), Vh))
    # Add noise on CSDM diagonal
    if nivBruit:
        for ff in range(Nf):
            K[:,:,ff] = K[:,:,ff] + (10.**(-float(nivBruit)/10.))*np.eye(Nr)*numpy.linalg.norm(K[:,:,ff])
    # Normalisation de K
    for ff in range(Nf):
	    # K[:,:,ff] = K[:,:,ff] / numpy.linalg.norm(K[:,:,ff],ord=2) # pour etre equivalent au code Matlab (norm L2 par defaut), par defaut norm Frobenius en Python
	    K[:,:,ff] = K[:,:,ff] / numpy.linalg.norm(K[:,:,ff])
    return K,SSVD


def __m2jPiF_over_c(frequencies, celerities,Nf,Nc):
    m2jPi = -2j * np.pi
    m2jPiF_over_c = np.empty((Nf,Nc), dtype=np.complex128)
    for cc,speed in enumerate(celerities):
        for ff,frequency in enumerate(frequencies):
            m2jPiF_over_c[ff,cc] = (frequency / speed) * m2jPi
    return m2jPiF_over_c


def beamforming_code(K,Kinv,Xr,Yr,theta,c,freq,beamformer='bartlett',ln_code=0):
    """beamforming: Python version of beamforming_code.m"""
    Nr , Nf , Nc = len(Xr) , len(freq) , len(c)
    if ln_code==1:
        import beamfortran
        [beam,beamstd,beamf]=beamfortran.computebeamct(theta*np.pi/180,Xr,Yr,freq,c,K,beamformer,Nr,len(theta),Nf,Nc,Kinv)
    elif ln_code==2:
	print 'C-API method not available for theta c beamformer'
	sys.exit(1)
    else:
	if beamformer=='bartlett':
	    methodLinear = True
	elif beamformer=='mvdr':
	    methodLinear = False
        else:
	    print 'undefined method : ', beamformer
	    sys.exit(1)
        beam = np.zeros((len(theta),Nc), dtype=np.float64)
        beamstd = np.zeros((len(theta),Nc), dtype=np.float64)
        beamf = np.empty((len(theta),Nc,Nf), dtype=np.float64)
	m2jPiF_over_c = __m2jPiF_over_c(freq, c,Nf,Nc)
        for tt,angle in enumerate(theta):
	    a = Xr * np.sin(angle*np.pi/180) + Yr * np.cos(angle*np.pi/180)
            for cc,speed in enumerate(c):
               for ff,frequency in enumerate(freq):
                   omega = np.exp(a * m2jPiF_over_c[ff,cc])           # [Nr]
#                   # Method ancien
	#	   conjOmega = np.conj(omega)
#		   if methodLinear: # bartlett
#		       replica = conjOmega / numpy.linalg.norm(conjOmega)
#		       beamf[tt,cc,ff] = abs(np.dot(np.conj(replica).T,(np.dot(K[:,:,ff],replica))))
		   # Method nouveau
                   if methodLinear: # bartlett
                       replica = omega / numpy.linalg.norm(omega)
                       beamf[tt,cc,ff] = abs(np.dot(replica.conj(),(np.dot(K[:,:,ff],replica))))
	       	   # Fin choix method 
	           else: # mvdr
		       #replica = omega / numpy.linalg.norm(omega)
                       # ancien d.conj replica = conjOmega / (np.dot(omega.T,np.dot(Kinv[:,:,ff],conjOmega))) / Nr
                       # ancien d.conj beamf[tt,cc,ff] = abs(np.dot(np.conj(replica).T,(np.dot(Kinv[:,:,ff],replica))))
                       replica = omega / (np.dot(omega.conj(),np.dot(Kinv[:,:,ff],omega))) / Nr
		       beamf[tt,cc,ff] = abs(np.dot(replica.conj(),(np.dot(Kinv[:,:,ff],replica))))
	beam = np.mean(beamf,2)
	beamstd = np.std(beamf,2)
    return beam,beamstd,beamf

#######################################################################################################

def beamforming_code_xy(K,Kinv,Xr,Yr,Zr,xgrid,ygrid,Hs,c,freq,beamformer='bartlett',ln_code=0,ln_savereplica=0):
    """beamforming: Python version of beamforming_code.m"""
    Nr , Nf , Nc = len(Xr) , len(freq) , len(c)
    if ln_code==1:
        import beamfortran
        [beam,beamstd,beamf]=beamfortran.computebeam(xgrid,ygrid,Hs,Xr,Yr,Zr,freq,c,K,beamformer,Nr,len(xgrid),len(ygrid),len(Hs),Nf,Nc,Kinv)
    elif ln_code==2:
        import beamformingc
        beam, beamf = beamformingc.beamXY(xgrid,ygrid,Hs,Xr,Yr,Zr,freq,c,K,beamformer,Nr,len(xgrid),len(ygrid),len(Hs),Nf,Nc,Kinv)
    else:
	if beamformer=='bartlett':
	    methodLinear = True
	elif beamformer=='mvdr':
	    methodLinear = False
        else:
	    print 'undefined method : ', beamformer
	    sys.exit(1)
        beam = np.zeros((len(xgrid),len(ygrid),len(Hs),Nc), dtype=np.float64)
        beamstd = np.zeros((len(xgrid),len(ygrid),len(Hs),Nc), dtype=np.float64)
        beamf = np.empty((len(xgrid),len(ygrid),len(Hs),Nc,Nf), dtype=np.float64)
	m2jPiF_over_c = __m2jPiF_over_c(freq, c,Nf,Nc)
	if ln_savereplica:
	    replicas = np.empty((len(xgrid),len(ygrid),len(Hs),Nc,Nf,Nr),dtype=complex)
        inv4Pi = 0.25 / np.pi
        for kk, zs in enumerate(Hs):
            for ii, xs in enumerate(xgrid):
                for jj, ys in enumerate(ygrid):
                    #a = ((Xr-xs)**2+(Yr-ys)**2+(Zr-zs)**2)**0.5 # [Nr]
                    a = np.sqrt(np.square(Xr-xs) + np.square(Yr-ys) + np.square(Zr-zs)) # [Nr]
		    inv_pi_a_4 = inv4Pi / a
		    for cc,speed in enumerate(c):
                        for ff,frequency in enumerate(freq):
                            #omega = 1/(4*np.pi*a)*np.exp(-1*1j*2*np.pi*frequency*a/speed)           # [Nr]
                            omega = np.exp(a * m2jPiF_over_c[ff,cc]) * inv_pi_a_4           # [Nr]
                            omega = omega / np.abs(omega) # normalise
                            omega = omega / numpy.linalg.norm(omega) # divise par sqrt(Nr)
                            # numpy.linalg.norm(x) ~ sqrt(add.reduce((x.conj() * x).ravel().real))
			    #conjOmega = np.conj(omega)
			    if ln_savereplica:
		                replicas[ii,jj,kk,cc,ff,:] = omega.conj()
                            if methodLinear: # bartlett
				# ancien : replica = conjOmega / numpy.linalg.norm(conjOmega)  ~ conjOmega / 1
#                                replicas[ii,jj,kk,cc,ff,:]=replica
				# ancien : quand on calculait K avec d.conj x d : beamf[ii,jj,kk,cc,ff] = abs(np.dot(omega.T,(np.dot(K[:,:,ff],conjOmega))))
                                beamf[ii,jj,kk,cc,ff] = abs(np.dot(omega.conj(),(np.dot(K[:,:,ff],omega))))
			    else: # mvdr
				# ancien : d.conj replica = conjOmega / (np.dot(omega.T,np.dot(Kinv[:,:,ff],conjOmega))) / Nr
				# ancien : d.conj beamf[ii,jj,kk,cc,ff] = abs(np.dot(np.conj(replica).T,(np.dot(Kinv[:,:,ff],replica))))
                                replica = omega / (np.dot(omega.conj(),np.dot(Kinv[:,:,ff],omega))) / Nr
				beamf[ii,jj,kk,cc,ff] = abs(np.dot(np.conj(replica),(np.dot(Kinv[:,:,ff],replica))))
        beam = np.mean(beamf,4)
	beamstd = np.std(beamf,4)
	print 'ln_savereplica:',ln_savereplica
        if ln_savereplica:
	    import scipy.io as sio
	    sio.savemat('replicas.mat', {'replicas': replicas[:,:,:,:,:,:].astype(np.complex64),'freq':freq,'xgrid': xgrid, 'ygrid': ygrid,'Hs':Hs,'c':c}, oned_as='row')
#    replicasave=replicas[20,25,0,0,:,:]  # x=100,y=100
#    xgridsave,ygridsave,zsave,csave=xgrid[20],ygrid[25],Hs[0],c[0]
#    import scipy.io as sio
#    sio.savemat('/home/lecointa/WORKDIR/replica.mat', {'replicasave': replicasave,'freq':freq,'xgrid': xgridsave, 'ygrid': ygridsave,'z':zsave,'c':csave}, oned_as='row')
    return beam,beamstd,beamf

#######################################################################################################

def distlatlong(latA, lonA, latB, lonB):
    """Calcule la distance entre 2 stations et les coordonnées de la station B en prenant A comme référence.
    Géodésique avec approximation Terre sphérique
    latA,lonA,latB,lonB sont donnés en degrés
    D  [ m ] : Distance la plus courte entre A et B
    XX [ m ] : On projette B en Bo tel que lonBo=lonB et latBo=latA
    YY [ m ] : On projette B en Bz tel que lonBz=lonA et latBz=latB
    """ 
    rT = 6378000.0   # Rayon Terrestre en m
    # passage de degre en radian
    latradA = (np.pi*latA)/180.0
    lonradA = (np.pi*lonA)/180.0
    latradB = (np.pi*latB)/180.0
    lonradB = (np.pi*lonB)/180.0
    # calcul distance la plus courte
    # D = (rT * np.arccos((np.sin(latradA) * np.sin(latradB)) + (np.cos(latradA) * np.cos(latradB) * np.cos(lonradB - lonradA))));
    # D2 = ((np.arccos((np.sin(latradA) * np.sin(latradB)) + (np.cos(latradA) * np.cos(latradB) * np.cos(lonradB - lonradA))))*180)/np.pi;
    # calcul coordonnees
    XXdz = rT * np.arccos((np.sin(latradB) * np.sin(latradB)) + (np.cos(latradB) * np.cos(latradB) * np.cos(lonradB - lonradA)))
    XX = rT * np.arccos((np.sin(latradA) * np.sin(latradA)) + (np.cos(latradA) * np.cos(latradA) * np.cos(lonradB - lonradA)))
    YY = rT * np.arccos((np.sin(latradA) * np.sin(latradB)) + (np.cos(latradA) * np.cos(latradB) ))
    if latB<latA:
        YY=-YY
    if lonB<lonA:
        XX=-XX
	XXdz=-XXdz
    return XX,YY

#def invdistlatlong(latA, lonA, yB, xB):
#    """Calcule les coordonnees en degres de la station B connaissant la position x,y, de la station B relativement a la station A
#    """
#    # passage de degre en radian
#    latradA = (np.pi*latA)/180
#    lonradA = (np.pi*lonA)/180
#    # calcul coordonnees
#    cosBmA = ( np.cos(xB/6378000) - np.sin() * np.sin() )
#    latradB = (np.pi*latB)/180
#    lonradB = (np.pi*lonB)/180
#    # calcul coordonnees
#    XX = (6378 * np.arccos((np.sin(latradB) * np.sin(latradB)) + (np.cos(latradB) * np.cos(latradB) * np.cos(lonradB - lonradA))))*1000
#    YY = (6378 * np.arccos((np.sin(latradA) * np.sin(latradB)) + (np.cos(latradA) * np.cos(latradB) * np.cos(lonradB - lonradB))))*1000
#    if latB<latA:
#       YY=-YY
#    if lonB<lonA:
#       XX=-XX
#    return lonB,latB

#######################################################################################################

def beamplot(speed,theta,beam,figtit,figname):
    pylab.clf()
    pylab.pcolormesh(speed, theta, beam)
    #contourf(speed, theta, beam,cmpa=cmap3)
    pylab.colorbar()
    pylab.axis([min(speed),max(speed),min(theta),max(theta)])
    lev = np.linspace(0,1,21) # choix des niveaux de contours
    C = pylab.contour(speed, theta, beam, lev, colors="k")
    pylab.clabel(C, fontsize=10)
    pylab.xlabel('speed (km/s)')
    pylab.ylabel('theta (degree)')
    pylab.title(figtit)
    #pylab.show()
    pylab.savefig(figname, bbox_inches=0)

#########################################################################################################

def beamplotxy(x,y,beam,X,Y,figtit,figname):
    pylab.clf()
    globvitred = [  [ 0.0000 , 0.0000 , 0.6000 ] ,
                    [ 0.0000 , 0.0250 , 0.6500 ] ,
                    [ 0.0000 , 0.0500 , 0.7000 ] ,
                    [ 0.0000 , 0.0750 , 0.7500 ] ,
                    [ 0.0000 , 0.1000 , 0.8000 ] ,
                    [ 0.0000 , 0.1250 , 0.8500 ] ,
                    [ 0.0000 , 0.1500 , 0.9000 ] ,
                    [ 0.0000 , 0.1750 , 0.9500 ] ,
                    [ 0.0000 , 0.2000 , 1.0000 ] ,
                    [ 0.0000 , 0.2500 , 1.0000 ] ,
                    [ 0.0000 , 0.3000 , 1.0000 ] ,
                    [ 0.0000 , 0.3500 , 1.0000 ] ,
                    [ 0.0000 , 0.4000 , 1.0000 ] ,
                    [ 0.0000 , 0.4500 , 1.0000 ] ,
                    [ 0.0000 , 0.5000 , 1.0000 ] ,
                    [ 0.0000 , 0.5500 , 1.0000 ] ,
                    [ 0.0000 , 0.6000 , 1.0000 ] ,
                    [ 0.0000 , 0.6500 , 1.0000 ] ,
                    [ 0.0000 , 0.7000 , 1.0000 ] ,
                    [ 0.0000 , 0.7500 , 1.0000 ] ,
                    [ 0.0000 , 0.8000 , 1.0000 ] ,
                    [ 0.0000 , 0.9000 , 1.0000 ] ,
                    [ 0.0000 , 1.0000 , 1.0000 ] ,
                    [ 0.2000 , 1.0000 , 1.0000 ] ,
                    [ 0.4000 , 1.0000 , 1.0000 ] ,
                    [ 0.6000 , 1.0000 , 1.0000 ] ,
                    [ 0.8000 , 1.0000 , 1.0000 ] ,
                    [ 0.8872 , 0.9049 , 0.8108 ] ,
                    [ 0.9745 , 0.8098 , 0.6216 ] ,
                    [ 0.9490 , 0.6196 , 0.2431 ] ,
                    [ 0.9383 , 0.5020 , 0.2206 ] ,
                    [ 0.9275 , 0.3843 , 0.1980 ] ,
                    [ 0.9167 , 0.2667 , 0.1755 ] ,
                    [ 0.9059 , 0.1490 , 0.1529 ] ,
                    [ 0.7647 , 0.0000 , 0.0431 ] ]
    import matplotlib.colors as col
    cmap3 = col.ListedColormap(globvitred, 'indexed')
    pylab.pcolormesh(x, y, np.transpose(beam),cmap=cmap3)
    #pylab.contourf(x, y, np.transpose(beam))
    pylab.colorbar()
    pylab.axis([min(x),max(x),min(y),max(y)])
    #lev = np.linspace(0,1,21) # choix des niveaux de contours
    #C = pylab.contour(x, y, np.transpose(beam), lev, colors="k")
    #pylab.clabel(C, fontsize=10)
    pylab.plot(X-np.mean(X),Y-np.mean(Y),'y^')
    pylab.xlabel('xgrid (m)')
    pylab.ylabel('ygrid (m)')
    pylab.title(figtit)
    #pylab.show()
    pylab.savefig(figname, bbox_inches=0)

#########################################################################################################

def matwrite(directory,filename,var_dict,ln_float32=0):
    """Write beam outputs given in the dictionary var_dict as argument
    Write as vartype [ default: float64 / ln_float32=1: float32 ]
    """
    import os
    import scipy.io as sio
    vartype = ('Float64','Float32')
    print(' --> Save output as '+vartype[ln_float32]+' in '+directory+os.sep+filename+'.mat'),
    if ln_float32:
        var_dict32 = {}
        for key in var_dict.keys():
	    if isinstance(var_dict[key],np.ndarray) and key!='lat' and key!='lon':
		var_dict32[key] = var_dict[key].astype(np.float32)
	    else:
		var_dict32[key] = var_dict[key]
        sio.savemat(directory+'/'+filename, var_dict32, appendmat=True, oned_as='row')
    else:
        sio.savemat(directory+'/'+filename, var_dict, appendmat=True, oned_as='row')

#########################################################################################################

def ncCreateVar(ncid,varclass): 
    """Create variable whose attributes (name, type, dimensions, unit) are detailed in varclass
    Maximum number of dimension for variable is 5
    """
    ncvar = ncid.createVariable(varclass.n, varclass.t, varclass.d)
    var = varclass.v
    if len(np.shape(var))<2:
        ncvar[:] = var
    elif len(np.shape(var))==2:
	ncvar[:,:] = var
    elif len(np.shape(var))==3:
	ncvar[:,:,:] = var
    elif len(np.shape(var))==4:
	ncvar[:,:,:,:] = var
    elif len(np.shape(var))==5:
	ncvar[:,:,:,:,:] = var
    if varclass.u:
        ncvar.units = varclass.u
    return
		
def ncCreateDim(ncid,dimclass):
    """Create dimension whose attributes (name,number of dim) are detailed in dimclass"""
    ncid.createDimension(dimclass.n,dimclass.d)
    return

def ncwrite(directory,filename,dimList,varList,SVNrev,appendnc=True):
    """Write beam outputs given in the list varList in a NetCDF-3 classic format file"""
    import os
    from scipy.io import netcdf
    print(' --> Save output in '+directory+os.sep+filename+'.nc'),
    if appendnc and filename[-3:] != ".nc":
        filename = filename + ".nc"
    f = netcdf.netcdf_file(directory+os.sep+filename, 'w')
    f.history = 'Beamforming output: NetCDF-3 Classic Format \n Beamforming code '+SVNrev
    for dimItem in dimList:
        ncCreateDim(f,dimItem)
    for varItem in varList:
        ncCreateVar(f,varItem)
    f.close()
    return

#########################################################################################################

def readposition(source,element):
    """lit la latitude dans un fichier SENSOR_position.txt"""
    f = open(source,'r')
    while 1:
        txt = f.readline()
        if txt =='':
            break
        if txt[0] != '%':
            tt = txt.split()
    if element == 'latitude':
        index=0
    elif element == 'longitude':
        index=1
    elif element == 'elevation':
        index=2
    return float(tt[index])

def writeTiminginit():
    """initialise le fichier qui contient le timing pour chaque etape pour chaque processus"""
    f = open('timing.dat','w')
    f.write('| Processus | PID  | Read data | DFT+CSDM |    BEAM    | Total nb of iterations | time/iterations | Write outputs |  Plot  |    Total   | \n')
    f.write('|           |      |   (sec)   |   (sec)  |    (sec)   |                        |    (microsec)   |      (sec)    |  (sec) |    (sec)   | \n')
    f.close()
    return

def writeTiming(processus,pid,tread,tinit,tbeam,nbit,twrite,tplot,ttot):
    """ecrit le timing dans le fichier qui contient le timing pour chaque etape pour chaque processus"""
    if processus == '0001':
        writeTiminginit()
    f = open('timing_'+str(processus)+'.dat','w') # 'a')
    f.write('|')
    f.write('{:^11}'.format(processus))
    f.write('|')
    f.write('{:^6}'.format(str(pid)))
    f.write('|')
    f.write('{:^11}'.format(str(tread)))
    f.write('|')
    f.write('{:^10}'.format(str(tinit)))
    f.write('|')
    f.write('{:^12}'.format(str(tbeam)))
    f.write('|')
    f.write('{:^24}'.format(str(nbit)))
    f.write('|')
    f.write('{:^17}'.format(str(1e6*tbeam/nbit)))
    f.write('|')
    f.write('{:^15}'.format(str(twrite)))
    f.write('|')
    f.write('{:^8}'.format(str(tplot)))
    f.write('|')
    f.write('{:^12}'.format(str(ttot)))
    f.write('| \n')
#    f.seek(109)
#    f.write('toto')
    f.close()
    return

