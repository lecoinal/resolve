# -*- coding: utf-8 -*-

#  =======================================================================================
#                       ***  Python script main_beamforming.py  ***
#   This is the main Python script to compute beamforming
#    
#  =======================================================================================
#   History :  1.0  : 12/2013  : A. Lecointre : Initial version
#	        
#	describe here	    
#	describe here
#	describe here	 
#  ---------------------------------------------------------------------------------------
#
#  ---------------------------------------------------------------------------------------
#   routines      : description
#                 : 
#                 : 
#                 : 
#                 :
#  ---------------------------------------------------------------------------------------
#  $Id: main_beamforming.py 35 2013-12-19 14:27:05Z lecointre $
#  ---------------------------------------------------------------------------------------

# declaration des librairies

import scipy.io as sio
import numpy as np
np.setbufsize(10e6)
from function_def import *
import ConfigParser
from obspy.core import UTCDateTime, read
import datetime
import os,pwd
import socket
import gc
import sys

tbegin = datetime.datetime.today()

def getParameters():
   import getopt
   def printUsageAndExit():
      print 'main_beamforming.py -i <cfgfile>'
      sys.exit(1)
   try:
      opts, args = getopt.getopt(sys.argv[1:],"hi:",["ifile="])
   except getopt.GetoptError:
      sys.exit(2)
   if len(opts) == 0:
      printUsageAndExit()
   else:
      for opt, arg in opts:
         if opt in ("-i", "--ifile"):
            cfgfile = arg
         else:
            printUsageAndExit()
   return cfgfile

#########################################################################
#if __name__ == "__main__":

cfgfile = getParameters()
print 'Input cfgfile is ', cfgfile
processus = cfgfile[9:13]

#####################################################################

pid=os.getpid()
print(pid) # processus identificator
machine = socket.gethostname()
print('Computing on '+machine)

#####################################################################
# Reading configuration file config_beam_proc<<PP>>.cfg

print(' --> Read configuration file '+cfgfile+' ...'),

cp = ConfigParser.ConfigParser()
cp.read(cfgfile)
cp.sections()

reseau = cp.get('inputs','reseau')
if reseau == 'resif':
    sousreseau = cp.get('inputs','sousreseau')
    year = cp.getint('inputs','year')
    day = cp.get('inputs','day')
    antenne = cp.get('inputs','antenne')
    stations = eval(cp.get('inputs','stations'), {}, {})
    channels = eval(cp.get('inputs','channels'), {}, {})
    location = cp.get('inputs','location')
    T1 = UTCDateTime(eval(cp.get('inputs','T1')))
    T2 = UTCDateTime(eval(cp.get('inputs','T2')))
elif reseau == 'XH':
    antenne = cp.get('inputs','antenne')
    stations = eval(cp.get('inputs','stations'), {}, {})
    channels = eval(cp.get('inputs','channels'), {}, {})
    T1 = UTCDateTime(eval(cp.get('inputs','T1')))
    T2 = UTCDateTime(eval(cp.get('inputs','T2')))
    day1 = T1.format_fissures()[4:7]
    day2 = T2.format_fissures()[4:7]
    year = T1.format_fissures()[0:4]
    if day1==day2:
        day=day1
    else:
	day=day1
elif reseau == 'SG':
    stations = eval(cp.get('inputs','stations'), {}, {})
    channels = eval(cp.get('inputs','channels'), {}, {})
    T1 = UTCDateTime(eval(cp.get('inputs','T1')))
    T2 = UTCDateTime(eval(cp.get('inputs','T2')))
    day1 = T1.format_fissures()[4:7]
    day2 = T2.format_fissures()[4:7]
    year = T1.format_fissures()[0:4]
    day=day1

ME = cp.get('jobs','Username')
Tinit = cp.get('jobs','Tinit')
Tprocess = cp.get('inputs','Tprocess')

PDIR = cp.get('paths','PDIR')
FDIR = cp.get('paths','FDIR')
DDIR = cp.get('paths','DDIR')
ODIR = cp.get('paths','ODIR')
RDIR = cp.get('paths','RDIR')

method = cp.get('parameters','method')
svd = cp.getint('parameters','svd')
meig = cp.getint('parameters','meig')
methodFabian = cp.getint('parameters','methodFabian')
decimateFactor = cp.getint('parameters','decimate_factor') 
nivBruit = cp.getint('parameters','nivBruit')
normalisation = cp.get('parameters','normalisation')
dtBouts = cp.getint('parameters','dtBouts')
azimuth = cp.getint('parameters','azimuth')

ln_deconv = cp.getint('deconv_param','ln_deconv')
if ln_deconv:
    seedrespUnits = cp.get('deconv_param','seedrespUnits')
    prefilter1 = cp.getfloat('deconv_param','prefilter1')
    prefilter2 = cp.getfloat('deconv_param','prefilter2')
    corners = cp.getint('deconv_param','corners')
    ln_zerophase = cp.get('deconv_param','ln_zerophase')
    water_level = cp.getfloat('deconv_param','water_level')

ln_prof = cp.getint('profiling','ln_prof')
ln_code = cp.getint('profiling','ln_code')

ln_plot = cp.getint('output','ln_plot')
ln_save = cp.getint('output','ln_save')
ln_saveallfrequencies = cp.getint('output','ln_saveallfrequencies')
ln_savereplica = cp.getint('output','ln_savereplica')
ln_readreplica = cp.getint('output','ln_readreplica')
ln_float32 = cp.getint('output','ln_float32')

if azimuth:
    mi = cp.getfloat('theta','min')
    ma = cp.getfloat('theta','max')
    st = cp.getfloat('theta','step')
    theta = np.arange(mi,ma+st,st)
else:
    mi = cp.getfloat('xy','xmin')
    ma = cp.getfloat('xy','xmax')
    st = cp.getfloat('xy','xstep')
    xgrid = np.arange(mi,ma+st,st)
    mi = cp.getfloat('xy','ymin')
    ma = cp.getfloat('xy','ymax')
    st = cp.getfloat('xy','ystep')
    ygrid = np.arange(mi,ma+st,st)
    mi = cp.getfloat('Hsource','min')
    ma = cp.getfloat('Hsource','max')
    st = cp.getfloat('Hsource','step')
    zs = np.arange(mi,ma+st,st)

mi = cp.getfloat('speed','min')
ma = cp.getfloat('speed','max')
st = cp.getfloat('speed','step')
speed = np.arange(mi,ma+st,st)
mi = cp.getfloat('indice_freq','min')
ma = cp.getfloat('indice_freq','max')
st = cp.getfloat('indice_freq','step')
indice_freq = np.arange(mi,ma+st,st)

print('  [Done]')
del ConfigParser

T2year = T2.year

########################################################################
# Read data

print(' --> Read data from array: '+reseau+' ...'),

t0=datetime.datetime.today()

# signal filtré 2-8Hz de Dimitri
if reseau=='GGAP':
    # only available on calculX and DDIR is supposed to be  '/lgit/'+ME+'/DATA/DZIGONE_THESE_FIG2.24/'
    mat_contents = sio.loadmat(DDIR+os.sep+'data_270210.mat')
    X = mat_contents['X']
    Y = mat_contents['Y']
    data = mat_contents['data']
    Fs=100

# lecture directe MiniSEED depuis /media/resif
if reseau=='resif':
    # only available on calculX and DDIR is supposed to be /media/resif/
    X = np.zeros(len(stations), dtype=np.float64)
    Y = np.zeros(len(stations), dtype=np.float64)
    for js in range(len(stations)):
        STATION = antenne+stations[js]
        CHANNEL = channels[js]
        fil = sousreseau+'.'+STATION+'.'+location+'.'+CHANNEL+'.D.'+str(year)+'.'+day
        st = read(DDIR+'/'+str(year)+'/'+sousreseau+'/'+STATION+'/'+CHANNEL+'.D/'+fil,starttime=T1, endtime=T2)
        if js == 0:
            data = np.zeros((st[0].stats.npts,len(stations)), dtype=np.float64)
        # Bandpass signal 2-8 Hz
        freqmin,freqmax=3,6
        st.detrend('linear')
        st.filter('bandpass', freqmin=freqmin, freqmax=freqmax)
        data[:,js] = st[0]
        # coordinates: Compute position (x,y) of sensors, using distlatlong
        if js == 0:
            lonref = readposition(PDIR+'/'+STATION+'_position.txt','longitude')
            latref = readposition(PDIR+'/'+STATION+'_position.txt', 'latitude')
        lon = readposition(PDIR+'/'+STATION+'_position.txt','longitude')
        lat = readposition(PDIR+'/'+STATION+'_position.txt', 'latitude')
        X[js],Y[js] = distlatlong(latref,lonref,lat,lon)
    # Get sampling frequency (Hz)
    Fs = st[0].stats.sampling_rate

# XH data Fabian Walter
if reseau=='XH':
    X = np.zeros(len(stations)*len(channels), dtype=np.float64)
    Y = np.zeros(len(stations)*len(channels), dtype=np.float64)
    Z = np.zeros(len(stations)*len(channels), dtype=np.float64)
    lon = np.zeros(len(stations)*len(channels), dtype=np.float64)
    lat = np.zeros(len(stations)*len(channels), dtype=np.float64)
    for js in range(len(stations)):
        STATION = stations[js]
        for jc in range(len(channels)):
            CHANNEL = channels[jc]
            fil = reseau+'.'+STATION+'..'+CHANNEL+'.D.'+str(year)+'.'+str(day)+'.seed'
            st = read(DDIR+os.sep+reseau+os.sep+STATION+os.sep+str(year)+os.sep+fil,starttime=T1, endtime=T2)

            # abort if gaps
            if st.count() != 1:
                print(' ')
                print('Error: '+STATION+' '+CHANNEL+' '+str(year)+' '+str(day)+' stream contains more than one trace') 
                print DDIR+os.sep+reseau+os.sep+STATION+os.sep+str(year)+os.sep+fil
                print T1
                print T2
                print st
                print('Beamforming code is not able to fill the gaps') 
                sys.exit()

            # Detrend / Bandpass ???
            #
	    # downsampling: Reduce the sampling rate of the data (optionnal)
	    if decimateFactor:
	        st[0].decimate(factor=decimateFactor, no_filter=False, strict_length=False)
            # Deconvolution
            if ln_deconv:
                seedresp = {'filename': RDIR+'/RESP.'+reseau+'.'+STATION+'..'+CHANNEL, 
                                                            # RESP filename
                             'date': st[0].stats.starttime, # UTCDateTime('2011-07-13T00:00:00.00000'),
                             'units': seedrespUnits         # Units to return response in ('DIS', 'VEL' or ACC)
                            }
                st[0].filter('bandpass',freqmin=prefilter1,freqmax=prefilter2,corners=corners,zerophase=ln_zerophase)
                st[0].simulate(paz_remove=None, seedresp=seedresp, water_level=water_level)
	    if js == 0 and jc == 0:
		data = np.zeros((st[0].stats.npts,len(stations)*len(channels)), dtype=np.float64)
            data[:,js*len(channels)+jc] = st[0]
        # Coordinates
            if js == 0 and jc == 0:
                lonref = readposition(PDIR+os.sep+STATION+'_position.txt','longitude')
                latref = readposition(PDIR+os.sep+STATION+'_position.txt', 'latitude')
            lon[js*len(channels)+jc] = readposition(PDIR+os.sep+STATION+'_position.txt','longitude')
            lat[js*len(channels)+jc] = readposition(PDIR+os.sep+STATION+'_position.txt', 'latitude')
            Z[js*len(channels)+jc] = readposition(PDIR+os.sep+STATION+'_position.txt', 'elevation')
            X[js*len(channels)+jc],Y[js*len(channels)+jc] = distlatlong(latref,lonref,lat[js*len(channels)+jc],lon[js*len(channels)+jc])
    Fs = st[0].stats.sampling_rate

# SJFZ
if reseau=='SG':
    # only available on calculX and DDIR is supposed to be /media/resif/
    X = np.zeros(len(stations), dtype=np.float64)
    Y = np.zeros(len(stations), dtype=np.float64)
    Z = np.zeros(len(stations), dtype=np.float64)
    CHANNEL = channels[0]  # there is only one unique channel for SG array
    # find the number of samples
    st0 = read(DDIR+'/'+str(year)+'/'+reseau+'/'+stations[0]+'/'+CHANNEL+'.D/'+reseau+'.'+stations[0]+'..'+CHANNEL+'.D.'+str(year)+'.'+day,starttime=T1, endtime=T2)
    # Get sampling frequency (Hz)
    Fs = st0[0].stats.sampling_rate
    # Adjust end time
    T2 = T2 - 1./float(Fs)
    # nb of samples is st0[0].stats.npts
    data = np.zeros((st0[0].stats.npts-1,len(stations)), dtype=np.float64)
    mat_contents = sio.loadmat(PDIR+os.sep+'reseau_64.mat') # /data/ondes/rouxphi/yehuda/june_2014/reseau_64.mat
    allstationname = mat_contents['all_stations']
    li=allstationname.tolist()
    alllon = mat_contents['x']
    alllat = mat_contents['y']
    allele = mat_contents['z']
    lonref = alllon[li.index(stations[0])] # readposition(PDIR+'/'+STATION+'_position.txt','longitude')
    latref = alllat[li.index(stations[0])] # readposition(PDIR+'/'+STATION+'_position.txt', 'latitude')
    eleref = allele[li.index(stations[0])] # readposition(PDIR+'/'+STATION+'_position.txt', 'latitude')
    for js in range(len(stations)):
        STATION = stations[js]
        fil = reseau+'.'+STATION+'..'+CHANNEL+'.D.'+str(year)+'.'+day
#        print('Read the station '+STATION+' '+str(js)+' / '+str(len(stations)))
        st = read(DDIR+'/'+str(year)+'/'+reseau+'/'+STATION+'/'+CHANNEL+'.D/'+fil,starttime=T1, endtime=T2)
#        if js == 0:
#            data = np.zeros((st[0].stats.npts,len(stations)), dtype=np.float64)
        data[:,js] = st[0]
        # coordinates: Compute position (x,y) of sensors, using distlatlong
#        if js == 0:
#            mat_contents = sio.loadmat(PDIR+os.sep+'reseau_64.mat') # /data/ondes/rouxphi/yehuda/june_2014/reseau_64.mat
#            allstationname = mat_contents['all_stations']
#            li=allstationname.tolist()
#            alllon = mat_contents['x']
#            alllat = mat_contents['y']
#            allele = mat_contents['z']
#            lonref = alllon[li.index(STATION)] # readposition(PDIR+'/'+STATION+'_position.txt','longitude')
#            latref = alllat[li.index(STATION)] # readposition(PDIR+'/'+STATION+'_position.txt', 'latitude')
#            eleref = allele[li.index(STATION)] # readposition(PDIR+'/'+STATION+'_position.txt', 'latitude')
        lon = alllon[li.index(STATION)] # readposition(PDIR+'/'+STATION+'_position.txt','longitude')
        lat = alllat[li.index(STATION)] # readposition(PDIR+'/'+STATION+'_position.txt', 'latitude')
        Z[js] = allele[li.index(STATION)] # readposition(PDIR+'/'+STATION+'_position.txt', 'latitude')
        X[js],Y[js] = distlatlong(latref,lonref,lat,lon)


# san andreas
if reseau=='XN':
    # only available on calculX and DDIR is supposed to be '/lgit/'+ME+'/DATA/XN/'
    STATIONS =  ['AHAB','BECH','CGAS','CRAK','CVCR','DBLT','FLIP','GLEN','GOBI','GULY','HIDE','ISKK','JETS','KARL','KOOL','LEEP','MOH9','NOXV','PAKD','PIES','PIGH','POLE','POND','POST','POWR','PRIS','RAIN','RCKY','SAGE','STGI']
    X = np.zeros(len(STATIONS), dtype=np.float64)
    Y = np.zeros(len(STATIONS), dtype=np.float64)
    data = np.zeros((864001,len(STATIONS)), dtype=np.float64)
    for js in range(len(STATIONS)):
        STATION = STATIONS[js]
        mat_contents = sio.loadmat(DDIR+os.sep+'XN_'+STATION+'_BHZ_20.mat')
	dat = mat_contents['toto']
        data[:,js]=np.squeeze(dat)
        lat = mat_contents['lat']
        lon = mat_contents['lon']
        if js == 0:
            Fs = mat_contents['Fsnew']
            latref = lat
            lonref = lon
        X[js],Y[js] = distlatlong(latref,lonref,lat,lon)

# Centers the domain for beam computation, remember the lat/lon of the center of the domain
Xc=np.mean(X)
Yc=np.mean(Y)
cos_lonc_moins_lonref = ( np.cos(Xc/6378000.0) - np.sin(latref*np.pi/180.0)*np.sin(latref*np.pi/180.0) ) / (np.cos(latref*np.pi/180.0)*np.cos(latref*np.pi/180.0))
lonc_moins_lonref = np.sign(Xc)*np.arccos(cos_lonc_moins_lonref)
lonc = lonref + lonc_moins_lonref*180.0/np.pi
latc = latref + (Yc/6378000.0)*180.0/np.pi
X = X - Xc
Y = Y - Yc

t1=datetime.datetime.today()
tread=(t1-t0).total_seconds()

print('  [Done]')
print('            elapsed time (sec): {:f}'.format(tread))

print('                shape(X):'+str(np.shape(X)))
print('                shape(Y):'+str(np.shape(Y)))
print('                shape(data):'+str(np.shape(data)))
print('                X.dtype:'+str(X.dtype))
print('                Y.dtype:'+str(Y.dtype))
print('                data.dtype:'+str(data.dtype))
print('                Fs:'+str(Fs))

#######################################################################################
# Compute beamforming

code=('Python','Fortran','API-C')
print(' --> Compute beamforming ('+code[ln_code]+')')
for iexec in range(1):
    print('      --> Execution: '+str(iexec)+' ...'),
    tt0=datetime.datetime.today()
    t0=datetime.datetime.today()
    K,SSVD = csdm(data,X,Y,float(Fs),indice_freq,nivBruit=nivBruit,normalisation=normalisation,dtBouts=dtBouts,svd=svd,meig=meig,methodFabian=methodFabian,ln_readreplica=ln_readreplica)
    del data # libere [taille de float64] x Nr x Nr x indice_freq
    gc.collect()
    # Inverse CSDM if method adaptive
    if method == 'mvdr':
        Kinv = invCsdm(K, len(X), len(indice_freq))
    else:
        Kinv = np.empty((len(X),len(X),len(indice_freq)),dtype=np.complex128)
    t1=datetime.datetime.today()
    beam = None
    beamf = None
    tinit=(t1-t0).total_seconds()
    t0=datetime.datetime.today()
    if azimuth:
        beam,beamstd,beamf=beamforming_code(K,Kinv,X,Y,theta,speed,indice_freq,beamformer=method,ln_code=ln_code)
    else:
        beam,beamstd,beamf=beamforming_code_xy(K,Kinv,X,Y,Z,xgrid,ygrid,zs,speed,indice_freq,beamformer=method,ln_code=ln_code,ln_savereplica=ln_savereplica)
    t1=datetime.datetime.today()
    tbeam=(t1-t0).total_seconds()
    tt1=datetime.datetime.today()
    print('  [Done]')
    tttot=(tt1-tt0).total_seconds()
    if azimuth:
        nbit = len(theta)*len(speed)*len(indice_freq)
        print('            elapsed time (sec): {:f}'.format(tttot))
        print('                |-> DFT+CSDM : {:f}'.format(tinit)+'   {:.4%}'.format(tinit/tttot))
	print('                |-> BEAM     : {:f}'.format(tbeam)+'   {:.4%}'.format(tbeam/tttot))
        print('                |-> nb of it in BEAM : [theta] : {:n}'.format(len(theta)))
        print('                |                      [c]     : {:n}'.format(len(speed)))
        print('                |                      [freq]  : {:n}'.format(len(indice_freq)))
        print('                |   Total : {:n}'.format(nbit)+' iterations')
        print('                |   average time (microsec) per it.: {:f}'.format(1e6*tbeam/nbit))
    else:
        nbit = len(zs)*len(xgrid)*len(ygrid)*len(speed)*len(indice_freq)
        print('            elapsed time (sec): {:f}'.format(tttot))
        print('                |-> DFT+CSDM : {:f}'.format(tinit)+'   {:.4%}'.format(tinit/tttot))
        print('                |-> BEAM     : {:f}'.format(tbeam)+'   {:.4%}'.format(tbeam/tttot))
        print('                |-> nb of it in BEAM : [Hs]    : {:n}'.format(len(zs)))
        print('                |                      [xgrid] : {:n}'.format(len(xgrid)))
        print('                |                      [ygrid] : {:n}'.format(len(ygrid)))
        print('                |                      [c]     : {:n}'.format(len(speed)))
        print('                |                      [freq]  : {:n}'.format(len(indice_freq)))
        print('                |   Total : {:n}'.format(nbit)+' iterations')
        print('                |   average time (microsec) per it.: {:f}'.format(1e6*tbeam/nbit))

########################################################################################
# Plot

tplot = None
if ln_plot:
    t0=datetime.datetime.today()
    if azimuth:
        print(' --> Plot in '+FDIR+os.sep+'beam_[T1]_[T2]_[F]_'+method+'_'+normalisation+'_'+reseau+'.eps ...'),
        tit=('BF - f:'+str(indice_freq[0])+':'+str(indice_freq[-1])+'Hz - '+method+' - '+normalisation+' - '+reseau)
        nam=(FDIR+os.sep+'beam_'+T1.format_iris_web_service()+'_'+T2.format_iris_web_service()+'_'+str(indice_freq[0])+'..'+str(indice_freq[-1])+'_'+method+'_'+normalisation+'_'+reseau+'.eps')
        beamplot(speed,theta,beam,tit,nam)
        for ff in range(len(indice_freq)):
            tit=('BF - f:'+str(indice_freq[ff])+'Hz - '+method+' - '+normalisation+' - '+reseau)
            nam=(FDIR+os.sep+'beam_'+T1.format_iris_web_service()+'_'+T2.format_iris_web_service()+'_'+str(indice_freq[ff])+'_'+method+'_'+normalisation+'_'+reseau+'.eps')
            beamplot(speed,theta,beamf[:,:,ff],tit,nam)
    else:
# plot 3d : http://matplotlib.org/mpl_toolkits/mplot3d/tutorial.html#d-plots-in-3d
        print(' --> Plot in '+FDIR+os.sep+'beam_[T1]_[T2]_[Z]_[C]_[F]_'+method+'_'+normalisation+'_'+reseau+'.eps ...'),
        for zz in range(len(zs)):
            for cc in range(len(speed)):
                tit=('BF - Hs:'+str(zs[zz])+' - c:'+str(speed[cc])+'m/s - f:'+str(indice_freq[0])+':'+str(indice_freq[-1])+'Hz - '+method+' - '+normalisation+' - '+reseau)
                nam=(FDIR+os.sep+'beam_'+T1.format_iris_web_service()+'_'+T2.format_iris_web_service()+'_'+str(zs[zz])+'_'+str(speed[cc])+'_'+str(indice_freq[0])+'..'+str(indice_freq[-1])+'_'+method+'_'+normalisation+'_'+reseau+'.eps')
                beamplotxy(xgrid,ygrid,beam[:,:,zz,cc],X,Y,tit,nam)
                for ff in range(len(indice_freq)):
                    tit=('BF - Hs:'+str(zs[zz])+' - c:'+str(speed[cc])+'m/s - f:'+str(indice_freq[ff])+'Hz - '+method+' - '+normalisation+' - '+reseau)
                    nam=(FDIR+os.sep+'beam_'+T1.format_iris_web_service()+'_'+T2.format_iris_web_service()+'_'+str(zs[zz])+'_'+str(speed[cc])+'_'+str(indice_freq[ff])+'_'+method+'_'+normalisation+'_'+reseau+'.eps')
                    beamplotxy(xgrid,ygrid,beamf[:,:,zz,cc,ff],X,Y,tit,nam)
    t1=datetime.datetime.today()
    tplot=(t1-t0).total_seconds()
    print('  [Done]')
    print('            elapsed time (sec): {:f}'.format(tplot))

########################################################################################
# Save outputs

twrite = None
SVNrevision = '$Revision: 116 $'
t0=datetime.datetime.today()
#directory = (ODIR+os.sep+reseau+os.sep+str(T2.year)+os.sep+'{:0>2}'.format(str(T2.month))+os.sep)
directory = ('.'+os.sep)
#filename = ('beam_'+T1.format_iris_web_service().replace(":",".")+'_'+T2.format_iris_web_service().replace(":",".")+'_'+method+'_'+normalisation+'_'+reseau)
filename = ('beam_'+'{:0>4}'.format(str(T1.year))+'_'+'{:0>3}'.format(str(T1.julday))+'_'+'{:0>2}'.format(str(T1.hour))+'{:0>2}'.format(str(T1.minute))+'{:0>2}'.format(str(T1.second))+'_'+'{:0>4}'.format(str(T2.year))+'_'+'{:0>3}'.format(str(T2.julday))+'_'+'{:0>2}'.format(str(T2.hour))+'{:0>2}'.format(str(T2.minute))+'{:0>2}'.format(str(T2.second))+'_'+method+'_'+normalisation+'_'+reseau)
if ln_save==3:
    # HDF5 output file format
    import h5py
    h5f = h5py.File(directory+os.sep+'dim_'+filename+'.h5','w')
    h5f.create_dataset('Xr',   shape=(X.shape), dtype='float32',data=X,compression='gzip', compression_opts=1, fletcher32='True')
    h5f.create_dataset('Yr',   shape=(Y.shape), dtype='float32',data=Y,compression='gzip', compression_opts=1, fletcher32='True')
    h5f.create_dataset('speed',shape=(speed.shape), dtype='float32',data=speed,compression='gzip', compression_opts=1, fletcher32='True')
    h5f.create_dataset('freq', shape=(indice_freq.shape), dtype='float32',data=indice_freq,compression='gzip', compression_opts=1, fletcher32='True')
    h5f.create_dataset('lon',  shape=(lon.shape), dtype='float32',data=lon,compression='gzip', compression_opts=1, fletcher32='True')
    h5f.create_dataset('lat',  shape=(lat.shape), dtype='float32',data=lat,compression='gzip', compression_opts=1, fletcher32='True')
    h5f.create_dataset('lonc', shape=(lonc.shape), dtype='float32',data=lonc,compression='gzip', compression_opts=1, fletcher32='True')
    h5f.create_dataset('latc', shape=(latc.shape), dtype='float32',data=latc,compression='gzip', compression_opts=1, fletcher32='True')
    if azimuth:
        h5f.create_dataset('theta', shape=(theta.shape), dtype='float32',data=theta,compression='gzip', compression_opts=1, fletcher32='True')
    else:
        h5f.create_dataset('xgrid', shape=(xgrid.shape), dtype='float32',data=xgrid,compression='gzip', compression_opts=1, fletcher32='True')
        h5f.create_dataset('ygrid', shape=(ygrid.shape), dtype='float32',data=ygrid,compression='gzip', compression_opts=1, fletcher32='True')
        h5f.create_dataset('Hs', shape=(zs.shape), dtype='float32',data=zs,compression='gzip', compression_opts=1, fletcher32='True')
        h5f.create_dataset('Zr', shape=(Z.shape), dtype='float32',data=Z,compression='gzip', compression_opts=1, fletcher32='True')
    h5f.close()
    h5fo = h5py.File(directory+os.sep+filename+'.h5','w')
    if ln_saveallfrequencies==1:
        h5fo.create_dataset('beamf',shape=(beamf.shape), dtype='float32',data=beamf,compression='gzip', compression_opts=1, fletcher32='True')
    elif ln_saveallfrequencies==-1:
        h5fo.create_dataset('beam',shape=(beam.shape), dtype='float32',data=beam,compression='gzip', compression_opts=1, fletcher32='True')
        h5fo.create_dataset('beamstd',shape=(beamstd.shape), dtype='float32',data=beamstd,compression='gzip', compression_opts=1, fletcher32='True')
    else:
        h5fo.create_dataset('beamf',shape=(beamf.shape), dtype='float32',data=beamf,compression='gzip', compression_opts=1, fletcher32='True')
        h5fo.create_dataset('beam',shape=(beam.shape), dtype='float32',data=beam,compression='gzip', compression_opts=1, fletcher32='True')
        h5fo.create_dataset('beamstd',shape=(beamstd.shape), dtype='float32',data=beamstd,compression='gzip', compression_opts=1, fletcher32='True')
    if svd:
        h5fo.create_dataset('SSVD',shape=(SSVD.shape), dtype='float32',data=SSVD,compression='gzip', compression_opts=1, fletcher32='True')
    h5fo.close()
elif ln_save==1:
    # MatLab output file format 
    var_dict = { 'Xr':X , 'Yr':Y , 'speed':speed , 'freq':indice_freq , 'PID':pid, 'lon':lon , 'lat':lat, 'lonc':lonc, 'latc':latc, 'T1':T1.format_iris_web_service().replace(":","."), 'T2':T2.format_iris_web_service().replace(":",".") , 'SVNrevision':SVNrevision }
    if azimuth:
	var_dict.update({ 'theta': theta })
    else:
	var_dict.update({ 'xgrid':xgrid , 'ygrid':ygrid , 'Hs':zs , 'Zr':Z })
    if ln_saveallfrequencies==1:
        var_dict.update({'beamf':beamf})
    elif ln_saveallfrequencies==-1:
	var_dict.update({'beam':beam, 'beamstd':beamstd})
    else:
	var_dict.update({'beam':beam, 'beamstd':beamstd, 'beamf':beamf})
    if svd:
	var_dict.update({'SSVD':SSVD})
    matwrite(directory,filename,var_dict,ln_float32=ln_float32)
elif ln_save==2:
    # NetCDF output file format
    if ln_float32:
        ncdatatype='f4'
    else:
	ncdatatype='f8'
    class NcDimension:
	def __init__(self,dimname,dim):
	    self.n = dimname
	    self.d = dim
    class NcVariable:
	def __init__(self,var,varname,vartype,vardim,varunit):
            self.v = var
	    self.n = varname
	    self.t = vartype
	    self.d = vardim
	    self.u = varunit
    dimList = [NcDimension('time',1),NcDimension('center',1),NcDimension('speed',len(speed)),NcDimension('freq',len(indice_freq)),NcDimension('sensor',len(X))]
    varList = [NcVariable(Tprocess,'time','f8',('time',),'seconds since '+Tinit)]
    varList.append(NcVariable(indice_freq,'frequency',ncdatatype,('time','freq'),'Hz'))
    varList.append(NcVariable(speed,'speed',ncdatatype,('time','speed'),'m/s'))
    varList.append(NcVariable(X,'Xr',ncdatatype,('sensor',),'m'))
    varList.append(NcVariable(Y,'Yr',ncdatatype,('sensor',),'m'))
    varList.append(NcVariable(lon,'lon','f8',('sensor',),'degree east'))
    varList.append(NcVariable(lat,'lat','f8',('sensor',),'degree north'))
    varList.append(NcVariable(lonc,'lonc','f8',('center',),'degree east'))
    varList.append(NcVariable(latc,'latc','f8',('center',),'degree north'))
    if azimuth:
	dimList.append(NcDimension('theta',len(theta)))
        varList.append(NcVariable(theta,'theta',ncdatatype,('theta',),'degree for the origin of plane wave k vector (clockwise from south)'))
        varList.append(NcVariable(beamf,'beamf', ncdatatype, ('theta','speed','freq'),0))
        varList.append(NcVariable(beam,'beam',ncdatatype,('theta','speed'),0))
        varList.append(NcVariable(beamstd,'beamstd', ncdatatype, ('theta','speed'),0))
    else:
	dimList.append(NcDimension('xgrid',len(xgrid)))
	dimList.append(NcDimension('ygrid',len(ygrid)))
	dimList.append(NcDimension('Hs',len(zs)))
        varList.append(NcVariable(xgrid,'xgrid', ncdatatype, ('xgrid',),'m'))
        varList.append(NcVariable(ygrid,'ygrid', ncdatatype, ('ygrid',),'m'))
        varList.append(NcVariable(zs,'Hs', ncdatatype, ('Hs',),'m'))
        varList.append(NcVariable(Z,'Zr', ncdatatype, ('sensor',),'m'))
        varList.append(NcVariable(beamf,'beamf', ncdatatype, ('xgrid','ygrid','Hs','speed','freq'),0))
        varList.append(NcVariable(beam,'beam', ncdatatype, ('xgrid','ygrid','Hs','speed'),0))
        varList.append(NcVariable(beamstd,'beamstd', ncdatatype, ('xgrid','ygrid','Hs','speed'),0))
    if svd:
	varList.append(NcVariable(SSVD,'SSVD', ncdatatype, ('sensor','freq'),0))
    ncwrite(directory,filename,dimList,varList,SVNrevision)
t1=datetime.datetime.today()
twrite=(t1-t0).total_seconds()
print('  [Done]')
print('            elapsed time (sec): {:f}'.format(twrite))

tend = datetime.datetime.today()
ttot = (tend-tbegin).total_seconds()

#########################################################################################
# Profiling: write timing.dat

if ln_prof:
    writeTiming(processus,pid,tread,tinit,tbeam,nbit,twrite,tplot,ttot)

#########################################################################################

print('shape(beam): ',np.shape(beam))
print('shape(beamf): ',np.shape(beamf))
print('sum(beam): ',np.sum(beam))
print('max(beam): ',np.max(beam))
print('min(beam): ',np.min(beam))
