#!/bin/bash

#  =======================================================================================
#                       ***  SHELL wrapper.ksh  ***
#   This is a wrapper to compute NBTASKS beamforming in parallel (parallelisation in time)
#    
#  =======================================================================================
#   History :  1.0  : 08/2013  : A. Lecointre : Initial version
#	        
#	describe here	    
#	describe here
#	describe here	 
#  ---------------------------------------------------------------------------------------
#
#  ---------------------------------------------------------------------------------------
#   routines      : description
#                 : 
#                 : 
#                 : 
#                 :
#  ---------------------------------------------------------------------------------------
#  $Id: wrapper.ksh 100 2016-11-17 15:50:16Z lecointre $
#  ---------------------------------------------------------------------------------------
#
#
# TODO
# - calcul automatique du nombre de tâches = durée totale / durée step
# - activer bash completion pour ce script : voir example dans /etc/bash_completion.d/...
# - float32 avant plot ? : pas utile en temps cpu ... comprendre pourquoi ?
# - SVD on CSDM : inverse csdm apres svd ? / voir methodes distinctes (toutes deux codees dans function_def.py)
# - add the option for output saving: beam for all frequencies or only mean and std of beam : OK
# - try to execute the code on university cluster (CIMENT) as grid jobs
# - test NetCDF output , and estimate how it is easy (or not) to read and manipulate NetCDF output with Matlab, etc... http://docs.scipy.org/doc/scipy/reference/generated/scipy.io.netcdf.netcdf_file.html#scipy.io.netcdf.netcdf_file : OK fonctionnel mais reste a harmoniser le code
# - repere un bug dans distlatlong de D. Zigone : intervertion A et B dans calcul de distance XX : corrigé : les diff sur le beam sont ~1e5% car on est dans un cas ou les stations sont tres proches, le bug aurait eu plus d'impact si station éloignées.
#
#
# Pour les tests de performance, penser à désactiver les services consommateurs de ressources
# (cron, atd, ...)
# Liste des services actifs :
# ---
# initctl list | grep -i running | sed 's/ *start\/running//' | sort
# ---
# Ou en utilisant la completion par bash-completion :
# ---
# sudo initctl stop + [touche tab]
# ---

set -e

##################################################################
# DO NOT CHANGE AFTER THIS LINE

getParam() {
   pair=$(grep "^ *$1 *= *" beam.cfg | sed 's/;.*$//')
   if [[ -z "${pair}" ]] ; then
      echo 0
   else
      echo $(echo ${pair} | awk -F = '{ print $2 }' | awk '{ print $1 }')
   fi
}

getParamSpecSection() {
   # Get a parameter in beam.cfg into a specific section
   n=$(cat beam.cfg | grep -n "^\[$2\]" | awk -F: '{print $1}')
   cat beam.cfg | tail -n +$n > zbeam.cfg
   pair=$(grep -m 1 "^ *$1 *= *" zbeam.cfg | sed 's/;.*$//')
   rm zbeam.cfg
   if [[ -z "${pair}" ]] ; then
      echo 0
   else
      echo $(echo ${pair} | awk -F = '{ print $2 }' | awk '{ print $1 }')
   fi
}

cptdim(){
	  if [[ $# == 1 ]] ; then 
             mini=min ; maxi=max ; pas=step
          else
             mini=$2 ; maxi=$3 ; pas=$4
	  fi
          min=$(getParamSpecSection $mini $1)
	  max=$(getParamSpecSection $maxi $1)
	  stp=$(getParamSpecSection $pas $1)
	  dim=$(echo "scale=3;$dim*(1+($max-($min))/$stp)" | bc)
}

incrementTag() {
	# fonction pour incrémenter une date de la forme HH:MM:SS connaissant un delai (en S) pourvant etre positif ou negatif
        Tbegin=$1
	AAAA=$(echo $Tbegin | awk -FT '{print $1}' | sed -e "s/\"//g")
	BBBB=$(echo $Tbegin | awk -FT '{print $2}' | sed -e "s/\"//g")
	delay=$2
	dateEpoch=$(date --date "${AAAA} ${BBBB}" +"%s")
	newDateEpoch=$((dateEpoch+delay))
	Tend=$(date --date "@$newDateEpoch" +"%Y-%m-%dT%T")
	echo '"'${Tend}'"'
}

set +x

# recuperer le PID de wrapper.ksh
PID=$(echo $$)
echo $PID

typeset -li NBPROCS=$(getParam nbProcs)
if [[ ${NBPROCS} == 0 ]] ; then
   # guess the number of cores
   NBPROCS=$(grep "cpu cores" /proc/cpuinfo | head -1 | awk '{print $NF}')
fi

Tinit=$(getParam Tinit)
step=$(getParam step)
overlap=$(getParam overlap)
NBTASKS=$(getParam nbTasks)
MACHINE=$(hostname)
oar=$(getParam oar)

if [[ $MACHINE == 'froggy1' || $MACHINE == 'froggy2' ]] ; then
    . /applis/ciment/v2/env.bash
    module load python/2.7.2_gcc-4.6.2
fi

# compile all python files
python2.7 -OO -m compileall -l . || exit 1

cat << eof > Makefile
# this Makefile has been automatically created

eof

ME=$(whoami)

# Give some informations about needed resources (computing + storage). 
typeset -li ln_save=$(getParam ln_save)
typeset -li ln_float32=$(getParam ln_float32)
if [[ ${ln_save} != 0 ]] ; then
   typeset -li ln_saveallfrequencies=$(getParam ln_saveallfrequencies)
   dim=1
   if [[ ${ln_saveallfrequencies} == -1 ]] ; then
      dim=$(echo "scale=3;$dim+1" | bc )
   elif [[ ${ln_saveallfrequencies} == 1 ]] ; then
      cptdim indice_freq
   elif [[ ${ln_saveallfrequencies} == 0 ]] ; then
      cptdim indice_freq
      dim=$(echo "scale=3;$dim+2" | bc )
   fi
   cptdim speed
   typeset -li azimuth=$(getParam azimuth)
   if [[ ${azimuth} == 1 ]] ; then
      cptdim theta
   elif [[ ${azimuth} == 0 ]] ; then
      cptdim xy xmin xmax xstep
      cptdim xy ymin ymax ystep
      cptdim Hsource
   fi
fi
if [[ ${ln_float32} == 0 ]] ; then
	OutputSize=$(echo "scale=9;$dim*8/(1024*1024)" | bc)   # en Moctet
elif [[ ${ln_float32} == 1 ]] ; then
	OutputSize=$(echo "scale=9;$dim*4/(1024*1024)" | bc)   # en Moctet
fi
TotOutputSize=$(echo "scale=9;$OutputSize*$NBTASKS" | bc)
echo "---------------------------------------"
echo "WARNING: Running $NBTASKS processes ..."
echo "         Each processus is writing $OutputSize MBytes"
echo "         Total Output Size: $TotOutputSize MBytes"
echo "---------------------------------------"

# Create as many beam_proc${PPPP}.cfg as number of processes you want to launch
echo -n 'all:' >> Makefile
Tb='"'${Tinit}'"'
echo "----------------------------"
for process in $(seq 1 $NBTASKS); do
   echo "Processus: " $process
   PPPP=$( printf "%04d" $process )
   Te=$(incrementTag $Tb $step)
   echo "Tb $Tb"
   echo "Te $Te"
   Tprocess=$((step+(process-1)*(step-overlap)))
   echo "Time in sec: " $Tprocess
   echo "----------------------------"
   cat beam.cfg | sed -e "s/<<T1>>/$Tb/" | sed -e "s/<<T2>>/$Te/" | sed -e "s/<<Tprocess>>/$Tprocess/" | sed -e "s/<<USERNAME>>/$ME/" > beam_proc${PPPP}.cfg
   echo -n ' beam_proc'${PPPP}'.txt' >> Makefile
   Tb=$(incrementTag $Te -$overlap)
done

# Create directory for outputs
RESEAU=$(getParam reseau)
YEAR=$(echo $Tinit | awk -F- '{print $1}')
MONTH=$(echo $Tinit | awk -F- '{print $2}')
ODIR=$(getParam ODIR | sed -e "s/<<USERNAME>>/$ME/")

# Create Makefile dependencies
cat << eof >> Makefile


eof

# does the user want to use C and Fortran optimizations ?
typeset -li ln_code=$(getParam ln_code)

# C optimization
if [[ ${ln_code} == 2 ]]; then
   # Compile beamformingc.c
#-L \${NPY_LIBS}/fft -l:fftpack_lite.so \
#-L \${NPY_LIBS}/lib -l:_compiled_base.so \
#-L \${NPY_LIBS}/linalg -l:lapack_lite.so \
#-L \${NPY_LIBS}/numarray -l:_capi.so \
#-L \${NPY_LIBS}/random -l:mtrand.so
   NUMPY_LIBRARIES_DIR=/usr/lib/python2.7/dist-packages/numpy
   cat << eof >> Makefile
NPY_LIBS=${NUMPY_LIBRARIES_DIR}
LIBS=-L \${NPY_LIBS}/core -l:_dotblas.so -l:multiarray.so -l:multiarray_tests.so -l:scalarmath.so -l:_sort.so -l:umath.so -l:umath_tests.so \
-Wl,-rpath=\${NPY_LIBS}/core

beamformingc.so : beamformingc.c
	gcc -pipe -shared -fPIC -O2 -std=c99 -I /usr/include/python2.7 -o beamformingc.so beamformingc.c \${LIBS}

eof
fi
# Fortran f2py optimization
if [[ ${ln_code} == 1 ]] ; then
   # Compile computebeam.f90 with f2py
   cat << eof >> Makefile
beamfortran.so : computebeam.f90
	f2py -c $< -m beamfortran --fcompiler=gnu95 --opt="-O2" # intelem ? # --f90flags=-O3

eof
fi
echo -n "%.txt : %.cfg" >> Makefile
if [[ ${ln_code} == 1 ]] ; then
   echo -n " beamfortran.so" >> Makefile
fi
if [[ ${ln_code} == 2 ]]; then
   echo -n " beamformingc.so" >> Makefile
fi
echo >> Makefile
#if [[ ${ln_code} == 2 ]]; then
#   echo "	export LD_LIBRARY_PATH=\${NPY_LIBS}/core" >> Makefile
#fi
cat << eof >> Makefile
	python2.7 -OOu ./main_beamforming.pyo -i $< > TXT/\$@
	rm $<

clean : 
	rm -f TXT/beam_proc*.txt
eof

# Avoid unsynchronized timestamps
# see http://www.open-mpi.org/faq/?category=building#unsynchronized-timestamps for more details
sleep 1

if [[ $oar == 1 ]] ; then
    sed -e "s/<<NBPROCS>>/$NBPROCS/" -e "s/<<NBTASKS>>/$NBTASKS/" submit_beam_oar.sh.skel > submit_beam_oar.sh
    chmod +x submit_beam_oar.sh
    echo "Now you have to submit job with the following command:"
    echo "oarsub -S ./submit_beam_oar.sh"
else
    ODIR="$ODIR/$RESEAU/$YEAR/"  # tout en vrac dans un dossier year pour eviter pb de chgt de mois $MONTH/"
    mkdir -p $ODIR/CFG
    mkdir TXT
    make clean
    #nice make -j ${NBPROCS}
    time make -j ${NBPROCS}
    for process in $(seq 1 $NBTASKS); do
        PPPP=$( printf "%04d" $process )
        if [ -f timing_${PPPP}.dat ] ; then
            cat timing_${PPPP}.dat >> timing.dat
            rm timing_${PPPP}.dat
        fi
        if [ -f timing.dat ] ; then mv timing.dat $ODIR/CFG/timing_PID${PID}.dat ; fi
    done
    cp beam.cfg $ODIR/CFG/beam_PID${PID}.cfg
fi

