A tool for re-organizing 10-min correlations daily datasets into one single several days datasets.

Example with ZO Argentiere dataset, during the 33days period 2018115 - 2018147 (complete graph for computing clock differences as in iworms projects)

For each pair of sensors:

33 daily [144, 2001] datasets -> 1 33-day [4752, 2001] dataset

```
> source /applis/site/guix-start.sh
> refresh_guix corrcat
> guix package -m manifest_corrcat.scm -p $GUIX_USER_PROFILE_DIR/corrcat 
> refresh_guix corrcat
Activate profile  /var/guix/profiles/per-user/lecoinal/corrcat
The following packages are currently installed in /var/guix/profiles/per-user/lecoinal/corrcat:
hdf5       	1.12.1	out	/gnu/store/1rjs998na0xd57ach5w0432z615p4j5b-hdf5-1.12.1
python-h5py	3.6.0 	out	/gnu/store/68nggkk7rxkmxn8k6iadh3fiqyb9cqvs-python-h5py-3.6.0
python     	3.9.9 	out	/gnu/store/sz7lkmic6qrhfblrhaqaw0fgc4s9n5s3-python-3.9.9
>
```

Here we read and write again the data (data duplication ...). We should look at the HDF5 virtual dataset feature (since HDF5 1.10):
* HDF5 virtual datasets : https://support.hdfgroup.org/HDF5/docNewFeatures/NewFeaturesVirtualDatasetDocs.html
* h5py VDS : https://docs.h5py.org/en/stable/vds.html
