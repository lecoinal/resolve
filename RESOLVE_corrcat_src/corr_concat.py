#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
#    This file is part of CORRCAT : a Python code for reorganizing several    #
#    daily 10-min correlations dataset into one single dataset covering       #
#    several consecutive days.                                                #
#                                                                             #
#    Copyright (C) 2022 Albanne Lecointre                                     #
#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
###############################################################################

import numpy as np
import h5py
import argparse

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="time-concatenate correlations")
    parser.add_argument("-net", default="ZO", type=str, help="network name, default is ZO")
    parser.add_argument("-yr", default="2018", type=int, help="year, default is 2018")
    parser.add_argument("-j1", default="115", type=int, help="first jday, default is 115")
    parser.add_argument("-j2", default="147", type=int, help="last jday, default is 147")

    args = parser.parse_args()   

    h5fo = h5py.File("out.h5", "w")

# create the empty datasets : as we know that all days are complete (98 sensors for all days, including day 115)   
    jd = args.j1
    h5f = h5py.File(args.net+"."+str(args.yr)+str(jd)+".000000.h5", "r")
    for gp0 in h5f:
        print(gp0)
        st0 = gp0.split(".")[1]
        ch0 = gp0.split(".")[3]
        for gp1 in h5f[gp0]:
            st1 = gp1.split(".")[1]
            ch1 = gp1.split(".")[3]
            if gp0 != gp1:
                for dset in h5f[gp0+"/"+gp1]:
                    dsetname = st0+"_"+st1+"_"+ch0+"_"+ch1+"_"+dset
                    #h5fo.create_dataset(dsetname, (144*33,2001), dtype='f4', compression="gzip", compression_opts=6)
                    h5fo.create_dataset(dsetname, (144*33,2001), dtype='f4')
    h5f.close()

# re-organize datasets
    ii = 0
    for jd in np.arange(args.j1,args.j2+1): 
        h5f = h5py.File(args.net+"."+str(args.yr)+str(jd)+".000000.h5", "r")
        ii+=1
        for gp0 in h5f:
            st0 = gp0.split(".")[1]
            ch0 = gp0.split(".")[3]
            for gp1 in h5f[gp0]:
                st1 = gp1.split(".")[1]
                ch1 = gp1.split(".")[3]
                if gp0 != gp1:    # do not take the autocorr
                    for dset in h5f[gp0+"/"+gp1]:
                        print(gp0+"/"+gp1+"/"+dset)
                        h5fo[st0+"_"+st1+"_"+ch0+"_"+ch1+"_"+dset][144*(ii-1):144*ii,:] = h5f[gp0+"/"+gp1+"/"+dset][:,:]
        h5f.close()
    h5fo.close()
