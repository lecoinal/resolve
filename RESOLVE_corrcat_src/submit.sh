#!/bin/bash
#OAR --project resolve
#OAR -l /nodes=1/cpu=1/core=1,walltime=15:00:00
#OAR -n corrcat

set -e

# CIMENT GRID Clusters
source /applis/site/guix-start.sh
refresh_guix corrcat


date

START=$(date +%s.%N)

cat $OAR_NODE_FILE

EXP=$1 # "EXP020" "EXP021"
YEAR=2018
NETWORK="ZO"
JD1=115
JD2=147



case "$CLUSTER_NAME" in
  luke)
    IDIR="$SHARED_SCRATCH_DIR/$USER/"
    ;;
  *)
    echo "CLUSTER_NAME: $CLUSTER_NAME not supported. Abort"
    exit 1
    ;;
esac

#####################################################

INPUT_DIR="/$IDIR/RESOLVE/CORRELATIONS/$EXP/10MIN"

###########################################################

PEXE=corr_concat.py

###########################################################

TMPDIR="/$SHARED_SCRATCH_DIR/$USER/corrcat_${OAR_JOB_ID}"
echo $TMPDIR
mkdir -p $TMPDIR

cp $PEXE $TMPDIR

cd $TMPDIR

###########################
## Get data

for jd in $(seq $JD1 $JD2) ; do
  ln -sf $INPUT_DIR/${NETWORK}.${YEAR}${jd}.000000.h5 .
done

/usr/bin/time -v python3 -u $PEXE -net $NETWORK -yr $YEAR -j1 $JD1 -j2 $JD2

h5repack -v -f GZIP=6 out.h5 outgz.h5
#rm out.h5

sync

date
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"
