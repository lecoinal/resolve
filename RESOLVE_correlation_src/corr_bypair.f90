!###############################################################################
!#    This file is part of CORR : a Fortran code for computing correlations    #
!#    among all pairs of STATIONS and CHANNELS from one single input file.     #
!#                                                                             #
!#    Copyright (C) 2016 Albanne Lecointre                                     #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################


!#---------------------------------
!$Id: corr_bypair.f90 196 2016-12-21 11:02:52Z lecointre $
!#---------------------------------

#ifdef lk_float
#  define MYKIND C_FLOAT
#  define MYKINDCPLX C_FLOAT_COMPLEX
#  define ZERO 0.0
#  define UN 1.0
#else
#  define MYKIND C_DOUBLE
#  define MYKINDCPLX C_DOUBLE_COMPLEX
#  define ZERO 0.d0
#  define UN 1.d0
#endif


PROGRAM CORR_BYPAIR

  USE HDF5 ! HDF5 module

  USE, intrinsic :: iso_c_binding

  IMPLICIT NONE

  include 'fftw3.f03'

  LOGICAL :: l_exist_namelist

  ! Output management

  ! Input parameters
  INTEGER :: nn_samprate   ! Input dataset frequency
  INTEGER :: nn_unitseg    ! length in sec of a unitary segment 1200 sec (24000 points at 20Hz)
  INTEGER :: nn_nbunitseg   ! number of unitary segments in one hour (5) 
  INTEGER :: nn_lag        ! the lag in sec for the correlation
  INTEGER :: nn_nextpow2   ! npad
  INTEGER :: maxlag      ! in nb of samples (5min*60*sampling rate) : 6000

  INTEGER :: npc         ! = 2*maxlag+1 : total nb of points for the output correlation : 12001
  INTEGER :: nph         ! the total number of samples in 20min segment

  CHARACTER(LEN=128) :: cn_filein
!  CHARACTER(LEN=11) :: cld
!  CHARACTER(LEN=7) :: clc
  CHARACTER(LEN=128), DIMENSION(:), ALLOCATABLE :: cldname
  CHARACTER(LEN=128) :: cd1,cd2
!  CHARACTER(LEN=7), DIMENSION(:), ALLOCATABLE :: clchname
  CHARACTER(LEN=128), DIMENSION(:,:), ALLOCATABLE :: clstname
  CHARACTER(LEN=128) :: cbuf
  CHARACTER(LEN=128), DIMENSION(2) :: cbuf2
  CHARACTER(LEN=2) :: chour,cmin

  ! Manipulation des fichiers et des datasets HDF5
  ! Input
  INTEGER(HID_T) :: infile_id      ! Input File identifier: this HDF5 file contains all the 1H (=5 x 20min) traces
  INTEGER(HID_T) :: dset_id        ! Input and Output Dataset identifiers (released immediately)
  INTEGER(HID_T) :: space_id       ! Input and Output Dataset's dataspace identifier (released immediately)
  INTEGER(HID_T) :: memspace_id    ! Input and Output Dataset's memspace identifier in the memory (released immediately)
  INTEGER(HSIZE_T), DIMENSION(1) :: indims     ! The hyperslab (memory) current dimension for input dataset : 20min at 20Hz = 24000
  INTEGER(HSIZE_T), DIMENSION(1) :: startin    ! The starting point to read 20min input dataset from input file 
  INTEGER(HSIZE_T), DIMENSION(1) :: countin    ! Into 2Ddataset from HDF5 file: nb of points when read 20min input data from input file : (/1,24000/)
  INTEGER(HSIZE_T), DIMENSION(1) :: memcountin ! Into memory: nb of points when read 20min input data from input file : (/24000/)
  INTEGER(HSIZE_T), DIMENSION(1), PARAMETER :: memstartin = (/0/) ! Input Dataset hyperslab memory starting point : always (/0/)
  ! Output
  INTEGER(HID_T) :: outfile_id     ! Output File identifier: the HDF5 File containing all the correlations for all the pairs
!  INTEGER(HSIZE_T), DIMENSION(2) :: dimsfi ! The total current dimension for output dataset
  INTEGER(HSIZE_T), DIMENSION(2) :: outdims ! The total current dimension for output dataset
  INTEGER(HID_T) :: grpsta_id
  INTEGER(HID_T) :: grpchan_id

  INTEGER(HSIZE_T), DIMENSION(2) :: count  
  INTEGER(HSSIZE_T), DIMENSION(2) :: offset 

  ! Iterators
  INTEGER   :: jglomin       ! iterator on the global min (from 0 to 1440)
  INTEGER   :: js            ! iterator on the 20min unitary segment
  INTEGER   :: jk,jtr1,jtr2  ! iterator on the station traces
  INTEGER   :: it            ! iterator on the time

  INTEGER   :: nn_glomin_beg,nn_glomin_end,nn_glomin_stp

  INTEGER   :: error ! Error flag

  ! Instrumentation du code pour analyse de performance
  INTEGER,DIMENSION(8) :: values
  REAL(C_FLOAT) :: elps           ! in sec

  INTEGER :: obj_type            ! Type of the object
  INTEGER :: nmembers            ! Number of group members
  INTEGER :: nbpair
  INTEGER :: npad                              ! =30000 ! 32768  ! 2^15 (nextpow2(dishape+maxlag=24000+6000))

  ! Buffers for input and output datasets
#ifdef lk_1bit
  INTEGER, DIMENSION(:), ALLOCATABLE :: ntr            ! nph=24000
#else
  REAL(C_FLOAT), DIMENSION(:), ALLOCATABLE :: ntr            ! nph=24000
#endif
  COMPLEX(MYKINDCPLX), pointer :: yout(:)
  type(C_PTR) :: pyout
  REAL(MYKIND), DIMENSION(:,:), ALLOCATABLE :: dcorr    ! Output correlation (when manyplan=F)
  REAL(MYKIND), pointer :: din(:)                    ! final result of IFFT (when manyplan=F)
  COMPLEX(MYKINDCPLX), pointer :: youtpair(:)
  COMPLEX(MYKINDCPLX), DIMENSION(:), ALLOCATABLE :: yout15 
  type(C_PTR) :: pyoutpair
  type(C_PTR) :: pdin
  REAL(MYKIND), pointer :: dinpad(:)  ! one station traces but only one 20min chunk, padded with zeros before or after
  type(C_PTR) :: pdinpad

  INTEGER(KIND=8) :: plan_forward,plan_backward

  ! Use the advanced interface for FFTW (plan_many)
  INTEGER :: rank
  INTEGER, DIMENSION(1) :: n_array
 
  ! Use the multithreaded FFTW
  INTEGER :: iret
  INTEGER :: nn_nbthread

  INTEGER :: nn_scaleoffset
  INTEGER(HID_T)  :: dcpl ! Handles
  INTEGER(HSIZE_T), DIMENSION(2)   :: chunk

  INTEGER :: nn_io
  REAL(MYKIND), DIMENSION(:,:,:), ALLOCATABLE :: dcorrdump

  ! Declare NAMELIST
  NAMELIST /namthread/ nn_nbthread
  NAMELIST /namio/ nn_scaleoffset,nn_io 
  NAMELIST /namparam/ cn_filein,nn_samprate,nn_lag,nn_unitseg,nn_nbunitseg,nn_nextpow2
  NAMELIST /namtime/ nn_glomin_beg,nn_glomin_end,nn_glomin_stp

  ! Read namelist
  INQUIRE(file='namelist', exist=l_exist_namelist)
  IF (.NOT. l_exist_namelist) THEN
     PRINT *,'The namelist does not exist'
     STOP
  ENDIF
  OPEN(11,file='namelist')
  PRINT *,'Reading namelist namthread'
    REWIND 11
    READ(11,namthread)
    PRINT *,'nn_nbthread = ',nn_nbthread
  PRINT *,'Reading namelist namio'
    REWIND 11
    READ(11,namio)
    PRINT *,'nn_io NOT USED as bypair code'
    PRINT *,'nn_scaleoffset = ',nn_scaleoffset
  PRINT *,'Reading namelist namparam'
    REWIND 11
    READ(11,namparam)
    PRINT *,'cn_filein: ',TRIM(cn_filein)
    PRINT *,'nn_samprate: ',nn_samprate,' Hz'
    PRINT *,'nn_unitseg: ',nn_unitseg,' sec'
    PRINT *,'nn_nbunitseg: ',nn_nbunitseg,' unitary segment(s)'
    PRINT *,'nn_lag: ',nn_lag,' sec'
    PRINT *,'nn_nextpow2: ',nn_nextpow2
  PRINT *,'Reading namelist namtime'
    REWIND 11
    READ(11,namtime)
    PRINT *,'nn_glomin_beg: ',nn_glomin_beg
    PRINT *,'nn_glomin_end: ',nn_glomin_end
    PRINT *,'nn_glomin_stp: ',nn_glomin_stp
  CLOSE(11)

  ! Compute some useful parameters
  maxlag = nn_lag*nn_samprate  ! as a number of samples : 200
  npc = 2*maxlag+1             ! final length of the correlation vector : 401
  nph = nn_unitseg*nn_samprate ! length of a unitary 10min segment : 60000
  IF ( nn_nextpow2 == 0 ) THEN
    npad = nph + maxlag          ! 60200 ( or 65536 = 2^16 (nextpow2(nph+maxlag)) FFTW3.3.4 "Arbitrary-size transforms" )
  ELSE
    npad = nn_nextpow2
  ENDIF
!  dimsfi = (/npc,nn_io/)                      ! 401,4
  outdims = (/npc,86400/(nn_unitseg*nn_nbunitseg)/)    ! 86400/600=144  86400/(600*6)=24
  indims = (/nph/)                      ! 60000
  memcountin = (/nph/)                  ! 10min a 100Hz = 60000 pts
  countin = (/nph/)                     ! 60000
!  count = (/npc,nn_io/)                     ! 401,1

  ! Read one time the entire file to get the total number of traces
  CALL h5open_f(error)
  if (error/=0) call exit(error)
  CALL h5fopen_f(TRIM(cn_filein), H5F_ACC_RDONLY_F, infile_id, error)
  if (error/=0) call exit(error)
  CALL h5gn_members_f(infile_id, "/", nmembers, error)
  if (error/=0) call exit(error)
  nbpair=nmembers*(nmembers+1)/2

print *,'nbpair',nbpair

  ! In and Out for the FFT forward (advanced interface: manyplan)
#ifdef lk_float
  pdinpad = fftwf_alloc_real(int(npad, C_SIZE_T))
#else
  pdinpad = fftw_alloc_real(int(npad, C_SIZE_T))
#endif
  call c_f_pointer(pdinpad, dinpad, [npad])
#ifdef lk_float
  pyout = fftwf_alloc_complex(int((npad/2+1), C_SIZE_T))
#else
  pyout = fftw_alloc_complex(int((npad/2+1), C_SIZE_T))
#endif
  call c_f_pointer(pyout, yout, [npad/2+1])

#ifdef lk_threads
  ! Multithreaded FFTW
  if ( nn_nbthread /= 0 ) then
#ifdef lk_float
    call sfftw_init_threads(iret)
    call sfftw_plan_with_nthreads(nn_nbthread)
#else
    call dfftw_init_threads(iret)
    call dfftw_plan_with_nthreads(nn_nbthread)
#endif
    print *,'iret:',iret
  endif
#endif


  ! Create the 2 plans for FFT forward and FFT backward
  CALL date_and_time(VALUES=values)
  elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)

  rank = 1
  n_array(1) = npad
  dinpad(:)=ZERO
  yout(:)=(ZERO,ZERO)
#ifdef lk_float
  CALL sfftw_plan_dft_r2c(plan_forward, rank, n_array, &
                                 dinpad, &
                                 yout, &
                                 FFTW_ESTIMATE)

  pyoutpair = fftwf_alloc_complex(int((npad/2+1), C_SIZE_T))
  call c_f_pointer(pyoutpair, youtpair, [npad/2+1])
  pdin = fftwf_alloc_real(int(npad, C_SIZE_T))
  call c_f_pointer(pdin, din, [npad])
  youtpair(:)=(ZERO,ZERO)
  CALL sfftw_plan_dft_c2r(plan_backward, rank, n_array, &
                              youtpair, &
                              din, &
                              FFTW_ESTIMATE)
#else
  CALL dfftw_plan_dft_r2c(plan_forward, rank, n_array, &
                                 dinpad, &
                                 yout, &
                                 FFTW_ESTIMATE)

  pyoutpair = fftw_alloc_complex(int((npad/2+1), C_SIZE_T))
  call c_f_pointer(pyoutpair, youtpair, [npad/2+1])
  pdin = fftw_alloc_real(int(npad, C_SIZE_T))
  call c_f_pointer(pdin, din, [npad])
  youtpair(:)=(ZERO,ZERO)
  CALL dfftw_plan_dft_c2r(plan_backward, rank, n_array, &
                              youtpair, &
                              din, &
                              FFTW_ESTIMATE)
#endif

  CALL date_and_time(VALUES=values)
  elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)-elps
  print *,'Init plans:',elps,'sec'

  ! Allocate array
  ALLOCATE(ntr(nph))
  ALLOCATE(cldname(nmembers)) 
!  ALLOCATE(clchname(nbpair),clstname(2,nbpair))
  ALLOCATE(clstname(2,nbpair))

  ALLOCATE(yout15(npad/2+1))

  ! Browse the input file to know the station and channel names
  DO jtr1 = 1, nmembers ! Loop on station traces
    CALL h5gget_obj_info_idx_f(infile_id, "/", jtr1-1, cbuf, obj_type, error)
    if (error/=0) call exit(error)
    cldname(jtr1)=TRIM(cbuf)
print *,'cldname',jtr1,TRIM(cldname(jtr1))
  ENDDO
  jk=0
  DO jtr1=1,nmembers
    cd1=TRIM(cldname(jtr1))
    DO jtr2=jtr1,nmembers
      jk=jk+1
      cd2=TRIM(cldname(jtr2))
      CALL sort_string(cd1,cd2,cbuf2)
      clstname(1,jk)=TRIM(cbuf2(1))
      clstname(2,jk)=TRIM(cbuf2(2))
print *,jk,TRIM(clstname(1,jk)),TRIM(clstname(2,jk))
    ENDDO
  ENDDO

! Find the total number of time iterations do allocate dcorr
  it=0
  ! Loop on global mins
  DO jglomin=nn_glomin_beg, nn_glomin_end, nn_glomin_stp
    it=it+1
  ENDDO
  ALLOCATE(dcorr(npc,it))
  chunk = (/npc,it/)

  ! Create output file
  CALL h5fcreate_f('c.h5', H5F_ACC_TRUNC_F, outfile_id, error)
  if (error/=0) call exit(error)

  ! General loop over the pairs of stations
  DO jk=1,nbpair
    CALL date_and_time(VALUES=values)
    elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)
!    cld=clstname(jk)
!    clc=clchname(jk)

    it=0
    ! Loop on global mins
    DO jglomin=nn_glomin_beg, nn_glomin_end,nn_glomin_stp

      write(chour,'(I0.2)') (jglomin-MOD(jglomin,60))/60
      write(cmin,'(I0.2)') MOD(jglomin,60)

      it=it+1
      youtpair(:)=(ZERO,ZERO)

      ! Loop on the 5 x 20min segments
      DO js=1, nn_nbunitseg

        ! Read the first trace
        !CALL h5dopen_f(infile_id, "/"//TRIM(cld(1:5))//"_"//TRIM(clc(1:3)), dset_id, error)
        CALL h5dopen_f(infile_id, "/"//TRIM(clstname(1,jk)), dset_id, error)
        if (error/=0) call exit(error)
        CALL h5dget_space_f(dset_id, space_id, error)
        if (error/=0) call exit(error)
        startin = (/(jglomin)*60*nn_samprate+(js-1)*nn_unitseg*nn_samprate/)
        CALL h5sselect_hyperslab_f (space_id, H5S_SELECT_SET_F, startin, countin, error)
        if (error/=0) call exit(error)
        CALL h5screate_simple_f(1, memcountin, memspace_id, error)
        if (error/=0) call exit(error)
        CALL h5sselect_hyperslab_f(memspace_id, H5S_SELECT_SET_F, memstartin, memcountin, error)
        if (error/=0) call exit(error)
#ifdef lk_1bit
        CALL H5dread_f(dset_id, H5T_NATIVE_INTEGER, ntr, indims, error, memspace_id, space_id)
#else
        CALL H5dread_f(dset_id, H5T_NATIVE_REAL, ntr, indims, error, memspace_id, space_id)
#endif
        if (error/=0) call exit(error)
        CALL h5sclose_f(memspace_id, error)
        if (error/=0) call exit(error)
        CALL h5sclose_f(space_id, error)
        if (error/=0) call exit(error)
        CALL h5dclose_f(dset_id, error)
        if (error/=0) call exit(error)
  
        ! Compute the FFT forward first when padding with zero after, and take the conjugate on the first trace
        dinpad(nph+1:npad)=ZERO
#ifdef lk_float
        dinpad(1:nph) = REAL(ntr(:))/SQRT(SUM((REAL(ntr(:))**2)))
        CALL sfftw_execute_dft_r2c(plan_forward, dinpad, yout)
        yout15(:) = conjg(yout(:))
#else
        dinpad(1:nph) = DBLE(ntr(:))/SQRT(SUM((DBLE(ntr(:))**2)))
        CALL dfftw_execute_dft_r2c(plan_forward, dinpad, yout)
        yout15(:) = dconjg(yout(:))
#endif
  
        ! Read the second trace
        !CALL h5dopen_f(infile_id, "/"//TRIM(cld(7:11))//"_"//TRIM(clc(5:7)), dset_id, error)
        CALL h5dopen_f(infile_id, "/"//TRIM(clstname(2,jk)), dset_id, error)
        if (error/=0) call exit(error)
        CALL h5dget_space_f(dset_id, space_id, error)
        if (error/=0) call exit(error)
        CALL h5sselect_hyperslab_f (space_id, H5S_SELECT_SET_F, startin, countin, error)
        if (error/=0) call exit(error)
        CALL h5screate_simple_f(1, memcountin, memspace_id, error)
        if (error/=0) call exit(error)
        CALL h5sselect_hyperslab_f(memspace_id, H5S_SELECT_SET_F, memstartin, memcountin, error)
        if (error/=0) call exit(error)
#ifdef lk_1bit
        CALL H5dread_f(dset_id, H5T_NATIVE_INTEGER, ntr, indims, error, memspace_id, space_id)
#else
        CALL H5dread_f(dset_id, H5T_NATIVE_REAL, ntr, indims, error, memspace_id, space_id)
#endif
        if (error/=0) call exit(error)
        CALL h5sclose_f(memspace_id, error)
        if (error/=0) call exit(error)
        CALL h5sclose_f(space_id, error)
        if (error/=0) call exit(error)
        CALL h5dclose_f(dset_id, error)
        if (error/=0) call exit(error)
    
        ! Compute again the FFT forward when padding with zero before, on the second trace
        dinpad(1:maxlag)=ZERO
        dinpad(maxlag+nph+1:npad)=ZERO
#ifdef lk_float
        dinpad(maxlag+1:maxlag+nph) = REAL(ntr(:))/SQRT(SUM((REAL(ntr(:))**2)))
        CALL sfftw_execute_dft_r2c(plan_forward, dinpad, yout)
#else
        dinpad(maxlag+1:maxlag+nph) = DBLE(ntr(:))/SQRT(SUM((DBLE(ntr(:))**2)))
        CALL dfftw_execute_dft_r2c(plan_forward, dinpad, yout)
#endif

        ! Sum all the js product of FFT+1 before computing inverse FFT
        youtpair(:) = youtpair(:) + yout15(:)*yout(:)

      ENDDO  ! End loop on js
     
      ! Compute the FFTW-1 and then corr
#ifdef lk_float
      call sfftw_execute_dft_c2r(plan_backward,youtpair,din)
#else
      call dfftw_execute_dft_c2r(plan_backward,youtpair,din)
#endif
      dcorr(:,it) = (UN / nn_nbunitseg) * din(1:2*maxlag+1) / npad

    ENDDO ! End loop on global minute

    ! Write corr into HDF5 output file 
    ! create the group in outfile id for output dataset 
    CALL create_or_open_group(outfile_id, "/"//TRIM(clstname(1,jk)), grpsta_id )
    CALL create_or_open_group(grpsta_id,  TRIM(clstname(2,jk)), grpchan_id)
    CALL h5gclose_f( grpsta_id, error)
    if (error/=0) call exit(error)
    ! Create the data space for the 2D output dataset
    CALL h5screate_simple_f(2, outdims, space_id, error)
    if (error/=0) call exit(error)

    ! Create the dataset creation property list, and set the chunk size.
    CALL h5pcreate_f(H5P_DATASET_CREATE_F, dcpl, error)
    if (error/=0) call exit(error)
#ifdef lk_scaleoffset
    CALL h5pset_scaleoffset_f(dcpl, 0, nn_scaleoffset, error)
    if (error/=0) call exit(error)
#endif
    CALL h5pset_chunk_f(dcpl, 2, chunk, error)
    if (error/=0) call exit(error)

    ! Create the dataset and write the data in the dataset
    CALL h5dcreate_f(grpchan_id, 'corr', H5T_NATIVE_REAL, space_id, dset_id, error, dcpl)
    if (error/=0) call exit(error)
    CALL h5dwrite_f(dset_id, H5T_NATIVE_REAL, REAL(dcorr(:,:)), outdims, error)
    if (error/=0) call exit(error)
  ! Close and release resources.
    CALL h5pclose_f(dcpl, error)
    if (error/=0) call exit(error)
    CALL h5dclose_f(dset_id, error)
    if (error/=0) call exit(error)
    CALL h5sclose_f(space_id, error)
    if (error/=0) call exit(error)
    CALL h5gclose_f(grpchan_id, error)
    if (error/=0) call exit(error)

    CALL date_and_time(VALUES=values)
    elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)-elps
    PRINT '("Total elapsed time to compute ",I5," corr for pair ",I6," ",A26," ",A26," ( "I2" thr) : ",f15.6," s")', &
          it,jk,TRIM(clstname(1,jk)),TRIM(clstname(2,jk)),nn_nbthread,elps

  ENDDO ! End general loop on the pairs of stations

  ! Close the input daily file
  CALL h5fclose_f(infile_id, error)
  if (error/=0) call exit(error)

  ! Close the 1day output file
  CALL h5fclose_f(outfile_id, error)
  if (error/=0) call exit(error)

#ifdef lk_float
  call fftwf_free(pdinpad)
  call fftwf_free(pyout)
  call fftwf_free(pdin)
  call fftwf_free(pyoutpair)

  CALL sfftw_destroy_plan(plan_forward)
  CALL sfftw_destroy_plan(plan_backward)
#ifdef lk_threads
  if ( nn_nbthread /= 0 ) then
    CALL sfftw_cleanup_threads()
  endif
#endif
#else
  call fftw_free(pdinpad)
  call fftw_free(pyout)
  call fftw_free(pdin)
  call fftw_free(pyoutpair)

  CALL dfftw_destroy_plan(plan_forward)
  CALL dfftw_destroy_plan(plan_backward)
#ifdef lk_threads
  if ( nn_nbthread /= 0 ) then
    CALL dfftw_cleanup_threads()
  endif
#endif
#endif
  CALL h5close_f(error)
  if (error/=0) call exit(error)

END PROGRAM CORR_BYPAIR

SUBROUTINE create_or_open_group(parent_id,gr_name,gr_id)

  USE HDF5 ! HDF5 module

  IMPLICIT NONE

  INTEGER(HID_T), INTENT(in)     :: parent_id
  CHARACTER(LEN=*), INTENT(in)   :: gr_name
  INTEGER(HID_T), INTENT(out)    :: gr_id
  LOGICAL                        :: link_exists
  INTEGER                        :: error

  ! Output file : create group if necessary, or simply open it
  CALL h5lexists_f(parent_id, gr_name, link_exists, error, H5P_DEFAULT_F)
  IF ( link_exists ) THEN
    CALL h5gopen_f(parent_id, gr_name, gr_id, error)
  ELSE
    CALL h5gcreate_f(parent_id, gr_name, gr_id, error)
  ENDIF
  if (error/=0) call exit(error)

END SUBROUTINE create_or_open_group

SUBROUTINE sort_string(str1,str2,strsort)

  IMPLICIT NONE

  CHARACTER(LEN = *), INTENT(in)  :: str1,str2
  CHARACTER(LEN = *), DIMENSION(2),  INTENT(out) :: strsort

  IF ( LGT(str1,str2) ) THEN
    strsort(1)=TRIM(str2)
    strsort(2)=TRIM(str1)
  ELSE
    strsort(1)=TRIM(str1)
    strsort(2)=TRIM(str2)
  ENDIF

END SUBROUTINE sort_string
