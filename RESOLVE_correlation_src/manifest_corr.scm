(specifications->manifest
  '("gfortran-toolchain"
    ;; single precision FFT computation
    "fftwf"
    ;; double precision FFT computation
    ;;"fftw"
    "hdf5"
    "hdf5:fortran"
    "zlib"
    ))
