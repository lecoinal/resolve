#!/bin/bash
#OAR --project resolve
#OAR -l /nodes=1/cpu=1/core=1,walltime=48:00:00
#OAR -n resolve_corr_bypair

set -e
source /applis/site/guix-start.sh
# Suppose that GUIX profile "corr" was created with : 
# guix install -p $GUIX_USER_PROFILE_DIR/corr gfortran-toolchain fftwf hdf5 hdf5:fortran zlib 
# choose fftwf or fftw (single or double precision computation) 
# Or with a GUIX manifest file 
# guix package -m manifest_corr.scm -p $GUIX_USER_PROFILE_DIR/corr 
refresh_guix corr

cat $OAR_NODE_FILE
NBCORE=$(cat $OAR_NODE_FILE | wc -l)
echo "NBCORE : $NBCORE"

case "$CLUSTER_NAME" in
  luke)
    LDIR="/$SHARED_SCRATCH_DIR/$USER/"
    ;;
esac

CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=00000}  # substitute if not initialized

TMPDIR="/$LDIR/CID_$CIGRI_CAMPAIGN_ID/oar.$OAR_JOB_ID"

mkdir -p /$LDIR/$CIGRI_CAMPAIGN_ID

mkdir -p $TMPDIR

YEAR=2018 # $2 # 2014
JDAY=$1 # 150
EXP=$2 # "EXP021"

F90EXE=corr_bypair

cp $F90EXE $TMPDIR/.

#NN_IO=1  # if only one daily corr...
NN_IO=24  # dump les IO corr toutes les 4h (24*10min) ... not used in bypair code

sed -e "s/<<YEAR>>/$YEAR/" -e "s/<<JDAY>>/$JDAY/" \
    -e "s/<<NN_IO>>/$NN_IO/" namelist.skel > $TMPDIR/namelist

cd $TMPDIR

ln -sf $LDIR/RESOLVE/PREPROCESS/${EXP}/ZO_${YEAR}_${JDAY}.h5 . 

echo "CORRELATION: Correlations:                      $YEAR $JDAY      Date and Time: $(date +%F" "%T"."%N)"

$SHARED_SCRATCH_DIR/$USER/bin/time -f "\t%M Maximum resident set size (KB)"  \
    ./$F90EXE

#mv c.h5 ZO.${YEAR}${JDAY}.000000.h5
h5repack -v -f GZIP=6 c.h5 ZO.${YEAR}${JDAY}.000000.h5
mv $TMPDIR /$LDIR/$CIGRI_CAMPAIGN_ID/.

echo "CORRELATION: End job:                           $YEAR $JDAY      Date and Time: $(date +%F" "%T"."%N)"

