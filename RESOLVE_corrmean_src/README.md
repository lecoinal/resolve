# srcPython

Calcul des moyennes journalieres a partir de corr 10min.
 - OK avec la presence de NaN, dans ce cas la moyenne est calculee sur moins de 144 pas de temps. L'information du nombre de pas de temps NaN n'est pas conservee.
 - Si une correlation 10min contenait a la fois du NaN/Inf et des valeurs float, arret et sort un code d'erreur.

# srcFortran

Calcul de moyenne globale a partir de moyennes journalieres.
 - Ne gere pas les NaN, car il n'est pas cense y en avoir dans les moyennes journalieres.

```
> source /applis/site/guix-start.sh
> guix package -m manifest_corrmean.scm -p $GUIX_USER_PROFILE_DIR/corrmean
> refresh_guix corrmean
```
