!###############################################################################
!#    This file is part of CORR : a Fortran code for computing global mean     #
!#    among several daily correlation files.                                   #
!#                                                                             #
!#    Copyright (C) 2016 Albanne Lecointre                                     #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################



!  =======================================================================================
!                       ***  Fortran program corrglobalmean.f90  ***
!   To compute global mean starting from n correlation daily files
!    
!  =======================================================================================
!   History :  1.0  : 09/2015  : A. Lecointre : Initial Fortran version
!               
!  ---------------------------------------------------------------------------------------
!
!  ---------------------------------------------------------------------------------------
!   methodology   : use h5fmount_f to mount all averaged daily files into a single one merge.h5 file
!
!   https://ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Computing_daily_and_monthly_means
!
!  ---------------------------------------------------------------------------------------
!  $Id: corrglobalmean.f90 122 2015-10-02 11:37:40Z lecointre $
!  ---------------------------------------------------------------------------------------

PROGRAM CORRGLOBALMEAN

  USE HDF5 ! HDF5 module
  USE H5LT

  IMPLICIT NONE

  LOGICAL :: l_exist_namelist
  LOGICAL :: link1_exists
  LOGICAL :: link2_exists
  LOGICAL :: link3_exists

  ! Output management

  ! Input parameters
  INTEGER :: nn_samprate   ! Input dataset frequency
  INTEGER :: nn_lag        ! the lag in sec for the correlation
  INTEGER :: maxlag      ! in nb of samples (5min*60*sampling rate) : 6000

  INTEGER :: npc         ! = 2*maxlag+1 : total nb of points for the output correlation : 12001

  CHARACTER(LEN=128), PARAMETER :: cn_filein="merge.h5"
  CHARACTER(LEN=3) :: cjd

  REAL(KIND=4), DIMENSION(:), ALLOCATABLE :: buf
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: cm

  ! Manipulation des fichiers et des datasets HDF5
  ! Input
  INTEGER(HID_T) :: infile_id      ! Input File identifier: this HDF5 file contains all the 10min corr
  INTEGER(HSIZE_T), DIMENSION(1) :: indims
!  ! Output
  INTEGER(HID_T) :: outfile_id     ! Output File identifier: the HDF5 File containing all the correlations for all the pairs
  INTEGER(HID_T) :: grpsta_id
  INTEGER(HID_T) :: grpchan_id

  INTEGER :: ijd,jd,jday1,jday2,nsta

  INTEGER :: narg,iarg
  CHARACTER(len=256) :: cldum,clo
  INTEGER :: i,j
  CHARACTER(len=256), DIMENSION(:), ALLOCATABLE :: sta

  INTEGER   :: error ! Error flag


  INTEGER(HID_T), DIMENSION(:), ALLOCATABLE :: file2_id
  INTEGER(HID_T) :: gid ! Group identifier

  ! Declare NAMELIST
!  NAMELIST /namio/ nn_scaleoffset 
  NAMELIST /namparam/ nn_samprate,nn_lag

  narg = iargc()

  iarg = 1
  DO WHILE ( iarg <= narg )
    CALL getarg ( iarg, cldum ) ; iarg = iarg + 1
    SELECT CASE ( cldum )
      CASE ('-jday1' ) ; CALL getarg ( iarg, clo ) ; READ(clo,*)jday1 ; iarg=iarg+1
      CASE ('-jday2' ) ; CALL getarg ( iarg, clo ) ; READ(clo,*)jday2 ; iarg=iarg+1
      CASE ('-n' ) ; CALL getarg ( iarg, clo ) ; READ(clo,*)nsta ; iarg=iarg+1
      CASE DEFAULT   ; PRINT *,TRIM(cldum),' : option not available.' ; STOP
    END SELECT
  ENDDO

  ! Read namelist
  INQUIRE(file='namelist', exist=l_exist_namelist)
  IF (.NOT. l_exist_namelist) THEN
     PRINT *,'The namelist does not exist'
     STOP
  ENDIF
  OPEN(11,file='namelist')
!  PRINT *,'Reading namelist namio'
!    REWIND 11
!    READ(11,namio)
!    PRINT *,'nn_scaleoffset = ',nn_scaleoffset
  PRINT *,'Reading namelist namparam'
    REWIND 11
    READ(11,namparam)
    PRINT *,'nn_samprate: ',nn_samprate,' Hz'
    PRINT *,'nn_lag: ',nn_lag,' sec'
  CLOSE(11)

  ALLOCATE(file2_id(jday2-jday1+1))

  CALL h5open_f(error)

  ! Mount all input files into a single one merge.h5
  CALL h5fcreate_f(TRIM(cn_filein), H5F_ACC_TRUNC_F, infile_id, error)
  ijd=0
  DO jd=jday1,jday2
    ijd=ijd+1
    write(cjd,'(I0.3)') jd
    ! Create group "/jd" inside file "merge.h5".
    CALL h5gcreate_f(infile_id, "/"//TRIM(cjd), gid, error)
    ! Open the jd file
    CALL h5fopen_f (TRIM(cjd)//".h5", H5F_ACC_RDONLY_F, file2_id(ijd), error)
    ! Mount the second file under the first file's "/jd" group.
    CALL h5fmount_f (infile_id, "/"//TRIM(cjd), file2_id(ijd), error)
    CALL h5gclose_f(gid, error)
  ENDDO

  ! Compute some useful parameters
  maxlag = nn_lag*nn_samprate  ! as a number of samples : 200
  npc = 2*maxlag+1             ! final length of the correlation vector : 401
  indims = (/npc/)

  ALLOCATE(sta(nsta))

  open(11, file="stalist", form="formatted", action="read")

  i=0
  DO
    read(11,*,end=1000)clo
    i = i + 1
    sta(i)=TRIM(clo)
  ENDDO
1000  close(11)

  ALLOCATE(buf(npc))
  ALLOCATE(cm(npc))
  
  !CALL h5fopen_f(TRIM(cn_filein), H5F_ACC_RDONLY_F, infile_id, error)

  ! Create output file
  CALL h5fcreate_f('cmean.h5', H5F_ACC_TRUNC_F, outfile_id, error)

  ! Loop over stalist
  DO i=1,nsta
    DO j=i,nsta
      cm(:)=0.d0
      print *,'Computing mean for ',TRIM(sta(i)),' ', TRIM(sta(j))
      ! Loop over jday
      ijd=0
      DO jd=jday1,jday2
        write(cjd,'(I0.3)') jd
        CALL h5lexists_f(infile_id,"/"//TRIM(cjd), link1_exists, error, H5P_DEFAULT_F)
        IF ( link1_exists ) THEN
          CALL h5lexists_f(infile_id,"/"//TRIM(cjd)//"/"//TRIM(sta(i))//"."//TRIM(cjd), link2_exists, error, H5P_DEFAULT_F)
          IF ( link2_exists ) THEN
            CALL h5lexists_f(infile_id, &
                 "/"//TRIM(cjd)//"/"//TRIM(sta(i))//"."//TRIM(cjd)//"/"//TRIM(sta(j))//"."//TRIM(cjd), &
                 link3_exists, error, H5P_DEFAULT_F)
            IF ( link3_exists ) THEN
              CALL h5ltread_dataset_float_f(infile_id, &
                   "/"//TRIM(cjd)//"/"//TRIM(sta(i))//"."//TRIM(cjd)//"/"//TRIM(sta(j))//"."//TRIM(cjd)//"/corr", &
                   buf, indims, error)
              cm(:)=cm(:)+DBLE(buf)
              ijd=ijd+1
            ENDIF
          ENDIF
        ENDIF
      ENDDO
      IF ( ijd /= 0 ) THEN
        cm(:)=cm(:)/DBLE(ijd)
        CALL create_or_open_group(outfile_id, TRIM(sta(i)), grpsta_id )
        CALL create_or_open_group(grpsta_id,  TRIM(sta(j)), grpchan_id)
        CALL h5gclose_f( grpsta_id, error)
        CALL h5ltmake_dataset_float_f(grpchan_id, 'corr', 1, indims, REAL(cm(:)), error)
        CALL h5gclose_f( grpchan_id, error)
      ENDIF
    ENDDO
  ENDDO

  ijd=0
  DO jd=jday1,jday2
    ijd=ijd+1
    write(cjd,'(I0.3)') jd
    ! Unmount the jd files
    CALL h5funmount_f (infile_id, "/"//TRIM(cjd), error)
    CALL h5fclose_f(file2_id(ijd), error)
  ENDDO

  ! Close the input file
  CALL h5fclose_f(infile_id, error)

  ! Close the output file
  CALL h5fclose_f(outfile_id, error)

  CALL h5close_f(error)

END PROGRAM CORRGLOBALMEAN

SUBROUTINE create_or_open_group(parent_id,gr_name,gr_id)

  USE HDF5 ! HDF5 module

  IMPLICIT NONE

  INTEGER(HID_T), INTENT(in)     :: parent_id
  CHARACTER(LEN=*), INTENT(in)   :: gr_name
  INTEGER(HID_T), INTENT(out)    :: gr_id
  LOGICAL                        :: link_exists
  INTEGER                        :: error

  ! Output file : create group if necessary, or simply open it
  CALL h5lexists_f(parent_id, gr_name, link_exists, error, H5P_DEFAULT_F)
  IF ( link_exists ) THEN
    CALL h5gopen_f(parent_id, gr_name, gr_id, error)
  ELSE
    CALL h5gcreate_f(parent_id, gr_name, gr_id, error)
  ENDIF

END SUBROUTINE create_or_open_group
