#!/bin/bash
#OAR --project resolve
#OAR -l /nodes=1/cpu=1/core=1,walltime=10:00:00
#OAR -n compute_mean
##OAR -p network_address='luke4'

set -e
source /applis/site/guix-start.sh
refresh_guix corrmean
# suppose that we have already installed gfortran, python3, h5py, hdf5 with manifest_corrmean.scm
# zlib                  1.2.11  out     /gnu/store/8qv5kb2fgm4c3bf70zcg9l6hkf3qzpw9-zlib-1.2.11
# hdf5                  1.12.1  fortran /gnu/store/78wc7qhmxz00id9y8plmnmcfvfzix747-hdf5-1.12.1-fortran
# hdf5                  1.12.1  out     /gnu/store/1rjs998na0xd57ach5w0432z615p4j5b-hdf5-1.12.1
# gfortran-toolchain    10.3.0  out     /gnu/store/glkrwmfxhqq11j526isr6iqslz1if7fx-gfortran-toolchain-10.3.0
# python-scipy          1.7.3   out     /gnu/store/x3xs6zmwkpflgksivzblw2q61v8vsni0-python-scipy-1.7.3
# python-h5py           3.6.0   out     /gnu/store/68nggkk7rxkmxn8k6iadh3fiqyb9cqvs-python-h5py-3.6.0
# python                3.9.9   out     /gnu/store/sz7lkmic6qrhfblrhaqaw0fgc4s9n5s3-python-3.9.9


YEAR=$2
JDAY1=$3
JDAY2=$4
FPCID=$5
NETWORK=$6

# example : > oarsub -S "./submit.sh 0 2018 115 147 EXP010 ZO"

# exemple compil sur luke:
# > source /applis/site/guix-start.sh
# > refresh_guix corrmean
# > make 
# h5fc -I/var/guix/profiles/per-user/lecoinal/corrmean/include -shlib -O2  -o corrglobalmean corrglobalmean.f90 

F90EXE=corrglobalmean

cat $OAR_NODE_FILE
NBCORE=$(cat $OAR_NODE_FILE | wc -l)
echo "NBCORE : $NBCORE"

echo $YEAR $JDAY1 $JDAY2 $FPCID

case "$CLUSTER_NAME" in
  luke)
    IDIR="${SHARED_SCRATCH_DIR}/$USER"
    TMPDIR="${SHARED_SCRATCH_DIR}/$USER/oar.corrmean.$OAR_JOB_ID";
    ;;
  *)
    echo "$CLUSTER_NAME not allowed, please use luke instead"
    exit 0
    ;;
esac

mkdir -p $TMPDIR
cp $F90EXE $TMPDIR
cp ${NETWORK}stalist $TMPDIR/stalist

cat namelist.mean > $TMPDIR/namelist

cd $TMPDIR

for JD in $(seq $JDAY1 $JDAY2) ; do
  JDAY=$(printf "%03d" $JD)
  ln -sf $IDIR/RESOLVE/CORRELATIONS/${FPCID}/DAILYMEAN/${NETWORK}.${YEAR}${JDAY}.000000.h5 ${JDAY}.h5
done

echo "Compute mean:                      $YEAR $JDAY1 - $JDAY2      Date and Time: $(date +%F" "%T"."%N)"

./$F90EXE -jday1 $JDAY1 -jday2 $JDAY2 -n $(wc -l stalist | awk '{print $1}')

echo "Move Results:                      $YEAR $JDAY1 - $JDAY2      Date and Time: $(date +%F" "%T"."%N)"

mkdir -p ${FPCID}/GLOBALMEAN
h5repack -v -f GZIP=6 cmean.h5 ${FPCID}/GLOBALMEAN/${NETWORK}.GLOBALMEAN.${YEAR}.${JDAY1}.${JDAY2}.000000.h5
rm cmean.h5

echo "End job:                           $YEAR $JDAY1 - $JDAY2      Date and Time: $(date +%F" "%T"."%N)"
