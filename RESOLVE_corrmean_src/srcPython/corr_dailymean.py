#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
#    This file is part of CORR : a Python code for computing daily mean       #
#    among several high frequency (e.g. 10-min) correlation files.            #
#                                                                             #
#    Copyright (C) 2016 Albanne Lecointre                                     #
#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
###############################################################################

import numpy as np
import scipy
#import argparse
import h5py
#import time
import sys

if __name__ == "__main__":

    h5f = h5py.File("in.h5", "r")
    h5fo = h5py.File("out.h5", "w")

    for gp0 in h5f:
        for gp1 in h5f[gp0]:
           for dset in h5f[gp0+"/"+gp1]:
               print(gp0+"/"+gp1+"/"+dset)

               c = h5f[gp0+"/"+gp1+"/"+dset][:,:]
               print(np.shape(c))  # 144,2001

               # On checke que pour chaque pas de temps, isfinite est toujours true ou toujours false
               for it in np.arange(np.shape(c)[0]):
                   # scipy logical_and ?
                   #print(np.isfinite(c[it,:]))
                   if (np.sum(np.isfinite(c[it,:])) != 0) and (np.sum(np.isfinite(c[it,:])) != np.shape(c)[1]):
                       print(it, ": presence de NaN/Inf ET de float dans la correlation, Abort")
                       sys.exit(1)         

                   if (np.sum(np.isfinite(c[it,:])) == 0):
                       print(it, " NaNs ", c[it,0:10])


               cmean = np.nanmean(c, axis=0)
               np.shape(cmean)
               h5fo[gp0+"/"+gp1+"/"+dset] = cmean


    h5fo.close()
    h5f.close()
