#!/bin/bash
#OAR --project iworms
#OAR -l /nodes=1/cpu=1/core=1,walltime=5:00:00
#OAR -n daymean

set -e

# CIMENT GRID Clusters
#source /applis/site/nix.sh
source /applis/site/guix-start.sh
refresh_guix corrmean
# suppose that we have already installed gfortran, python3, h5py, hdf5 with manifest_corrmean.scm
# zlib              	1.2.11	out    	/gnu/store/8qv5kb2fgm4c3bf70zcg9l6hkf3qzpw9-zlib-1.2.11
# hdf5              	1.12.1	fortran	/gnu/store/78wc7qhmxz00id9y8plmnmcfvfzix747-hdf5-1.12.1-fortran
# hdf5              	1.12.1	out    	/gnu/store/1rjs998na0xd57ach5w0432z615p4j5b-hdf5-1.12.1
# gfortran-toolchain	10.3.0	out    	/gnu/store/glkrwmfxhqq11j526isr6iqslz1if7fx-gfortran-toolchain-10.3.0
# python-scipy      	1.7.3 	out    	/gnu/store/x3xs6zmwkpflgksivzblw2q61v8vsni0-python-scipy-1.7.3
# python-h5py       	3.6.0 	out    	/gnu/store/68nggkk7rxkmxn8k6iadh3fiqyb9cqvs-python-h5py-3.6.0
# python            	3.9.9 	out    	/gnu/store/sz7lkmic6qrhfblrhaqaw0fgc4s9n5s3-python-3.9.9


date

START=$(date +%s.%N)

cat $OAR_NODE_FILE

JD=$1
EXP="EXP020"
YEAR=2018
NETWORK="ZO"

case "$CLUSTER_NAME" in
  luke)
    # suppose that we have already install python3 with h5py and matplotlib: nix-env -if python3-env.nix
    IDIR="$SHARED_SCRATCH_DIR/$USER/"
    ;;
  *)
    echo "CLUSTER_NAME: $CLUSTER_NAME not supported. Abort"
    exit 1
    ;;
esac

#####################################################

INPUT_DIR="/$IDIR/RESOLVE/CORRELATIONS/$EXP/10MIN"
INPUT_FILE="${NETWORK}.${YEAR}${JD}.000000.h5"

###########################################################

PEXE=corr_dailymean.py

###########################################################

CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=00000}  # substitute if not initialized

mkdir -p /$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID

TMPDIR="/$SHARED_SCRATCH_DIR/$USER/daymean_${CIGRI_CAMPAIGN_ID}_${OAR_JOB_ID}_${JD}"
echo $TMPDIR
mkdir -p $TMPDIR

cp $PEXE $TMPDIR

cd $TMPDIR

###########################
## Get data

ln -sf $INPUT_DIR/$INPUT_FILE in.h5

/usr/bin/time -v python3 -u $PEXE 
#-n $nbpair -t 501 -tb 0 -te 1 -itb 500 -ite 1501 -istaref $((indstaref-1)) -f 16.5 --ip inp1.h5 -nc ZO -j $JD

rm in.h5 $PEXE
h5repack -v -f GZIP=6 out.h5 $INPUT_FILE
rm out.h5

mv $TMPDIR /$SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID/.

sync

date
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"
