# Preprocessing données SSGS (Madison, Ugo Nanni)

Sur luke|dahu , profil GUIX avec python-obspy et python-h5py

```
> source /applis/site/guix-start.sh
> refresh_guix prep_obspy        # creation d'un profil dédié
This profile (prep_obspy) does not exist.
Try to install a new package in this profile to create it:

               guix install -p /var/guix/profiles/per-user/lecoinal/prep_obspy <some_package>

               
> guix install -p /var/guix/profiles/per-user/lecoinal/prep_obspy python@3 python-obspy python-h5py
guix install: warning: Consider running 'guix pull' followed by
'guix package -u' to get up-to-date packages and security updates.

The following packages will be installed:
   python       3.8.2
   python-h5py  2.10.0
   python-obspy 1.2.2

[...]
>  refresh_guix prep_obspy
Activate profile  /var/guix/profiles/per-user/lecoinal/prep_obspy
The following packages are currently installed in /var/guix/profiles/per-user/lecoinal/prep_obspy:
python	3.8.2	out	/gnu/store/v1l6cm8aa47zsxvjjmzd5rpdbbslzpc8-python-3.8.2
python-obspy	1.2.2	out	/gnu/store/krhmy8biygvrvcrhhb2yvqr5adxgj7ij-python-obspy-1.2.2
python-h5py	2.10.0	out	/gnu/store/sf9ivqanbh1gaz4shb1da4ljiisqjdqc-python-h5py-2.10.0

# Verification de l'environnement :

> which python3
/var/guix/profiles/per-user/lecoinal/prep_obspy/bin/python3
python3
Python 3.8.2 (default, Jan  1 1970, 00:00:01) 
[GCC 7.5.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import obspy
>>> obspy.__path__   
['/var/guix/profiles/per-user/lecoinal/prep_obspy/lib/python3.8/site-packages/obspy']
>>> obspy.__version__
'1.2.2'

>>> import h5py
>>> h5py.__path__
['/var/guix/profiles/per-user/lecoinal/prep_obspy/lib/python3.8/site-packages/h5py']
>>> h5py.__version__
'2.10.0'
>>> h5py.get_config().mpi
False            # On n'utilise pas le h5py-mpi ici.
```


# Preprocess : conversion MSEED -> HDF5

J'ai converti les données de la double à la simple precision !

```
> oarsub -S ./submit_sggs.sh
# environ 0.1 sec par trace de 2h : total (90+96) * 0.1 = 18sec
```

```
> msi -tg SGGS_100Hz_20190808T220000-20190809T000000_filt_interp_resample_trim.mseed 
   Source                Start sample             End sample        Gap  Hz  Samples
1B_1110__GP1      2019,220,22:00:00.000000 2019,221,00:00:00.000000  ==  100 720001
1B_1110__GP2      2019,220,22:00:00.000000 2019,221,00:00:00.000000  ==  100 720001
1B_1110__GPZ      2019,220,22:00:00.000000 2019,221,00:00:00.000000  ==  100 720001
[...]
SG_R2108__GN1     2019,220,22:00:00.000000 2019,221,00:00:00.000000  ==  100 720001
SG_R2108__GN2     2019,220,22:00:00.000000 2019,221,00:00:00.000000  ==  100 720001
SG_R2108__GNZ     2019,220,22:00:00.000000 2019,221,00:00:00.000000  ==  100 720001
Total: 90 trace(s) with 90 segment(s)
```

```
> $ ls -lh
-rw-r--r-- 1 lecoinal l-isterre 128M May  4 14:10 1B_2019_220.h5
-rw-r--r-- 1 lecoinal l-isterre 133M May  4 14:11 1B_2019_224.h5
-rw-r--r-- 1 lecoinal l-isterre  79M May  4 14:10 SG_2019_220.h5
-rw-r--r-- 1 lecoinal l-isterre  86M May  4 14:11 SG_2019_224.h5

> h5ls -v 1B_2019_220.h5
Opened "1B_2019_220.h5" with sec2 driver.
1B.1110..GP1.D.2019.220  Dataset {720001/720001}
    Location:  1:800
    Links:     1
    Chunks:    {5626} 22504 bytes
    Storage:   2880004 logical bytes, 2315230 allocated bytes, 124.39% utilization
    Filter-0:  shuffle-2 OPT {4}
    Filter-1:  deflate-1 OPT {6}
    Filter-2:  fletcher32-3  {}
    Type:      native float
1B.1110..GP2.D.2019.220  Dataset {720001/720001}
    Location:  1:2326734
    Links:     1
    Chunks:    {5626} 22504 bytes
    Storage:   2880004 logical bytes, 2329334 allocated bytes, 123.64% utilization
    Filter-0:  shuffle-2 OPT {4}
    Filter-1:  deflate-1 OPT {6}
    Filter-2:  fletcher32-3  {}
    Type:      native float
1B.1110..GPZ.D.2019.220  Dataset {720001/720001}
[...]
```

Stockage des données au format HDF5 sur Mantis2
```
$ ils -l SGGS
/mantis/home/lecoinal/SGGS:
  lecoinal          0 imag;nigel3-r1-pt;nigel3-r1    133729014 2021-05-04.14:18 & 1B_2019_220.h5
  lecoinal          0 imag;nigel1-r1-pt;nigel1-r1    138762063 2021-05-04.14:18 & 1B_2019_224.h5
  lecoinal          0 imag;nigel1-r2-pt;nigel1-r2     82457367 2021-05-04.14:18 & SG_2019_220.h5
  lecoinal          0 imag;nigel2-r1-pt;nigel2-r1     89648785 2021-05-04.14:18 & SG_2019_224.h5

```

NB : Je n'ai pas enlevé la reponse instrumentale, à voir pour plus tard ?
Les fichiers RESP sont ici : 
```
$ ls -ltr /summer/resolve/Argentiere/UGO/TEST_MFP_MADISON/RESPONSE/
-rw-r--r-- 1 nanniu pr-resolve 9864 Mar 17 23:13 RESP.UW.R101..GNZ.DTSOLO.5.1850.43000.76_7
-rw-r--r-- 1 nanniu pr-resolve 9876 Mar 17 23:13 RESP.1B.1110..GP1.ZLAND_GEN2.5.UNK.UNK.76_7

```
