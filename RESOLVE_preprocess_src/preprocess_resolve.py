#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
#    This file is part of PREPROCESS : a Python code for preprocessing raw    #
#    input MSEED files (gap-fill, bandpass-filter, ...) and convert them      #
#    into HDF5 datasets that can be used as input for CORR (correlations) or  #
#    MFP (Matched Field Processing) codes                                     #
#                                                                             #
#    Copyright (C) 2018 Albanne Lecointre                                     #
#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
###############################################################################

#  =======================================================================================
#                       ***  Python script preprocess_resolve.py  ***
#   This is the main Python script to compute the initial preprocessing on raw 
#   RESOLVE Argentiere MSEED data
#    
#  =======================================================================================
#   History :  1.0  : 11/2018  : A. Lecointre : Initial Python version
#	        
#  ---------------------------------------------------------------------------------------
#
#  ---------------------------------------------------------------------------------------
#   methodology   : gapfilling : fill remaning gap with zero
#                                do not try to get missing samples from J-1 or J+1 mseed files
#                 : bandpass-filter between FB1 and FB2 (corner=4, zerophase=true)
#                 : Float32 encoding
#                 : byte-shuffle filter + gzip force 6 compression
#                 : output writing as : 1 dataset per trace and per day in HDF5 daily files
#
#  https://ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-resolve
#
#  ---------------------------------------------------------------------------------------
#  $Id$
#  ---------------------------------------------------------------------------------------

from obspy.core import read,UTCDateTime,Trace
import numpy as np
import os
import sys
import h5py

def preprocess(df):
	
	# Read
	
	s=read(df)
	
	# Gap filling : fill with zero
	
	net = s[0].stats.network
	sta = s[0].stats.station
	loc = s[0].stats.location
	channel = s[0].stats.channel
	year = s[0].stats.starttime.year
	jd = s[0].stats.starttime.julday
	
	datatype = s[0].data.dtype

	meta = {'station': sta, 'location': loc, 'network': net, 'channel': channel}
	
	stend = s[-1].stats.endtime
	
	trb = Trace(data=np.zeros(1,dtype=datatype),header=meta)
	trb.stats.delta = s[0].stats.delta
	trb.stats.starttime = UTCDateTime(str(year)+"{:03d}".format(jd)+"T000000.000000Z")
	if s[0].stats.starttime != trb.stats.starttime:
		s.append(trb)
	
	tre = Trace(data=np.zeros(1,dtype=datatype),header=meta)
	tre.stats.delta = s[0].stats.delta
	tre.stats.starttime = UTCDateTime(str(year)+"{:03d}".format(jd+1))-s[0].stats.delta
	if stend != tre.stats.endtime:
		s.append(tre)
	
	s.merge(method=0, fill_value=0)
	
	# Bandpass filter : fb1 - fb2 Hz

	fb1=<<FB1>>
	fb2=<<FB2>>
	s.filter("bandpass", freqmin=fb1, freqmax=fb2, corners=4, zerophase=True)
	
	# Write outputs
	
	SDSdir = '.'
	SDSfile = net+'.'+sta+'.'+loc+'.'+channel+'.D.'+"{:04d}".format(year)+'.'+"{:03d}".format(jd)

	try:
	    os.stat(SDSdir)
	except:
	    os.makedirs(SDSdir)
	
	s[0].data=s[0].data.astype('float32')
	
	h5fo = h5py.File(SDSdir+'/'+net+'_'+"{:04d}".format(year)+'_'+"{:03d}".format(jd)+'.h5','a')
	
	h5fo.create_dataset(SDSfile,shape=(s[0].stats.npts,), dtype='float32',data=s[0].data[:], shuffle=True, compression='gzip', compression_opts=6, fletcher32='True')

        # cut only first hour
	#h5fo = h5py.File(SDSdir+'/'+net+'_'+"{:04d}".format(year)+'_'+"{:03d}".format(jd)+'_2018116T000000.000000_2018116T005959998000.h5','a')
	#h5fo.create_dataset(SDSfile,shape=(int(s[0].stats.npts/24.0),), dtype='float32',data=s[0].data[0:int(3600*s[0].stats.sampling_rate)], shuffle=True, compression='gzip', compression_opts=6, fletcher32='True')
	#h5fo.create_dataset(SDSfile,shape=(int(s[0].stats.npts/24.0),), dtype='float32',data=s[0].data[0:int(3600*s[0].stats.sampling_rate)], compression='gzip', compression_opts=6, fletcher32='True')

	h5fo.close()

if __name__ == '__main__':
	import sys,time
	t=time.time()
	preprocess(sys.argv[1])
	print(sys.argv[1],time.time()-t," sec")
