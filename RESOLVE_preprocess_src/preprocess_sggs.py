#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
#    This file is part of PREPROCESS : a Python code for preprocessing raw    #
#    input MSEED files (gap-fill, bandpass-filter, ...) and convert them      #
#    into HDF5 datasets that can be used as input for CORR (correlations) or  #
#    MFP (Matched Field Processing) codes                                     #
#                                                                             #
#    Copyright (C) 2018 Albanne Lecointre                                     #
#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
###############################################################################

#  =======================================================================================
#                       ***  Python script preprocess_sggs.py  ***
#   This is the main Python script to compute the initial preprocessing on SGGS 
#   MSEED data
#    
#  =======================================================================================
#   History :  1.0  : 05/2021  : A. Lecointre : Initial Python version
#	        
#  ---------------------------------------------------------------------------------------
#
#  ---------------------------------------------------------------------------------------
#   methodology   : 
#                 : Float32 encoding
#                 : byte-shuffle filter + gzip force 6 compression
#                 : output writing as : 1 dataset per trace and per day in HDF5 daily files
#
#
#  ---------------------------------------------------------------------------------------

from obspy.core import read,UTCDateTime,Trace
import numpy as np
import os
import sys
import h5py

def preprocess(t):
	
	net = t.stats.network
	sta = t.stats.station
	loc = t.stats.location
	channel = t.stats.channel
	year = t.stats.starttime.year
	jd = t.stats.starttime.julday
	
	datatype = t.data.dtype

	# Write outputs
	
	SDSdir = '.'
	SDSfile = net+'.'+sta+'.'+loc+'.'+channel+'.D.'+"{:04d}".format(year)+'.'+"{:03d}".format(jd)

	try:
	    os.stat(SDSdir)
	except:
	    os.makedirs(SDSdir)
	
	t.data=t.data.astype('float32')
	
	h5fo = h5py.File(SDSdir+'/'+net+'_'+"{:04d}".format(year)+'_'+"{:03d}".format(jd)+'.h5','a')
	
	h5fo.create_dataset(SDSfile,shape=(t.stats.npts,), dtype='float32',data=t.data[:], shuffle=True, compression='gzip', compression_opts=6, fletcher32='True')

	h5fo.close()

if __name__ == '__main__':
	import sys,time
	s = read(sys.argv[1], headonly=True)
	print(s.__str__(extended=True))
	s = read(sys.argv[1])
	itr = 0
	for tr in s:
		t=time.time()
		preprocess(tr)
		print(itr, tr, time.time()-t," sec")
		itr += 1
