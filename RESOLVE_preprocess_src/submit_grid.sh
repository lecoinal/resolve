#!/bin/bash
#OAR --project resolve
##OAR --project iste-equ-ondes
#OAR -l /core=1,walltime=03:00:00
#OAR -n resolve_4_8Hz

#  =======================================================================================
#   ***  Shell wrapper to submit preprocess_resolve.py in the CIMENT grid ***
#    
#  =======================================================================================
#   History :  1.0  : 11/2018  : A. Lecointre : Initial version
#               
#  ---------------------------------------------------------------------------------------
#  $Id$
#  ---------------------------------------------------------------------------------------

set -e

source /applis/site/guix-start.sh
# source /soft/env.bash   # ISTERRE cluster with module

# args
if [ $# -ne 4 ]; then
    echo "ERROR: Bad number of parameters"
    echo "USAGE: $0 <doy> <bandpass_freqmin> <bandpass_freqmax> <year>"
    exit 1
fi

case $CLUSTER_NAME in
  ISTERRE)
    module load python/python3.7
    PYV="python3.7"
    ;;
  *)
    refresh_guix prep_obspy
# python	3.8.2	out	/gnu/store/v1l6cm8aa47zsxvjjmzd5rpdbbslzpc8-python-3.8.2
# python-obspy	1.2.2	out	/gnu/store/krhmy8biygvrvcrhhb2yvqr5adxgj7ij-python-obspy-1.2.2
# python-h5py	2.10.0	out	/gnu/store/sf9ivqanbh1gaz4shb1da4ljiisqjdqc-python-h5py-2.10.0
    PYV="python3"
    ;;
esac


JDAY=$1
FB1=$2
FB2=$3
YEAR=$4

CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=00000} # substitute if not initialized

case $CLUSTER_NAME in
  ISTERRE)
    INPUTDIR="/data/projects/pacific/lecointre/EXTRACTION_TOMOYA/ZZ/"
    CHANNEL="EHZ.D"
    ;;
  *)
#INPUTDIR="/summer/resolve/Argentiere/DATA/RESIF_Nodes/"
# Note: As I experienced lots of Input-Output Error on SUMMER from luke29, luke37, ... , I decided to copy the entire RSEIF_Nodes directory (1.7TB) from resolve SUMMER space into BETTIK
    #INPUTDIR="/bettik/lecoinal/RESOLVE/RESIF_Nodes/"
    INPUTDIR="/bettik/PROJECTS/pr-resolve/lecoinal/RESIF_Nodes/"
    CHANNEL="DPZ.D"
    ;;
esac

PEXE=preprocess_resolve.py

cat $OAR_NODE_FILE

TMPDIR="/$SHARED_SCRATCH_DIR/$USER/oar.${OAR_JOB_ID}"

echo $TMPDIR

mkdir -p $TMPDIR

sed -e "s/<<FB1>>/$FB1/" -e "s/<<FB2>>/$FB2/" $PEXE > $TMPDIR/$PEXE

cd $TMPDIR

# Get MSEED input files from SUMMER resolve

# Note: set -e seems to be inefficient on UNIX commands ($cmd) when they are under the scope of : sg pr-perseusproject -c "$cmd"
#for f in $(sg pr-resolve -c "ls ${INPUTDIR}/*/${CHANNEL}/*.${JDAY}") ; do echo $f ; sg pr-resolve -c "ln -s $f ." ; done
#for f in $(sg pr-resolve -c "ls ${INPUTDIR}/*/${CHANNEL}/*.${JDAY}") ; do echo $f ; sg pr-resolve -c "rsync -av $f ." ; done
# Note: As I experienced lots of Input-Output Error on SUMMER from luke29, luke37, ... , I decided to copy the entire RSEIF_Nodes directory (1.7TB) from resolve SUMMER space into BETTIK
for f in $(ls ${INPUTDIR}/*/${CHANNEL}/*${YEAR}.${JDAY} ${INPUTDIR}/*/${CHANNEL}/*${YEAR}.${JDAY}.gz) ; do echo $f ; ln -s $f . ; done
#for f in $(ls ${INPUTDIR}/*/*/*${YEAR}.${JDAY}) ; do echo $f ; ln -s $f . ; done


echo "Preprocess                    : Date and Time: $(date +%F" "%T"."%N)"

mkdir -p /$SHARED_SCRATCH_DIR/$USER/${CIGRI_CAMPAIGN_ID}

#for f in $(ls *${YEAR}.$JDAY *${YEAR}.$JDAY.gz) ; do
#  filesize=$(stat -L --printf="%s" $f)
#  if [[ "$filesize" > "20" ]] ; then
#    $PYV $PEXE $f
#  fi
#done
for f in $(ls *${YEAR}.$JDAY *${YEAR}.$JDAY.gz) ; do
  $PYV $PEXE $f
done

mv $TMPDIR /$SHARED_SCRATCH_DIR/$USER/${CIGRI_CAMPAIGN_ID}/.

echo "End job                       : Date and Time: $(date +%F" "%T"."%N)"
