#!/bin/bash
#OAR --project resolve
#OAR -l /core=1,walltime=00:10:00
#OAR -n resolve_ssgs_mseed_to_hdf5

#  =======================================================================================
#   ***  Shell wrapper to submit preprocess_sggs.py in the CIMENT grid ***
#    
#  =======================================================================================
#   History :  1.0  : 05/2021  : A. Lecointre : Initial version
#               
#  ---------------------------------------------------------------------------------------

set -e

source /applis/site/guix-start.sh

refresh_guix prep_obspy

# python	3.8.2	out	/gnu/store/v1l6cm8aa47zsxvjjmzd5rpdbbslzpc8-python-3.8.2
# python-obspy	1.2.2	out	/gnu/store/krhmy8biygvrvcrhhb2yvqr5adxgj7ij-python-obspy-1.2.2
# python-h5py	2.10.0	out	/gnu/store/sf9ivqanbh1gaz4shb1da4ljiisqjdqc-python-h5py-2.10.0

CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=00000} # substitute if not initialized

INPUT_DIR="/mantis/home/lecoinal/to_ship"

PEXE=preprocess_sggs.py

cat $OAR_NODE_FILE

TMPDIR="/$SHARED_SCRATCH_DIR/$USER/oar.$OAR_JOB_ID"

echo $TMPDIR

mkdir -p $TMPDIR

sed -e "s/<<FB1>>/$FB1/" -e "s/<<FB2>>/$FB2/" $PEXE > $TMPDIR/$PEXE

cd $TMPDIR

# Get MSEED input files from Mantis2
iget -rv $INPUT_DIR .


PYV="python3"

echo "Preprocess                    : Date and Time: $(date +%F" "%T"."%N)"

mkdir -p /$SHARED_SCRATCH_DIR/$USER/${CIGRI_CAMPAIGN_ID}

for f in $(ls to_ship/*.mseed) ; do
  echo $f
  $PYV $PEXE $f
done

mv $TMPDIR /$SHARED_SCRATCH_DIR/$USER/${CIGRI_CAMPAIGN_ID}/.

echo "End job                       : Date and Time: $(date +%F" "%T"."%N)"
