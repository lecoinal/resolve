#!/bin/bash
#OAR --project resolve
##OAR --project iste-equ-ondes
#OAR -l /core=1,walltime=00:30:00
#OAR -n teterousse
#OAR -t devel

#  =======================================================================================
#   ***  Shell wrapper to submit preprocess_teterousse.py in the CIMENT grid ***
#    
#  =======================================================================================
#   History :  1.0  : 11/2022  : A. Lecointre : Initial version
#               
#  ---------------------------------------------------------------------------------------

set -e

source /applis/site/guix-start.sh

refresh_guix prep_obspy
# python	3.8.2	out	/gnu/store/v1l6cm8aa47zsxvjjmzd5rpdbbslzpc8-python-3.8.2
# python-obspy	1.2.2	out	/gnu/store/krhmy8biygvrvcrhhb2yvqr5adxgj7ij-python-obspy-1.2.2
# python-h5py	2.10.0	out	/gnu/store/sf9ivqanbh1gaz4shb1da4ljiisqjdqc-python-h5py-2.10.0


TAG=$1   # 2022.05.02 -- 2022.05.22

CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=00000} # substitute if not initialized

INPUTDIR="/bettik/lecoinal/pr-teterousse/nodes_202205_mseed/"
CHANNEL=".Z.miniseed"

PEXE=preprocess_teterousse.py

cat $OAR_NODE_FILE

TMPDIR="/$SHARED_SCRATCH_DIR/$USER/oar.${OAR_JOB_ID}"

echo $TMPDIR

mkdir -p $TMPDIR

cp $PEXE $TMPDIR/$PEXE

cd $TMPDIR

# Get MSEED input files from /bettik

for f in $(ls ${INPUTDIR}/*/*${TAG}*${CHANNEL}) ; do echo $f ; ln -s $f . ; done


echo "Preprocess                    : Date and Time: $(date +%F" "%T"."%N)"

mkdir -p /$SHARED_SCRATCH_DIR/$USER/${CIGRI_CAMPAIGN_ID}

for f in $(ls *${TAG}*${CHANNEL}) ; do
  dirname=$(ls -l $f | awk '{print $NF}' | awk -F'/' '{print $(NF-1)}')
  echo "$f $dirname"
  python3 $PEXE $f $dirname
done

mv $TMPDIR /$SHARED_SCRATCH_DIR/$USER/${CIGRI_CAMPAIGN_ID}/.

echo "End job                       : Date and Time: $(date +%F" "%T"."%N)"
