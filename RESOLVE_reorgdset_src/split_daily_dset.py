#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
#    This file is part of UTILS : a Python code for extracting subsets of     #
#    MFP outputs                                                              #
#                                                                             #
#    Copyright (C) 2021 Albanne Lecointre                                     #
#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
###############################################################################


import numpy as np
import h5py
import argparse
import time

""" 
    module for extracting one single JD from beam output 
    from MFP computation
    in the context on distributing MFP outputs on Zenodo

"""

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="extract and reorganize MFP dataset")
    parser.add_argument("-i", default="in.h5", type=str, help="input file (will be a hdf5 file, default is in.h5)")
    parser.add_argument("-o", default="out.h5", type=str, help="output file (will be a hdf5 file, default is out.h5)")
    parser.add_argument("-ijd", default=0, type=int, help="0-based index of extracted julian day, default is 0)")
    args = parser.parse_args()

    inputFile = args.i
    outputFile = args.o
    #nd = 34      # number of days
    nd = 1       # number of days
    ns = 29      # number of starting point
    nt = 172799  # number of timesteps in one day
    nn = 98      # number of nodes
    nf = 16      # number of sub-frequencies
    iijd = args.ijd
    idns = 3     # nb of dims for startpoint

    h5f = h5py.File(inputFile, "r")

    h5fo = h5py.File(outputFile, "w")
    h5fo.attrs['description'] = np.string_('This dataset contains the seismic source locations computed from Matched Field Processing applied over 34 days of continuous records from ZO network (dense nodal seismic array temporary experiment on Alpine Glacier of Argentiere, RESIF-SISMOB).')
    h5fo.attrs['src_code'] = np.string_('This dataset is the output of Fortran source code beamforming_optim (GNU GPLv3.0), part of resolve project.')
    h5fo.attrs['src_code_repository'] = np.string_('https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/resolve/-/tree/master')
    h5fo.attrs['src_code_GITrevision'] = np.string_('7910e80c')
    h5fo.attrs['CiGri campaign id'] = np.string_('15423')
    h5fo.attrs['CiGri jobs logs'] = np.string_('https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/resolve/-/wikis/BeamformingCoh#cigri-campaign-id-15423')

    tstart = time.time()
    print("Copying dimensions ... : ", end='')

    dset = h5fo.create_dataset('/centralFreq',shape=(nf,), dtype='f4')
    dset.attrs['units'] = np.string_('Hz')
    dset.attrs['description'] = np.string_('Central frequency in which raw data are filtered before applying MFP')
    dset[:] = h5f['/freq_centrale'][:]
    
    dset = h5fo.create_dataset('/startpoint',shape=(idns, ns), dtype='f4', shuffle=True, compression='gzip', compression_opts=6)
    dset.attrs['units'] = np.string_('meters')
    dset.attrs['description'] = np.string_('Localization (x,y,altitude) of starting points for optimization')
    dset[:,:] = h5f['/startpoint'][:,:]

    dset = h5fo.create_dataset('/doy',shape=(nd,), dtype='i2',shuffle=True, compression='gzip', compression_opts=6)
    dset.attrs['description'] = np.string_('Day of year 2018')
    dset[:] = h5f['/JD'][iijd]

    dset = h5fo.create_dataset("xyznode", (3,nn), dtype='f4',shuffle=True, compression='gzip', compression_opts=6)
    dset.attrs['units'] = np.string_('meters')
    dset.attrs['description'] = np.string_('Localization (x,y,altitude) of nodes')
    tmp = np.loadtxt('xyz.txt')
    for idir in range(3):
        dset[idir,:] = tmp[:,idir]
    elps = time.time() - tstart
    print(elps," sec")

    tstart = time.time()
    print("Copying variable availdata ... : ", end='')

    dset = h5fo.create_dataset('/availdata',shape=(nt, nn), dtype='i1', compression='gzip', compression_opts=6)
    dset.attrs['description'] = np.string_('Flag value indicating if there was data or zero-filled gap at each timestep and for each node')
    dset[:,:] = h5f['/'+str(iijd)+'/availdata'][:,:]
    elps = time.time() - tstart
    print(elps," sec")

    tstart = time.time()
    print("Copying variable neval ... : ", end='')
    dset = h5fo.create_dataset('/neval',shape=(nf, ns, nt), dtype='i2', shuffle=True, compression='gzip', compression_opts=6)
    dset.attrs['description'] = np.string_('Number of function evaluations for the optimization')
    for iinf in range(nf):
        dset[iinf,:,:] = h5f['/neval'][iinf,iijd,:,:]   # 16, 34, 29, 172799
    elps = time.time() - tstart
    print(elps," sec")

    tstart = time.time()
    print("Copying variable xopt ... : ", end='')
    dset = h5fo.create_dataset('/xopt',shape=(nf, ns, nt), dtype='f4', shuffle=True, compression='gzip', compression_opts=6)
    dset.attrs['units'] = np.string_('meters')
    dset.attrs['description'] = np.string_('Localization x of optimized function')
    dset.attrs['UTM x-position for the grid origin'] = np.string_('343058.45425510209')
    for iinf in range(nf):
        dset[iinf,:,:] = h5f['/data_final_all'][iinf,iijd,0,:,:]    # {16, 34, 5, 29, 172799}
    elps = time.time() - tstart
    print(elps," sec")

    tstart = time.time()
    print("Copying variable yopt ... : ", end='')
    dset = h5fo.create_dataset('/yopt',shape=(nf, ns, nt), dtype='f4', shuffle=True, compression='gzip', compression_opts=6)
    dset.attrs['units'] = np.string_('meters')
    dset.attrs['description'] = np.string_('Localization y of optimized function')
    dset.attrs['UTM y-position for the grid origin'] = np.string_('5091922.07665306050')
    for iinf in range(nf):
        dset[iinf,:,:] = h5f['/data_final_all'][iinf,iijd,1,:,:]    # {16, 34, 5, 29, 172799}
    elps = time.time() - tstart
    print(elps," sec")

    tstart = time.time()
    print("Copying variable zopt ... : ", end='')
    dset = h5fo.create_dataset('/zopt',shape=(nf, ns, nt), dtype='f4', shuffle=True, compression='gzip', compression_opts=6)
    dset.attrs['units'] = np.string_('meters')
    dset.attrs['description'] = np.string_('Localization altitude of optimized function')
    for iinf in range(nf):
        dset[iinf,:,:] = h5f['/data_final_all'][iinf,iijd,2,:,:]    # {16, 34, 5, 29, 172799}
    elps = time.time() - tstart
    print(elps," sec")

    tstart = time.time()
    print("Copying variable copt ... : ", end='')
    dset = h5fo.create_dataset('/copt',shape=(nf, ns, nt), dtype='f4', shuffle=True, compression='gzip', compression_opts=6)
    dset.attrs['units'] = np.string_('meters.sec-1')
    dset.attrs['description'] = np.string_('Velocity of optimized function')
    for iinf in range(nf):
        dset[iinf,:,:] = h5f['/data_final_all'][iinf,iijd,3,:,:]    # {16, 34, 5, 29, 172799}
    elps = time.time() - tstart
    print(elps," sec")

    tstart = time.time()
    print("Copying variable fopt ... : ", end='')
    dset = h5fo.create_dataset('/fopt',shape=(nf, ns, nt), dtype='f4', shuffle=True, compression='gzip', compression_opts=6)
    dset.attrs['units'] = np.string_('meters')
    dset.attrs['description'] = np.string_('Value of optimized function')
    for iinf in range(nf):
        dset[iinf,:,:] = h5f['/data_final_all'][iinf,iijd,4,:,:]    # {16, 34, 5, 29, 172799}
    elps = time.time() - tstart
    print(elps," sec")

    h5f.close()
    h5fo.close()

