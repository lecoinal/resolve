#!/bin/bash
##OAR --project resolve
#OAR --project iste-equ-ondes
#OAR -l /core=1,walltime=10:00:00
##OAR -l /core=1,walltime=00:05:00
#OAR -n reorg_dset
#OAR -p network_address NOT like 'ist-calcul13.u-ga.fr'

set -e

source /soft/env.bash
module load python/python3.9
#/usr/bin/time -v python3 -u reorg_dset.py -i beam_15423.h5 -o /nfs_scratch/lecoinal/MFP.h5

cat $OAR_NODE_FILE

TMPDIR="/$SHARED_SCRATCH_DIR/$USER/tmp/oar.$OAR_JOB_ID"
mkdir -p $TMPDIR
cp extract_dset.py $TMPDIR
cp xyz.txt $TMPDIR
cd $TMPDIR

IINF=$1  # 3 à 15 pour 8 à 20Hz

#ln -sf /data/projects/resolve/Argentiere/ALBANNE/BEAM/EXP000/XYZC_ASA047_BOUNDZ5_COH/beam_14774.h5 . 
#/usr/bin/time -v python3 -u reorg_dset.py -i beam_14774.h5 -o /nfs_scratch/lecoinal/MFP.h5

# Extract Fc=17Hz and JD=115 non coh
#ln -sf /data/projects/resolve/Argentiere/ALBANNE/BEAM/EXP000/XYZC_ASA047_BOUNDZ5/out_13874_14453.h5 . 
#/usr/bin/time -v python3 -u extract_dset.py -i out_13874_14453.h5 -o /nfs_scratch/lecoinal/beam_13874_14453_17hz_jd115.h5

# Extract Fc=17Hz and JD=115 coh
ln -sf /data/projects/resolve/Argentiere/ALBANNE/BEAM/EXP000/XYZC_ASA047_BOUNDZ5_COH/beam_15423.h5 .
/usr/bin/time -v python3 -u extract_dset.py -iinf $IINF -i beam_15423.h5 -o /data/projects/resolve/Argentiere/ALBANNE/tmp/beam_coh_15423_$((IINF+5))hz_jd115_jd120.h5

