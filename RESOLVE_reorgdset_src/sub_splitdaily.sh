#!/bin/bash
#OAR --project iste-equ-ondes
#OAR -l /core=1,walltime=24:00:00
#OAR -n splitdaily_dset

set -e

source /soft/env.bash
module load python/python3.7


# Extract succesively all jd
ln -sf /data/projects/resolve/Argentiere/ALBANNE/BEAM/EXP000/XYZC_ASA047_BOUNDZ5_COH/beam_15423.h5 .

jd=114   # First day with 0-based index is 114
for ijd in $(seq 0 33) ; do
  /usr/bin/time -v python3 -u split_daily_dset.py -ijd $ijd -i beam_15423.h5 -o /data/projects/resolve/Argentiere/ALBANNE/tmp/beam_15423_jd${jd}.h5
  jd=$((jd+1))
done
