:warning: Les données du projet teterousse ont été déplacées de /bettik à mantis
```
/bettik/lecoinal/pr-teterousse
->
/mantis/home/lecoinal/pr-teterousse
```

# Preparation des données teterousse pour code MFP

## MSEED input dataset

Input dataset :

```
> ls /data/projects/teterousse/nodes_202205_mseed
node_01_2  node_02_1  node_02_6  node_03_5  node_04_3  node_05_1  node_06_1  node_06_6  node_07_5  node_08_4  node_09_4  node_A
node_01_3  node_02_2  node_03_1  node_03_6  node_04_4  node_05_2  node_06_2  node_07_1  node_07_6  node_08_5  node_09_5  node_B
node_01_4  node_02_3  node_03_2  node_03_7  node_04_5  node_05_3  node_06_3  node_07_2  node_08_1  node_08_6  node_10    node_C
node_01_5  node_02_4  node_03_3  node_04_1  node_04_6  node_05_4  node_06_4  node_07_3  node_08_2  node_09_2  node_11    node_D
node_01_6  node_02_5  node_03_4  node_04_2  node_04_7  node_05_5  node_06_5  node_07_4  node_08_3  node_09_3  node_12
> ls /data/projects/teterousse/nodes_202205_mseed/node_01_2/*Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400001.1.2022.05.02.17.50.42.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400002.2.2022.05.03.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400003.3.2022.05.04.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400004.4.2022.05.05.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400005.5.2022.05.06.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400006.6.2022.05.07.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400007.7.2022.05.08.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400008.8.2022.05.09.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400009.9.2022.05.10.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400010.10.2022.05.11.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400011.11.2022.05.12.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400012.12.2022.05.13.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400013.13.2022.05.14.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400014.14.2022.05.15.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400015.15.2022.05.16.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400016.16.2022.05.17.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400017.17.2022.05.18.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400018.18.2022.05.19.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400019.19.2022.05.20.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400020.20.2022.05.21.00.00.00.000.Z.miniseed
/data/projects/teterousse/nodes_202205_mseed/node_01_2/453000254.2000000400021.21.2022.05.22.00.00.00.000.Z.miniseed
```

59 nodes, only Z components, 23 days of data between 3 and 22 May 2022 (2022.05.02 - 2022.05.27)

500Hz nodes and 1000 Hz nodes

we decimate to 500Hz (for 1000Hz nodes, we erase one out of two sample)

we copy these input on /bettik

```
login@mantis-cargo:~$ rsync -av --exclude '*.[EN].miniseed' ist-oar.isterre:/data/projects/teterousse/nodes_202205_mseed /bettik/lecoinal/pr-teterousse/.
[...]
```


NB : j'ai concaténé ces 2 fichiers : 

```
lecoinal@f-dahu:/bettik/lecoinal/pr-teterousse/nodes_202205_mseed/node_06_6$ msi -tg 453000294.600000600001.1.2022.05.02.18.07.40.000.Z.miniseed
   Source                Start sample             End sample        Gap  Hz  Samples
01_6.00__HHZ      2022,122,18:07:40.000000 2022,122,18:22:28.000000  ==  500 444001
Total: 1 trace(s) with 1 segment(s)
lecoinal@f-dahu:/bettik/lecoinal/pr-teterousse/nodes_202205_mseed/node_06_6$ msi -tg 453000294.600000600002.2.2022.05.02.18.23.34.000.Z.miniseed
   Source                Start sample             End sample        Gap  Hz  Samples
01_6.00__HHZ      2022,122,18:23:34.000000 2022,123,00:00:00.000000  ==  500 10093001
Total: 1 trace(s) with 1 segment(s)
lecoinal@f-dahu:/bettik/lecoinal/pr-teterousse/nodes_202205_mseed/node_06_6$ cat 453000294.600000600001.1.2022.05.02.18.07.40.000.Z.miniseed 453000294.600000600002.2.2022.05.02.18.23.34.000.Z.miniseed > cat.mseed
lecoinal@f-dahu:/bettik/lecoinal/pr-teterousse/nodes_202205_mseed/node_06_6$ msi -tg cat.mseed 
   Source                Start sample             End sample        Gap  Hz  Samples
01_6.00__HHZ      2022,122,18:07:40.000000 2022,122,18:22:28.000000  ==  500 444001
01_6.00__HHZ      2022,122,18:23:34.000000 2022,123,00:00:00.000000 66   500 10093001
Total: 1 trace(s) with 2 segment(s)
lecoinal@f-dahu:/bettik/lecoinal/pr-teterousse/nodes_202205_mseed/node_06_6$ mv cat.mseed 453000294.600000600001.1.2022.05.02.18.07.40.000.Z.miniseed
lecoinal@f-dahu:/bettik/lecoinal/pr-teterousse/nodes_202205_mseed/node_06_6$ rm 453000294.600000600002.2.2022.05.02.18.23.34.000.Z.miniseed
lecoinal@f-dahu:/bettik/lecoinal/pr-teterousse/nodes_202205_mseed/node_06_6$ 
```

et aussi celui la : 

```
lecoinal@f-dahu:/bettik/lecoinal/pr-teterousse/nodes_202205_mseed/node_06_4$ msi -tg 453000101.600000400018.18.2022.05.19.00.00.00.000.Z.miniseed
   Source                Start sample             End sample        Gap  Hz  Samples
01_4.00__HHZ      2022,139,00:00:00.000000 2022,139,01:06:28.000000  ==  500 1994001
Total: 1 trace(s) with 1 segment(s)
lecoinal@f-dahu:/bettik/lecoinal/pr-teterousse/nodes_202205_mseed/node_06_4$ msi -tg 453000101.600000400019.19.2022.05.19.01.07.34.000.Z.miniseed
   Source                Start sample             End sample        Gap  Hz  Samples
01_4.00__HHZ      2022,139,01:07:34.000000 2022,140,00:00:00.000000  ==  500 41173001
Total: 1 trace(s) with 1 segment(s)
lecoinal@f-dahu:/bettik/lecoinal/pr-teterousse/nodes_202205_mseed/node_06_4$ cat 453000101.600000400018.18.2022.05.19.00.00.00.000.Z.miniseed 453000101.600000400019.19.2022.05.19.01.07.34.000.Z.miniseed > cat.miniseed
lecoinal@f-dahu:/bettik/lecoinal/pr-teterousse/nodes_202205_mseed/node_06_4$ mv cat.miniseed 453000101.600000400018.18.2022.05.19.00.00.00.000.Z.miniseed
lecoinal@f-dahu:/bettik/lecoinal/pr-teterousse/nodes_202205_mseed/node_06_4$ rm 453000101.600000400019.19.2022.05.19.01.07.34.000.Z.miniseed
lecoinal@f-dahu:/bettik/lecoinal/pr-teterousse/nodes_202205_mseed/node_06_4$ 

```


Python code to convert MSEED into HDF5 : [preprocess_teterousse.py](../RESOLVE_preprocess_src/preprocess_teterousse.py)

and associated submissionn script : [submit_teterousse.sh](../RESOLVE_preprocess_src/submit_teterousse.sh)

cigri campaign id 22193

script jdl pour la grille : 
```
killeen:~/TETEROUSSE$ cat teterousse.jdl 
{
  "name": "teterousse",
  "resources": "/nodes=1/cpu=1/core=1",
  "exec_file": "{HOME}/tmp/resolve/RESOLVE_preprocess_src/submit_teterousse.sh",
  "exec_directory": "{HOME}/tmp/resolve/RESOLVE_preprocess_src/",
  "param_file": "{HOME}/TETEROUSSE/teterousse_params.txt",
  "test_mode": "false",
  "type": "best-effort",
  "clusters": {
    "dahu": {
      "project": "resolve",
      "walltime": "00:30:00"
    }
  }
}

```
et fichier de param associé : 
```
killeen:~/TETEROUSSE$ cat teterousse_params.txt 
2022.05.02
2022.05.03
2022.05.04
2022.05.05
2022.05.06
2022.05.07
2022.05.08
2022.05.09
2022.05.10
2022.05.11
2022.05.12
2022.05.13
2022.05.14
2022.05.15
2022.05.16
2022.05.17
2022.05.18
2022.05.19
2022.05.20
2022.05.21
2022.05.22
2022.05.23
2022.05.24
2022.05.25
2022.05.26
2022.05.27

```

26 jobs de 2022.05.02 a 2022.05.27

jobs monocore dahu de 15min

outputs : 

```
lecoinal@f-dahu:/bettik/lecoinal/pr-teterousse/PREPROCESS$ du -sch *
790M	2022_122.h5
4.7G	2022_123.h5
6.2G	2022_124.h5
6.1G	2022_125.h5
5.9G	2022_126.h5
6.0G	2022_127.h5
5.9G	2022_128.h5
5.9G	2022_129.h5
5.9G	2022_130.h5
6.2G	2022_131.h5
6.2G	2022_132.h5
6.1G	2022_133.h5
6.1G	2022_134.h5
6.1G	2022_135.h5
6.2G	2022_136.h5
6.1G	2022_137.h5
6.2G	2022_138.h5
6.3G	2022_139.h5
6.7G	2022_140.h5
6.4G	2022_141.h5
3.4G	2022_142.h5
234M	2022_143.h5
226M	2022_144.h5
211M	2022_145.h5
225M	2022_146.h5
134M	2022_147.h5
120G	total


$ for f in $(ls *h5) ; do echo -n "$f " ; h5ls $f | wc -l ; done
2022_122.h5 31
2022_123.h5 59
2022_124.h5 59
2022_125.h5 59
2022_126.h5 59
2022_127.h5 59
2022_128.h5 59
2022_129.h5 59
2022_130.h5 59
2022_131.h5 59
2022_132.h5 59
2022_133.h5 59
2022_134.h5 59
2022_135.h5 59
2022_136.h5 59
2022_137.h5 59
2022_138.h5 59
2022_139.h5 59
2022_140.h5 59
2022_141.h5 58
2022_142.h5 59
2022_143.h5 2
2022_144.h5 2
2022_145.h5 2
2022_146.h5 2
2022_147.h5 2

```

jd=141, il manque le node node_06_5.2022.141

ok pb de nommage dans input mseed : z versus Z :

```
mv 453000091.600000500019.19.2022.05.21.00.00.00.000.z.miniseed 453000091.600000500019.19.2022.05.21.00.00.00.000.Z.miniseed
```

donc relance le job pour 2022.05.21, ca corrige :

```
2022_141.h5 59
```

# Preparation des fichiers métriques

We use this coordinate file [name_coord.txt](./name_coord.txt)

```
> awk '{print $2 " " $3}' name_coord.txt > lonlat.txt
```

Use of deg2utm from MatLab to convert lon / lat into x / y coordinates

```
>> load lonlat.txt
>> whos lonlat.txt
  Name         Size            Bytes  Class     Attributes

  lonlat      60x2               960  double              
>> [x,y,utmzone]=deg2utm(lonlat(:,2),lonlat(:,1));
>> sprintf('%15.5f %15.5f \n',[x y]')             

ans =

    '   330613.34547   5080469.51287 
        330610.52849   5080444.74944 
        330607.20513   5080419.82841 
        330603.30074   5080395.63757 
        330605.82790   5080371.16763 
        330667.56098   5080481.86504 
        330664.19920   5080457.75022 
        330661.98915   5080432.72849 
        330660.49343   5080408.28826 
        330658.40083   5080378.53785 
        330655.95396   5080361.67160 
        330701.43366   5080474.51372 
        330697.56441   5080447.28722 
        330695.44067   5080421.52133 
        330690.83995   5080395.76048 
        330691.43714   5080372.27756 
        330692.14969   5080351.28923 
        330688.16968   5080322.28101 
        330735.72873   5080466.04444 
        330732.62017   5080437.73189 
        330732.14139   5080415.79212 
        330730.24067   5080388.33582 
        330727.93120   5080366.45937 
        330725.83666   5080340.64479 
        330725.77740   5080316.36546 
        330768.34542   5080453.83932 
        330766.37673   5080431.95462 
        330763.53668   5080405.24098 
        330763.70515   5080381.65419 
        330762.32606   5080354.72518 
        330762.21961   5080333.58313 
        330800.24362   5080448.88315 
        330799.53081   5080421.16982 
        330799.00099   5080399.99266 
        330795.98676   5080370.61459 
        330796.04740   5080345.36259 
        330794.74402   5080324.67753 
        330829.95850   5080436.73275 
        330832.85996   5080414.58845 
        330830.79764   5080385.50375 
        330829.07299   5080363.46237 
        330827.89719   5080338.09780 
        330826.68552   5080314.15231 
        330870.77730   5080431.50433 
        330869.18078   5080402.72287 
        330866.00099   5080381.10207 
        330863.23186   5080354.40920 
        330860.36735   5080329.46614 
        330857.66805   5080304.26253 
        330899.99000   5080400.55889 
        330894.82470   5080376.90001 
        330892.62435   5080350.07259 
        330891.13289   5080326.73986 
        330929.74525   5080350.73154 
        330939.17664   5080374.39896 
        330945.13298   5080395.00073 
        330592.01838   5080504.13349 
        330528.99473   5080374.87693 
        330946.51312   5080440.65283 
        330990.41606   5080277.54402 
     '


>> mean(x)

ans =

     3.307660541204411e+05

>> mean(y)

ans =

     5.080390622567304e+06

>> x=x-mean(x);
>> y=y-mean(y);
>> sprintf('%15.5f %15.5f \n',[x y]')

ans =

    '     -152.70865        78.89030 
          -155.52563        54.12687 
          -158.84899        29.20585 
          -162.75338         5.01501 
          -160.22622       -19.45494 
           -98.49314        91.24248 
          -101.85492        67.12766 
          -104.06497        42.10592 
          -105.56069        17.66569 
          -107.65329       -12.08471 
          -110.10016       -28.95097 
           -64.62046        83.89116 
           -68.48971        56.66465 
           -70.61345        30.89876 
           -75.21417         5.13792 
           -74.61698       -18.34500 
           -73.90443       -39.33334 
           -77.88444       -68.34156 
           -30.32539        75.42188 
           -33.43395        47.10932 
           -33.91273        25.16956 
           -35.81345        -2.28674 
           -38.12292       -24.16320 
           -40.21746       -49.97778 
           -40.27672       -74.25711 
             2.29130        63.21675 
             0.32261        41.33205 
            -2.51744        14.61841 
            -2.34897        -8.96838 
            -3.72806       -35.89738 
            -3.83451       -57.03944 
            34.18950        58.26058 
            33.47669        30.54725 
            32.94687         9.37009 
            29.93264       -20.00798 
            29.99328       -45.25998 
            28.68990       -65.94503 
            63.90438        46.11018 
            66.80584        23.96588 
            64.74352        -5.11881 
            63.01887       -27.16019 
            61.84307       -52.52477 
            60.63140       -76.47026 
           104.72318        40.88176 
           103.12666        12.10031 
            99.94687        -9.52050 
            97.17774       -36.21337 
            94.31323       -61.15643 
            91.61393       -86.36003 
           133.93588         9.93632 
           128.77058       -13.72256 
           126.57023       -40.54997 
           125.07877       -63.88270 
           163.69113       -39.89103 
           173.12252       -16.22361 
           179.07886         4.37816 
          -174.03574       113.51092 
          -237.05939       -15.74563 
           180.45900        50.03027 
           224.36194      -113.07855 
     '
```

The resulting metric files: `/data/projects/teterousse/lecoinal/METRIC/zreseau_TETR_2022_${jday}.txt`

N.B.: Node D and node 01.2 were inverted, thus we switch to these metric files : `/data/projects/teterousse/lecoinal/METRIC_invert_D_1.2/zreseau_TETR_2022_${jday}.txt`

![teterousse array](./teterousse.png "teterousse array")

---

# MFP computation

To compute the MFP, we use only the 20 days of continuous data with 59 active sensors (not 60) : 2022_123.h5 - 2022_142.h5

See `/data/projects/teterousse/lecoinal/BEAM` and associated README files

When applying z penalty, we define the parametric surface as (we used the coordinates of the 60 sensors [xyz.txt](./xyz.txt) to define this parametric surface) :

```
>> load xyz.txt

>> whos
  Name       Size            Bytes  Class     Attributes

  xyz       60x3              1440  double              

>> f=fit([xyz(:,1),xyz(:,2)],xyz(:,3),'poly22')

     Linear model Poly22:
     f(x,y) = p00 + p10*x + p01*y + p20*x^2 + p11*x*y + p02*y^2
     Coefficients (with 95% confidence bounds):
       p00 =        3226  (3224, 3228)
       p10 =      0.2659  (0.2544, 0.2774)
       p01 =    -0.03745  (-0.06177, -0.01313)
       p20 =   0.0003916  (0.0002947, 0.0004885)
       p11 =    0.001063  (0.0007508, 0.001375)
       p02 =    0.001246  (0.0006824, 0.001809)

>> format long
>> f.p00      
     3.225909553703242e+03
>> f.p10
   0.265918541111595
>> f.p01
  -0.037448818782826
>> f.p20
     3.915892876883469e-04
>> f.p11
   0.001062948775002
>> f.p02
   0.001245640067525

```

![parametric fit function](./fit.png)


![elevation difference between stations and parametric surface](./zbound.png)
