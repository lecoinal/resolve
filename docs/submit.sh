#!/bin/bash

#########################################################

# parameters that would be updated in namelist.skel template

IFILE="ZO_2018_121.h5"
XSTART=20
YSTART=-20
ZSTART=2113.73

#########################################################

set -e

INPUT_DIR="$HOME/tmp/"  # where is ZO_2018_121.h5 input file 
METRIC_DIR="/tmp/resolve/docs/"  # where is zreseau_ZO_2018_121.txt metric file

##########################################################

getParam() {
   # Get a parameter value from namelist
   value=$(grep "^ *$1 *= *" namelist | sed 's/!.*$//' | awk -F= '{print $2}')
   echo $value
}


START=$(date +%s.%N)

echo "$IFILE $XSTART $YSTART $ZSTART"

EXE="beamforming_optim"

sed -e "s/<<<IFILE>>>/$IFILE/" -e "s/<<<XSTART>>>/$XSTART/" -e "s/<<<YSTART>>>/$YSTART/" -e "s/<<<ZSTART>>>/$ZSTART/" \
    namelist_arg_example.skel > namelist

# Get metrics and mask
cat ${METRIC_DIR}/zreseau_${IFILE/\.h5/}.txt > zreseau.txt

# Get inputs
ln -sf ${INPUT_DIR}/${IFILE} .

wc -l zreseau.txt  # nb_sta after trim energy limit

./$EXE

# output compression (not always useful) and rename
#h5repack -i out.h5 -o out_${IFILE/\.h5/}.h5 -f GZIP=6 -v
#rm out.h5

# prevent from crash of $SHARED_SCRATCH_DIR just after job termination (when using shared BeeGFS filesystem)
sync 

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "ElapsedTime: $DIFF sec"
